import {createSelector} from "reselect";
import {ARDM_AUTH_SIGNIN_CHANGE, ARDM_AUTH_SIGNIN_IS_MISTAKE} from "PagesARDM/auth/signIn/action";




let initialState = {
    isMistakeLogin: null,
    email: '',
    password: '',

}

const signIn = (state = initialState, action ) => {
    switch (action.type) {
        case ARDM_AUTH_SIGNIN_IS_MISTAKE : {
            return {...state, isMistakeLogin: action.value}
        }
        case ARDM_AUTH_SIGNIN_CHANGE : {
            return {...state, [action.alias]: action.value}
        }

        default:
            return state;
    }
};
export default signIn


//
const getIsMistakeLogin = (state) => state.ARDM.auth.signIn.isMistakeLogin
export const getIsMistakeSigninReselect = createSelector(getIsMistakeLogin,(isMistakeLogin)=> isMistakeLogin)

const getEmailSignin = (state) => state.ARDM.auth.signIn.email
export const getEmailSigninReselect = createSelector(getEmailSignin,(email)=> email)

const getPasswordSignin = (state) => state.ARDM.auth.signIn.password
export const getPasswordSigninReselect = createSelector(getPasswordSignin,(password)=> password)
