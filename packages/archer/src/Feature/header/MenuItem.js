import React, {useEffect, useState} from "react";
import styled, {keyframes} from "styled-components/macro";
import {CSSTransition} from "react-transition-group";
import ClientSection from "FeatureARDM/header/ClientSection";
import {connect} from "react-redux";
import {toggleIsDrawerHomePage, getIsDrawerHomePageServesSelector} from "PagesARDM/main/reducer/siteMap";
import {LosoS} from "Icons/header";

const MenuItem =(props)=> {
    const { isDrawerHomePageServes, toggleIsDrawerHomePage} = props;
    return (
        <Item
            onClick={()=> toggleIsDrawerHomePage(true)}
            isDrawerHomePageServes={isDrawerHomePageServes}
            {...props}
        >
            <NewIconStyledBlock >
                <Icon/>
            </NewIconStyledBlock>

            <CSSTransition
                unmountOnExit
                in={isDrawerHomePageServes}
                timeout={{ appear:0, enter: 0, exit: 500 }}
                classNames='slideIn'
                appear
            >
                <AnimationTooltip>
                    <ClientSection />
                </AnimationTooltip>
            </CSSTransition>
        </Item>
    );
};

const mapStateToProps = (state) => {
    return {
        isDrawerHomePageServes: getIsDrawerHomePageServesSelector(state)
    }
}

export default connect(mapStateToProps,{
    toggleIsDrawerHomePage
})(MenuItem)


const SectionAbsolute = styled.div`
  padding: 20px;
  position: absolute;
  top: 0;
  z-index: 1000;
  box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.2);
  background-color: #fff;
  color: #000;
  font-size: 14px;

`;
const Icon = styled(LosoS)`
    border-radius: 50%;
    width: 100%;
    height: 100%;
    & > rect {
      transition: ease 0.4s;
    }
`;
const NewIconStyledBlock = styled.div`
  width: 24px;
  height: 24px;
  cursor: pointer;
  &:hover > ${Icon} > rect {
    fill-opacity: 1;
  }
`;

const AnimationTooltip = styled(SectionAbsolute)`
    top: 0;
    height: 100vh;
    display: flex;
    flex-direction: column;
    width: 250px;
    padding: 0;
`;


const Item = styled.div`
  max-width: 200px;
  height: 100%;
  padding: 0 3px 0 20px;
  z-index: ${({isDrawerHomePageServes}) => isDrawerHomePageServes ? 10 : 1};
  position: relative;
  display: flex;
  align-items: center;
  background-color: ${props => props.theme.heavy};
  font-size: 16px;
  font-family: ProximaNova-Semibold, sans-serif;
  color: #ffffff;
  line-height: 1;
  cursor: pointer;
  transition: 0.3s all;
`;
