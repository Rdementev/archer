import React, {useEffect, useState} from 'react'
import {compose} from "redux";
import styled , {withTheme} from 'styled-components/macro'
import {toggleIsShowOverlay} from "PagesARDM/main/reducer/siteMap";
import {connect} from "react-redux";
import {CloseIcon} from 'Icons'
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {getListDocSelector} from "PagesARDM/pagedocs/reducer/pageDocList";
import {withRouter} from "react-router-dom";
import SelectSearch from "ComponentsARJTTD/selectSearch";
import {ardmPagesCreateFormsPagedocsRequest} from "PagesARDM/pagedocs/actions";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";

const ur_face = [
    {
        id: 1,
        name: 'Индивидуальный предприниматель',
    },
    {
        id: 2,
        name: 'Общество с ограниценной ответственностью',
    },
]
const categoryDefault = [
    {
        id: 1,
        name: 'Регистрация',
    },
    {
        id: 2,
        name: 'Внесение изменений',
    },
    {
        id: 3,
        name: 'Ликвидация',
    },
]

const ModalCreateDocument = (props) => {
    const { setShowModal, location, history, toggleIsShowOverlay, ardmPagesCreateFormsPagedocsRequest } = props
    const [urFace, setUrFace] = useState('')
    const [category, setCategory] = useState('')
    const [query, setQueryy] = useState('')

    // юридическое лицо
    const [displayValueUrFace, setDisplayValueUrFace] = useState('Выбрать')
    const [inputValueUrFace, setInputValueUrFace] = useState('Выбрать')
    const handleClickItemUrFace = (item) => {
        setDisplayValueUrFace(<DisplayValue>{item[0].name}</DisplayValue>)
        setInputValueUrFace(item[0].name)
        setUrFace(item[0])
    }
    // категория документа
    const [displayValueCategory, setDisplayValueCategory] = useState('Выбрать')
    const [inputValueCategory, setInputValueCategory] = useState('Выбрать')
    const handleClickItemCategory = (item) => {
        setDisplayValueCategory(<DisplayValue>{item[0].name}</DisplayValue>)
        setInputValueCategory(item[0].name)
        setCategory(item[0])

    }
    useEffect(()=>{
        const b = urFace.name === 'Индивидуальный предприниматель' ? 'ИП' : 'ООО'
        if(category.name && b) setQueryy(category.name.concat(' ', b))
    },[urFace, category])

    const handleClickClose = () => {
        setShowModal(false)
        toggleIsShowOverlay(false)
    }

    const handleClickCreate = () => {
        const data = {
            group_form_id: category.id,
            form_name: query,
        }
        ardmPagesCreateFormsPagedocsRequest(data)
        handleClickClose()
    }


    return (
        <Container>
            <Header>
                <Title>
                    Создание документа
                </Title>

                <BlockIconClose onClick={()=>{handleClickClose()}}>
                    <CircleCloseBtn/>
                </BlockIconClose>
            </Header>
            <Section>
                <SectionBlock>
                    <SectionTitle>
                        Юридическое лицо :
                    </SectionTitle>
                    <StyleSelectSearch>
                        <SelectSearch list={ur_face}
                                      displayValue={displayValueUrFace}
                                      value={inputValueUrFace}
                                      onClick={handleClickItemUrFace}/>
                    </StyleSelectSearch>
                </SectionBlock>
                <SectionBlock>
                    <SectionTitle>
                        Категория :
                    </SectionTitle>
                    <StyleSelectSearch>
                        <SelectSearch list={categoryDefault}
                                      displayValue={displayValueCategory}
                                      value={inputValueCategory}
                                      onClick={handleClickItemCategory}/>
                    </StyleSelectSearch>
                </SectionBlock>
            </Section>
            <Footer>
                <Button style={{marginRight:'10px'}} onClick={()=>{handleClickCreate()}}>Создать</Button>
                <WhiteButton onClick={()=>{handleClickClose()}}>Закрыть</WhiteButton>
            </Footer>

       </Container>
    )
}

const mapStateToProps = (state) => {
    return {

    }
}
export default compose(
    connect(mapStateToProps, {
        toggleIsShowOverlay, ardmPagesCreateFormsPagedocsRequest
    }),
    withRouter,
    withTheme
)(ModalCreateDocument)
//

const Header = styled.div`
    padding: 15px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #E9E9EA;
`;
const Title = styled.h4`
    margin: 0;
`;
const StyleSelectSearch = styled.div`
    width: 300px;
`;
const DisplayValue = styled.div`
    color: #000;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    text-transform: none;
`;
const Section = styled.div`
    padding: 15px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    text-transform: uppercase;
`;
const SectionBlock = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
   &:not(:last-child){
    margin-bottom: 20px;
   }
`;
const SectionTitle = styled.h5`
    margin: 0 20px 0 0;
    font-size: 11px;
    color: #1D2124;
    font-weight: 400;
    white-space: nowrap;
    min-width: 140px;
`;
const Footer = styled.div`
    padding: 15px;
    display: flex;
    justify-content: flex-end;
`;
const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    max-width: 500px;
    width: 100%;
    background: #fff;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 4px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.3)
`;
const BlockIconClose = styled.div`
    width: 14px;
    height: 14px;
`;
