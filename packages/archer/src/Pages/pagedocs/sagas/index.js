import {fork, all} from "redux-saga/effects";
import {getFormsSaga} from "./getForms";
import {createFormSaga} from "./createForm";
import {getFormDataSaga} from "./getFormData";
import {updateFormDataSaga} from "./updateFormData";
import {deleteFormSaga} from "./deleteForm";
import {requestPagedoc} from "./request";
import {buildFormSaga} from "./buildForm";
import {downloadFormSaga} from "./downloadForm";

export function* pageDocsSaga() {
    yield all ([
        fork(getFormsSaga),
        fork(createFormSaga),
        fork(getFormDataSaga),
        fork(updateFormDataSaga),
        fork(requestPagedoc),
        fork(buildFormSaga),
        fork(downloadFormSaga),
        fork(deleteFormSaga),
    ])
}