
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {put} from "redux-saga/effects";
import {ARDM_PAGES_UPDATE_PERSONAL_REQUEST, ardmPagesGetPersonalRequest} from "../actions";

function* prepareRequest ( payload) {
    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetPersonalRequest())
}

function* prepareFailure () {

}

export const updatePersonal = () => {
    return createRequestRest({
        url: '/autodoc/update_individual',
        action: ARDM_PAGES_UPDATE_PERSONAL_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}