import React, {useState} from 'react'
import {connect} from "react-redux";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import styled from "styled-components/macro";
import {NavLink} from "react-router-dom";
import {
    getAgreeingSignupReselect, getDoubleSendSignupReselect,
    getEmailSignupReselect,
    getLoginSignupReselect,
    getPasswordSignupReselect,
    getShowCheckEmailReselect
} from "./reducer";
import {SendEmail} from '../icons'
import {ardmAuthSignUpChangeDoubleSendEmail, ardmAuthSignUpGetSignUp} from "./action";
import {withRouter} from "react-router";
import {compose} from "redux";

const SignUpCheck = (props) => {
    const {login, email, password, agreeing, doubleSend, ardmAuthSignUpGetSignUp} = props

    const handleClick = () => {
        ardmAuthSignUpGetSignUp(login ,email, password, agreeing)
    }

    return (
        <Container>
            <StyledSignUpCheck>
                <Header>
                    <SubTitle>
                        Вам отправлено письмо для активации аккаунта.
                    </SubTitle>
                </Header>
                <Offer>
                    <BlockIcon>
                        <SendEmail/>
                    </BlockIcon>
                    <Title>
                        Проверьте <br/> вашу почту
                    </Title>
                    <OfferText>
                        Не закрывайте данное окно и проверьте Ваш email, чтобы активировать Ваш аккаунт.
                    </OfferText>
                </Offer>
                <Footer>
                    {!doubleSend
                        ? (<BlockButton onClick={(e) => {handleClick()}}>
                        <Button>
                            Повторно отправить письмо активации аккаунта
                        </Button>
                    </BlockButton>)
                        : (
                            <OfferText>
                                Письмо для активации аккаунта отправленно повторно.
                            </OfferText>
                        )
                    }
                </Footer>
            </StyledSignUpCheck>
            <BlockInfo>

            </BlockInfo>
        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        login: getLoginSignupReselect(state),
        email: getEmailSignupReselect(state),
        password: getPasswordSignupReselect(state),
        agreeing: getAgreeingSignupReselect(state),
        doubleSend: getDoubleSendSignupReselect(state)
    }
}

export default compose(
    connect(mapStateToProps, {
        ardmAuthSignUpGetSignUp, ardmAuthSignUpChangeDoubleSendEmail
    }),
    withRouter,
)(SignUpCheck)
//
const BlockInfo = styled.div`
    width: 65%;
    height: 100vh;
    display: flex;
    background: radial-gradient(circle, rgba(81,109,190,1) 0%, rgba(41,68,147,1) 76%);
`;
const StyledSignUpCheck = styled.div`
    display: flex;
    width: 35%;
    padding: 50px;
    background: #fff;
    margin: auto;
    flex-direction: column;
`;

const Container = styled.div`
    position: relative;
    width: 100vw;
    height: 100vh;
    display: flex;
`;
const BlockIcon = styled.div`
    display: flex;
    & > svg {
    margin: auto;
    }
`;
const Header = styled.div`
`;
const Title = styled.h3`
    color: #000;
    letter-spacing: 4px;
    text-transform: uppercase;
    text-align: center;
    font-weight: 400;
    font-size: 20px;
    max-width: 200px;
    margin: auto;
    
`;
const SubTitle = styled.h3`
    color: #000;
    text-align: center;
    font-weight: 400;
`;
const Offer = styled.div`
    height: 100%;
    margin: 30px 0;
`;
const OfferText = styled.div`
    margin: 10px 0 0 0;
    text-align: center;
`;

const BlockButton = styled.div`
    padding: 0 20px;
    margin: auto;
`;
const Footer = styled.div`
    display: flex;
    padding: 20px 0;
`;


