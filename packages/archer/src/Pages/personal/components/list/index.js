import React, {useEffect} from 'react'
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import moment from "moment";
import {
    deletePersonal,
} from "PagesARDM/personal/actions";
import styled from "styled-components/macro";
import Table from "@material-ui/core/Table";
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {changeOrganizationData, setPredstavitelForFirm} from "PagesARDM/organization/actions";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import {WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {ardmFeatureRegOOOSetProxy} from "FeatureARDM/createDocument/registrationOOO/actions";
import {ardmFeatureRegOOOSetDirPersonalId} from "FeatureARDM/createDocument/registrationOOO/actions";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";


const headCells = [
    {id: 1, numeric: 'left', alias: 'surnName',  disablePadding: true, label: 'Фамилия'},
    {id: 2, numeric: 'left', alias: 'name',  disablePadding: false, label: 'Имя'},
    {id: 3, numeric: 'left', alias: 'second_name',  disablePadding: false, label: 'Отчество'},
    {id: 4, numeric: 'center', alias: 'dateOfBirth',  disablePadding: false, label: 'Дата рождения'},
];


function RenderTableHeader(props) {
    const { } = props;
    return (
        <TableHeader >
            <TableCheck >

            </TableCheck>
            <TableHeaderItem>
                {props.headCells.map((headCell) => (
                    <Item
                        key={headCell.id}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                    >
                        {headCell.label}
                    </Item>
                ))}
            </TableHeaderItem>
            <div style={{minWidth:'70px'}}>Действия</div>
        </TableHeader>
    );
}

const ListPersonal = (props) => {
    const {personal, setShow, onClickPers, setShowCreate, setEditPersonalId, setShowUpdate} = props

    const [searchPersonalInput, setSearchPersonalInput] = React.useState("");
    const [searchResultsPersonal, setSearchResults] = React.useState(personal);

    useEffect(() => {
        const results = personal.filter(person =>
            person.last_name.toLowerCase().includes(searchPersonalInput.toLowerCase()) ||
            person.first_name.toLowerCase().includes(searchPersonalInput.toLowerCase())
        );
        setSearchResults(results);
    }, [personal, searchPersonalInput])

    const handleClickEditPersonal = (id) => {
        setEditPersonalId(id)
        setShowUpdate(true)
    }

    //получаем пользователей
    const getPersonal = () => {
        return searchResultsPersonal.map((item, i) => {
            const birthday = moment(item.birthday).format('DD.MM.YYYY')
            if (item.id) {
                return (
                    <Row>
                        <TableCheck >
                            <Checkbox className='table__check table__check_item'
                            />
                        </TableCheck>
                        <RowUser
                            onClick={(e) => handleClickUser( item.id )}
                            tabIndex={-1}
                            key={item.id}
                        >
                            <TableHeaderItem>
                                <Item>{item.last_name}</Item>
                                <Item>{item.first_name} </Item>
                                <Item>{item.second_name}</Item>
                                <Item >{birthday}</Item>
                            </TableHeaderItem>
                        </RowUser>
                        <Item style={{minWidth:'70px', width:'70px', cursor:'pointer', zIndex: '100', marginLeft: 'auto'}}>
                            <BlockIcon onClick={(e) => {handleClickEditPersonal(item.id)}}>
                                <EditOutlinedIcon fontSize={'small'}/>
                            </BlockIcon>
                        </Item>
                    </Row>
                )
            }
        })
    }

    //клик на пользователя в базе
    const handleClickUser = (id) => {
        onClickPers(id)
        setShow(false)
    }

    // клик по кнопке создать
    const handleCreatePersonal = () => {
        setShowCreate(true)
    }

    // поиск по пользователям
    const handleChange = e => {
        setSearchPersonalInput(e.target.value);
    };

    return (
        <>
            <BlockInput>
                <Input onChange={(e)=>{handleChange(e)}} placeholder='Поиск'/>
            </BlockInput>
            <RenderTableHeader rowCount={personal.length}
                               headCells={headCells} //заголвки таблицы
            />
            <TableContainer>
                <Table aria-labelledby="tableTitle"
                       size={'medium'}
                       aria-label="enhanced table">
                    {getPersonal()}
                </Table>
            </TableContainer>
            <Footer>
                <BlockButtonCreate>
                    <WhiteButton onClick={() => {handleCreatePersonal()}}>
                        Создать
                    </WhiteButton>
                </BlockButtonCreate>
                <BlockButton>
                    <Button>Выбрать</Button>
                </BlockButton>
            </Footer>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
};

export default connect(mapStateToProps, {
    setPredstavitelForFirm, changeOrganizationData,
    ardmFeatureRegOOOSetProxy, ardmFeatureRegOOOSetDirPersonalId
})(ListPersonal)

//



const BlockIcon = styled.div`
        margin-left: auto;
`;

const BlockButton = styled.div`
      width: 150px;
      height: 30px;
      display: flex;
      align-items: center;
`;
const BlockButtonCreate = styled(BlockButton)`
     margin-right: 15px;
`;
const Footer = styled.div`
      display: flex;
      justify-content: center;
      margin-top: 20px;
`;

const TableContainer = styled.div`
    overflow-y: auto;
    flex-grow: 1;
`;

const BlockInput = styled.div`
   margin-bottom: 10px;
   height: 32px;
`;

const TableHeader = styled.div`
    display: flex;
    background: #1f367d14;
    padding: 20px;
    height: 50px;
    align-items: center;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
`;
const TableCheck = styled.div`
  max-width: 50px;
  width: 100%;
`;

const TableHeaderItem = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    width: 100%;
    
`;

const Item = styled.div`
    width: 100%;
    text-overflow: ellipsis;
    overflow: hidden;
    padding: 0 10px;
    display: flex;
    align-items: center;
`;
const RowUser = styled.div`
   width: 100%;
   display: flex;
   padding: 10px 0;
   cursor:pointer;
`;
const Row = styled.div`
    display: flex;
    padding:  0 20px;
    align-items: center;
    border: 1px solid #e0e4f1;
    border-top: none;
    height: 50px;
    &:last-child {
        border-bottom-right-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    &:hover{
        background: #e4e9f145;
        transition: ease 0.5s;
    }
`;

