import createRequestRest from "ApiARJTTD/createRequestRest";
import {setUser} from "ReducersARDM/user";
import {put, call} from "redux-saga/effects";
import {isShowPreloader} from "ReducersARDM/requests";
import {push, } from "connected-react-router";
import {ARDM_AUTH_GET_LOGIN} from "../action";
import {updateLocalStorage} from "Core/utils/persist";

function* prepareRequest(payload) {
    // if(payload.email === '1200021rd@gmail.com'){
    //     const user = {is_auth:true, email:'1200021rd@gmail.com'}
    //     yield put(setUser(user))
    //     return
    // } else if(payload.email === 'k2580592@yandex.ru'){
    //     const user = {is_auth:true, email:'k2580592@yandex.ru'}
    //      yield put(setUser(user))
    //     return
    // }
    yield put(isShowPreloader(true))
    return payload;
}

function* prepareSuccess(response, data) {
    yield put(setUser(response.data))
    yield call(updateLocalStorage)
    yield put(isShowPreloader(false))

}
function* prepareFailure(response, data) {
    if(response.status === 405 ){
        yield put(push('/auth/confirm_email'))
    } else if (response.status === 401){

    }
    yield put(isShowPreloader(false))
}

export const getSignIn = () => {
    return createRequestRest({
        url: "/login",
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        action: ARDM_AUTH_GET_LOGIN,
        method: 'get'
    })
}



