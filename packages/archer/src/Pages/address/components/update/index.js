import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import NewModal from "FeatureARDM/modal/newmodal";
import {
    getArdmPagesAddressReselect,
} from "../../reducer/address";
import {ardmPagesUpdateAddressesRequest} from "../../actions";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import {WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {validationInteger} from "UtilsARJTTD/validation";
import Promto from "ComponentsARJTTD/promto";
import RequiredField from "ComponentsARJTTD/requiredField";

const UpdateAddress = (props) => {
    const {address, show, setShow, editAddressId, ardmPagesUpdateAddressesRequest} = props

    const [postal_code, setpostal_code] = useState('')
    const [postal_codeIsValidation, setpostal_codeIsValidation] = useState('')
    const [region_kladr_id, setregion_kladr_id] = useState('')
    const [region_kladr_idIsValidation, setregion_kladr_idIsValidation] = useState('')
    const [tax_office_legal, settax_office_legal] = useState('')
    const [region_type_full, setregion_type_full] = useState('')
    const [region, setregion] = useState('')
    const [area_type_full, setarea_type_full] = useState('')
    const [area, setarea] = useState('')
    const [city_type_full, setcity_type_full] = useState('')
    const [city, setcity] = useState('')
    const [settlement_type_full, setsettlement_type_full] = useState('')
    const [settlement, setsettlement] = useState('')
    const [street_type_full, setstreet_type_full] = useState('')
    const [street, setstreet] = useState('')
    const [house_type_full, sethouse_type_full] = useState('')
    const [house, sethouse] = useState('')
    const [block_type_full, setblock_type_full] = useState('')
    const [block, setblock] = useState('')
    const [flat_type_full, setflat_type_full] = useState('')
    const [flat, setflat] = useState('')

    useEffect(() => {
        const result = address.find(item => item.id === editAddressId)
        if (!result) return
        setpostal_code(result.postal_code)
        setregion_kladr_id(result.region_kladr_id)
        settax_office_legal(result.tax_office_legal)
        setregion_type_full(result.region_type_full)
        setregion(result.region)
        setarea_type_full(result.area_type_full)
        setarea(result.area)
        setcity_type_full(result.city_type_full)
        setcity(result.city)
        setsettlement_type_full(result.settlement_type_full)
        setsettlement(result.settlement)
        setstreet_type_full(result.street_type_full)
        setstreet(result.street)
        sethouse_type_full(result.house_type_full)
        sethouse(result.house)
        setblock_type_full(result.block_type_full)
        setblock(result.block)
        setflat_type_full(result.flat_type_full)
        setflat(result.flat)
    }, [editAddressId, address])

    const handleCloseModalAddress = () => {
        setShow(false)
    }
    const handleClickUpdateAddress = () => {
        const data = {
            id: editAddressId,
            postal_code: postal_code,
            region_kladr_id: region_kladr_id,
            tax_office_legal: tax_office_legal,
            region_type_full: region_type_full,
            region: region,
            area_type_full: area_type_full,
            area: area,
            city_type_full: city_type_full,
            city: city,
            settlement_type_full: settlement_type_full,
            settlement: settlement,
            street_type_full: street_type_full,
            street: street,
            house_type_full: house_type_full,
            house: house,
            block_type_full: block_type_full,
            block: block,
            flat_type_full: flat_type_full,
            flat: flat,
        }
        ardmPagesUpdateAddressesRequest(data)
    }

    return (
        <NewModal showModal={show}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Редактирование адреса</ModalTitle>
                    <BlockIcon onClick={()=>{setShow(false)}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ModalBody>
                    <Grid1>
                        <BlockFieldStyled>
                            <Title>
                                <TitleF>Индекс</TitleF>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={postal_code}
                                       onBlur={()=>{validationInteger(postal_code, setpostal_codeIsValidation, 6 , 6)}}
                                       placeholder={'Введите индекс'}
                                       onChange={(e) => {setpostal_code(e.target.value)}}/>
                            </BlockInput>
                            {postal_codeIsValidation && <Promto text={postal_codeIsValidation}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <Title>
                                <TitleF>Код регион</TitleF>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={region_kladr_id}
                                       onBlur={()=>{validationInteger(region_kladr_id, setregion_kladr_idIsValidation, 2 , 2)}}
                                       placeholder={'Введите код региона'}
                                       onChange={(e) => {setregion_kladr_id(e.target.value)}}/>
                            </BlockInput>
                            {region_kladr_idIsValidation && <Promto text={region_kladr_idIsValidation}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyledIFNS>
                            <TitleField>Ифнс</TitleField>
                            <BlockInput>
                                <Input value={tax_office_legal} onChange={(e) => {settax_office_legal(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyledIFNS>
                    </Grid1>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Район</TitleField>
                            <BlockInput>
                                <Input value={area_type_full}
                                       placeholder={'Введите район'}
                                       onChange={(e) => {setarea_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование района</TitleField>
                            <BlockInput>
                                <Input value={area}
                                       placeholder={'Введите наименование района'}
                                       onChange={(e) => {setarea(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Город</TitleField>
                            <BlockInput>
                                <Input value={city_type_full}
                                       placeholder={'Введите город'}
                                       onChange={(e) => {setcity_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование города</TitleField>
                            <BlockInput>
                                <Input value={city}
                                       placeholder={'Введите наименование города'}
                                       onChange={(e) => {setcity(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Населенный пункт</TitleField>
                            <BlockInput>
                                <Input value={settlement_type_full}
                                       placeholder={'Введите населенный пункт'}
                                       onChange={(e) => {setsettlement_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование населенного пункта</TitleField>
                            <BlockInput>
                                <Input value={settlement}
                                       placeholder={'Введите наименование населенного пункта'}
                                       onChange={(e) => {setsettlement(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>

                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Улица</TitleField>
                            <BlockInput>
                                <Input value={street_type_full}
                                       placeholder={'Введите улицу'}
                                       onChange={(e) => {setstreet_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование улицы</TitleField>
                            <BlockInput>
                                <Input value={street}
                                       placeholder={'Введите наименование улицы'}
                                       onChange={(e) => {setstreet(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid3>
                        <BlockFieldStyled>
                            <TitleField>Дом</TitleField>
                            <BlockInput>
                                <Input value={house_type_full}
                                       placeholder={'Введите дом'}
                                       onChange={(e) => {sethouse_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Номер дома</TitleField>
                            <BlockInput>
                                <Input value={house}
                                       placeholder={'Введите номер дома'}
                                       onChange={(e) => {sethouse(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Корпус</TitleField>
                            <BlockInput>
                                <Input value={block_type_full}
                                       placeholder={'Введите корпус'}
                                       onChange={(e) => {setblock_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Номер корпуса</TitleField>
                            <BlockInput>
                                <Input value={block}
                                       placeholder={'Введите номер корпуса'}
                                       onChange={(e) => {setblock(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid3>

                    <Grid4>
                        <OfficeBlock>
                            <BlockFieldStyled>
                                <TitleField>Офис</TitleField>
                                <BlockInput>
                                    <Input value={flat_type_full}
                                           placeholder={'Введите офис'}
                                           onChange={(e) => {setflat_type_full(e.target.value)}}/>
                                </BlockInput>
                            </BlockFieldStyled>
                            <BlockFieldStyled>
                                <TitleField>Номер офиса</TitleField>
                                <BlockInput>
                                    <Input value={flat}
                                           placeholder={'Введите номер офиса'}
                                           onChange={(e) => {setflat(e.target.value)}}/>
                                </BlockInput>
                            </BlockFieldStyled>
                        </OfficeBlock>
                    </Grid4>
                </ModalBody>
                <Footer>
                    <ButtonBlock>
                        <WhiteButton onClick={() => {
                            handleClickUpdateAddress()
                        }}>Сохранить</WhiteButton>
                    </ButtonBlock>
                </Footer>
            </ContainerModal>
        </NewModal>
    )
}

const mapStateToProps = (state) => {
    return {
        address: getArdmPagesAddressReselect(state),
    }
}

export default connect(mapStateToProps, {
    ardmPagesUpdateAddressesRequest

})(UpdateAddress)
//
const ButtonBlock = styled.div`
    width: 150px;
    height: 30px;
    margin-top: auto;
`;
const Footer = styled.div`
   display: flex; 
   justify-content: center;
`;
const ContainerModal = styled.div`
    padding: 20px;
    grid-template-rows: 50px minmax(300px, 100%) 50px;
    display: grid;
    min-width: 800px;
    max-height: 800px;
    height: 100%;
    overflow: hidden;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
`;

const ModalBody = styled.div`
    flex-grow: 1;
    flex-shrink: 0;
    display: flex;
    flex-direction: column;
`;
const Grid1 = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 2fr;
    margin-bottom: 30px;
`;
const Grid2 = styled.div`
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin-bottom: 30px;
`;
const Grid3 = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    margin-bottom: 30px;
`;
const Grid4 = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
`;
const ModalOffer = styled.div`
    
`;

const OfficeBlock = styled.div`
   display: flex;
`;

const ModalHeader = styled.div`
   display: flex;
   height: 50px;
   justify-content: space-between;
`;
const ModalTitle = styled.h4`
  
`;

const TitleField = styled.div`
    font-size: 12px;
    font-weight: 500;
    line-height: 10px;
    margin-bottom: 5px;
`;
const TitleF = styled(TitleField)`
    font-size: 12px;
    font-weight: 500;
    line-height: 10px;
    margin: 0;
`;
const Title = styled.div`
    display: flex;
    margin-bottom: 5px;
`;
const BlockInput = styled.div`
    height: 32px;
    margin: 0 0 3px 0;
`;
const BlockFieldStyled = styled.div`
    position: relative;
    padding: 0 5px;
    width: 100%;
    max-height: 48px;
`;
const BlockFieldStyledIFNS = styled(BlockFieldStyled)`
    padding: 10px 5px;
    max-width: 100px;
    margin-left: auto;
`;
const BlockIcon = styled.div`
    cursor: pointer;
    width: 14px;
    height: 14px;
`;
