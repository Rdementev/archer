function fabrika(light, lightMedium, medium, semiHeavy, heavy, root, color ){
    return {
        root: root,
        light: light,
        lightMedium: lightMedium,
        medium: medium,
        semiHeavy: semiHeavy,
        heavy:heavy,
        color: color,

        input:{
            height: '35px',
            borderColor: heavy,
            padding: '10px 20px 10px 10px'
        },
        btn: {

        },


    }
}

export const defaultTheme = {
    1: fabrika('#EBEFF2', '#D0D5D9', '#F2AE72', '#D9653B', '#73061A', '#fff', '#000'),
    2: fabrika('#C2BFD9', '#8F92BF', '#253259', '#F2AE72', '#D9896C', '#fff', '#000'),
    3: fabrika('#F2E2DC', '#F2C1AE', '#96D9D9', '#D98982', '#A64949', '#fff', '#000'),
    4: fabrika('#F2F2F2', '#BFAEA8', '#8C5454', '#BF323442', '#BE0B0C0D', '#fff', '#000'),
    5: fabrika('#FBFBFD', '#E5E5E5', '#4F69B2', '#284493', '#1C3375', '#fff', '#000'),
    6: fabrika('#F5E9DF', '#A7D0D9', '#8C030E', '#F2C49B', '#D9946C', '#fff', '#000'),


}
