import React from "react";
import styled from 'styled-components/macro'

const RequiredField = () => {
    return <Style>*</Style>
}
export default RequiredField

const Style = styled.span`
    color: red;
    font-size: 15px;
    margin-left: 3px;
    display: flex;
    align-items: center;
    height: 10px;
`;
