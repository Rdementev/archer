import React, {Suspense, useEffect, useState} from 'react'
import {connect, useDispatch} from "react-redux";
import Preloader from "ComponentsARJTTD/preloader";
import {Route, Switch, Redirect} from "react-router-dom";
import styled from "styled-components/macro";
import SignIn from "./signIn";
import SignUp from "./signUp";
import ResetPassword from "./resetPassword";
import ResetPasswordCheck from "./resetPassword/resetPasswordCheck";
import ConfirmEmail from "./confirmEmail";
import {compose} from "redux";
import {withReducer} from "Core/HOC/withReducer";
import createAuthReducer from "PagesARDM/auth/reducer";
import SignUpCheck from "./signUp/signUpCheck";
import SetPassword from "./setPassword";
import {clearReducer} from "Core/utils/settingsState";


const Auth = (props) => {
    useEffect(()=>{
        return function () {
            clearReducer(['auth'])
        }
    },[])
    return (
            <AppStyle>
                <Suspense fallback={<Preloader/>}>
                    <Switch>
                        <Route path={'/auth/confirm_email'} render={() => <ConfirmEmail  />}/>
                        <Route path={'/auth/set_password'} render={() => <SetPassword  />}/>

                        <Route exact path={'/auth/signin'} render={() => <SignIn  />}/>
                        <Route exact path="/auth/signup" component={SignUp} />
                        <Route exact path="/auth/signup_check" component={SignUpCheck} />
                        <Route exact path="/auth/reset_password" component={ResetPassword} />
                        <Route exact path="/auth/reset_password_check" component={ResetPasswordCheck} />
                        <Redirect to={'/auth/signin'}/>
                    </Switch>
                </Suspense>
            </AppStyle>
        )

}
const mapStateToProps = (state) => {
    return {

    }
}

export default compose(
    connect(mapStateToProps, {

    }),
    withReducer({'auth': createAuthReducer})
)(Auth)
//
const AppStyle = styled.div`
   font-family: "Open sans", sans-serif;
`;
