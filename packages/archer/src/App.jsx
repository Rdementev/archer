import React, {Component, Suspense, useEffect, useState} from 'react';
import './App.css';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router'
import {CSSTransition} from "react-transition-group";
import {connect} from "react-redux";
import {toggleIsDrawerHomePage, getIsShowOverlaySelector} from "PagesARDM/main/reducer/siteMap";
import PrivateRoute from "RoutesARDM/PrivateRoute";
import {ConnectedRouter} from 'connected-react-router'
import {withTheme} from "HOCARJTTD/withTheme";
import {getTempoMessagesReselect} from "ReducerARJTTD/flashMessages";
import {deleteFlashMessage} from "ReducerARJTTD/flashMessages";
import Preloader from "ComponentsARJTTD/preloader";
import styled, {ThemeProvider} from 'styled-components/macro'
import {compose} from "redux";

const Serves = React.lazy(() => import('TemplatesARDM'));
const ARCSModule = React.lazy(()=> import ('TemplatesARDM/arcs'))
const Auth = React.lazy(()=> import ('PagesARDM/auth'))

const App = (props) => {
    const {history, theme} = props
    const { toggleIsDrawerHomePage, isShowOverlay} = props

    return (
        // <BrowserRouter>
        <ConnectedRouter history={history}>
            <ThemeProvider theme={theme}>
            <AppStyle>
                <Suspense fallback={<Preloader/>}>
                    <Switch>
                        <PrivateRoute path="/auth" component={Auth} type="login"/>
                        <PrivateRoute path={'/serves'} type="client" component={Serves}/>
                        <Route path={'/'} component={ARCSModule}/>
                    </Switch>
                </Suspense>
                <CSSTransition
                    unmountOnExit
                    in={isShowOverlay}
                    timeout={{appear: 0, enter: 0, exit: 500}}
                    classNames='overlay__home_page'
                    appear
                >
                    <Overlay onClick={() => {toggleIsDrawerHomePage(false)}}/>
                </CSSTransition>
            </AppStyle>
            </ThemeProvider>
        </ConnectedRouter>
        // </BrowserRouter>
    );

}


const mapStateToProps = (state) => {
    return {
        isShowOverlay: getIsShowOverlaySelector(state),
        tempoMessage: getTempoMessagesReselect(state),
    }
}


export default compose(
    withTheme,
    connect(mapStateToProps, {
        toggleIsDrawerHomePage, deleteFlashMessage
    })
)(App)
//


const AppStyle = styled.div`
      height: 100vh;
    overflow-y: hidden;
    .overlay__home_page-appear{
    opacity: 0;
    left: 0;
    }
    .overlay__home_page-enter{
      opacity: 0;
      left: 0;
    }
    
    .overlay__home_page-enter-done{
      opacity: 1;
      transition: opacity 500ms, left 510ms ;
      z-index: 9;
    }
    
    .overlay__home_page-exit{
    z-index: 9;
      opacity: 1;
    }
    
    .overlay__home_page-exit-active{
      opacity: 0;
      transition: opacity 500ms;
    }
    
    
    .tempo_message-appear{
        opacity: 0;
        bottom: 0;
    }
    .tempo_message-enter{
      opacity: 0;
      bottom: 0;
    }
    
    .tempo_message-enter-done{
      opacity: 1;
      bottom: 20px;
      transition: ease opacity 0.5s, bottom 1s ;
    }
    
    .tempo_message-exit{
      bottom: 20px;
      opacity: 1;
      
    }
    
    .tempo_message-exit-active{
      opacity: 0;
      bottom: 0;
      transition: ease opacity 1s, bottom 0.5s;
    }
`;
const Overlay = styled.div`
    top: 0;
    left:0;
    height: 100vh;
    width: 100vw;
    background-color: #0000004d;
    position: absolute;
`;
