import createRequestRest from "ApiARJTTD/createRequestRest";
import {put} from "redux-saga/effects";
import {setUser} from "ReducersARDM/user";

function* prepareFailure(response, pyload) {
    yield put(setUser({}))
}

export const isLoggedIn = () => {
    return createRequestRest({
        url: "/is_logged_in",
        prepareFailure,
        action: 'ARDM_AUTH_IS_LOGGED_IN',
        method: 'get'
    })
}



