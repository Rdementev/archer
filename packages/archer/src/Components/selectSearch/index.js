import React, {useEffect, useState} from 'react'
import styled, {withTheme} from 'styled-components/macro';
import {SelectSearch} from "@rdementev/lib";
import {compose} from "redux";

const SelectSearchContainer = (props) => {
    const { theme, style,  } = props
    const StyledSelectSearch = {
        blockInput: {

        },

        buttonSelect: {
            height: '100%',
            background: '#fff',
            color: 'rgb(0,0,0)',
            display: 'flex',
            padding: '10px 30px 10px 10px',
            borderColor: '#d9d9d9',
            textTransform: 'none',
            transition: 'ease 0.3s',
            '&:hover > div > svg > path' : {
                fill: '#000'
            },
            '&:hover ' : {
                borderColor: theme.semiHeavy,
            }
        },
        buttonSpan: {
            width: '100%',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
        },
        styledContainer: {
            height: '32px',
        },
        blockIcon: {
            position: 'absolute',
            top: 0,
            right: 0,
            '& > svg > path' : {
                fill: 'rgba(0,0,0,0.7)'
            },

        },
        blockIconInput: {
            borderColor: 'transparent',
        },
        blockIconInputClose: {},
        suggestion: {
            padding: '5px',
            backgroundColor: '#FFF',
            zIndex: '2',
        },
        group: {

        },
        itemGroup: {
            borderBottom: '1px solid #e8eaf3',
            marginBottom: '3px',
            padding: '4px 10px',
        },
        itemName: {
            overflow: 'hidden',
            textOverflow: 'eclipse'
        },
        itemBlock: {
            borderRadius: '5px',
            '&:hover': {
                background: 'rgba(41, 68, 147, 0.05)'
            }
        },
        input: {
            borderRadius: '4px',
            background: '#fff',
            borderColor: '#e0e4f1',
            paddingRight: '30px',
            whiteSpace: 'nowrap',
            textOverflow: 'eclipse',
            height: '32px',
            '&:hover ' : {
                borderColor: theme.semiHeavy,
            },
            '&:focus ' : {
                borderColor: theme.semiHeavy,
            },
            '&:active ' : {
                borderColor: theme.semiHeavy,
            }

        },


    }

    const getStyle = () => {
        if(style) {
            Object.keys(StyledSelectSearch).map(item => {
                Object.keys(style).map(elem => {
                    if(item === elem){
                        StyledSelectSearch[item] = {...StyledSelectSearch[item], ...style[elem] }
                    }
                })
            })
        }
        return StyledSelectSearch
    }
     return (
         <Container >
            <SelectSearch placeholder={'Выбрать'}
                          other={null}
                          multi={false}
                          onClickClear={false}
                          search={false}
                          styled={getStyle()}
                          fill={'#fff'}
                          {...props}/>
        </Container>)


}
export default compose(withTheme)(SelectSearchContainer)
//
const Container = styled.div`
  height: 100%;
`;
