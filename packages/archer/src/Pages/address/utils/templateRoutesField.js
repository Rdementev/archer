const Index = 'Индекс';
const RegionId = 'Код региона';
const RaionType = "Район";
const Raion = 'Наименование района';
const CityType  = 'Город';
const City  = 'Наименование города';
const SettlementType =   'Населенный пункт';
const Settlement =   'Наименовани населенного пункта';
const StreetType = 'Улица';
const Street = 'Наименование улицы';
const HouseType =   'Дом';
const House =  'Номер дома';
const BlockType = 'Корпус';
const Block = 'Номер корпуса';
const FlatType = 'Офис';
const Flat = 'Номер офиса';
const Region = 'Регион';
const RegionType = 'Наименование региона'



export const templateRoute = {
    postal_code: Index,
    region_kladr_id:RegionId,
    region_type_full:Region,
    region: RegionType,
    area_type_full:RaionType,
    area : Raion,
    city_type_full:CityType,
    city: City,
    settlement_type_full: SettlementType,
    settlement: Settlement,
    street_type_full: StreetType,
    street: Street,
    house_type_full: HouseType,
    house: House,
    block_type_full: BlockType,
    block: Block,
    flat_type_full: FlatType,
    flat: Flat,

};


