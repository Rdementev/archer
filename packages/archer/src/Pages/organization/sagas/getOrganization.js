import createRequestRest from 'ApiARJTTD/createRequestRest'
import {put} from "redux-saga/effects";
import {ARDM_PAGES_GET_ORGANIZATION_REQUEST, ardmPagesGetOrganizationSuccess} from "../actions";

function* prepareSuccess (response, payload) {
  yield put(ardmPagesGetOrganizationSuccess(response.data))
}

function* prepareFailure () {

}

export const getOrganization = () => {
    return createRequestRest({
        url: '/autodoc/get_organizations',
        action: ARDM_PAGES_GET_ORGANIZATION_REQUEST,
        prepareSuccess,
        prepareFailure,
        mathod:'get'
    })
}