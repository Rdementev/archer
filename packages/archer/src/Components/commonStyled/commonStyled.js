import styled from "styled-components";
import Input from "ComponentsARJTTD/input/inputComponent";

export const ItemField = styled.div`
    padding: 11px;
    min-width: 150px;
`

export const ItemValue = styled.div`
    cursor:pointer;
    padding: 10px;
    line-height: 1.15;
    border: 1px solid transparent;
`
export const StyledInput = styled(Input)`
    width: 200px;
`
export const StyledGruop = styled.div`
   align-items:center;
`

export const DocumentCategoryTitle = styled.h4`
    padding: 0 0 5px 0;
    border-bottom: 1px solid #e0e4f1;
`;


export const BlockItemCategory = styled.div`
    margin-top: 10px;
    position:relative;
`;

export const StyledTypeSection = styled.div`
     padding: 20px 40px 20px 20px;
     display: flex;
     align-items: center;
     justify-content: space-between;
`;



