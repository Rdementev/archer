import createRequestRest from "ApiARJTTD/createRequestRest";
import {setUser} from "ReducersARDM/user";
import {put} from "redux-saga/effects";
import {push} from "connected-react-router";
import {ARDM_AUTH_GET_LOGOUT} from "../action";
import {clearLocalStorage, clearReducer} from "Core/utils/settingsState";

function* prepareRequest() {
    yield put(setUser({}))
    clearLocalStorage()
}

function* prepareSuccess(response, data) {
    yield put(push('/auth/signin'))
    clearLocalStorage()
    clearReducer()

}


export const getLogout = () => {
    return createRequestRest({
        url: "/logout",
        prepareRequest,
        prepareSuccess,
        action: ARDM_AUTH_GET_LOGOUT,
        method: 'get',
    })
}



