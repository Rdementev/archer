import React from 'react'
import {getSignIn} from '../signIn/sagas'
import {getLogout} from '../signIn/sagas/logOut'
import {getSignUp} from '../signUp/sagas'
import {resetPassword} from '../resetPassword/sagas'
import {setPassword} from '../setPassword/sagas'
import {getConfirmEmail} from '../confirmEmail/sagas'
import {isLoggedIn} from './isLoggedIn'
import {all, fork} from "redux-saga/effects";


export default function* () {
    yield all([
        fork(getSignIn),
        fork(getLogout),
        fork(isLoggedIn),
        fork(getConfirmEmail),
        fork(resetPassword),
        fork(setPassword),
        fork(getSignUp)
    ])
}