import React, {useEffect, useState} from 'react'
import {compose} from "redux";
import BreadcrumbContainer from "FeatureARDM/breadcrumb";
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {connect} from "react-redux";
import styled from "styled-components/macro";
import StepHeader from "./components/stepHeader";
import {
    getARDMFeatureRegistrationOOOTypeAddressIdReselect,
    getARDMFeatureRegistrationOOOTypeCreateDateReselect,
    getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect,
    getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect,
    getARDMFeatureRegistrationOOOTypeDirPositionReselect,
    getARDMFeatureRegistrationOOOTypeDirTimeReselect,
    getARDMFeatureRegistrationOOOTypeIndividualsReselect,
    getARDMFeatureRegistrationOOOTypeNameEnglishReselect,
    getARDMFeatureRegistrationOOOTypeNameEngReselect,
    getARDMFeatureRegistrationOOOTypeNameRussianReselect,
    getARDMFeatureRegistrationOOOTypeOrganizationsReselect,
    getARDMFeatureRegistrationOOOTypePeaceFormIdReselect,
    getARDMFeatureRegistrationOOOTypePredsidatelReselect,
    getARDMFeatureRegistrationOOOTypeProxyReselect,
    getARDMFeatureRegistrationOOOTypeRateNalReselect,
    getARDMFeatureRegistrationOOOTypeRegOrganReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect,
    getARDMFeatureRegistrationOOOTypeResponseReselect,
    getARDMFeatureRegistrationOOOTypeSecretarReselect, getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect,
    getARDMFeatureRegistrationOOOTypeSendReselect,
    getARDMFeatureRegistrationOOOTypeSizecapitalReselect,
    getARDMFeatureRegistrationOOOTypeSysNalReselect,
    getARDMFeatureRegOOOActiveStepIdReselect,
    getARDMFeatureRegOOOValidationCommonReselect,
    getARDMFeatureRegOOOValidationFDAReselect,
    getARDMFeatureRegOOOValidationUNOReselect,
} from "./reducer";
import CommonInfoRegOOO from "./components/common";
import Founders from "./components/founders_dir_address";
import UstavNalog from "./components/ustav_nalog";
import {ardmFeatureRegOOOChangeActiveStepId} from "./actions";
import {ardmPagesGetFormDataRequest} from "PagesARDM/pagedocs/actions";
import {getARDMFeatureRegistrationOOOTypeNameRusReselect} from "./reducer";
import Preloader from "ComponentsARJTTD/preloader";
import {
    ardmFeatureRegOOOChangeCompleted,
    ardmFeatureRegOOOChangeValidationCommon
} from "./actions";
import {validationInteger, validationRus} from "UtilsARJTTD/validation";
import {checkCommonRegistrationOOO, checkFDARegistrationOOO} from "./utils";
import {
    ardmFeatureRegOOOChangeValidationFDA,
    ardmFeatureRegOOOChangeValidationUNO
} from "Feature/createDocument/registrationOOO/actions";
import {checkUNORegistrationOOO} from "Feature/createDocument/registrationOOO/utils";

const whiteButtonStyled =  {
    width: '150px',
}
const buttonStyled =  {
    width: '150px',
    marginLeft: '10px'
}
const buttonStyledSave =  {
    width: '200px',
    marginLeft: '10px',
    fontSize: '12px',
    whiteSpace: 'no wrap',
}

const RegistrationOOO = (props) => {
    const { activeStepId, ardmFeatureRegOOOChangeActiveStepId,
        ardmFeatureRegOOOChangeCompleted,
        type_proxy_id,
        type_reg,
        type_send,
        type_create_date,
        type_response,
        type_company_name_rus,
        type_size_capital,
        type_company_name_russian,

        type_individuals,
        type_organizations,
        type_peace_form,
        type_dir_personal_id,
        type_dir_position,
        type_dir_time,
        type_address_id,

        type_sys_nal,
        type_secretar,
        type_predsidatel,
        type_rate_nal,
        type_reg_organ,
        type_selected_okved,

        ardmFeatureRegOOOChangeValidationCommon,
        ardmFeatureRegOOOChangeValidationFDA,
        ardmFeatureRegOOOChangeValidationUNO,

        common,
        fda,
        uno,
        ardmPagesGetFormDataRequest, location, } = props
    const [isFinish, setIsFinish] = useState(false)
    const [lastLink, setLastLink] = useState('')
    const elem = document.querySelector(".wrapperContentResult");
    const clientHeight = elem && elem.clientHeight;
    const url = location.pathname.split('/')
    const count = url.length
    const id = url[count - 1]



    // ===============================================================
    //  уставной капитал
    const [type_size_capitalIsValidation, setType_size_capitalIsValidation] = useState(false)
    const [size_capitalIsValidation, setSize_capitalIsValidation] = useState(false)
    const [size_capitalIsTouch, setSize_capitalIsTouch] = useState(false)
    useEffect(() => {
        validationInteger(type_size_capital, setType_size_capitalIsValidation)
        if(+type_size_capital < 10000){
            return setSize_capitalIsValidation(false)
        }
        setSize_capitalIsValidation(true)
    },[type_size_capital])
    // ===============================================================

    // ===============================================================
    //  сокращенное наименование на русском
    const [type_company_name_rusIsValidation, setType_company_name_rusIsValidation] = useState(false)
    const [type_company_name_rusIsTouch, setType_company_name_rusIsTouch] = useState(false)

    useEffect(() => {
        validationRus(type_company_name_rus, setType_company_name_rusIsValidation)
    },[type_company_name_rus])
    // ===============================================================

    // ===============================================================
    //  полное наименование на русском
    const [type_company_name_russianIsValidation, setType_company_name_russianIsValidation] = useState(false)
    const [type_company_name_russianIsTouch, setType_company_name_russianIsTouch] = useState(false)

    useEffect(() => {
        validationRus(type_company_name_russian, setType_company_name_russianIsValidation)
    },[type_company_name_russian])
    // ===============================================================




    useEffect(() => {
        const validationCommon = checkCommonRegistrationOOO(
            type_proxy_id,
            type_reg,
            type_send,
            type_size_capital,
            type_create_date,
            type_response,
            type_company_name_rus,
            type_company_name_russian,
            type_size_capitalIsValidation,
            size_capitalIsValidation,
            type_company_name_rusIsValidation,
            type_company_name_russianIsValidation,
        )
        ardmFeatureRegOOOChangeValidationCommon(validationCommon)
    }, [
        type_proxy_id,
        type_reg,
        type_send,
        type_size_capital,
        type_create_date,
        type_response,
        type_company_name_rus,
        type_company_name_russian,
        type_size_capitalIsValidation,
        size_capitalIsValidation,
        type_company_name_rusIsValidation,
        type_company_name_russianIsValidation,

    ])



    const [initialInd, setInitialInd] = useState(0)
    const [initialOrg, setInitialOrg] = useState(0)
    const [isEqually, setIsEqually] = useState(true)

    useEffect(()=>{
        const validationFDA =  checkFDARegistrationOOO(type_reg, type_size_capital, type_send, type_proxy_id, type_peace_form, type_response,
            type_individuals, type_organizations, type_dir_personal_id, type_dir_position,
            type_dir_time, type_address_id, isEqually)
        ardmFeatureRegOOOChangeValidationFDA(validationFDA)
    },[
        type_individuals,
        type_organizations,
        type_reg,
        type_send,
        type_response,
        type_proxy_id,
        type_peace_form,
        type_dir_personal_id,
        type_dir_position,
        type_dir_time,
        type_address_id,
        type_size_capital,
        isEqually
    ])

    useEffect(()=>{
        const total = (+initialInd + +initialOrg) === +type_size_capital
        setIsEqually(total)
    },[initialInd, initialOrg, ])

    useEffect(()=>{
        let initialInd = 0
        if(type_individuals && type_individuals.length > 0){
            return type_individuals.reduce(function (accumulator, currentValue) {
                return setInitialInd(accumulator + currentValue.peace_sum)
            }, initialInd);
        }
        return setInitialInd(0)
    },[type_size_capital, type_individuals])

    useEffect(()=>{
        let initialOrg = 0
        if( type_organizations && type_organizations.length > 0){
            return type_organizations.reduce(function (accumulator, currentValue) {
                setInitialOrg(accumulator + currentValue.peace_sum)
            }, initialOrg);
        }
        return setInitialOrg(0)
    },[type_size_capital, type_organizations])




    useEffect(() => {
        const validationUNO =  checkUNORegistrationOOO(
            type_sys_nal,
            type_rate_nal,
            type_selected_okved,
            type_secretar,
            type_predsidatel,
            type_reg_organ,
            type_reg,
            type_individuals,
            type_organizations
        )
        ardmFeatureRegOOOChangeValidationUNO(validationUNO)
    },[type_sys_nal,
        type_rate_nal,
        type_selected_okved,
        type_secretar,
        type_predsidatel,
        type_reg_organ,
        type_reg,
        type_individuals,
        type_organizations])





    useEffect(()=>{
        if(common && fda && uno) {
            ardmFeatureRegOOOChangeCompleted(true)
          return setIsFinish(true)
        }
        ardmFeatureRegOOOChangeCompleted(false)
        setIsFinish(false)
    },[common, fda, uno,])

    useEffect(()=>{
        getLastLink(type_company_name_rus)
    },[type_company_name_rus, location])

    const handleClick = (value) => {
        if(activeStepId === 1 && value === 'prev') return
        if(activeStepId === 3 && value === 'next') return
        const id = value === 'next' ? activeStepId + 1 : activeStepId - 1
        ardmFeatureRegOOOChangeActiveStepId(id)
    }
    const handleClickReady = () => {

    }

    const getLastLink = (type_company_name_rus) => {
        if(type_company_name_rus) return setLastLink(`OOO «‎${type_company_name_rus}»‎`)
        setLastLink(`OOO «...»‎`)
    }

    const breadcrumb = [
        {
            title: 'сервис',
            link: '/serves',
        },
        {
            title: 'регистрационные документы',
            link: '/serves/document',
        },
        {
            title: `${lastLink}`,
            link: '',
        },
    ]

    useEffect(()=>{
        ardmPagesGetFormDataRequest({id:id})
    },[id])

    const getContent = (activeStepId) => {
        switch (activeStepId) {
            case 1 : {
                return <CommonInfoRegOOO  type_size_capitalIsValidation={type_size_capitalIsValidation}
                                          size_capitalIsValidation={size_capitalIsValidation}
                                          size_capitalIsTouch={size_capitalIsTouch}
                                          setSize_capitalIsTouch={setSize_capitalIsTouch}
                                          type_company_name_rusIsValidation={type_company_name_rusIsValidation}
                                          type_company_name_rusIsTouch={type_company_name_rusIsTouch}
                                          setType_company_name_rusIsTouch={setType_company_name_rusIsTouch}
                                          type_company_name_russianIsValidation={type_company_name_russianIsValidation}
                                          type_company_name_russianIsTouch={type_company_name_russianIsTouch}
                                          setType_company_name_russianIsTouch={setType_company_name_russianIsTouch}/>
            }
            case 2 : {
                return <Founders isEqually={isEqually}/>
            }
            case 3 : {
                return <UstavNalog/>
            }
        }

    }

    return (
        <>
        <BreadcrumbContainer list={breadcrumb}/>
        <StepHeader/>
        <PageWrapper>
            <Preloader/>
            <ContentWrapper className="wrapperContentResult" height={clientHeight}>
                <ContentData>
                    {getContent(activeStepId)}
                </ContentData>
                <BlockButton>
                    <WhiteButton styled={whiteButtonStyled} onClick={() => {handleClick('prev')}}>Назад</WhiteButton>
                    {activeStepId === 3
                        ? <Button disabled={!isFinish} styled={buttonStyledSave} onClick={() => {handleClickReady()}}>Подготовить документы</Button>
                        : <Button styled={buttonStyled} onClick={() => {handleClick('next')}}>Далее</Button>
                    }
                </BlockButton>
            </ContentWrapper>
        </PageWrapper>
    </>
    )
}

const mapStateToProps = (state) => {
    return {
        activeStepId: getARDMFeatureRegOOOActiveStepIdReselect(state),
        type_company_name_rus: getARDMFeatureRegistrationOOOTypeNameRusReselect(state),

        common: getARDMFeatureRegOOOValidationCommonReselect(state),
        fda: getARDMFeatureRegOOOValidationFDAReselect(state),
        uno: getARDMFeatureRegOOOValidationUNOReselect(state),


        type_proxy_id: getARDMFeatureRegistrationOOOTypeProxyReselect(state),
        type_reg: getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_send: getARDMFeatureRegistrationOOOTypeSendReselect(state),
        type_size_capital: getARDMFeatureRegistrationOOOTypeSizecapitalReselect(state),
        type_create_date: getARDMFeatureRegistrationOOOTypeCreateDateReselect(state),
        type_response: getARDMFeatureRegistrationOOOTypeResponseReselect(state),
        type_company_name_russian: getARDMFeatureRegistrationOOOTypeNameRussianReselect(state),
        type_company_name_eng: getARDMFeatureRegistrationOOOTypeNameEngReselect(state),
        type_company_name_english: getARDMFeatureRegistrationOOOTypeNameEnglishReselect(state),



        type_individuals: getARDMFeatureRegistrationOOOTypeIndividualsReselect(state),
        type_organizations: getARDMFeatureRegistrationOOOTypeOrganizationsReselect(state),
        type_dir_is_dir_address: getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect(state),
        type_peace_form:  getARDMFeatureRegistrationOOOTypePeaceFormIdReselect(state),
        type_dir_personal_id:  getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect(state),
        type_dir_position:  getARDMFeatureRegistrationOOOTypeDirPositionReselect(state),
        type_dir_time:  getARDMFeatureRegistrationOOOTypeDirTimeReselect(state),
        type_address_id:  getARDMFeatureRegistrationOOOTypeAddressIdReselect(state),


        type_sys_nal: getARDMFeatureRegistrationOOOTypeSysNalReselect(state),
        type_secretar: getARDMFeatureRegistrationOOOTypeSecretarReselect(state),
        type_predsidatel: getARDMFeatureRegistrationOOOTypePredsidatelReselect(state),
        type_rate_nal: getARDMFeatureRegistrationOOOTypeRateNalReselect(state),
        type_reg_organ: getARDMFeatureRegistrationOOOTypeRegOrganReselect(state),
        type_selected_okved: getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect(state),
    }
}

export default compose(
    connect(mapStateToProps,{
         ardmFeatureRegOOOChangeActiveStepId, ardmPagesGetFormDataRequest, ardmFeatureRegOOOChangeCompleted,
        ardmFeatureRegOOOChangeValidationCommon, ardmFeatureRegOOOChangeValidationFDA, ardmFeatureRegOOOChangeValidationUNO
    }),
)(RegistrationOOO)
//
const PageWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  overflow: auto;
  position: relative;
`;
const ContentWrapper = styled.div`
    background-color: ${({transparent}) =>
    transparent ? transparent : "#f5f8fa"};
    padding: 10px 20px;
    justify-content: space-between;
    display: flex;
    flex-direction: column;
    width: 100%;
    min-height: 500px;
    height: 100%;
`;
const ContentData = styled.div`
    overflow-y: auto;
    overflow-x: hidden;
     height: 100%;
`;
const BlockButton = styled.div`
    margin-top: 10px;
    display: flex;
`;
