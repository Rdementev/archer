
import {ARDM_PAGES_GET_ORGANIZATION_SUCCESS} from "../actions";




let initialState = {
    organization: [],
};

const getUpdateOrganizationDataAfterEdit = (organization, organizationDefault, action) => {
    return organization.map(item => {
        if(item.id === action.id){
            let updateOrganization = {
                id: action.id,
                addressId: item.addressId,
                description: item.description,
            }
            Object.keys(item).map(key => {
                organizationDefault.map(elem => {
                    if(key === elem.alias){
                        if(elem.sexType){
                            if(elem.value === true){
                                updateOrganization = {...updateOrganization, [key]: elem.sexType}
                            }return
                        } updateOrganization = {...updateOrganization, [key]: elem.value}
                    }
                })
            })
            return updateOrganization
        }
        return {...item}
    })
}


const organization = (state = initialState, action) => {
    switch (action.type) {
        case ARDM_PAGES_GET_ORGANIZATION_SUCCESS : {
            return {...state, organization: action.payload}
        }
    }
    return state
};


export default organization;
