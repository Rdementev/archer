import createRequestRest from "ApiARJTTD/createRequestRest";
import {GET_ARDM_FEATURE_OKVEDS_SUGGESTIONS_REQUEST, getARDMFeatureOkvedsSuggestionsSuccess} from "../actions";
import {put, all, select, debounce} from "redux-saga/effects";
import {getARDMOkvedsQueryReselect} from "../reducer";



function* prepareRequest () {
    const [search_key] = yield all([
        select(getARDMOkvedsQueryReselect)
    ])
    const data = {search_key:search_key, lim: 10}
    return data
}

function* prepareSuccess(response, payload) {
    yield put(getARDMFeatureOkvedsSuggestionsSuccess(response.data))
}

function* prepareFailure(response, payload) {
}

export const getMatchingOkvadsSaga = () => {
    return createRequestRest({
        url: '/autodoc/get_matching_okvads',
        action: GET_ARDM_FEATURE_OKVEDS_SUGGESTIONS_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        timeOut: 500,

    }, debounce)
}
