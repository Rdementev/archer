import createRequestRest from "ApiARJTTD/createRequestRest";
import {ARDM_PAGES_UPDATE_PAGEDOCS_REQUEST, ardmPagesUpdateFormsPagedocsSuccess,ardmPagesUpdateFormsPagedocsFailure, ardmPagesBuildFormRequest} from "../actions";
import {all, put, select} from "redux-saga/effects";
import {addPreloader, deletePreloader} from "ReducerARJTTD/preloader";


function* prepareRequest(payload) {
    yield put(addPreloader())
   return payload
}
function* prepareSuccess(result, payload) {
    yield put(ardmPagesBuildFormRequest({form_id: payload.id}))
    yield put(ardmPagesUpdateFormsPagedocsSuccess(payload))
    yield put(deletePreloader())
}
function* prepareFailure(result, payload) {
    yield put(deletePreloader())
    yield put(ardmPagesUpdateFormsPagedocsFailure(payload))

}

export const updateFormDataSaga = () => {
    return createRequestRest({
        url: '/autodoc/update_form_data',
        action: ARDM_PAGES_UPDATE_PAGEDOCS_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}