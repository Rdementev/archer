import createRequestRest from "ApiARJTTD/createRequestRest";
import {put,race} from "redux-saga/effects";
import {isShowPreloader} from "ReducersARDM/requests";
import {push} from "connected-react-router";
import {ARDM_AUTH_RESETPASSWORD_GET_PASSWORD} from "../action";

function* prepareRequest(payload) {
    yield put(isShowPreloader(true))
    return payload;

}

function* prepareSuccess(response, data) {
    yield put(push('/auth/reset_password_check'))
    yield put(isShowPreloader(false))


}
function* prepareFailure(response, data) {
    yield put(isShowPreloader(false))


}

export const resetPassword = () => {
    return createRequestRest({
        url: "/heimdallr/reset_password",
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        action: ARDM_AUTH_RESETPASSWORD_GET_PASSWORD,
        method: 'get'
    })
}



