import createRequestRest from "ApiARJTTD/createRequestRest";
import {put,call} from "redux-saga/effects";
import {isShowPreloader} from "ReducersARDM/requests";
import {push} from "connected-react-router";
import {
    ARDM_AUTH_SIGNUP_GET_SIGN_UP,
    ardmAuthSignUpChangeDoubleSendEmail,
    ardmAuthSignUpShowCheckEmail
} from "../action";

function* prepareRequest(payload) {
    yield put(isShowPreloader(true))
    return payload;

}

function* prepareSuccess(response, data) {
    // const delay = time => new Promise(resolve => setTimeout(resolve, time));
    yield put(isShowPreloader(false))
    // yield put(ardmAuthSignUpShowCheckEmail(true))
    yield put(push('/auth/signup_check'))
    
    // yield put(ardmAuthSignUpChangeDoubleSendEmail(true))
    // yield call(delay, 5000)
    // yield put(push('/auth/confirm_email'))

}
function* prepareFailure(response, data) {
    yield put(isShowPreloader(false))
}

export const getSignUp = () => {
    return createRequestRest({
        url: "/heimdallr/register",
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        action: ARDM_AUTH_SIGNUP_GET_SIGN_UP,
        method: 'post'
    })
}



