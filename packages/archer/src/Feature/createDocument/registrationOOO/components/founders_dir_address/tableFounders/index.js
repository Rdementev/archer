import React, {useEffect, useRef, useState} from 'react';
import {connect} from "react-redux";
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import styled from "styled-components/macro";
import {setEditPersonalId} from "PagesARDM/personal/actions";
import InputSimpleFraction from "ComponentsARJTTD/input/inputSimpleFraction";
import InputPersent from "ComponentsARJTTD/input/inputPersent";
import InputTenFraction from "ComponentsARJTTD/input/inputTenFraction";
import {getARDMOrganizationReselect} from "PagesARDM/organization/reselect";
import ChoisePersonal from "PagesARDM/personal";
import ChoiseOrganization from "PagesARDM/organization";
import {ardmFeatureRegOOOCreateFounderIndividuals} from "FeatureARDM/createDocument/registrationOOO/actions";
import {
    ardmFeatureRegOOOChangePeaceValueIndividuals,
    ardmFeatureRegOOOChangePeaceValueOrganizations,
    ardmFeatureRegOOOClearFounders, ardmFeatureRegOOOCreateFounderOrganizations,
    ardmFeatureRegOOODeleteFounderIndividuals,
    ardmFeatureRegOOODeleteFounderOrganizations,
} from "../../../actions";
import {
    getARDMFeatureRegistrationOOOTypePeaceFormIdReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect,
    getARDMFeatureRegistrationOOOTypeSizecapitalReselect
} from "../../../reducer";
import {v4 as uuidv4} from "uuid";
import {changeOrganizationData} from "PagesARDM/organization/actions";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import UpdatePersonal from "Pages/personal/components/update";
import UpdateOrganization from "Pages/organization/components/update";
import {transparentize} from "polished";
import {Trash} from "Icons";


const EnhancedTableHead = (props) =>  {
    const { onSelectAllClick, numSelected , type_peace_form  } = props;
    const headCells = [
        {id: 'name', numeric: false, disablePadding: true, label: 'Наименование'},
        {id: 'peace_value', numeric: true, disablePadding: false, label: 'Размер доли' + ' ' + `${type_peace_form}`},
        {id: 'peace_sum', numeric: 'center', disablePadding: false, label: 'Номинальная стоимость'},
    ];

    return (
        <TableHeader>
            <TableCheck>
                <Checkbox checked={numSelected} onChange={(e)=>{onSelectAllClick(e)}} className='table__check table__check_item'/>
            </TableCheck>
            <TableHeaderItem>
                {headCells.map((headCell) => (
                    <Item key={headCell.id}
                          style={{maxWidth: '150px', }}
                          padding={headCell.disablePadding ? 'none' : 'default'}>
                            {headCell.label}
                    </Item>
                ))}
            </TableHeaderItem>
            <BlockAction>Действия</BlockAction>
        </TableHeader>
    );
}

const EnhancedTableToolbar = (props) => {
    const {numSelected, title, selected,ardmFeatureRegOOODeleteFounderIndividuals,ardmFeatureRegOOOCreateFounderOrganizations,
        ardmFeatureRegOOODeleteFounderOrganizations, ardmFeatureRegOOOCreateFounderIndividuals,
        showUpdatePersonal, setShowUpdatePersonal, editPersonalId,
        showUpdateOrganization, setShowUpdateOrganization, editOrganizationId,
        setSelectedFounder, showPersonalModal, setShowPersonalModal,changeOrganizationData,
        showOrganizitionModal, setShowOrganizitionModal} = props;
    const deleteFounder = () => {
        if(title === 'individuals'){
            ardmFeatureRegOOODeleteFounderIndividuals(selected)
        } else {
            ardmFeatureRegOOODeleteFounderOrganizations(selected)
        }
        setSelectedFounder([])
    }

    const handleClickAddFounder = () => {
        if(title === 'individuals') {
            setShowPersonalModal(true)
        } else {
            setShowOrganizitionModal(true)
        }
    }

    const handleClickPersonal = (id) => {
        if(title === 'individuals'){
            //собираем фаундера
            const unic = uuidv4();
            const founder = { id: unic,
                clientId:id,
                alias1: 'peace_value1',
                alias2: 'peace_value2',
                peace_value: '',
                peace_value1: '',
                peace_value2: '', }
            ardmFeatureRegOOOCreateFounderIndividuals(founder)
            setShowPersonalModal(false)
        }
        else {
            changeOrganizationData(8,id)
        }

    }

    const handleClickOrganization = (id) => {
            //собираем фаундера
            const unic = uuidv4();
            const founder = { id: unic,
                clientId:id,
                alias1: 'peace_value1',
                alias2: 'peace_value2',
                peace_value: '',
                peace_value1: '',
                peace_value2: '', }
        ardmFeatureRegOOOCreateFounderOrganizations(founder)
        setShowOrganizitionModal(false)
    }

    return (
        <>
        <HeaderToolBar >
            {numSelected > 0 ? (
                <HeaderToolBarTitle  >
                    {numSelected} selected
                </HeaderToolBarTitle>
            ) : (
                <HeaderToolBarTitle>
                    {title === 'individuals' ? 'Учредители физические лица' : 'Учредители юридические лица'}
                </HeaderToolBarTitle>
            )}
            {numSelected > 0
                ? (
                        <IconButton aria-label="delete">
                            <DeleteOutlineIcon onClick={() => {deleteFounder()}}/>
                        </IconButton>
                ) : (
                    <IconButton aria-label="Добавить">
                        <PersonAddIcon onClick={() => {handleClickAddFounder()}}/>
                    </IconButton>
            )}
        </HeaderToolBar>
        {showPersonalModal && <ChoisePersonal onClickPers={handleClickPersonal}
                         show={showPersonalModal}
                         setShow={setShowPersonalModal}/>}
        {showOrganizitionModal && <ChoiseOrganization onClickOrg={handleClickOrganization}
                             onClickPers={handleClickPersonal}
                             show={showOrganizitionModal}
                             setShow={setShowOrganizitionModal}/>}
        {showUpdatePersonal && <UpdatePersonal show={showUpdatePersonal}
                                               setShow={setShowUpdatePersonal}
                                               editPersonalId={editPersonalId}/>}
        {showUpdateOrganization && <UpdateOrganization show={showUpdateOrganization}
                                              editOrganizationId={editOrganizationId}
                                              setShow={setShowUpdateOrganization}/>}
        </>
    );
};

const TableFounders = (props) => {
    const {title, founders, personal, organization, type_peace_form, ardmFeatureRegOOODeleteFounderIndividuals,
        ardmFeatureRegOOODeleteFounderOrganizations, ardmFeatureRegOOOCreateFounderIndividuals,
        ardmFeatureRegOOOCreateFounderOrganizations, ardmFeatureRegOOOChangePeaceValueIndividuals,
        changeOrganizationData, ardmFeatureRegOOOChangePeaceValueOrganizations,ardmFeatureRegOOOClearFounders,
        type_reg, type_size_capital} = props

    const [bodyHeight, setBodyHeight] = useState('')
    const [selectedFounder, setSelectedFounder] = useState([])
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('calories');
    const [doubleFounders, setDoubleFounders] = useState([]);
    const [showPersonalModal, setShowPersonalModal] = useState(false)
    const [showOrganizitionModal, setShowOrganizitionModal] = useState(false)

    const refTableBody = useRef(false)

    const [showUpdatePersonal, setShowUpdatePersonal] = useState(false)
    const [editPersonalId, setEditPersonalId] = useState(false)

    const [showUpdateOrganization, setShowUpdateOrganization] = useState(false)
    const [editOrganizationId, setEditOrganizationId] = useState(false)


    useEffect(()=>{
        if(refTableBody.current){
            setBodyHeight(refTableBody.current.clientHeight)
        }
    })

    useEffect(() => {
        getDoubleFounder()
    }, [founders])

    useEffect(() => {
        if(type_reg === 'Упращенная регистрация ООО'){
            ardmFeatureRegOOOClearFounders()
        }
    }, [type_reg])


    //проверяем есть ли учредители привязанные к одному пользователю
    const getDoubleFounder = () => {
        const a = {}
        for (let i = 0; i < founders.length; i++) {
            for (let j = i+1; j < founders.length; j++) {
                if(founders[i].clientId === founders[j].clientId) {
                    a[founders[i].clientId] = founders[i].clientId
                }
            }
        }
        setDoubleFounders(Object.keys(a))
    }

    const handleSelectAllClick = (e) => {
        if (e.target.checked) {
            const newSelecteds = founders.map(n => n === n ? n.id : null);
            return setSelectedFounder(newSelecteds)

        }
        setSelectedFounder([])
    };

    const handleClick = (event, id,) => {
        const selectedIndex = selectedFounder.indexOf(id);
        let newSelected = [];
        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selectedFounder, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selectedFounder.slice(1));
        } else if (selectedIndex === selectedFounder.length - 1) {
            newSelected = newSelected.concat(selectedFounder.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selectedFounder.slice(0, selectedIndex),
                selectedFounder.slice(selectedIndex + 1),
            );
        }
        setSelectedFounder(newSelected)
    };

    const isSelected = (id) => selectedFounder.indexOf(id) !== -1;

    const handleClickFounder = (id) => {
        if (title === 'individuals') {
            setEditPersonalId(id)
            setShowUpdatePersonal(true)
        } else if (title === 'organizations') {
            setEditOrganizationId(id)
            setShowUpdateOrganization(true)
        }
    }

    const handleClickDeleteFounder = (e, id) => {
      const newSelected = selectedFounder.filter(item => !id.includes(item))
        if(title === 'individuals'){
            ardmFeatureRegOOODeleteFounderIndividuals(id)
        } else {
            ardmFeatureRegOOODeleteFounderOrganizations(id)
        }
        setSelectedFounder(newSelected)
    }

    const handleChangeInputPeace = (value, id, alias) => {
        if(title === 'individuals'){
            ardmFeatureRegOOOChangePeaceValueIndividuals(value, id, alias)
        } else {
            ardmFeatureRegOOOChangePeaceValueOrganizations(value, id, alias)
        }
    }

    const getRowsTable = () => {
        return founders.map((row, index) => {
                const obj =  title === 'individuals'
                    ?  personal.find(item => item.id === row.clientId)
                    : organization.find(item => item.id === row.clientId)
                if(!obj) return

                const name = title === 'individuals'
                    ?  obj.last_name.concat(' ', obj.first_name, ' ', obj.second_name )
                    : obj.name_full

                const isItemSelected = isSelected(row.id);
                const id = row.clientId.toString()
                const double = doubleFounders.includes(id)


                return (
                    <Row style={double ? {background:'#f2764c8c'} : {background:'#fff'}}>
                        <TableCheck>
                            <Checkbox className='table__check table__check_item'
                                      onChange={(e) => {handleClick(e, row.id)}}
                                      checked={isItemSelected}/>
                        </TableCheck>
                        <RowUser
                            tabIndex={-1}
                            key={row.id}
                        >
                            <TableHeaderItem >
                                <ItemName style={{cursor:'pointer', textTransform: 'uppercase'}} onClick={(e) => handleClickFounder(row.clientId)}>{name ? name : ''}</ItemName>
                                <Item>
                                    {type_peace_form === 'в виде простой дроби'
                                        ? <InputSimpleFraction alias1={row.alias1}
                                                               type_size_capital={type_size_capital}
                                                               alias2={row.alias2}
                                                               group={title === 'individuals' ? 'type_individuals': 'type_organizations'}
                                                               value2={row.peace_value2}
                                                               value1={row.peace_value1}
                                                               onChange={handleChangeInputPeace} id={row.id}/>
                                        : type_peace_form === 'в виде процентов'
                                            ? <InputPersent type_size_capital={type_size_capital}
                                                             group={title === 'individuals' ? 'type_individuals': 'type_organizations'}
                                                             value={row.peace_value}
                                                             onChange={handleChangeInputPeace} id={row.id}/>
                                            : <InputTenFraction type_size_capital={type_size_capital}
                                                                value={row.peace_value}
                                                                group={title === 'individuals' ? 'type_individuals': 'type_organizations'}
                                                                onChange={handleChangeInputPeace} id={row.id}/>
                                    }
                                </Item>
                                <Item>{row.peace_sum === 'Infinity' || row.peace_sum === 'NaN' ? '' : row.peace_sum} руб.</Item>
                            </TableHeaderItem>
                        </RowUser>

                        <BlockIconDelete >
                            <Util onClick={(e) => {handleClickDeleteFounder(e,[row.id])}}>
                                <IconTrash/>
                            </Util>
                        </BlockIconDelete>
                    </Row>
                );
            })
    }

    return (
        <Container>
            <EnhancedTableToolbar numSelected={selectedFounder.length}
                                  title={title}
                                  showUpdatePersonal={showUpdatePersonal}
                                  setShowUpdatePersonal={setShowUpdatePersonal}
                                  editPersonalId={editPersonalId}
                                  showUpdateOrganization={showUpdateOrganization}
                                  setShowUpdateOrganization={setShowUpdateOrganization}
                                  editOrganizationId={editOrganizationId}
                                  ardmFeatureRegOOOCreateFounderOrganizations={ardmFeatureRegOOOCreateFounderOrganizations}
                                  changeOrganizationData={changeOrganizationData}
                                  ardmFeatureRegOOOCreateFounderIndividuals={ardmFeatureRegOOOCreateFounderIndividuals}
                                  showPersonalModal={showPersonalModal}
                                  showOrganizitionModal={showOrganizitionModal}
                                  setShowOrganizitionModal={setShowOrganizitionModal}
                                  setShowPersonalModal={setShowPersonalModal}
                                  selected={selectedFounder}
                                  ardmFeatureRegOOODeleteFounderIndividuals={ardmFeatureRegOOODeleteFounderIndividuals}
                                  ardmFeatureRegOOODeleteFounderOrganizations={ardmFeatureRegOOODeleteFounderOrganizations}
                                  setSelectedFounder={setSelectedFounder}/>
            <TableContainer>
                <Table aria-labelledby="tableTitle"
                       size={'medium'}
                       aria-label="enhanced table">
                    <EnhancedTableHead
                        numSelected={selectedFounder.length}
                        type_peace_form={type_peace_form}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        rowCount={founders.length}
                        selectedFounderGroup={selectedFounder}/>
                    <TableBody bodyHeight={bodyHeight} ref={refTableBody}>
                        {getRowsTable()}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
}

const mapStateToProps = (state, props) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        organization: getARDMOrganizationReselect(state),
        type_peace_form: getARDMFeatureRegistrationOOOTypePeaceFormIdReselect(state),
        type_reg: getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_size_capital: getARDMFeatureRegistrationOOOTypeSizecapitalReselect(state)
    }
};


export default connect(mapStateToProps, {
    ardmFeatureRegOOODeleteFounderIndividuals, ardmFeatureRegOOOCreateFounderOrganizations,
    ardmFeatureRegOOODeleteFounderOrganizations,ardmFeatureRegOOOChangePeaceValueIndividuals,
    ardmFeatureRegOOOChangePeaceValueOrganizations, ardmFeatureRegOOOClearFounders,
    ardmFeatureRegOOOCreateFounderIndividuals, changeOrganizationData,
})(TableFounders)

const Item = styled.div`
    text-overflow: ellipsis;
    overflow: hidden;
`;
const ItemName = styled.div`
    text-overflow: ellipsis;
    padding: 0 0 3px 0;
    overflow: hidden;
    &:hover {
      text-decoration: underline;
    }
`;
const IconButton = styled.div`
    cursor: pointer;
    width: 35px;
    height: 35px;
    border-radius: 50%;
    transition: ease 0.5s;
    display: flex;
    &:hover {
      background: #cccccc7a;
    }
    & > svg {
      margin: auto;
    }
`;
const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;
const TableContainer = styled.div`
    height: 100%;
`;
const Table = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    height: 100%;
`;
const TableBody = styled.div`
    overflow-y: auto;
    //background: #f5f8fa;
    display: flex;
    flex-grow: 1;
    flex-direction: column;
    max-height: ${({bodyHeight}) => bodyHeight}px;
`;
const Util = styled.button`
    display: flex;
    margin-left: 10px;
    width: 28px;
    padding: 0;
    height: 28px;
    border: none;
    cursor: pointer;
    background: #fff;
    color: ${props => transparentize(0, props.theme.heavy)};
    border-radius: 4px;
    align-items: center;
    transition: ease 0.4s;
    &:active {
      background-color: #ccc;
    }
    & > svg {
      margin: auto;
      transition: ease 0.4s;
    }
    &:hover {
      transform: translate(0, -2px);
      box-shadow: 0 10px 14px rgba(0,0,0,0.2);
    }
    &:hover > svg {
    }
    &:hover > svg > path {
      fill: ${props => transparentize(0, props.theme.heavy)};
    }
`;
const IconTrash = styled(Trash)`

`;
const HeaderToolBar = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 7px 10px;
    text-align: center;
`;
const HeaderToolBarTitle = styled.h5`
    margin: 0;
    display: flex;
    align-items: center;
    font-size: 18px;
`;
const RowUser = styled.div`
    width: 100%;
    display: flex;
    max-height: 40px;
`;
const Row = styled.div`
    font-size: 12px;
    display: flex;
    padding: 7px 10px;
    align-items: center;
    border: 1px solid #e0e4f1;
    border-top: none;
    
    &:hover{
        background: #e4e9f145;
        transition: ease 0.5s;
    }
`;

const TableHeaderItem = styled.div`
    display: grid;
    grid-template-columns: 3fr 2fr 2fr;
    width: 100%;
    align-items: center;
`;
const BlockAction = styled.div`
    display: flex;
    min-width: 70px;
    justify-content: flex-end;
    align-items: center;
`;
const TableCheck = styled.div`
    max-width: 50px;
    width: 100%;
    display: flex;
`;

const BlockIconDelete = styled.div`
    display: flex;
    justify-content: flex-end;
    min-width: 70px;
    z-index: 1;
    cursor: pointer;
`;
const TableHeader = styled.div`
    font-size: 12px;
    display: flex;
    background: #1f367d14;
    padding: 7px 10px;
  
`;
