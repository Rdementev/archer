import React, {useEffect, useRef, useState} from 'react'
import {connect, useDispatch} from "react-redux";
import styled from 'styled-components/macro'
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import HeaderClient from "./components/subHeader";
import {CloseIcon, Check, Copy, Settings, Trash, DownLoadIcon} from "Icons";
import {transparentize} from "polished";
import BreadCrumb from "ComponentsARJTTD/breadcrumb";
import {
    getARDMPagedocsIsLoadMore,
    getListDocSelector,

} from "./reducer/pageDocList";
import {StarOutLine, Star} from "IconsARDM";
import moment from "moment";
import {
    ardmPagesDeleteFormsPagedocsRequest,
    ardmPagesDownloadFormRequest,
    ardmPagesGetFormsPagedocsRequest
} from "./actions";
import Preloader from "ComponentsARJTTD/preloader";
import {getARDMPagedocsIsLoadReselect} from "Pages/pagedocs/reducer/pageDocList";

const headerCell = [
    {id: 2, label: 'Клиент'},
    {id: 3, label: 'Описание'},
    {id: 4, label: 'Создано'},
]
const breadcrumb = [
    {
        title: 'сервис',
        link: '/serves',
    },
    {
        title: 'регистрационные документы',
    },
]

const PageDoc = (props) => {
    const {location, history, listDoc, isLoad, ardmPagesDownloadFormRequest, isLoadMore, ardmPagesDeleteFormsPagedocsRequest} = props
    const [isFetching, setIsFetching] = useState(null)
    const ListRef = useRef(null)

    useEffect(() => {
        if (!isFetching && isLoad) return;
        // ardmPagesGetFormsPagedocsRequest()
        setIsFetching(false)
    }, [isFetching, isLoad]);



    const handleScroll = (e) => {
        let el = ListRef.current

        if(el.scrollTop > el.scrollHeight - 1000 && isLoadMore){
            return setIsFetching(true)
        }
    }

    const getHeaderCell = (headerCell) => {
        return (
            <TableHeader>
                <TableIcons>
                    <IconStar>
                        <StarOutLine/>
                    </IconStar>
                    <IconsBlock>
                        <IconCheck/>
                    </IconsBlock>
                </TableIcons>
                <TableHeaderItem>
                    {headerCell.map((headCell) => (
                        <Item key={headCell.id}>
                            {headCell.label}
                        </Item>
                    ))}
                </TableHeaderItem>
                <Action>Управление</Action>
            </TableHeader>
        )
    }

    const handleClickDelete = (id) => {
        const data = {id: id}
        ardmPagesDeleteFormsPagedocsRequest(data)
    }
    const handleClickSave = (item) => {
    if(!item.build_data) return
        const fileName = item.form_name === 'Регистрация ООО'
            ? 'Регистрация'.concat(' ', item.description)
            : ''
        const data = {
            form_id: item.id,
            file_id: 1,
            fileName:fileName
        }
        ardmPagesDownloadFormRequest(data)
    }
    const getRowsTable = (data) => {
        if(data && data.length > 0){
            return data.map(item => {
                const active = item.form_data
                return (
                    <Row onDoubleClick={(e) => {handleClick(item)}}>
                        <TableIcons>
                            <IconStar>
                                {item.favorite
                                    ? <StarOutLine/>
                                    : <Star/>
                                }
                            </IconStar>
                            {item.completed
                                ? (
                                    <IconBlock background={'#46B04A'}>
                                        <IconCheck />
                                    </IconBlock>

                                )
                                : (
                                    <IconBlock background={'#F2764C'}>
                                        <IconClose />
                                    </IconBlock>
                                )}
                        </TableIcons>
                        <TableHeaderItem>
                            <Item>
                                <TitleClient onClick={(e) => {handleClick(item)}}>
                                    {item.description}
                                </TitleClient>
                            </Item>
                            <Item>{item.form_name}</Item>
                            <Item>{moment(item.created).format('DD.MM.YYYY')}</Item>
                            {/*<Item>{item.client_id}</Item>*/}
                        </TableHeaderItem>
                        <Action>
                            <Utils>
                                {/*<Util>*/}
                                {/*    <IconCopy/>*/}
                                {/*</Util>*/}
                                <Util active={active} onClick={()=>{handleClickSave(item)}}>
                                    {!item.build_data && <Overlay/>}
                                    <DownLoad/>
                                </Util>
                                <Util active={true} onClick={()=>{handleClickDelete(item.id)}}>
                                    <IconTrash/>
                                </Util>
                            </Utils>
                        </Action>
                    </Row>
                )
            })
        }
     return (
         <NotFound>Нет данных по запросу</NotFound>
     )
    }
    const handleClick = (item) => {
        let url
        if(item.form_name === 'Регистрация ООО'){
            url = location.pathname.concat('/reg-ooo', '/', `${item.id}`)
        } else if(item.form_name === 'Регистрация ИП'){
            url = location.pathname.concat('/reg-ip', '/', `${item.id}`)
        }
        history.push(url)
    }

    return (
        <>
            <HeaderClient />
            <BreadCrumb list={breadcrumb}/>
            <PageDocWrapper>
                {getHeaderCell(headerCell)}
                <RowsWrapper ref={ListRef}
                             onScroll={handleScroll}>
                    <Preloader/>
                    {getRowsTable(listDoc)}
                </RowsWrapper>
            </PageDocWrapper>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        listDoc: getListDocSelector(state),
        isLoadMore: getARDMPagedocsIsLoadMore(state),
        isLoad: getARDMPagedocsIsLoadReselect(state)
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, {
        ardmPagesDeleteFormsPagedocsRequest,ardmPagesGetFormsPagedocsRequest, ardmPagesDownloadFormRequest
    })
)(PageDoc)
//
const TableHeader = styled.div`
    display: grid;
    grid-template-columns: 70px minmax(200px, 100%) 120px;
    padding: 10px 20px;
    background: #fff;
    border-radius: 4px;
    align-items: center;
`;
const Item = styled.div`
    line-height: 10px;
    display: flex;
    white-space: nowrap;
    align-items: center;
    padding: 10px 5px;
    text-overflow: ellipsis;
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 0;
    transition: ease 0.3s;
`;
const TitleClient = styled.div`
    overflow: hidden;
    text-overflow: ellipsis;
    transition: ease 0.4s;
    border-bottom: 1px solid transparent;
    &:hover {
      border-color: #000;
    }
`;
const Action = styled.div`
    text-align: right;
    padding: 10px 0;
`;
const NotFound = styled.h2`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 0;
    margin: 0;
    color: rgba(0,0,0,0.7);
`;
const Util = styled.div`
    display: flex;
    position: relative;
    margin-left: 10px;
    width: 28px;
    height: 28px;
    background: #fff;
    color: ${props => transparentize(0, props.theme.heavy ) };
    border-radius: 4px;
    align-items: center;
    transition: ease 0.4s;
    border: 1px solid ${props => transparentize(0.7, props.theme.heavy ) };
    & > svg {
      margin: auto;
      transition: ease 0.4s;
    }
    &:hover {
      border: ${({active}) => active ? '1px solid transparent' : ''};
      box-shadow: ${({active}) => active ? `0 10px 14px ${props => transparentize(0.8, props.theme.heavy )}` : '' };
      transform: ${({active}) => active ? 'translate(0, -2px)' : ''}
    }
    
    &:hover > svg > path {
      fill: ${props => transparentize(0, props.theme.heavy ) };
    }
`;
const Utils = styled.div`
    display: flex;
    justify-content: flex-end;
`;
const Overlay = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    opacity: 0.8;
    background-color: #f7f7f7;
`;
const IconStar = styled.div`
    width: 16px;
    height: 16px;
    display: flex;
    & > svg {
        width: 16px;
        height: 16px;
    }
`;
const TableIcons = styled.div`
    display: flex;
    padding: 10px 0;
    align-items: center;
    width: 100%;
    justify-content: space-between;
    max-width: 50px;
`;
const IconsBlock = styled.div`
    width: 16px;
    height: 16px;
    background: ${props => transparentize(0, props.theme.heavy ) };
    box-shadow: 0 4px 4px ${props => transparentize(0.7, props.theme.heavy ) };;
    display: flex;
    border-radius: 50%;
    & > svg {
    margin: auto;
    width: 8px;
    height: 8px;
    }
`;
const IconBlock = styled.div`
    width: 16px;
    height: 16px;
    background: ${({background}) => background ? background : '#294493'};
    display: flex;
    border-radius: 50%;
    box-shadow: 0 4px 4px ${({background}) => background 
    ? transparentize(0.7, background )  
    : transparentize(0.7, '#294493') };
    & > svg {
    margin: auto;
    width: 8px;
    height: 8px;
    }
`;
const IconCheck = styled(Check)`
    
`;
const IconClose = styled(CloseIcon)`
    
`;

const Row = styled.div`
    display: grid;
    cursor: pointer;
    grid-template-columns: 70px minmax(200px, 100%) 120px;
    padding: 10px 20px;
    background: #fff;
    border-bottom: 1px solid transparent;
    border-bottom: 1px solid #e0e4f1;
    &:first-child{
        border-top-right-radius: 4px;
        border-top-left-radius: 4px;
    }
    &:last-child{
        border-bottom-right-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    &:hover {
      background: ${props => transparentize(0.85, props.theme.heavy ) };
      border-bottom: 1px solid ${props => props.theme.semiHeavy};
    }
    
`;
const RowsWrapper = styled.div`
    margin-top: 8px;
    background: #fff;
    border-radius: 4px;
    overflow: auto;
    position: relative;
`;

const PageDocWrapper = styled.div`
    background: #e5e5e54d;
    padding: 10px 20px;
    height: 100%;
    display: grid;
    grid-template-rows: 60px minmax(100px, 100%);
    overflow: auto;
`;
const TableHeaderItem = styled.div`
    display: grid;
    grid-template-columns: minmax(200px, 50%) 200px 200px;
    width: 100%;  
    
`;
const IconCopy = styled(Copy)`

`;
const IconSettings = styled(Settings)`

`;
const IconTrash = styled(Trash)`

`;
const DownLoad = styled(DownLoadIcon)`
    width: 18px;
    height: 18px;
  & > path {
    fill: #787598;
  }
`;
