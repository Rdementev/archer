import React, {useEffect, useRef, useState} from "react";
import {compose} from "redux";
import NewModal from "FeatureARDM/modal/newmodal";
import styled from "styled-components/macro";
import SearchBlockContainer from "ComponentsARJTTD/searchBlock/searchBlock";
import OkvedsTable from "./components/table";
import {connect} from "react-redux";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import {
    getARDMOkvedsCountReselect,
    getARDMOkvedsQueryReselect,
    getARDMOkvedsShowSuggestionsReselect,
    getARDMOkvedsSuggestionsReselect
} from "./reducer";
import {Ring} from "Feature/okveds/icons";
import {changeARDMFeatureOkvedsQuery, getARDMFeatureOkvedsSuggestionsRequest} from "./actions";
import {toggleARDMFeatureOkvedsShowSuggestions} from "Feature/okveds/actions";


const style = {
    input: {
        background: '#fff',
        color: '#000',
        '&:hover':{
            color: '#000',
        },
        '&:focus':{
            color: '#000',
        }
    },

}
const Okveds = (props) => {
    const {setShow, show, setSelected, type_selected_okved,
        query,
        showSuggestions,
        suggestions,
        toggleARDMFeatureOkvedsShowSuggestions,
        getARDMFeatureOkvedsSuggestionsRequest,
        changeARDMFeatureOkvedsQuery,
    } = props


    const handleClickItemSuggestionList = (item) => {
        setSelected(item)
    }
    // тайм аут запросов - пока печатаешь запрос не идет

    const onInputChange = (value) => {
        changeARDMFeatureOkvedsQuery(value)
        toggleARDMFeatureOkvedsShowSuggestions(true);
        getARDMFeatureOkvedsSuggestionsRequest(value)
    };

    useEffect(()=>{
        getSuggestionList()
    },[suggestions])


    // получаем список окведов из фильтра
    const getSuggestionList = () => {
        if(suggestions.length > 0){
            return suggestions.map(item => {
                const select = type_selected_okved && type_selected_okved.find(elem => elem.code === item.code)
                return (
                    <StyleElemOkved onClick={(e) => {handleClickItemSuggestionList(item)}}>
                        <BlockIconSuggestion select={select}>
                            <Ring/>
                        </BlockIconSuggestion>
                        <ItemSuggestion  >
                            <CodeSuggestion >
                                {item.code}
                            </CodeSuggestion>
                            <TitleSuggestion>
                                {item.title}
                            </TitleSuggestion>
                        </ItemSuggestion>
                    </StyleElemOkved>
                )
            })
        }
        return (
            <div  style={{padding: '5px 5px 5px 20px', display: 'flex'}}>
                <StyleElemOkved className='d-flex'>
                    <div  style={{display: 'flex'}}>
                        <div style={{minWidth:'70px'}}>
                            Нет совпадений
                        </div>
                    </div>
                </StyleElemOkved>
            </div>
        )

    }

    return (
        <NewModal showModal={show} setShow={setShow}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Справочник ОКВЭД</ModalTitle>
                    <BlockIconClose onClick={()=>{setShow(false)}}>
                        <CircleCloseBtn/>
                    </BlockIconClose>
                </ModalHeader>
                <ModalOffer>
                    <SearchBlockContainer style={style}
                                          value={query}
                                          onChange={onInputChange} />
                    {query && showSuggestions &&
                    <SuggestionContainer>
                        <Suggestion  >
                            {getSuggestionList()}
                        </Suggestion>
                    </SuggestionContainer>}
                </ModalOffer>
                <ModalBody>
                    <OkvedsTable type_selected_okved={type_selected_okved}
                                 setSelected={setSelected}/>
                </ModalBody>
            </ContainerModal>
        </NewModal>
    )
}

const mapStateToProps = (state) => {
    return {
        query: getARDMOkvedsQueryReselect(state),
        showSuggestions: getARDMOkvedsShowSuggestionsReselect(state),
        suggestions: getARDMOkvedsSuggestionsReselect(state),
        count: getARDMOkvedsCountReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
        toggleARDMFeatureOkvedsShowSuggestions,
        getARDMFeatureOkvedsSuggestionsRequest,
        changeARDMFeatureOkvedsQuery
    })
)(Okveds)

//
const ContainerModal = styled.div`
    display: grid;
    background: #f5f8fa;
    grid-template-rows: 50px 50px minmax(100px, 100%);
    max-height: 800px;
    height: 100%;
    max-width: 800px;
    width: 100%;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: 0 14px 34px rgba(0,0,0,0.4);
`;
const ModalHeader = styled.div`
   padding: 10px 20px;
   display: flex;
   justify-content: space-between;
   border-bottom: 1px solid #bec0cc;
`;
const ModalOffer = styled.div`
   padding: 10px 35px 5px 20px;
   position: relative;
`;
const ModalBody = styled.div`
   padding: 5px 20px 10px;
   flex-grow: 1;
   display: flex;
   background: #f5f8fa;
`;

const ModalTitle = styled.h4`
  
`;
const BlockIcon = styled.div`
    cursor: pointer;
`;
const BlockIconClose = styled.div`
    cursor: pointer;
   width: 14px;
   height: 14px;
`;
const SuggestionContainer = styled.div`
    position: absolute;
    background: #f5f8fa;
    border: 1px solid #e0e4f1;
    min-height: 500px;
    height: 100%;
    z-index: 2;
    left: 20px;
    right: 35px;
    top: 100%;
    box-shadow: 0 14px 34px rgb(0 0 0 / 0.3);
    border-radius: 8px;
    
   
`;

const Suggestion = styled.div`
   margin-right: 5px;
    overflow: auto;
    padding: 15px 10px;
    height: 100%;
    ::-webkit-scrollbar {
  width: 4px;
}

/* Track */
::-webkit-scrollbar-track {
  border-radius: 3px;
}

/* Handle */
::-webkit-scrollbar-thumb {
  border-radius: 3px;
  background: rgba(31, 54, 125, 0.16);
}

::-webkit-scrollbar-thumb:hover{
  background: rgba(31, 54, 125, 0.16);
}
    
`;


const StyleElemOkved = styled.div`
    cursor: pointer;
    width: 100%;
    display: flex;
    position: relative;
    align-items: center;
    border-radius: 4px;
    transition: ease 0.3s;
    &:hover {
      background-color: #eaeef3;
    }
`;
const ItemSuggestion = styled.div`
    display: flex;
    align-items: center;
    padding: 10px;
`;
const CodeSuggestion = styled.div`
  margin: 0 10px 0 0 ;
  min-width: 70px;
`;
const BlockIconSuggestion = styled.div`
  margin: 0 0 0 10px;
  & > svg > circle{ 
        stroke-width: 1.5;
        stroke: ${({select}) => select ? ' #284493' : ''} ;
        fill: ${({select}) => select ? ' #284493' : ''} ;
    }
`;

const TitleSuggestion = styled.div`
  margin: 0;
`;


