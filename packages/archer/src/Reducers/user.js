import {createSelector} from "reselect";

export const SET_USER = 'SET_USER'
export const LOG_OUT_USER = 'LOG_OUT_USER'

const user = (state = {}, action) => {
    switch (action.type) {
        case SET_USER : {
            return action.payload
        }

    }
    return state
};

export const setUser = (payload) => ({type: SET_USER, payload })

export default user;

//
const getEmailUserSelect = (state) => state.user.email
export const getEmailUserReselect = createSelector(getEmailUserSelect, (email)=> email)
const getUserSelect = (state) => state.user
export const getUserReselect = createSelector(getUserSelect, (user) => user)
