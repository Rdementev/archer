import React from "react";
import TypeProxy from "./type_proxy";
import {compose} from "redux";

const TypeProxyContainer = (props) => {
    const {proxyId, setProxyId} = props
    return <TypeProxy proxyId={proxyId} setProxyId={setProxyId}/>
}

export default compose()(TypeProxyContainer)
