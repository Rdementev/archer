import React, {useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {OpenGroup, Ring} from '../../icons/index'
import Item from "../item";

const Items = (props) => {
    const {elems, subElem, type_selected_okved, setSelected} = props
    const [showElem, setShowElem] = useState([])


    const handleOpenElem = (id) => {
        if(!showElem.includes(id)){
            setShowElem([...showElem, id])
        } else {
            const newShowElem = showElem.filter(item => item !== id)
            setShowElem(newShowElem)
        }

    }


    const handleSelectSubGroup = (id, item) => {
        setSelected(item)
    }

    const getActualElemsGroup = (id) => {
        let actualSubElem = subElem.filter(item => item && item.code.substr(0,7) === id)
        const actual = {
            actualSubElem:actualSubElem
        }
        return actual
    }

    const getElems = (elems) => {
        return elems.map((item) => {
            const actual = getActualElemsGroup(item.code)
            const isSolo = elems &&  elems.length === 1
            const active = showElem.includes(item.code)
            const select = type_selected_okved && type_selected_okved.find(elem => elem.code === item.code)
            const isClose = item.type === 'elem'
            return (
                <Container active={active}>
                    <BlockSubGroup isSolo={isSolo} active={active} >
                        <SubGroupItems >
                            <BlockIconSubGroup isClose={isClose} active={active} onClick={()=>{handleOpenElem(item.code)}}>
                                {active ? <OpenGroup/> :  <OpenGroup/>  }
                            </BlockIconSubGroup>
                            <BlockSelect onClick={()=>{handleSelectSubGroup(item.code,item)}}>
                                <BlockIconSubGroupSelect select={select} >
                                    <Ring/>
                                </BlockIconSubGroupSelect>
                                <ItemSubGroupCode>
                                    {item.code}
                                </ItemSubGroupCode>
                                <ItemSubGroupTitle>
                                    {item.title}
                                </ItemSubGroupTitle>
                            </BlockSelect>
                        </SubGroupItems>
                        {active && <Item type_selected_okved={type_selected_okved}
                                          setSelected={setSelected}
                                          subElems={actual.actualSubElem}/>}
                    </BlockSubGroup>
                </Container>
            )
        })
    }

    return (
        <>
            {getElems(elems)}
        </>
    )
}

export default compose()(Items)
//

const SubGroupItems = styled.div`
    cursor: pointer;
    display: flex;
    padding: 5px;
    margin: 5px 0;
    border-radius: 4px;
    transition: ease 0.3s;
    &:hover {
      background: #2944930d;
    }
`;
const Container = styled.div`
    margin-left: 14px;
    border-left: 1px solid #E2E7EE;
    &:last-child{
      border-color: transparent;
    }
`;
const BlockSubGroup = styled.div`
    padding: 3px 20px;
    position: relative;
    
    &::before {
    content: '';
    display: block;
    height: 29px;
    position: absolute;
    top: -5px;
    left: -1px;
    width: 15px;
    border-bottom: 1px solid #E2E7EE;
    border-left: 1px solid #E2E7EE;
    border-bottom-left-radius: 8px;
    }
`;
const BlockIconSubGroup = styled.div`
    margin: 0 5px;
    width: 10px;
    display: flex;
    transform: ${({active}) => active ? 'rotate(180deg)' : ''} ;
    & > svg > g > path{ 
      fill: ${({active}) => active ? ' #284493' : ''} ;
    }
    & > svg {
      display: ${({isClose}) => isClose ? 'none' : 'flex'};
      margin-top: 8px;
    }
`;
const BlockIconSubGroupSelect = styled.div`
    margin: 0 5px;
    & > svg > circle{ 
        stroke-width: 1.5;
        stroke: ${({select}) => select ? ' #284493' : ''} ;
        fill: ${({select}) => select ? ' #284493' : ''} ;
    }
`;
const BlockSelect = styled.div`
    display: flex;
    width: 100%;
`;
const ItemSubGroupCode = styled.div`
    margin-right: 5px;
    min-width: 55px;
`;
const ItemSubGroupTitle = styled.div`
    
`;
