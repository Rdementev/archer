import React, {useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {OpenGroup, Ring} from '../../icons/index'
import Items from "../items";
import {getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect} from "FeatureARDM/createDocument/registrationOOO/reducer";
import {connect} from "react-redux";


const Section = (props) => {
    const {subGroup, elem, subElem, type_selected_okved, setSelected} = props
    const [showSubGroup, setShowSubGroup] = useState([])


    const handleOpenSubGroup = (id) => {
        if(!showSubGroup.includes(id)){
            setShowSubGroup([...showSubGroup, id])
        } else {
            const newShowSubGroup = showSubGroup.filter(item => item !== id)
            setShowSubGroup(newShowSubGroup)
        }

    }
    const handleSelectSubGroup = (id, item) => {
        setSelected(item)
    }

    const getActualElemsGroup = (id) => {
        let actualElem = elem.filter(item => item && item.code.substr(0,5) === id)
        let actualSubElem = subElem.filter(item => item && item.code.substr(0,5) === id)
        const actual = {
            actualElem:actualElem,
            actualSubElem:actualSubElem
        }
        return actual
    }

    const getSubGroup = (subGroup) => {
        return subGroup.map((item) => {
            const actual = getActualElemsGroup(item.code)
            const isSolo = subGroup &&  subGroup.length === 1
            const active = showSubGroup.includes(item.code)
            const select = type_selected_okved && type_selected_okved.find(elem => elem.code === item.code)
            const isClose = item.type === 'elem'
            return (
                <Container active={active}>
                    <BlockSubGroup isSolo={isSolo} active={active} >
                        <SubGroupItems >
                            <BlockIconSubGroup isClose={isClose} active={active} onClick={()=>{handleOpenSubGroup(item.code)}}>
                                {active ? <OpenGroup/> :  <OpenGroup/>  }
                            </BlockIconSubGroup>
                            <BlockSelect onClick={()=>{handleSelectSubGroup(item.code,item)}}>
                                <BlockIconSubGroupSelect select={select} >
                                    <Ring/>
                                </BlockIconSubGroupSelect>
                                <ItemSubGroupCode>
                                    {item.code}
                                </ItemSubGroupCode>
                                <ItemSubGroupTitle>
                                    {item.title}
                                </ItemSubGroupTitle>
                            </BlockSelect>
                        </SubGroupItems>
                        {active && <Items type_selected_okved={type_selected_okved}
                                          setSelected={setSelected}
                                          subElem={actual.actualSubElem}
                                          elems={actual.actualElem}/>}
                    </BlockSubGroup>
                </Container>
            )
        })
    }

    return (
        <>
            {getSubGroup(subGroup)}
        </>
    )
}

const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps, {
    }
))(Section)
//
const SubGroupItems = styled.div`
    cursor: pointer;
    display: flex;
    padding: 5px;
    margin: 5px 0;
    border-radius: 4px;
    transition: ease 0.3s;
    &:hover {
      background: #2944930d;
    }
`;
const Container = styled.div`
    margin-left: 14px;
    border-left: 1px solid #E2E7EE;
    &:last-child{
      border-color: transparent;
    }
`;
const BlockSubGroup = styled.div`
    padding: 3px 20px;
    position: relative;
    
    &::before {
    content: '';
    display: block;
    height: 29px;
    position: absolute;
    top: -5px;
    left: -1px;
    width: 15px;
    border-bottom: 1px solid #E2E7EE;
    border-left: 1px solid #E2E7EE;
    border-bottom-left-radius: 8px;
    }
`;
const BlockIconSubGroup = styled.div`
    margin: 0 5px;
    width: 10px;
    display: flex;
    transform: ${({active}) => active ? 'rotate(180deg)' : ''} ;
    & > svg > g > path{ 
      fill: ${({active}) => active ? ' #284493' : ''} ;
    }
    & > svg {
      display: ${({isClose}) => isClose ? 'none' : 'flex'};
      margin-top: 8px;
    }
`;
const BlockIconSubGroupSelect = styled.div`
    margin: 0 5px;
    & > svg > circle{ 
        stroke-width: 1.5;
        stroke: ${({select}) => select ? ' #284493' : ''} ;
        fill: ${({select}) => select ? ' #284493' : ''} ;
    }
`;
const BlockSelect = styled.div`
    display: flex;
    width: 100%;
`;
const ItemSubGroupCode = styled.div`
    margin-right: 5px;
    min-width: 55px;
`;
const ItemSubGroupTitle = styled.div`
    
`;
