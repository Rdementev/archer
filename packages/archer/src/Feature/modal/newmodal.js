import React, {useEffect, useRef, useState} from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components/macro'
import {CSSTransition} from "react-transition-group";

const modalRoot = document.getElementById('modal')

const NewModal = (props) => {
    const {styled, showModal, setShow} = props

    return (
        <ModalBlock>
            <CSSTransition
                unmountOnExit
                in={showModal}
                timeout={{appear: 0, enter: 0, exit: 0}}
                classNames='modal__block'
                appear
            >
                <ContainerModal>
                    {ReactDOM.createPortal(
                        <Modal styled={styled} >
                            {props.children}
                        </Modal>,
                        modalRoot
                    )}
                </ContainerModal>
            </CSSTransition>
        </ModalBlock>
    );
};

export default NewModal
//
const Modal = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #08194a52;
    z-index: 1;

`;
const ContainerModal = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
`;




const ModalBlock = styled.div`
  .modal__block-appear{
        opacity: 0;
    }
    .modal__block-enter{
        opacity: 0.3;
    }
    .modal__block-enter-done{
        opacity: 1;
        transition: opacity 300ms;
    }
    .modal__block-exit{
        opacity: 1;
    }
    .modal__block-exit-active{
        opacity: 0;
        transition: opacity 0.3s ease;
    }
`;
