import {takeEvery, put, all} from "redux-saga/effects";
import {ARDM_PAGES_REQUEST_PAGEDOC_DATA} from "../../actions";
import {ardmPagesGetAddressesRequest} from "PagesARDM/address/actions";
import {ardmPagesGetPersonalRequest} from "PagesARDM/personal/actions";
import {ardmPagesGetOrganizationRequest} from "PagesARDM/organization/actions";
import {ardmPagesGetFormsPagedocsRequest} from "PagesARDM/pagedocs/actions";
import {getARDMFeatureOkveds, getARDMFeatureOkvedsCategories} from "FeatureARDM/okveds/actions";

function* worker(payload) {
    yield all([
        yield put(ardmPagesGetFormsPagedocsRequest()),
        yield put(ardmPagesGetAddressesRequest()),
        yield put(ardmPagesGetPersonalRequest()),
        yield put(getARDMFeatureOkvedsCategories()),
        yield put(getARDMFeatureOkveds()),
        yield put(ardmPagesGetOrganizationRequest()),
    ])

}
export function* requestPagedoc() {
    yield takeEvery(ARDM_PAGES_REQUEST_PAGEDOC_DATA, worker)
}