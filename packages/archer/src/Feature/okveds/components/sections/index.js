import React, {useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {CloseHierarchei, OpenGroup, OpenHierarchie} from '../../icons/index'
import Section from "../section";
import {connect} from "react-redux";

const Sections = (props) => {
    const {group, subGroup, elem, subElem, setSelected, type_selected_okved} = props
    const [showGroup, setShowGroup] = useState([])

    const handleOpenCategory = (id) => {
        if(!showGroup.includes(id)){
            setShowGroup([...showGroup, id])
        } else {
            const newshowGroup = showGroup.filter(item => item !== id)
            setShowGroup(newshowGroup)
        }
    }

    const getActualSubGroup = (id) => {

        let actualSubGroup = subGroup.filter(item => item && item.code.substr(0,4) === id)
        let actualElem = elem.filter(item => item && item.code.substr(0,4) === id)
        let actualSubElem = subElem.filter(item => item && item.code.substr(0,4) === id)

        const actual = {
            actualSubGroup:actualSubGroup,
            actualElem:actualElem,
            actualSubElem:actualSubElem
        }

        return actual
    }

    const getGroup = (group) => {
        return group.map((item) => {
            const actual = getActualSubGroup(item.code)

            const isSolo = group &&  group.length === 1
            const active = showGroup.includes(item.code)
            return (
                <Container active={active}>
                    <BlockGroup isSolo={isSolo} active={active} >
                        <GroupItems onClick={()=>{handleOpenCategory(item.code)}}>
                            <BlockIconOpenGroup active={active}>
                                {active ? <OpenGroup/> :  <OpenGroup/>  }
                            </BlockIconOpenGroup>
                            <ItemGroupCode>
                                {item.code}
                            </ItemGroupCode>
                            <ItemGroupTitle>
                                {item.title}
                            </ItemGroupTitle>
                        </GroupItems>
                        <BlockSection>
                            {active && <Section subGroup={actual.actualSubGroup}
                                                type_selected_okved={type_selected_okved}
                                                setSelected={setSelected}
                                                elem={actual.actualElem}
                                                subElem={actual.actualSubElem}/>}
                        </BlockSection>
                    </BlockGroup>
                </Container>
            )
        })
    }

    return (
        <>
            {getGroup(group)}
        </>
    )
}

const mapStateToProps = (state) => {
    return {

    }
}
export default compose(
    connect(mapStateToProps, {

    })
)(Sections)
//
const GroupItems = styled.div`
    cursor: pointer;
    display: flex;
    padding: 5px;
    margin: 5px 0;
    border-radius: 4px;
    transition: ease 0.3s;
    &:hover {
      background: #2944930d;
    }
`;
const BlockSection = styled.div`
    
    
`;
const Container = styled.div`
    margin-left: 10px;
    border-left: 1px solid #E2E7EE;
    &:last-child{
      border-color: transparent;
    }
`;
const BlockGroup = styled.div`
    padding: 2px 20px;
    position: relative;
    &::before {
        content: '';
        display: block;
        height: 55px;
        position: absolute;
        top: -31px;
        left: -1px;
        width: 15px;
        border-bottom: 1px solid #E2E7EE;
        border-left: 1px solid #E2E7EE;
        border-bottom-left-radius: 8px;
    }
`;
const BlockIconOpenGroup = styled.div`
    margin: 0 5px;
    display: flex;
    transform: ${({active}) => active ? 'rotate(180deg)' : ''} ;
    & > svg > g > path{ 
      fill: ${({active}) => active ? ' #284493' : ''} ;
    }
    & > svg {
      margin-top: 8px;
    }
`;
const ItemGroupCode = styled.div`
    margin-right: 5px;

`;
const ItemGroupTitle = styled.div`
    
`;
