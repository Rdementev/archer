import React, {useEffect, useRef, useState} from 'react'
import styled from 'styled-components/macro'
import Input from "ComponentsARJTTD/input/inputComponent";
import {compose} from "redux";
import {connect} from "react-redux";
import RequiredField from "ComponentsARJTTD/requiredField";
import Promto from "ComponentsARJTTD/promto";

const TypeSizeCapital = (props) => {
    const {onChange, value, valueIsValidation, sizeValueIsValidation, setValueIsTouch, valueIsTouch} = props

    return (
    <>
        <Header>
            <Title>Размер уставного капитала</Title>
            <RequiredField/>
        </Header>
        <Offer>
            <StyledInput>
                <Input  value={value}
                        onBlur={()=>{setValueIsTouch(true)}}
                        onChange={(e) => {onChange(e.target.value)}}/>
            </StyledInput>
            {valueIsTouch ? !valueIsValidation
                ? ''
                : <Promto text={valueIsValidation}/>
            : ''}
            {!sizeValueIsValidation && <SizeMistake>Размер уставного капитала должен быть не менее 10000 руб.</SizeMistake>}
        </Offer>
    </>
    )
}
const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps,{}),
)(TypeSizeCapital)
//



const Header = styled.div`
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
   padding: 0 10px 10px 10px;
   display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
`;
const SizeMistake = styled.div`
       font-size: 12px;
    font-weight: 600;
    max-width: 230px;
    margin-top: 10px;
`;
const Offer = styled.div`
  padding: 0 10px;
  position: relative;
`;
const StyledInput = styled.div`
   width: 300px;
   position: relative;
   height: 32px;
`;

const Body = styled.div`
  
`;
const Footer = styled.div`
  
`;
