import styled from "styled-components";

export const Button = styled.button`
  padding: 5px 10px;
  width: 100%;
  height: 100%;
  text-align: center;
  font-family: "Open-sans", sans-serif;
  font-size: 12px;
  font-weight: 600;
  cursor: pointer;
  transition: 0.3s all;
  background-color: #294493;
  color: #ffffff;
  border-radius: 4px;
  border: none;
  
  &:hover {
    background-color: #294493;
    color: #fff;
    box-shadow: 0 4px 17px rgba(41,68,147,0.4);
  }

  &:active {
    background-color: #182B65;
    color: #ffffff;
    box-shadow: none;
    transform: scale(0.95);
  }

  &:disabled {
    color: rgba(0,0,0,0.4);
    background-color: #D9D9D9;
    cursor: unset;
    box-shadow: none;
  }
  ${({styled}) => styled}
`;

export const WhiteButton = styled(Button)`
  background-color: #ffffff;
  border: 1px solid #D7D9DE;
  color: #5F5F5F;
 
  &:hover {
    background-color: #fff;
    color: #5F5F5F;
    border-color: #5F5F5F;
    box-shadow: none;
  }
  &:active {
    background-color: #E7E7E7;
    color: #5F5F5F;
    box-shadow: none;
    transform: scale(0.95);
    border-color: #5F5F5F;
  }
  
  ${({styled}) => styled}
`;




