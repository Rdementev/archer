import React, {useEffect, useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import {
    getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect,
    getARDMFeatureRegistrationOOOTypeDirPositionReselect,
    getARDMFeatureRegistrationOOOTypeDirTimeReselect,
    getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect
} from "../../../reducer";
import styled from "styled-components/macro";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import ChoisePersonal from "PagesARDM/personal";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import {setEditPersonalId} from "PagesARDM/personal/actions";
import Input from "ComponentsARJTTD/input/inputComponent";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {
    ardmFeatureRegOOOChangeDirPosition,
    ardmFeatureRegOOOSetAddressId, ardmFeatureRegOOOSetDirPersonalId,
    ardmFeatureRegOOOSetDirTime,
    ardmFeatureRegOOOSetDirIsDirAddress
} from "../../../actions";
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import RequiredField from "ComponentsARJTTD/requiredField";
import UpdatePersonal from "Pages/personal/components/update";

const list = [
    {id:1, name: 'Бессрочно',},
    {id:2, name: '1 год',},
    {id:3, name: '3 год',},
    {id:4, name: '5 лет',},
]

const TypeDir = (props) => {
    const {personal, address, type_dir_personal_id, type_dir_is_dir_address, ardmFeatureRegOOOChangeDirPosition,
        ardmFeatureRegOOOSetAddressId,ardmFeatureRegOOOSetDirPersonalId,
        type_dir_position, type_dir_time, ardmFeatureRegOOOSetDirTime, ardmFeatureRegOOOSetDirIsDirAddress} = props

    const [show, setShow] = useState(false)
    const [dirObj, setDirObj] = useState(false)
    const [name, setName] = useState(false)
    const [valueTime, setValueTime] = useState('')
    const [showUpdate, setShowUpdate] = useState(false)

    useEffect(()=>{
        const obj =  personal.find(item => item.id === type_dir_personal_id)
        setDirObj(obj)
    },[type_dir_personal_id, personal])

    useEffect(()=> {
        if(dirObj){
            const dirObjName = dirObj.last_name.concat(' ' , dirObj.first_name, ' ', dirObj.second_name )
            setName(dirObjName)
        }
    },[dirObj, address, personal,])

    useEffect(()=> {
        if(type_dir_time){
            const active = list.find(item => item.name === type_dir_time)
            setValueTime(active.name)
        }
    },[type_dir_time])


    const handleClickFounder = () => {
        setShowUpdate(true)
    }
    const handleClick = (item, multi, other) => {
        ardmFeatureRegOOOSetDirTime(item[0].name)
    }
    const handleChange = (value) => {
        ardmFeatureRegOOOChangeDirPosition(value)
    }
    const handleChangeDirAddress = (e) => {
        const value = e.target.checked
        ardmFeatureRegOOOSetDirIsDirAddress(e.target.checked)
        if(value){
            ardmFeatureRegOOOSetAddressId(dirObj.address_id)
        } else {
            ardmFeatureRegOOOSetAddressId(false)
        }
    }

    const handleClickPersonal = (id) => {
        ardmFeatureRegOOOSetDirPersonalId(id)
        setShow(false)
    }

    return (
        <>
            <Container>
                <Header>
                    <Title>Сведения о директоре</Title>
                </Header>
                <Offer>
                    <OfferBlock>
                        <OfferText>
                            <OfferSpan>Должность</OfferSpan>
                            <RequiredField/>
                        </OfferText>
                        <StyledPositionDir>
                            <Input value={type_dir_position} onChange={(e)=>{handleChange(e.target.value)}}/>
                        </StyledPositionDir>
                    </OfferBlock>

                    <OfferBlock>
                        <OfferText>
                            <OfferSpan>Срок избрания</OfferSpan>
                            <RequiredField/>
                        </OfferText>
                        <StyledTimeDir>
                            <SelectSearchContainer list={list}
                                                   value={type_dir_time}
                                                   displayValue={type_dir_time}
                                                   onClick={handleClick}/>
                        </StyledTimeDir>
                    </OfferBlock>

                    <StyledNameDir>
                        <NameDir onClick={()=>{handleClickFounder()}}>
                            {name}
                        </NameDir>
                    </StyledNameDir>
                </Offer>
                <Footer>
                    <StyledButton onClick={()=>{setShow(true)}}>
                        <Button>Выбрать</Button>
                    </StyledButton>
                    {name && dirObj.address_id && <StyledCheckbox>
                        <Checkbox onChange={(e) => {handleChangeDirAddress(e)}}
                                  checked={type_dir_is_dir_address}
                                  rightText={'Зарегистрировать фирму на домашний адрес руководителя'}/>
                    </StyledCheckbox>}
                </Footer>
            </Container>
            {showUpdate && <UpdatePersonal show={showUpdate}
                                           setShow={setShowUpdate}
                                           editPersonalId={dirObj.id}/>}
            <ChoisePersonal show={show} setShow={setShow} onClickPers={handleClickPersonal}/>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        type_dir_personal_id: getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect(state),
        type_dir_position: getARDMFeatureRegistrationOOOTypeDirPositionReselect(state),
        type_dir_time: getARDMFeatureRegistrationOOOTypeDirTimeReselect(state),
        type_dir_is_dir_address: getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        address: getArdmPagesAddressReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetDirIsDirAddress, ardmFeatureRegOOOSetDirTime, ardmFeatureRegOOOChangeDirPosition,
        ardmFeatureRegOOOSetAddressId, ardmFeatureRegOOOSetDirPersonalId
    }),
)(TypeDir)
//
const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
    padding: 5px 10px;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    
`;
const OfferBlock = styled.div`
    &:not(:last-child){
      margin-bottom: 10px;
    }
`;
const OfferText = styled.div`
    margin-bottom: 5px;
    display: flex;
`;
const OfferSpan = styled.span`
    font-size: 14px;
`;

const StyledNameDir = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    flex-grow: 1;
    font-size: 16px;
    text-transform: uppercase;
`;
const NameDir = styled.div`
    transition: ease 0.3s;
    &:hover {
      color: #000
    }
`;
const StyledPositionDir = styled.div`
    height: 32px;
`;
const StyledTimeDir = styled.div`
    height: 32px;
`;
const StyledButton = styled.div`
    width: 150px;
    margin-right: 20px;
`;
const StyledCheckbox = styled.div`
   & > label > p {
    max-width: 250px;
   }
`;
const Footer = styled.div`
    padding: 10px;
    margin-top: auto;
    display: flex;
`;
