import React from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {Renta} from 'IconsARDM'
import {Switch, Route, withRouter} from "react-router-dom";
import ClientAddress from "./clientAddress";


const sidebar = [
    {id:1, title: 'Профиль', icon: 'renta' , link: 'profile'},
    {id:2, title: 'Аренда адресов', icon: 'profile' , link: 'addresses'},
    // {id:3, title: '', icon: '' , link: 'addresses'},
    // {id:4, title: '', icon: '' , link: 'addresses'},
]

const templateIcons = {
    renta: Renta,
    profile: Renta,
}

const ClientLk = (props) => {
    const { history } = props

    const handleClick = (link) => {
        history.push(`/serves/account/${link}`)
    }

    const getSideBar = (sidebar) => {
        return sidebar.map(item => {
            const Icon = templateIcons[item.icon]
            return (
                <BlockMenuItem onClick={()=>handleClick(item.link)}>
                    <ItemIconBlock>
                        <Icon/>
                    </ItemIconBlock>
                    <ItemNameBlock>
                        {item.title}
                    </ItemNameBlock>
                </BlockMenuItem>
            )
        })
    }
    return (
        <Container>
            <BlockMenu>
                {getSideBar(sidebar)}
            </BlockMenu>
            <Switch>
                <Route exact path={'/serves/account/addresses'}  render={() => <ClientAddress/>}/>
            </Switch>

        </Container>
    )
}


export default compose(
    withRouter
)(ClientLk)
//
const Container = styled.div`
    height: 100%;
    padding: 10px 20px;
    display: grid;
    grid-template-columns: minmax(120px, 120px) minmax(200px, 100%);
    grid-gap: 10px;
`;
const BlockMenu = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  background-color: #1C3375;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`;

const BlockOffer = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  background-color: #fff;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`;

const OfferHeader = styled.div`
  background-color: red;
`;

const OfferBody = styled.div`
  
`;
const OfferTable = styled.div`
 
`;
const BlockMenuItem = styled.div`
 padding: 10px 5px;
 border-radius: 4px;
 transition: ease 0.4s;
 cursor: pointer;
 text-align: center;
 display: flex;
 flex-direction: column;
 &:hover {
  background-color: rgba(255,255,255,0.2);
 }
`;
const ItemIconBlock = styled.div`
    width: 40px;
    height: 40px;
    margin: 0 auto;
    & > svg {
      width: 100%;
      height: 100%;
      
    }
`;

const ItemNameBlock = styled.div`
 font-weight: 600;
 font-size: 10px;
 margin-top: 10px;  
 color: rgba(255,255,255,0.7);
 
`;