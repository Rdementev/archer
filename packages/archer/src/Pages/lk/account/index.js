import React from "react";
import styled from "styled-components/macro";
import {compose} from "redux";



const Account = ( ) => {
    return (
        <Container>
            <Header>
                <Title>
                    Мои показатели
                </Title>

            </Header>
            <Body>

            </Body>
        </Container>
    )
}

export default compose()(Account)
//
const Container = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  padding: 20px 0;
`;
const Title = styled.h3`
  margin: 0;
`;

const Body = styled.div`
  flex-grow: 1;
  background-color: #fff;
  border-radius: 4px;
`;
