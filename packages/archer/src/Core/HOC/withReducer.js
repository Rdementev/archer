import React from "react";
import {store} from "Core/store";
import {clearLocalStorage} from "../utils/settingsState";


export const withReducer = (reducers, module) => WrappedComponent => {
    const Extended = (props) => {
        store.injectReducer(reducers, module);
        clearLocalStorage()
        return <WrappedComponent {...props} />
    };

    return Extended;
};
