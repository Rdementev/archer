import { ReactComponent as ArrowLeft } from "./arrow_left.svg";
import { ReactComponent as CloseIcon } from "./CloseIcon.svg";
import { ReactComponent as Check } from "./check.svg";
import { ReactComponent as Copy } from "./copy.svg";
import { ReactComponent as Settings } from "./settings.svg";
import { ReactComponent as Trash } from "./trash.svg";
import { ReactComponent as StatusDone } from "./StatusDone.svg";
import {ReactComponent as BallIcon} from './Balls.svg'
import {ReactComponent as StarOutLine} from './star_outline.svg'
import {ReactComponent as Star} from './star.svg'
import {ReactComponent as DownLoadIcon} from './downloadIcon.svg'
import {ReactComponent as Renta} from './renta.svg'
import {ReactComponent as Cancel} from './cancel_bt.svg'





export { ArrowLeft,
    Renta,
    Cancel,
    CloseIcon,
    Check,
    Copy,
    Settings,
    Trash,
    StatusDone,
    BallIcon,
    StarOutLine,
    Star,
    DownLoadIcon};
