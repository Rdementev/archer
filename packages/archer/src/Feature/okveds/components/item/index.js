import React, {useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {OpenGroup, Ring} from '../../icons/index'

const Item = (props) => {
    const {subElems, type_selected_okved, setSelected} = props
    const handleSelectElem = (id, item) => {
        setSelected(item)
    }

    const getElem = (elems) => {
        return elems.map((item) => {
            const isSolo = elems &&  elems.length === 1
            const active = type_selected_okved.find(elem => elem.code === item.code)
            return (
                <Container active={active}>
                    <BlockElems isSolo={isSolo} active={active} >
                        <ElemsItems onClick={()=>{handleSelectElem(item.code, item)}}>
                            <BlockIconElems active={active}>
                                <Ring />
                            </BlockIconElems>
                            <ItemElem>
                                {item.code}
                            </ItemElem>
                            <ItemElemsTitle>
                                {item.title}
                            </ItemElemsTitle>
                        </ElemsItems>
                    </BlockElems>
                </Container>
            )
        })
    }


    return (
        <>
            {getElem(subElems)}
        </>
    )
}

export default compose()(Item)
//
const ElemsItems = styled.div`
    cursor: pointer;
    display: flex;
    padding: 5px;
    margin: 5px 0;
    border-radius: 4px;
    transition: ease 0.3s;
    &:hover {
      background: #2944930d;
    }
`;
const Container = styled.div`
    margin-left: 14px;
    border-left: 1px solid #E2E7EE;
    &:last-child{
      border-color: transparent;
    }
`;
const BlockElems = styled.div`
    padding: 3px 20px;
    position: relative;
    
    &::before {
    content: '';
    display: block;
    height: 29px;
    position: absolute;
    top: -5px;
    left: -1px;
    width: 15px;
    border-bottom: 1px solid #E2E7EE;
    border-left: 1px solid #E2E7EE;
    border-bottom-left-radius: 8px;
    }
`;
const BlockIconElems = styled.div`
    margin: 0 5px;
    & > svg > circle { 
        stroke-width: 1.5;
        stroke: ${({active}) => active ? ' #284493' : ''} ;
        fill: ${({active}) => active ? ' #284493' : ''} ;
    }
`;
const ItemElem = styled.div`
    margin-right: 5px;
    min-width: 70px;
`;
const ItemElemsTitle = styled.div`
    
`;
