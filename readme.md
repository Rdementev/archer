Запуск проекта:

Устанавливаем yarn глобально (1.0+)
Тянем сабмодули:
git submodule sync --recursive
git submodule update --init --recursive --remote --merge
Пишем yarn в корневой папке проекта
После cd /packages/archer заводим npm start

//api

/login
/logout
/is_logged_in
/heimdallr/check_if_email_is_used
/heimdallr/check_if_login_is_used
/heimdallr/confirm_email
/heimdallr/login
/heimdallr/register
/heimdallr/resend_confirm_email_message
/heimdallr/reset_password
/heimdallr/set_password

/autodoc/create_address
/autodoc/get_addresses
/autodoc/delete_address
/autodoc/update_address

/autodoc/create_individual
/autodoc/delete_individual
/autodoc/update_individual
/autodoc/get_individuals

/autodoc/delete_organization
/autodoc/create_organization
/autodoc/get_organizations
/autodoc/update_organization

/autodoc/get_okvad
/autodoc/get_okvad_section
/autodoc/get_matching_okvads
search_key
lim


/autodoc/get_ready_forms
search_key: str
Lim: int
Off: int
group_form_id: int
org_type: int

/autodoc/create_new_form
group_form_id: int
form_name: str (Регистрация ООО)

/autodoc/get_form_data
id: int

/autodoc/update_form_data
id: int


/autodoc/delete_form
id: int    

