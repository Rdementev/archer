import {all, fork} from "redux-saga/effects";
import {createOrganization} from "./createOrganization";
import {getOrganization} from "./getOrganization";
import {updateOrganization} from "./updateOrganization";


export function* organizationSaga() {
    yield all([
        fork(createOrganization),
        fork(getOrganization),
        fork(updateOrganization),
    ])
}