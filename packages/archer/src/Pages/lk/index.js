import React from "react";
import {compose} from "redux";
import {useSelector} from "react-redux";
import styled from 'styled-components/macro'
import BreadcrumbContainer from "ComponentsARJTTD/breadcrumb";
import {Switch} from "react-router-dom";
import AccountRoute from "./routes";


const breadcrumb = [
    {id: 1, title: 'Главное меню', link: '/serves'},
    {id: 1, title: 'Личный кабинет', link: ''},
]

const Lk = (props) => {
    const user = useSelector(state => state.user)

    return (
        <Container>
            <BreadcrumbContainer list={breadcrumb}/>
            <Switch>
                <AccountRoute path={'/serves/account'}  user={user} {...props}/>
            </Switch>
        </Container>
    )
}

export default compose()(Lk)
//
const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #F6F6F6;
  flex-grow: 1;
`;
