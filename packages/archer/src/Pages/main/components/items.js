import React, {useEffect, useState} from 'react'
import { connect } from 'react-redux'
import SectionItem from "./sectionItem";
import {CSSTransition} from "react-transition-group";
import styled from "styled-components/macro";
import {NavLink, withRouter} from "react-router-dom";


const Items = (props) => {
    const { item, color,  bodySection,  history } = props
    const [entered, setEntered] = useState(false);

    const EnterLinkRef = React.useRef()

    useEffect(()=>{
        setEntered(true)
    },[])

    const delay = (e, link, child , title) => {
        setEntered(false)
        e.preventDefault()
        if(child){
            setTimeout(() => {
                history.push(link)
            },300)
            return e.stopPropagation()
        }
        setTimeout(() => {
            history.push(link)
        },300)
    }

    return (
        <>
           <CSSTransition
                    key={item.id}
                    unmountOnExit
                    in={entered}
                    timeout={{ appear:0, enter: 0, exit: 300 }}
                    classNames='roll'
                    appear
                >
                <ListItem onClick={(e)=>{delay(e, item.location, false, item.title)}} to={item.location} >
                    <ListItemBody >
                    <BlockIcon>
                        <BlockTop>
                            <BlockMiddle/>
                        </BlockTop>
                    </BlockIcon>
                    <BodyItems   >
                        <SectionItem
                            delay={delay}
                            ref={EnterLinkRef}
                            parentColor={color}
                            bodySection={bodySection}
                            pageName={item.name}
                            func={item.func}/>
                    </BodyItems>
                    </ListItemBody>
                    <HeaderItem>
                        {item.title}
                    </HeaderItem>
                </ListItem>
            </CSSTransition>
        </>


    )
}

const RouterListItemMenu = withRouter(Items)

export default connect(null,{

})(RouterListItemMenu)


const ItemName = styled.h3`
    max-width: 150px;
    font-size: 18px;
    font-family: "ProximaNova-Regular",sans-serif;
    font-weight: 500;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: 0;
    line-height: 24px;
    &:hover {
     overflow: visible;
     word-break: break-word;
    }
`;

const BlockTop = styled.div`
    width: 60px;
    height: 60px;
    background: rgba(115, 6, 26, 0.1);
    position: absolute;
    left: 0;
    top: 0;
    border-radius: 15px;
    padding: 6px;
    box-shadow: 6px 6px 3px 0 #73061a33;
    transition: ease 0.3s;
`;

const ListItem = styled(NavLink)`
   width: 260px;
   padding: 20px;
    height: 184px;
    background-color: #fff;
    color: #000;
    transition: all 0.5s;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    border-radius: 4px;
    &:not(:last-child){
       margin-right: 20px;
    }
    &:hover {
        ${ItemName}{
            text-decoration: underline;
            text-decoration-color: #d5d5d5;
        }
        box-shadow: 0 24px 24px rgba(0, 0, 0, 0.1);
        color: #021140;
        transform: translate(0, -10px);
    }
`;


const ListItemBody = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const HeaderItem = styled.h3`
    text-align: center;
    margin: 10px 0 0 0;
    font-size: 15px;
    color: #040a1c;
    letter-spacing: 4px;
    line-height: 17px;
`;

const BodyItems = styled.div`
   max-width: 140px;
   
`;
const BlockIcon = styled.div`
    width: 66px;
    height: 66px;
    position: relative;
`;

const BlockMiddle = styled.div`
    z-index: 2;
    width: 48px;
    height: 48px;
    border-radius: 15px;
`;




