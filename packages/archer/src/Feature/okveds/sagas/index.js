import {all, fork} from "redux-saga/effects";
import {getOkvedSaga} from "./getOkvedSaga";
import {getOkvedCategoriesSaga} from "./getOkvedCategoriesSaga";
import {getMatchingOkvadsSaga} from "./getMatchingOkvads";


export function* getOkvedsSaga () {
    yield all([
        fork(getOkvedSaga),
        fork(getMatchingOkvadsSaga),
        fork(getOkvedCategoriesSaga)
    ])
}