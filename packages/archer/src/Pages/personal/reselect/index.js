import {createSelector} from 'reselect'

const ArdmPagesPersonalGetPersonal = state => state.ARDM.personal.personal
export const ArdmPagesPersonalGetPersonalReselect = createSelector(ArdmPagesPersonalGetPersonal,(personal) =>personal )