import React, {useEffect, useState} from 'react'
import Input from "ComponentsARJTTD/input/inputComponent";
import styled from 'styled-components/macro'

const InputTenFraction = (props) => {
    const { group, value, onChange, id, } = props
    const [valueValid , setValueValid] = useState(null)
    const [color , setColor] = useState()

    useEffect(()=>{
        fas()
    },[value, valueValid])

    const handleChange = (e) => {
        const foo = e.target.value
        const prevFoo = e.target.defaultValue
        const valueRex = /^[0-9]*[.,]?[0-9]+$/g;
        if(foo === '1.' || foo === '0.' ){
            return onChange('', id, 'peace_value')
        }else if (valueRex.test(e.target.value) || foo.length < 1) {
            if (!prevFoo) {
                if (+foo !== 1) {
                    if(+foo === 0){
                        let newFoo = `${foo}.`
                        setValueValid(true);
                        return onChange(newFoo, id, 'peace_value')
                    }
                    let newFoo = `0.${foo}`
                    setValueValid(true);
                    return onChange(newFoo, id, 'peace_value')
                } else if (+foo === 1) {
                    let newFoo = `${foo}.0`
                    setValueValid(true);
                    return onChange(newFoo, id, 'peace_value')
                }
            } else {
                if (+foo > 1) {
                    return
                }
                setValueValid(true);
                return onChange(foo, id, 'peace_value')
            }
        }
    }

    const fas = () => {
        if(valueValid === null){
            return setColor('#e0e4f1')
        }
        if(valueValid){
            return setColor('#068223')
        } setColor('#950101')
    }

    const handleOnBlur = () => {
        if(valueValid === null){
            setColor('#950101')
        }
        if(!value){
            setColor('#950101')
        }
    }

    return (
        <BlockInput >
            <Input
                style={{borderColor:`${color}`, background:'#f5f8fa'}}
                onChange={(e)=>{handleChange(e)}}
                onBlur={()=>{handleOnBlur()}}
                value={value}
                />
        </BlockInput>
    )
}
export default InputTenFraction
//
const BlockInput = styled.div`
  width: 100px;
   height: 25px;
`;