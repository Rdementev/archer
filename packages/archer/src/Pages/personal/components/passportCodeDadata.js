import * as React from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';
import styled from 'styled-components/macro'
import 'ComponentsARJTTD/input/StyleAddressDaData.css';
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import {
    getQuery,
    getSuggestions,
    setSuggestionIndex,
    toggleInputFocused,
    toggleShowSuggestions
} from "PagesARDM/personal/reducer/passportCode";
import {getDataApi} from "Core/api";
import {setPassportGivenByCode} from "Pages/personal/actions";



const wordsToPass = ['г', 'респ', 'ул', 'р-н', 'село', 'деревня', 'поселок', 'пр-д', 'пл', 'к', 'кв', 'обл', 'д'];

const defaultSuggestion = {
    data: {},
    unrestricted_value: '',
    value: ''
};

const defaultEndpoint = {
    api: 'suggestions/api/4_1/rs/suggest',
    host: 'https://suggestions.dadata.ru'
};

const defaultClasses = {
    'react-dadata__custom-action': 'react-dadata__suggestion react-dadata__custom-action',
    'react-dadata__suggestion': 'react-dadata__suggestion',
    'react-dadata__suggestion-note': 'react-dadata__suggestion-note',
    'react-dadata__suggestions': 'react-dadata__suggestions'
};

const getStylingProps = (baseClass, customStyles = {}, additionalClass) => {
    return customStyles[baseClass] && typeof customStyles[baseClass] === 'object'
        ? {
            className: `${defaultClasses[baseClass] || baseClass} ${additionalClass || ''}`.trim(),
            style: customStyles[baseClass]
        }
        : {
            className: `${defaultClasses[baseClass] || baseClass} ${additionalClass || ''} ${customStyles[baseClass] ||
            ''}`.trim()
        };
};

const backslashTailFix = uriPart => (uriPart.endsWith('/') ? uriPart.slice(0, -1) : uriPart);

const buildTargetURI = customEndpoint => {
    if (typeof customEndpoint === 'string') {
        if (/^http[s]?:/g.test(customEndpoint) || customEndpoint.startsWith('/')) {
            // Full path of host (API placed automatically - back compatibility to v1.2.8 and later)
            return backslashTailFix(`${customEndpoint}/${defaultEndpoint.api}`);
        }
    } else if (customEndpoint instanceof Object) {
        // Customize by object
        const endpointObject = { ...defaultEndpoint, ...customEndpoint };
        return `${backslashTailFix(endpointObject.host)}/${backslashTailFix(endpointObject.api)}`;
    }

    // Default
    return backslashTailFix(`${defaultEndpoint.host}/${defaultEndpoint.api}`);
};

const getHighlightWords = query => {
    const words = query.replace(',', '').split(' ');
    const filteredWords = words.filter(word => wordsToPass.indexOf(word) < 0);
    return filteredWords;
};

const fakeRandomKey = () =>
    Math.random()
        .toString(16)
        .slice(2);

const SuggestionInfo = ({ data = {}, type }) => (
    <div className="react-dadata__suggestion-info">
    <span>
      {[type === 'party' ? data.inn || null : data.bic || null, (data.address && data.address.value) || null].join(' ')}
    </span>
    </div>
);



const renderCustomActions = ({ customActions, customStyles, suggestions }, muteEventHandler, onBlur) => {
    if (!customActions) return [];

    let actions = customActions instanceof Function ? customActions(suggestions) : customActions;

    actions = actions instanceof Array ? actions : actions ? [actions] : false;

    return actions && actions.length
        ? [<hr key={'custom-actions-line'} className="actions-delimiter" />].concat(
            actions.map(node => (
                <div
                    key={fakeRandomKey()}
                    onMouseDown={muteEventHandler}
                    onClick={onBlur}
                    {...getStylingProps('react-dadata__custom-action', customStyles)}
                >
                    {node}
                </div>
            ))
        )
        : false;
};

const SuggestionsList = ({
                             actions = [],
                             customStyles,
                             onSuggestionClick,
                             query,
                             suggestionIndex,
                             suggestions,
                             type
                         }) => {
    return (
        !!(suggestions.length || actions.length) && (
            <div {...getStylingProps('react-dadata__suggestions', customStyles)}>
                {suggestions.map(({ value, data }, index) => {
                        return (

                            <div
                                key={fakeRandomKey()}
                                onMouseDown={() => {
                                    onSuggestionClick(index);
                                }}
                                {...getStylingProps(
                                    'react-dadata__suggestion',
                                    customStyles,
                                    index === suggestionIndex && 'react-dadata__suggestion--current'
                                )}
                            >
                                <Highlighter
                                    highlightClassName="react-dadata--highlighted"
                                    searchWords={getHighlightWords(query)}
                                    textToHighlight={value}
                                    autoEscape
                                />
                                {(type === 'party' || type === 'bank') && <SuggestionInfo data={data} type={type} />}
                            </div>
                        )
                    }
                )}
                {actions}
            </div>
        )
    );
};

class PassportCodeDadata extends React.PureComponent {

    onInputFocus = (e) => {
        this.props.handleTouch()
        if (this.props.passport_given_by_code) {
            this.fetchSuggestions({ inputFocused: true, showSuggestions: true });
        }
        this.props.toggleInputFocused(true);
    };
    onInputBlur = () => {
        this.props.toggleInputFocused(false);
    };

    // тайм аут запросов - пока печатаешь запрос не идет
    debounce = (func, cooldown = 500) => {
        return (...args) => {
            if (this.debounceTimer) {
                clearTimeout(this.debounceTimer);
            }
            this.debounceTimer = setTimeout(() => {
                func(...args);
            }, cooldown);
        };
    };

    onInputChange = event => {
        const { value } = event.target;
        this.props.setpassport_given_by_code(value);
        this.props.toggleShowSuggestions(true);
        this.debounce(this.fetchSuggestions)({ inputFocused: true, showSuggestions: true });
        !value && this.clear();
    };

    onKeyPress = event => {
        const { suggestionIndex, suggestions } = this.props;
        if (event.which === 40 && suggestionIndex < suggestions.length - 1) {
            // Arrow down
            this.props.setSuggestionIndex(this.props.suggestionIndex + 1)
            // this.setState(prevState => ({ suggestionIndex: prevState.suggestionIndex + 1 }));
        } else if (event.which === 38 && suggestionIndex > 0) {
            // Arrow up
            this.props.setSuggestionIndex(this.props.suggestionIndex - 1)
            // this.setState(prevState => ({ suggestionIndex: prevState.suggestionIndex - 1 }));
        } else if (event.which === 39 && suggestionIndex >= 0) {
            // Arrow right
            this.selectSuggestion(this.props.suggestionIndex, true);
        } else if (event.which === 13 && suggestionIndex >= 0) {
            // Enter
            event.preventDefault();
            event.stopPropagation();
            this.selectSuggestion(this.props.suggestionIndex);

        }
    };

    fetchSuggestions = () => {
        let payload = {
            query: this.props.passport_given_by_code ,
            count: this.props.count || 5
        };

        getDataApi.getFMS(payload).then(response => {
            if(response.status === 200){
                const  suggestions  = response.data.suggestions;
                if (suggestions && suggestions.length) {
                    this.props.getSuggestions(suggestions)
                    this.props.setSuggestionIndex(0)
                }
            }
        })

    };

    onSuggestionClick = index => {
        if (this.props.suggestions[index]) {
            this.selectSuggestion(index);
        }
        //сетаем данные в глобальный стейт
    };

    clear = () => {
        this.props.setpassport_given_by('');
        this.props.toggleShowSuggestions(false)
    };

    selectSuggestion = (index, showSuggestions = false) => {
        const { suggestions } = this.props;
        const { value } = suggestions[index];
        const { data } = suggestions[index];
        this.props.setpassport_given_by_code(data.code);
        this.props.setpassport_given_by(value)
        this.props.toggleShowSuggestions(showSuggestions)
    };

    muteEventHandler = e => {
        e.preventDefault();
        e.stopPropagation();
    };

    render() {
        const { inputFocused, passport_given_by_code, showSuggestions, suggestionIndex, suggestions, type } = this.props;
        const {
            allowClear,
            className,
            customActions,
            customInput,
            customStyles,
            placeholder,
            styles
        } = this.props;

        const showSuggestionsList = inputFocused && showSuggestions;

        const inputConfig = {
            autoComplete: 'new-password',
            className: `react-dadata__input${allowClear ? ' react-dadata__input-clearable' : ''}`,
            onBlur: this.onInputBlur,
            onChange: this.onInputChange,
            onFocus: this.onInputFocus ,
            onKeyDown: this.onKeyPress,
            placeholder: placeholder,
            value: passport_given_by_code
        };
        return (
            <div className={`react-dadata react-dadata__container ${className}`} style={styles}>
                <BlockInput>
                    {customInput(inputConfig)}
                </BlockInput>
                {allowClear && passport_given_by_code && (
                    <span className="react-dadata__input-suffix" onClick={this.clear}>
            <i className="react-dadata__icon react-dadata__icon-clear" />
          </span>
                )}
                {showSuggestionsList && (
                    <SuggestionsList
                        actions={
                            customActions &&
                            renderCustomActions({ customActions, customStyles, suggestions }, this.muteEventHandler, this.onInputBlur)
                        }
                        customStyles={customStyles}
                        suggestions={suggestions}
                        suggestionIndex={suggestionIndex}
                        query={passport_given_by_code}
                        type={type}
                        onSuggestionClick={this.onSuggestionClick}
                    />
                )}
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        inputFocused: state.ARDM.passportCode.inputFocused,
        showSuggestions: state.ARDM.passportCode.showSuggestions,
        suggestionIndex: state.ARDM.passportCode.suggestionIndex,
        suggestions: state.ARDM.passportCode.suggestions,
        type: state.ARDM.passportCode.type,
        count: state.ARDM.passportCode.count,

    }
};

export default connect(mapStateToProps,{
    getQuery, toggleShowSuggestions, getSuggestions,
    setSuggestionIndex, toggleInputFocused, setPassportGivenByCode,
})(PassportCodeDadata);

PassportCodeDadata.propTypes = {
    allowClear: PropTypes.bool,
    autocomplete: PropTypes.bool,
    city: PropTypes.bool,
    className: PropTypes.string,
    count: PropTypes.number,
    customActions: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    customEndpoint: PropTypes.oneOfType([PropTypes.object, PropTypes.shape, PropTypes.string]),
    customInput: PropTypes.func,
    customStyles: PropTypes.object,
    debounce: PropTypes.number,
    onChange: PropTypes.func,
    onIdleOut: PropTypes.func,
    payloadModifier: PropTypes.oneOfType([PropTypes.object, PropTypes.shape, PropTypes.func]),
    placeholder: PropTypes.string,
    showNote: PropTypes.bool,
    silentQuery: PropTypes.string,
    style: PropTypes.objectOf(PropTypes.string),
    token: PropTypes.string.isRequired,
    type: PropTypes.string
};



PassportCodeDadata.defaultProps = {
    customInput: params =>  <Input placeholder="Начните вводить..." {...params} />
};
//

const BlockInput = styled.div`
height: 32px;
& > input {
  font-size: 12px;
  ::placeholder{
  color: rgba(0,0,0,0.6);
  }
}
`;




