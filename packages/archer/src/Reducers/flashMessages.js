import shortId from "shortid";
const ADD_FLASH_MESSAGE = "ADD_FLASH_MESSAGE";
const DELETE_FLASH_MESSAGE = "DELETE_FLASH_MESSAGE";
const DELETE_ALL_FLASH_MESSAGE = "DELETE_ALL_FLASH_MESSAGE";
const GENERATE_TEMPO_MESSAGE = "GENERATE_TEMPO_MESSAGE";

//
// export default (state = [], action = {}) => {
//   switch (action.type) {
//     case ADD_FLASH_MESSAGE:
//       return !state.find(message => message.id === action.id)
//           ? [
//             ...state,
//             {
//               id: action.id || shortId.generate(),
//               type: action.messageType,
//               text: action.message,
//             },
//           ]
//           : state;
//
//     case DELETE_FLASH_MESSAGE:
//       return [...state.filter(message => message.id !== action.id)];
//     case DELETE_ALL_FLASH_MESSAGE:
//       return [];
//     default:
//       return state;
//   }
// };

export const addFlashMessage = (message, messageType, id) => ({type: ADD_FLASH_MESSAGE, message, messageType, id})
export const deleteFlashMessage = id => ({type: DELETE_FLASH_MESSAGE, id})
export const deleteAllFlashMessage = () =>  ({type: DELETE_ALL_FLASH_MESSAGE});
export const generateTempoMessage = (message, messageType, id, delayMS) => ({type: GENERATE_TEMPO_MESSAGE, message, messageType, id, delayMS})
