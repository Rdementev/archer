import { call, put, takeEvery } from "redux-saga/effects";
import makeRequest, {makeRequestDaData} from "ApiARJTTD";
import {generateTempoMessage} from "ReducerARJTTD/flashMessages";
const SUCCESS_STATUS = '2'

// по умолчанию обработчик формирует объект из данных и текста сообщения
const defaultPrepareSuccess = (result, successText) => ({
    data: result.data,
    successText,
});

// по умолчанию обработчик ошибок формирует строку из ошибки из бэка
const defaultPrepareFailure = (result, text) => `${text}. ${result.data.data}`;

// Функция для создания запросов.
// На вход 2 объекта - 1. Данные для создания запроса; 2. Данные, которые попадают в сам запрос
// Можно передавать url в данные для запроса (payload)
// Если в объекте 1 нет url, тогда идет проверка url в payload. Далее он удаляется и не передается в сам запрос

function* defaultRequestTake(

    {
        url, // путь
        failure, // событие, которое будет сгенерировано при ошибке
        success, // событие, которое будет сгенерировано при успешном ответе
        prepareRequest, // обработчик данных перед отправкой (return из этой функции - данные для запроса)
        prepareSuccess, // обработчик для успешного ответа; есть дефолтный обработчик
        failureText = "Произошла ошибка", // текст, который будет во всплывающем сообщении при ошибке
        successText, // текст, который будет во сплывающем сообщении при успешном ответе (сообщение появляется только если передать этот параметр)
        method = "post", //  метод запроса
        prepareFailure, // обработчик для ошибки; есть дефолтный обработчик
        propsRequest = [], // массив дополнительных параметров для запроса
        isOutputMessageFailure = true, // выводить ли сообщение об ошибке при ошибке, по умолчанию - выводить
        daData = false
    },

    { payload, type },

) {
    try {
        // обработка данных перед отправкой, если есть prepareRequest

        const data = prepareRequest ? yield call(prepareRequest, payload, type) : payload;
        // url из данных или из payload
        const urlRequest = url || (payload && payload.url) || (data && data.url);
        const dataRequest = { ...(data || {}) };
        // если в данных есть url - удаляем его
        if (dataRequest && dataRequest.url) delete dataRequest.url;
        // если есть обработчик данных перед отправкой и он вернул false или нет url - не отправляем запрос

        if ((!data && prepareRequest) || !urlRequest) return 0;

        // определяем куда отправлять запрос
        const Request = daData ? makeRequestDaData : makeRequest

        // отправка запроса
        const result = yield call(Request, method, urlRequest, dataRequest, ...propsRequest);

        const responseDaData = result && result.suggestions ? true : false
        let status
        if(responseDaData && daData) {
            status = '202'
        } else { status = result.status.toString().split('')}

        // если ошибка
        if (status[0] !== '2') {
            // если флаг true - выводим сообщение об ошибке
            if (isOutputMessageFailure) {
                yield put(generateTempoMessage(failureText, "failure", type));
            }

            // обработка данных (переданным обработчиком или дефолтным)
            const response = prepareFailure
                ? yield call(prepareFailure, result, data)
                : yield call(defaultPrepareFailure, result, failureText);

            // генерация события ошибки
            if (failure) yield put(failure(response));
        }
        // если все ок
        else {

            // обработка данных (переданным обработчиком или дефолтным)
            const response = prepareSuccess
                ? yield call(prepareSuccess, result, { payload, type, data })
                : yield call(defaultPrepareSuccess, result, successText);

            // если есть successText - выводим переданное сообщение
            if (successText) yield put(generateTempoMessage(successText, "success", type));

            // генерация события ок
            if (success) yield put(success(response));
        }
    } catch (e) {
        console.error("Error in createRequestRest", e);
    }
}

export default function* defaultRequest(data) {
    // data.action - событие, при котором должен отправляться запрос
    yield takeEvery(data.action, defaultRequestTake, data);
}
