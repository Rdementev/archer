import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'

import {compose} from "redux";
import {connect} from "react-redux";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {getARDMFeatureRegistrationOOOTypeRateNalReselect} from "FeatureARDM/createDocument/registrationOOO/reducer";
import {ardmFeatureRegOOOSetRateNalog} from "FeatureARDM/createDocument/registrationOOO/actions";
import RequiredField from "ComponentsARJTTD/requiredField";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import {getARDMOrganizationReselect} from "PagesARDM/organization/reselect";
import {ardmFeatureRegOOOSetPredsidatel} from "../../../actions";
import UpdatePersonal from "Pages/personal/components/update";

const styledSelect = {
    input: {
        paddingRight: '45px',
    },
    blockIconClear: {
        background: '#9C99B8',
    },
}
const TypePredsidatel = (props) => {
    const {personal,list, selected, ardmFeatureRegOOOSetPredsidatel, organization, selectedSecretar} = props
    const [isOrganization, setIsOrganization] = useState(false)
    const [name, setName] = useState('')
    const [resultList, setResultList] = useState(list)
    const [showUpdate, setShowUpdate] = useState(false)
    const [pers, setPers] = useState(null)

    useEffect(() => {
        const result = list.map(item => {
            return {
                ...item,
                items: item.items.filter(elem => elem.id !== selectedSecretar.id)
            }
        })
        setResultList(result)
    },[list, selectedSecretar])


    useEffect(()=> {
        if(selected.type === 'organizations'){
            setIsOrganization(true)
            return getPredstavitel(selected)
        }
        setIsOrganization(false)
    },[selected])

    //если это фирма ищем ее представителя
    const getPredstavitel = (selected) => {
        const org = organization.find(org => org.id === selected.clientId)
        if(!org) return
        const pers = personal.find(pers => pers.id === org.predstavitel_id)
        if(!pers) return
        setPers(pers)
        const name = pers.last_name.concat(' ', pers.first_name, ' ', pers.second_name)
        setName(name)
    }

    const handleClickItem = (item, multi , other) => {
        if(!multi && item){
            const {id, clientId, type, name, ...other} = item[0]
            const data = {
                id: id,
                clientId: clientId,
                type: type,
                name: name,
            }
            ardmFeatureRegOOOSetPredsidatel(data)
        }
        if(!item){
            ardmFeatureRegOOOSetPredsidatel({})
        }
    }

    return (
        <>
            <Header>
                <Title>Председатель</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <StyledSelectSearch>
                    <SelectSearchContainer onClick={handleClickItem}
                                           onClickClear={handleClickItem}
                                           clear={true}
                                           style={styledSelect}
                                           value={selected.name || ''}
                                           displayValue={selected.name || ''}
                                           list={resultList}/>
                </StyledSelectSearch>
            </Offer>
            <Body>
                {isOrganization &&
                <StyledNameDir>
                    Представитель
                    <NameDir onClick={()=>{setShowUpdate(true)}}>
                        {name ? name : 'Представитель не указан' }
                    </NameDir>
                </StyledNameDir>}
            </Body>
            {showUpdate && <UpdatePersonal show={showUpdate}
                                           setShow={setShowUpdate}
                                           editPersonalId={pers.id}/>}
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        type_rate_nal: getARDMFeatureRegistrationOOOTypeRateNalReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        organization: getARDMOrganizationReselect(state),
    }
}
export default compose(
    connect(mapStateToProps,{
        ardmFeatureRegOOOSetRateNalog, ardmFeatureRegOOOSetPredsidatel
    }),
)(TypePredsidatel)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 5px 10px;
`;
const StyledSelectSearch = styled.div`
    min-width: 250px;
    
    width: 100%;
    height: 30px;
`;
const Body = styled.div`
  
`;
const StyledNameDir = styled.div`
    padding: 10px;
`;
const NameDir = styled.div`
    transition: ease 0.3s;
    font-size: 15px;
    text-transform: uppercase;
    cursor: pointer;
    &:hover {
      color: #000
    }
`;

