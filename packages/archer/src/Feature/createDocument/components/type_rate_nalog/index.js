import React from "react";
import TypeRateNal from "./rate_nal";
import {compose} from "redux";

const list = [
    {id:1, name: '6% от всех доходов'},
    {id:2, name: 'от 5% до 15% от разницы доходов и расходов'},
]

const TypeRateNalContainer = (props) => {
    const {typeRateNal, setTypeRateNal,} = props
    return <TypeRateNal typeRateNal={typeRateNal} setTypeRateNal={setTypeRateNal} list={list}/>
}

export default compose()(TypeRateNalContainer)
