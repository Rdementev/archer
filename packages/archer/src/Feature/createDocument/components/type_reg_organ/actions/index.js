export const ARDM_CHANGE_REG_ORGAN_IS_LOAD = 'ARDM_CHANGE_REG_ORGAN_IS_LOAD'
export const ardmChangeRegOrganIsLoad = (value) => ({type: ARDM_CHANGE_REG_ORGAN_IS_LOAD, value})

export const GET_TYPE_REG_ORGAN_IFNS_DATA = 'GET_TYPE_REG_ORGAN_IFNS_DATA'
export const getTypeRegOrganIfnsData = (payload) => ({type: GET_TYPE_REG_ORGAN_IFNS_DATA, payload})
