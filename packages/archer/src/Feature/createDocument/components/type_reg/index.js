import React from "react";
import TypeReg from "./type_reg";
import {compose} from "redux";

const TypeRegContainer = (props) => {
    const {value, setValue, list} = props
    return <TypeReg value={value} setValue={setValue} list={list}/>
}

export default compose()(TypeRegContainer)
