import React, {useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {Cancel} from 'IconsARDM'
import {withRouter} from "react-router-dom";
import ActiveAddress from "./components/active";
import WaitAddress from "./components/wait";
import EndAddress from "./components/end";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {Controls as C} from '@rdementev/components'

const itemsPanel = [
    {id: 1, title: 'Действующие', icon: ''},
    {id: 2, title: 'В ожидание', icon: ''},
    {id: 3, title: 'Завершенные', icon: ''},
]
const listView = [
    {
        id: 1,
        name: 'Неделя',
        alias: 'week',

    },
    {
        id: 2,
        name: 'Месяц',
        alias: 'month',

    },

]

const AddminAddress = (props) => {
    const { history} = props
    const [activeItemId, setActiveItemId] = useState(1)
    const [periodView, setPeriodView] = useState('month')
    const [periodViewDisplayValue, setPeriodViewDisplayValue] = useState('Месяц')
    const [scrollOnToday, setScrollOnToday] = useState(false)

    const getItemsPanel = (itemsPanel) => {
        return itemsPanel.map(item => {
            const active = activeItemId === item.id
            return (
                <ItemBlock active={active} onClick={()=>{setActiveItemId(item.id)}}>
                    <ItemText>
                        {item.title}
                    </ItemText>
                </ItemBlock>
            )
        })
    }


    const getContent = (id) => {
        switch (id) {
            case 1 : {
                return <ActiveAddress periodView={periodView}
                                      setScrollOnToday={setScrollOnToday}
                                      scrollOnToday={scrollOnToday}/>
            }
            case 2: {
                return <WaitAddress/>
            }
            case 3: {
                return <EndAddress/>
            }
        }
    }

    const handleClickItemPeriod = (item, multi, other) => {
        if(!multi){
            setPeriodView(item[0].alias)
            setPeriodViewDisplayValue(item[0].name)
        }

    }


    return (
        <Container>
            <Header>
                <BlockIcon onClick={()=>{history.push('/serves/account')}}>
                    <Cancel/>
                </BlockIcon>
                <Title>
                    Аренда адресов
                </Title>
            </Header>
            <Panel>
                {getItemsPanel(itemsPanel)}
                {activeItemId === 1 &&
                    <SubPanel>
                        <BlockButton>
                            <WhiteButton onClick={(e)=>{setScrollOnToday(true)}}>
                                Сегодня
                            </WhiteButton>
                        </BlockButton>
                        <BlockSelectSearch>
                            <SelectSearchContainer onClick={handleClickItemPeriod}
                                                   displayValue={periodViewDisplayValue}
                                                   placeholder={'Период'}
                                                   list={listView}/>
                        </BlockSelectSearch>
                    </SubPanel>
               }

            </Panel>
            <Body>
                {getContent(activeItemId)}
            </Body>
        </Container>
    )
}


export default compose(
    withRouter
)(AddminAddress)
//
const Container = styled.div`
  height: 100%;
  width: 100%;
  display: grid;
  grid-template-rows: 40px 50px minmax(100px, 100%);
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  color: #131340;
`;
const BlockIcon = styled.div`
  display: flex;
  width: 20px;
  height: 20px;
  margin-right: 10px;
  cursor: pointer;
  & > svg {
      width: 100%;
      height: 100%;
  }
  &:hover > svg > rect {
    stroke: #131340;
  }
  &:hover > svg > rect  {
    transition: ease 0.4s;
  }
`;
const Title = styled.h3`
  margin: 0;
`;

const Body = styled.div`
  flex-grow: 1;
`;
const Panel = styled.div`
  padding: 10px;
  background-color: #fff;
  border-radius: 4px;
  display: flex;
`;
const SubPanel = styled.div`
  margin-left: auto;
  display: flex;
  justify-content: flex-end;
  width: 50%;
`;
const BlockSelectSearch = styled.div`
  max-width: 220px;
  width: 100%;
  max-height: 30px;
  height: 100%;
  margin-left: 10px;
`;
const BlockButton = styled.div`
  max-width: 150px;
  width: 100%;
  height: 30px;
  & > button {
      text-align: left;
  }
`;
const ItemBlock = styled.div`
  padding: 10px 15px;
  border-radius: 4px;
  border: ${({active}) => active ? '1px solid #ECEEF3' : '1px solid transparent'};
  background-color: ${({active}) => active ? '#F9F9F9' : 'transparent'};
  transition: ease 0.4s;
  font-weight: 600;
  cursor: pointer;
  &:hover {
    border-color: #ECEEF3;
    background-color: #F9F9F9;
  }
  &:not(:last-child){
    margin-right: 10px;
  }
`;
const ItemText = styled.div`
  line-height: 10px;
`;
