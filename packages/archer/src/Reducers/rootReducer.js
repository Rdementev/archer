import {combineReducers} from "redux";
import {connectRouter} from 'connected-react-router'
import {persistReducer} from "redux-persist";
import requests from "./requests";
import siteMap from "PagesARDM/main/reducer/siteMap";
import user from "./user";
import addressClient from "PagesARDM/address/reducer/addressClient";
import address from "PagesARDM/address/reducer/address";
import okved from "FeatureARDM/okveds/reducer";
import organization from "PagesARDM/organization/reducer/organizations";
import passportCode from "PagesARDM/personal/reducer/passportCode";
import personal from "PagesARDM/personal/reducer/personal";
import {registrationOOO, registrationIP} from "FeatureARDM/createDocument/reducer";
import pageDocList from "PagesARDM/pagedocs/reducer/pageDocList";
import {appAuthReducer} from "PagesARDM/auth/reducer";
import regOrgan from "FeatureARDM/createDocument/components/type_reg_organ/reducer";
import {appReducerARJTTD} from "ReducerARJTTD"
import preloader from "ReducerARJTTD/preloader"


const appReducers = () => combineReducers({
        registrationOOO,
        registrationIP,
        addressClient,
        address,
        okved,
        organization,
        passportCode,
        personal,
        requests,
        siteMap,
        pageDocList,
        regOrgan,

        auth: appAuthReducer,

})


const CreateReducers = (history, persistConfig, asyncReducer) => {
        return persistReducer(persistConfig, combineReducers({
                router: connectRouter(history),
                user,
                tempoMessage:appReducerARJTTD,
                preloader,
                ARDM: appReducers(),
                ...asyncReducer
        }))
}






export default CreateReducers
