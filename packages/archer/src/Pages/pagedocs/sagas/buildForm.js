import createRequestRest from "ApiARJTTD/createRequestRest";
import {ARDM_PAGES_BUILD_FORM_REQUEST,
    ardmPagesBuildFormSuccess,
    ardmPagesBuildFormFailure} from "../actions";
import {all, put, select} from "redux-saga/effects";
import {addPreloader, deletePreloader} from "ReducerARJTTD/preloader";


function* prepareRequest(payload) {
    yield put(addPreloader())
    return payload
}
function* prepareSuccess(result, payload) {
    yield put(ardmPagesBuildFormSuccess())
    yield put(deletePreloader())

}
function* prepareFailure(result, payload) {
    yield put(ardmPagesBuildFormFailure())
    yield put(deletePreloader())

}

export const buildFormSaga = () => {
    return createRequestRest({
        url: '/autodoc/build_form',
        action: ARDM_PAGES_BUILD_FORM_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}