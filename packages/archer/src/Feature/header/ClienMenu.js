import React, {Component} from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import MenuItem from "FeatureARDM/header/MenuItem";


class ClientMenu extends Component {
    render() {
        return (
            <StyledUl>
                <MenuItem />
            </StyledUl>
        );
    }
}

const mapStateToProps = state => {
    return {

    }
};

export default withRouter(connect(mapStateToProps,{

})(ClientMenu));

//

const StyledUl = styled.div`
  height: 100%;
  display: flex;
  &:hover > div {
    transform: translate(0, 0);
  }
`;

