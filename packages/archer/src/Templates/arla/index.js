import {appARLAReducer} from 'RootReducerARLA'
import ARLA from "ExportARLA";
import {compose} from "redux";
import {withReducer} from "CoreARDM/HOC/withReducer";
import React, {useEffect} from "react";
import {withTheme} from "CoreARDM/HOC/withTheme";


const ArlaModule = (props) => {
    return <ARLA />
}

export default compose(
    withReducer({ARLA: appARLAReducer}),
    withTheme
)(ArlaModule)