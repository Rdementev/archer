import React, {useEffect, useRef, useState} from "react";
import Input from "ComponentsARJTTD/input/inputComponent";
import styled from "styled-components/macro";
import {ardmFeatureRegOOOSetTypeNameEng} from "../../../actions";
import {connect} from "react-redux";
import {compose} from "redux";
import {withTheme} from "styled-components";


const styleInput = {
    border:'none',
    height: '30px'
}

const ContainerInputEng = (props) =>{
    const {ardmFeatureRegOOOSetTypeNameEng, value, theme} = props
    const [rus, setRus] = useState(false)
    const [enter, setEnter] = useState(false)
    const rusRef = useRef(null)

    const toggleInputRus = () => {
        setRus(true)
    }
    const handleClickOutSide = (e) => {
        const item = rusRef.current
        if(e.path){
            if(!e.path.includes(item)){
                setRus(false)
            }
        }
    }

    useEffect(()=> {
        document.addEventListener('click', handleClickOutSide)
        return function () {
            document.removeEventListener('click', handleClickOutSide)
        }
    })
    return (
        <>
        <StyledInput onClick={()=>{toggleInputRus(!rus)}}
                     ref={rusRef}
                     enter={enter}
                     theme={theme}
                     onMouseLeave={()=>{setEnter(false)}}
                     onMouseEnter={()=>{setEnter(true)}}
                     rus={rus}>
            <RightInput>OOO «</RightInput>
            {rus ? <Input style={styleInput} autoFocus={true} value={value} onChange={(e) => {
                ardmFeatureRegOOOSetTypeNameEng(e.target.value)
            }}/> : <InputText>{value}</InputText> }
            <LeftInput>»</LeftInput>
        </StyledInput>
        </>
    )
}
const mapStateToProps =(state) => {
    return
}
export default compose(connect(mapStateToProps,{
        ardmFeatureRegOOOSetTypeNameEng
}),
    withTheme
)(ContainerInputEng)
//
const StyledInput = styled.div`
    height: 33px;
    margin: 0 10px 0 0;
    padding: 0 10px;
    border-radius: 4px;
    background: #fff;
    border: 1px solid #e5e5e5;
    border-color: ${({enter, rus, theme}) => enter || rus ? theme.semiHeavy : '#e5e5e5' };
    transition: ease 0.3s;
    font-size: 100%;
    line-height: 1.15;
    display: flex;
    
`;
const RightInput = styled.div`
    white-space: nowrap;
    display: flex;
    align-items: center;
`;
const InputText = styled.span`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
    padding: 10px 0;
    line-height: 11px;
`;
const LeftInput = styled.div`
    white-space: nowrap;
    display: flex;
    align-items: center;
`;