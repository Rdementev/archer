import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import {DatePicker} from "antd";
import * as moment from "moment";
import locale from "antd/es/date-picker/locale/ru_RU";
import {compose} from "redux";
import {connect} from "react-redux";
import {getARDMFeatureRegistrationOOOTypeCreateDateReselect} from "../../../reducer";
import {ardmFeatureRegOOOChangeTypeCreateDate} from "../../../actions";
import RequiredField from "ComponentsARJTTD/requiredField";

const TypeCreateDate = (props) => {
    const {type_create_date, ardmFeatureRegOOOChangeTypeCreateDate} = props

    const handleChangeDate = (sad, date) => {
        const dates = moment(sad).format('YYYY-MM-DD')
        if(!sad) return ardmFeatureRegOOOChangeTypeCreateDate(null)
        ardmFeatureRegOOOChangeTypeCreateDate(dates)
    };

    return (
    <>
        <Header>
            <Title>Дата создания</Title>
            <RequiredField/>
        </Header>
        <Offer>
            <StyledDatePicker>
                    <DatePicker locale={locale}
                                value={type_create_date && moment(type_create_date)}
                                placeholder={'Выберите дату'}
                                style={{width: '100%'}}
                                onChange={handleChangeDate}
                                format="DD.MM.YYYY"/>

            </StyledDatePicker>
        </Offer>
        <Body></Body>
        <Footer></Footer>
    </>
    )
}
const mapStateToProps = (state) => {
    return {
        type_create_date: getARDMFeatureRegistrationOOOTypeCreateDateReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {ardmFeatureRegOOOChangeTypeCreateDate})
)(TypeCreateDate)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
    
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
   
`;
const Offer = styled.div`
  padding: 0 10px;
`;
const StyledDatePicker = styled.div`
  width: 300px;
`;

const Body = styled.div`
  
`;
const Footer = styled.div`
  
`;
