import React, {Component, useEffect, useRef, useState} from "react";
import {connect, useDispatch} from "react-redux";
import styled, {keyframes} from "styled-components";

import {Check, StatusDone} from 'Icons'
import {HeaderProfile, HeaderExit} from 'Icons/header'
import {compose} from "redux";
import {getUserReselect} from "ReducersARDM/user";
import {ardmAuthGetLogout} from "PagesARDM/auth/signIn/action";
import {withRouter} from "react-router-dom";


const ClientAvatar = (props) => {
    const { user, ardmAuthGetLogout, history} = props;
    const [isVisibleList, setIsVisibleList] = useState(false)
    const [showToolTip, setShowToolTip] = useState(false)
    const dispatch = useDispatch();
    const AvatarRef = useRef()

    useEffect(() => {
        document.addEventListener("click", handleClickOutSide, false)
        return function () {
            document.removeEventListener("click", handleClickOutSide, false);
        }
    })

    const handleClickOutSide = (e) => {
        const item = AvatarRef.current
        if (!e.path.includes(item)) {
            setIsVisibleList(false)
        }
    };
    const handleClickProfile = (e) => {
        history.push('/serves/account')
        setShowToolTip(false)
        setIsVisibleList(false)
    };

    const handleClickLogout = () => {
        dispatch({type: 'DELETE_AUTH_REDUCE'})
        ardmAuthGetLogout()

    }

    return (
        <AvatarBlock onClick={() => {setIsVisibleList(!isVisibleList)}}
                     onMouseEnter={() => {setShowToolTip(true)}}
                     onBlur={() => {setShowToolTip(false)}}
                     onMouseLeave={() => {setShowToolTip(false)}}
                     ref={AvatarRef}>
            <>
                <>
                    <HeaderBlockIcon isVisibleList={isVisibleList}>
                        <Icon isVisibleList={isVisibleList}/>
                    </HeaderBlockIcon>
                </>
            </>
            {isVisibleList && (
                <AvatarList>
                    <AvatarBlockList>
                       <Avatar>
                           <IconUser/>
                       </Avatar>
                        <AvatarInfo>
                            <div style={{fontSize: '16px',  fontFamily: 'Lato-Bold'}}>
                                {user.login}
                            </div>
                            <div style={{fontSize: '12px', color: 'rgba(0, 0, 0, 0.7)'}}>
                                {user.email}
                            </div>
                        </AvatarInfo>
                    </AvatarBlockList>
                    <div    style={{padding: '7px 10px'}} >
                        <AvatarListItem onClick={() => {handleClickProfile()}}>
                            <Icon style={{width: '14px', height: '16px', margin: '0 12px 0 0'}} />
                            <AvatarText>Профиль</AvatarText>
                        </AvatarListItem>
                        <AvatarListItemBot className="Exit" onClick={()=>{handleClickLogout()}}>
                            <IconExit style={{width: '14px', height: '16px', margin: '0 12px 0 0'}} />
                            <AvatarText>Выйти</AvatarText>
                        </AvatarListItemBot>
                    </div>
                </AvatarList>
            )}
            {isVisibleList && <AvatarInfoStatus>
                <IconUtil/>
            </AvatarInfoStatus>}
        </AvatarBlock>
    );

}

const mapStateToProps = state => ({
    user: getUserReselect(state)
});

export default compose(
    connect(mapStateToProps,{ardmAuthGetLogout}),
    withRouter,
)(ClientAvatar);

//

const AvatarText = styled.div`
  
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 16px;
    color: #000000;
    opacity: 0.9;
`;

const Icon = styled(HeaderProfile)`
   margin: auto;
   width: 12px;
   height: 12px;
   & > * {
    fill: ${({isVisibleList}) => isVisibleList ? '#fff' : '#2C2C55'};
   }
`;

export const IconUtil = styled(StatusDone)`
  margin: auto;
  & > g > * {
     fill: #4F69B2;
  }
`;

export const AvatarInfoStatus = styled.div`
    display:flex;
    width: 12px;
    height: 12px;
    border-radius: 2px;
    position: absolute;
    top: -4px;
    right: -4px;
    background: #fff;
    box-shadow: 0 4px 14px #2844934d;
`;


const HeaderBlockIcon = styled.div`
  width: 22px;
  height: 22px;
  display: flex;
  align-items: center;
  transition: all 0.3s;
  background-color: rgba(255, 255, 255, 0.25);
  border-radius: 50%;
  
  &:hover {
     background-color: ${props => props.theme.medium};
  }
  & ${Icon} { 
     & > * {
      fill: #fff;
     }
  }
`;

const AvatarBlock = styled.div`
  width: 22px;
  height: 22px;
  display: flex;
  align-items: center;
  position: relative;
  cursor: pointer;
`;


const animation = keyframes`
  0%{
    transform: rotateY(100deg);
    opacity: 0;
  }
  100%{
    transform: rotateY(0);
    opacity: 1;
  }
`;
const AvatarList = styled.ul`
    margin: 0;
    width: 210px;
    position: absolute;
    top: 48px;
    right: 0;
    z-index: 1000;
    background: #fff;
    border: 1px solid #f2f2f2;
    color: #000;
    border-radius: 4px;
    box-shadow: 0px 6px 24px rgba(0, 0, 0, 0.06);
    animation: ${animation} 0.3s 0s;
`;

const AvatarListItem = styled.div`
  padding: 8px 12px;
  border-radius: 2px;
  display: flex;
  align-items: center;
  font-size: 13px;
  white-space: nowrap;
  cursor: pointer;

  &:hover {
    background: #f2f2f2;
  }
`;
const AvatarListItemBot = styled(AvatarListItem)`
 
`;
const AvatarBlockList = styled.div`
    display: flex;
    padding: 10px 10px;
    border-bottom: 1px solid #ECEEF3;
`;
const Avatar = styled.div`
  margin-right: 10px;
  background: url(${({avatar}) => avatar ? avatar : ''}) no-repeat, center, center;
  background-size: cover;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  
`;
const IconExit = styled(HeaderExit)`
  
`;
const AvatarInfo = styled.div`
    width: 100%
    display: flex;
    flex-direction: column;
    max-height: 40px;
`;

const IconUser = styled(HeaderProfile)`
    width: 40px;
    height: 40px;
    color: #000;
    
    & > * {
        width: 40px;
    height: 40px;
    
    }
`;
