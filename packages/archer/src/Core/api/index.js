import * as axios from "axios";
import config from 'Core/api/config'

const instanceAddress = axios.create({
    baseURL: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/',
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Token 1a9c056367d8a334350890c2a06e8506233720ac"
    },
});

const instance = axios.create({
    // baseURL: config.urlMode,
    credentials: "same-origin",
    headers: {
        "Content-Type": "application/json",
    },
});

const makeRequest = (
    method = "get",
    url = "/",
    data = {}, //данные для запроса
    headers = null, // заголовки
    isFile = false, // true - если отправляется файл, а не json
    allResponse = false, // получить весь ответ, а не только внутренний
    otherParamsRequest = {}, // дополнительные параметры
) => {
    const newRequest = {
        method,
        url,
        ...otherParamsRequest,
    };
    //дополнительные заголовки
    if (headers) newRequest.headers = headers;

    //если get - формируем строку из данных
    if (method.toLowerCase() === "get") {
        const get = Object.keys(data)
            .map(key => `${key}=${data[key]}`)
            .join("&");

        newRequest.url = `${url}?${get}`;
    } else {
        newRequest.data = {...data};
    }

    // если отправляется файл
    if (isFile) {
        newRequest.data = data;
        newRequest.headers = {
            "Content-Type": "multipart/form-data",
        };
    }
    return instance(newRequest)
        .then(response => {
            return allResponse ? response : response.data;
        })
        .catch(error => {
            if (error.response) {
                if (error && error.response && error.response.status === 500) {
                    console.warn("URL", newRequest);
                }
                return error.response;
            } else {
                error.response = {};
                return error.response;
            }
        });
};

export const makeRequestDaData = (
    method = 'get',
    url = '/',
    data = {}, //данные для запроса
    headers = null, // заголовки
    isFile = false, // true - если отправляется файл, а не json
    allResponse = false, // получить весь ответ, а не только внутренний
    otherParamsRequest = {},
) => {
    const newRequest = {
        method,
        url,
        ...otherParamsRequest,
    };
    //дополнительные заголовки
    if (headers) newRequest.headers = headers;

    //если get - формируем строку из данных
    if (method.toLowerCase() === "get") {
        const get = Object.keys(data)
            .map(key => `${key}=${data[key]}`)
            .join("&");

        newRequest.url = `${url}?${get}`;
    } else {
        newRequest.data = {...data};
    }

    // если отправляется файл
    if (isFile) {
        newRequest.data = data;
        newRequest.headers = {
            "Content-Type": "multipart/form-data",
        };
    }
    return instanceAddress(newRequest)
        .then(response => {
            return allResponse ? response : response.data;
        })
        .catch(error => {
            if (error.response) {
                if (error && error.response && error.response.status === 500) {
                    console.warn("URL", newRequest);
                }
                return error.response;
            } else {
                error.response = {};
                return error.response;
            }
        });
}


export const getDataApi = {
    getAddress(query) {
        return instanceAddress.post(`suggest/address`, query)
    },
    getFMS(query) {
        return instanceAddress.post(`suggest/fms_unit`, query)
    },
    getDataRegFNSById(query) {
        return instanceAddress.post('findById/fns_unit', query)
    },
}

export default makeRequest




