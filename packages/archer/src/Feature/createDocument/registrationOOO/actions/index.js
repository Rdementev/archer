// ARDM_FEATURE_REGOOO_
// ardmFeatureRegOOO

// validation
export const ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_COMMON = 'ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_COMMON'
export const ardmFeatureRegOOOChangeValidationCommon = (value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_COMMON, value})

export const ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_FDA = 'ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_FDA'
export const ardmFeatureRegOOOChangeValidationFDA = (value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_FDA, value})

export const ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_UNO = 'ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_UNO'
export const ardmFeatureRegOOOChangeValidationUNO = (value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_UNO, value})






export const ARDM_FEATURE_REGOOO_CHANGE_ACTIVE_STEP_ID = 'ARDM_FEATURE_REGOOO_CHANGE_ACTIVE_STEP_ID'
export const ardmFeatureRegOOOChangeActiveStepId = (id) => ({type: ARDM_FEATURE_REGOOO_CHANGE_ACTIVE_STEP_ID, id})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_REG = 'ARDM_FEATURE_REGOOO_SET_TYPE_REG'
export const ardmFeatureRegOOOSetTypeRegId = (value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_REG, value})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_SEND = 'ARDM_FEATURE_REGOOO_SET_TYPE_SEND'
export const ardmFeatureRegOOOSetTypeSendId = (value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_SEND, value})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUS = 'ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUS'
export const ardmFeatureRegOOOSetTypeNameRus = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUS,  value})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUSSIAN = 'ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUSSIAN'
export const ardmFeatureRegOOOSetTypeNameRussian = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUSSIAN,  value})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENG = 'ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENG'
export const ardmFeatureRegOOOSetTypeNameEng = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENG,  value})
//
export const ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENGLISH = 'ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENGLISH'
export const ardmFeatureRegOOOSetTypeNameEnglish = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENGLISH,  value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_TYPE_SIZE_CAPITAL = 'ARDM_FEATURE_REGOOO_CHANGE_TYPE_SIZE_CAPITAL'
export const ardmFeatureRegOOOChangeTypeSizecapital = ( value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_TYPE_SIZE_CAPITAL, value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_TYPE_CREATE_DATE = 'ARDM_FEATURE_REGOOO_CHANGE_TYPE_CREATE_DATE'
export const ardmFeatureRegOOOChangeTypeCreateDate = ( value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_TYPE_CREATE_DATE, value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_TYPE_RESPONSE = 'ARDM_FEATURE_REGOOO_CHANGE_TYPE_RESPONSE'
export const ardmFeatureRegOOOChangeTypeResponse = ( value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_TYPE_RESPONSE, value})
//
export const ARDM_FEATURE_REGOOO_SET_PROXY = 'ARDM_FEATURE_REGOOO_SET_PROXY'
export const ardmFeatureRegOOOSetProxy = (id) => ({type: ARDM_FEATURE_REGOOO_SET_PROXY, id})
//
export const ARDM_FEATURE_REGOOO_SET_PEACE_FORM = 'ARDM_FEATURE_REGOOO_SET_PEACE_FORM'
export const ardmFeatureRegOOOSetPeaceFormId = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_PEACE_FORM, value})
//
export const ARDM_FEATURE_REGOOO_CREATE_FOUNDER_INDIVIDUALS = 'ARDM_FEATURE_REGOOO_CREATE_FOUNDER_INDIVIDUALS'
export const ardmFeatureRegOOOCreateFounderIndividuals = ( founder) => ({type: ARDM_FEATURE_REGOOO_CREATE_FOUNDER_INDIVIDUALS, founder})
//
export const ARDM_FEATURE_REGOOO_DELETE_FOUNDER_INDIVIDUALS = 'ARDM_FEATURE_REGOOO_DELETE_FOUNDER_INDIVIDUALS'
export const ardmFeatureRegOOODeleteFounderIndividuals = ( id) => ({type: ARDM_FEATURE_REGOOO_DELETE_FOUNDER_INDIVIDUALS, id})
//
export const ARDM_FEATURE_REGOOO_CLEAR_FOUNDERS = 'ARDM_FEATURE_REGOOO_CLEAR_FOUNDERS'
export const ardmFeatureRegOOOClearFounders = () => ({type: ARDM_FEATURE_REGOOO_CLEAR_FOUNDERS})
//
export const ARDM_FEATURE_REGOOO_SET_PEACE_SUM = 'ARDM_FEATURE_REGOOO_SET_PEACE_SUM'
export const ardmFeatureRegOOOSetPeaceSum = (alias, id, value) => ({type: ARDM_FEATURE_REGOOO_SET_PEACE_SUM, alias, id, value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_INDIVIDUALS = 'ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_INDIVIDUALS'
export const ardmFeatureRegOOOChangePeaceValueIndividuals = (value, id, alias) => ({type: ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_INDIVIDUALS, value, id , alias})
//
export const ARDM_FEATURE_REGOOO_CLEAR_PEACE_VALUE = 'ARDM_FEATURE_REGOOO_CLEAR_PEACE_VALUE'
export const ardmFeatureRegOOOClearPeaceValue = () => ({type: ARDM_FEATURE_REGOOO_CLEAR_PEACE_VALUE})
//
export const ARDM_FEATURE_REGOOO_CREATE_FOUNDER_ORGANIZATIONS = 'ARDM_FEATURE_REGOOO_CREATE_FOUNDER_ORGANIZATIONS'
export const ardmFeatureRegOOOCreateFounderOrganizations = ( founder) => ({type: ARDM_FEATURE_REGOOO_CREATE_FOUNDER_ORGANIZATIONS, founder})
//
export const ARDM_FEATURE_REGOOO_DELETE_FOUNDER_ORGANIZATIONS = 'ARDM_FEATURE_REGOOO_DELETE_FOUNDER_ORGANIZATIONS'
export const ardmFeatureRegOOODeleteFounderOrganizations = ( id) => ({type: ARDM_FEATURE_REGOOO_DELETE_FOUNDER_ORGANIZATIONS, id})
//
export const ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_ORGANIZATIONS = 'ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_ORGANIZATIONS'
export const ardmFeatureRegOOOChangePeaceValueOrganizations = ( value, id, alias) => ({type: ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_ORGANIZATIONS, value, id, alias})
//
export const ARDM_FEATURE_REGOOO_SET_DIR_PERSONAL_ID = 'ARDM_FEATURE_REGOOO_SET_DIR_PERSONAL_ID'
export const ardmFeatureRegOOOSetDirPersonalId = ( id) => ({type: ARDM_FEATURE_REGOOO_SET_DIR_PERSONAL_ID, id})
//
export const ARDM_FEATURE_REGOOO_SET_DIR_POSITION = 'ARDM_FEATURE_REGOOO_SET_DIR_POSITION'
export const ardmFeatureRegOOOChangeDirPosition = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_DIR_POSITION, value})
//
export const ARDM_FEATURE_REGOOO_SET_DIR_TIME = 'ARDM_FEATURE_REGOOO_SET_DIR_TIME'
export const ardmFeatureRegOOOSetDirTime = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_DIR_TIME, value})
//
export const ARDM_FEATURE_REGOOO_SET_DIR_IS_DIR_ADDRESS = 'ARDM_FEATURE_REGOOO_SET_DIR_IS_DIR_ADDRESS'
export const ardmFeatureRegOOOSetDirIsDirAddress = ( value) => ({type: ARDM_FEATURE_REGOOO_SET_DIR_IS_DIR_ADDRESS, value})
//
export const ARDM_FEATURE_REGOOO_SET_ADDRESS_ID = 'ARDM_FEATURE_REGOOO_SET_ADDRESS_ID'
export const ardmFeatureRegOOOSetAddressId = ( id) => ({type: ARDM_FEATURE_REGOOO_SET_ADDRESS_ID, id})
//
export const ARDM_FEATURE_REGOOO_SET_USTAV_MIN_ADDRESS = 'ARDM_FEATURE_REGOOO_SET_USTAV_MIN_ADDRESS'
export const ardmFeatureRegOOOSetUstavMinAddress = (value) => ({type: ARDM_FEATURE_REGOOO_SET_USTAV_MIN_ADDRESS, value})
//
export const ARDM_FEATURE_REGOOO_SET_USTAV_CERTIFY_SOLUTIONS = 'ARDM_FEATURE_REGOOO_SET_USTAV_CERTIFY_SOLUTIONS'
export const ardmFeatureRegOOOSetUstavCertifySolutions = (value) => ({type: ARDM_FEATURE_REGOOO_SET_USTAV_CERTIFY_SOLUTIONS, value})
//
export const ARDM_FEATURE_REGOOO_SET_USTAV_OKVED_IN_USTAV = 'ARDM_FEATURE_REGOOO_SET_USTAV_OKVED_IN_USTAV'
export const ardmFeatureRegOOOSetUstavOkvedInUstav = (value) => ({type: ARDM_FEATURE_REGOOO_SET_USTAV_OKVED_IN_USTAV, value})
//
export const ARDM_FEATURE_REGOOO_SET_SYS_NALOG = 'ARDM_FEATURE_REGOOO_SET_SYS_NALOG'
export const ardmFeatureRegOOOSetSysNalog = (value) => ({type: ARDM_FEATURE_REGOOO_SET_SYS_NALOG, value})
//
export const ARDM_FEATURE_REGOOO_SET_RATE_NALOG = 'ARDM_FEATURE_REGOOO_SET_RATE_NALOG'
export const ardmFeatureRegOOOSetRateNalog = (value) => ({type: ARDM_FEATURE_REGOOO_SET_RATE_NALOG, value})
//
export const ARDM_FEATURE_REGOOO_SET_SECRETAR = 'ARDM_FEATURE_REGOOO_SET_SECRETAR'
export const ardmFeatureRegOOOSetSecretar = (value) => ({type: ARDM_FEATURE_REGOOO_SET_SECRETAR, value})
//
export const ARDM_FEATURE_REGOOO_SET_PREDSIDATEL = 'ARDM_FEATURE_REGOOO_SET_PREDSIDATEL'
export const ardmFeatureRegOOOSetPredsidatel = (value) => ({type: ARDM_FEATURE_REGOOO_SET_PREDSIDATEL, value})
//
export const ARDM_FEATURE_REGOOO_SET_SELECTED_OKVEDS = 'ARDM_FEATURE_REGOOO_SET_SELECTED_OKVEDS'
export const ardmFeatureRegOOOSetSelectedOkveds = (value) => ({type: ARDM_FEATURE_REGOOO_SET_SELECTED_OKVEDS, value})
//
export const ARDM_FEATURE_REGOOO_SET_FAVORITE_OKVED = 'ARDM_FEATURE_REGOOO_SET_FAVORITE_OKVED'
export const ardmFeatureRegOOOSetFavoriteOkved = (value) => ({type: ARDM_FEATURE_REGOOO_SET_FAVORITE_OKVED, value})
//
export const ARDM_FEATURE_REGOOO_SET_REG_ORGAN = 'ARDM_FEATURE_REGOOO_SET_REG_ORGAN'
export const ardmFeatureRegOOOSetRegOrgan = (value) => ({type: ARDM_FEATURE_REGOOO_SET_REG_ORGAN, value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_REG_ORGAN = 'ARDM_FEATURE_REGOOO_CHANGE_REG_ORGAN'
export const ardmFeatureRegOOOChangeRegOrgan = (alias, value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_REG_ORGAN, alias, value})
//
export const ARDM_FEATURE_REGOOO_CHANGE_COMPLETED = 'ARDM_FEATURE_REGOOO_CHANGE_COMPLETED'
export const ardmFeatureRegOOOChangeCompleted = (value) => ({type: ARDM_FEATURE_REGOOO_CHANGE_COMPLETED, value})


