
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {ARDM_PAGES_CREATE_ORGANIZATION_REQUEST, ardmPagesGetOrganizationRequest} from "../actions";
import {put} from "redux-saga/effects";

function* prepareRequest ( payload) {
    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetOrganizationRequest())
}

function* prepareFailure () {

}

export const createOrganization = () => {
    return createRequestRest({
        url: '/autodoc/create_organization',
        action: ARDM_PAGES_CREATE_ORGANIZATION_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}