import React, {useEffect, useRef, useState} from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import moment from 'moment'
import 'moment/locale/ru'
import {connect} from "react-redux";
import {Tooltip as T} from '@rdementev/components'
import {Resizable, ResizableBox} from 'react-resizable';


moment.locale("ru")

const epic = [
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},
    {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''}, {
        id: 1,
        title: 'first_address first_address first_addressfirst_address first_addressfirst_addressfirst_addressfirst_address',
        start_date: '31.12.2020',
        end_date: '31.11.2021'
    },
    {id: 2, title: 'second_address', start_date: '10.07.2019', end_date: '10.06.2020'},
    {id: 3, title: 'three_address', start_date: '20.10.2019', end_date: '20.09.2020'},
    {id: 3, title: 'three_address', start_date: '', end_date: ''},


]
const daysMap = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ',];
const monthMap = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];

const mydiff = (date1, date2, interval) => {
    let second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    let timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years":
            return date2.getFullYear() - date1.getFullYear();
        case "months":
            return (
                (date2.getFullYear() * 12 + date2.getMonth())
                -
                (date1.getFullYear() * 12 + date1.getMonth())
            );
        case "weeks"  :
            return Math.floor(timediff / week);
        case "days"   :
            return Math.floor(timediff / day);
        case "hours"  :
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return undefined;
    }
}

const ItemBlock = styled.div`
    height: 40px;
    
    &:last-child{
      border-bottom-color: #c1c7d0;
    }
    &:nth-child(even){
      background-color: rgb(244, 245, 247);
    }
    border-bottom: 1px solid #f0f1f4;
`;

const Target = T.attached(ItemBlock)

const ActiveAddress = (props) => {
    const {periodView, scrollOnToday, setScrollOnToday} = props

    let date = new Date(); // текущая дата
    let day = date.getDay() // текущий день
    let year = date.getFullYear(); // текущий год
    let month = date.getMonth(); // тукещий месяц
    let startDate = moment(date.setFullYear(date.getFullYear() - 1)).format('YYYY');  // год начала
    let endDate = moment(date.setFullYear(date.getFullYear() + 2)).format('YYYY'); // год окончания
    let firstDay = new Date(startDate, month, day) // дата момент начала
    let endDay = new Date(endDate, month, day) // дата момент окончания
    let nextYear = endDay.getFullYear()
    let prevYear = firstDay.getFullYear()


    const [widthContainer, setWidthContainer] = useState(null)
    const [dates, setDates] = useState([])
    const [datesDay, setDatesDay] = useState([])
    const [monthMonth, setMonthMonth] = useState([])
    const [todayPosition, setTodayPosition] = useState(null)
    const [containerWeek, setContainerWeek] = useState(false)
    const [containerPeriods, setContainerPeriods] = useState(false)
    const [widthOffer, setWidthOffer] = useState(false)
    const [press, setPress] = useState(false)
    const [startPos, setStartPos] = useState(false)
    const refToday = useRef(null)
    const refAddressList = useRef(null)
    const refTitleAddress = useRef(null)
    const refOffer = useRef(null)

    useEffect(() => {
        scrollToMyRef(widthContainer, todayPosition)
    }, [periodView, widthContainer, todayPosition, scrollOnToday])

    useEffect(() => {
        setWidthOffer(300)
    }, [])

    useEffect(() => {
        if (scrollOnToday) {
            scrollToMyRef(widthContainer, todayPosition)
        }
        setScrollOnToday(false)
    }, [scrollOnToday])

    useEffect(() => {
        if (periodView === 'week') {
            setWidthContainer(datesDay.length * 30)
        } else {
            setWidthContainer(monthMonth.length * 100)
        }
        renderMonth(periodView)
        getPeriods(widthContainer, periodView)
    }, [periodView, monthMonth, datesDay])

    useEffect(() => {
        getAllDay(prevYear, year, nextYear, month, monthMap)
    }, [])

    useEffect(() => {
        getAllMonth(firstDay, endDay, month, monthMap, prevYear, nextYear)
    }, [])

    useEffect(() => {
        getTodayPosition(periodView, datesDay, widthContainer, monthMonth, month, monthMap)
    }, [widthContainer])

    const scrollToMyRef = (widthContainer, todayPosition) => {
        const a = todayPosition / 100
        refAddressList.current.scrollTo(todayPosition - a, 0)
    }

    const getNumberOfDays = (year, month) => {
        return 40 - new Date(year, month, 40).getDate();
    }

    const getDayDetails = (args) => {
        let date = args.index - args.firstDay;
        let day = args.index % 7;
        let prevMonth = args.i - 1;
        let prevYear = args.prevYear;
        if (prevMonth < 0) {
            prevMonth = 11;
            prevYear--;
        }
        let prevMonthNumberOfDays = getNumberOfDays(prevYear, prevMonth);
        let _date = (date < 0 ? prevMonthNumberOfDays + date : date % args.numberOfDays) + 1;
        let month = date < 0 ? -1 : date >= args.numberOfDays ? 1 : 0;
        let timestamp = new Date(args.prevYear, args.i, _date).getTime();
        let dateString = moment(timestamp).format('DD.MM.YYYY')
        return {
            date: _date,
            day,
            month,
            timestamp,
            dayString: daysMap[day],
            nameMonth: args.name,
            color: args.color,
            dateString
        }
    }

    const getAllDay = (prevYear, year, nextYear, month, monthMap) => {
        const getAllPrevDays = () => {
            let name
            let monthArr = []
            for (let i = month; i <= 11; i++) {
                name = monthMap[i].concat("'", prevYear.toString().slice(2))
                let firstDay = (new Date(prevYear, i)).getDay();
                let numberOfDays = getNumberOfDays(prevYear, i);
                let monthArray = [];
                let rows = 6;
                let currentDay = null;
                let index = 0;
                let cols = 7;
                let color = i % 2 === 0
                for (let row = 0; row < rows; row++) {
                    for (let col = 0; col < cols; col++) {
                        currentDay = getDayDetails({
                            index,
                            numberOfDays,
                            firstDay,
                            prevYear,
                            i,
                            name,
                            color,
                        });
                        monthArray.push(currentDay);
                        index++;
                    }
                }
                const actualDay = monthArray.filter(item => item.month === 0)
                monthArr = [...monthArr, ...actualDay]
            }

            const obj = monthArr.find(item => item.dayString === 'ПН')
            const result = monthArr.filter(item => item.timestamp >= obj.timestamp)
            return result

        }
        const getAllNowDays = (prevYear) => {
            let name
            let monthArr = []
            for (let i = 0; i <= 11; i++) {
                name = monthMap[i]
                let firstDay = (new Date(prevYear, i)).getDay();
                let numberOfDays = getNumberOfDays(prevYear, i);
                let monthArray = [];
                let rows = 6;
                let currentDay = null;
                let index = 0;
                let cols = 7;
                let color = i % 2 === 0
                for (let row = 0; row < rows; row++) {
                    for (let col = 0; col < cols; col++) {
                        currentDay = getDayDetails({
                            index,
                            numberOfDays,
                            firstDay,
                            prevYear,
                            i,
                            name,
                            color,
                        });
                        monthArray.push(currentDay);
                        index++;
                    }
                }
                const actualDay = monthArray.filter(item => item.month === 0)
                monthArr = [...monthArr, ...actualDay]
            }
            return monthArr
        }
        const getAllNextDays = (prevYear) => {
            let name
            let monthArr = []
            for (let i = 0; i <= month; i++) {
                name = monthMap[i].concat("'", nextYear.toString().slice(2))
                let firstDay = (new Date(prevYear, i)).getDay();
                let numberOfDays = getNumberOfDays(prevYear, i);
                let monthArray = [];
                let rows = 6;
                let currentDay = null;
                let index = 0;
                let cols = 7;
                let color = i % 2 === 0
                for (let row = 0; row < rows; row++) {
                    for (let col = 0; col < cols; col++) {
                        currentDay = getDayDetails({
                            index,
                            numberOfDays,
                            firstDay,
                            prevYear,
                            i,
                            name,
                            color,
                        });
                        monthArray.push(currentDay);
                        index++;
                    }
                }
                const actualDay = monthArray.filter(item => item.month === 0)
                monthArr = [...monthArr, ...actualDay]
            }
            return monthArr
        }

        const allPrevDays = getAllPrevDays()
        const allNowDays = getAllNowDays(year)
        const allNextDays = getAllNextDays(nextYear)
        const allDays = allPrevDays.concat(allNowDays, allNextDays)

        let dayWidth = parseFloat((100 / allDays.length).toFixed(4))
        let rightDay = 100 - dayWidth
        let leftDay = 0
        for (let i = 0; i <= allDays.length - 1; i++) {
            if (rightDay <= 0) rightDay = 0
            allDays[i] = {...allDays[i], position: {left: leftDay, right: rightDay}}
            leftDay = leftDay + dayWidth
            rightDay = 100 - dayWidth - leftDay
            if (rightDay === 0) break
        }
        let size = 7;
        let subarray = [];
        for (let i = 0; i < Math.ceil(allDays.length / size); i++) {
            subarray[i] = allDays.slice((i * size), (i * size) + size);
        }
        let width = parseFloat((100 / subarray.length).toFixed(4))
        let right = 100 - width
        let left = 0

        for (let i = 0; i <= subarray.length - 1; i++) {
            if (right <= 0) right = 0
            subarray[i] = {childs: subarray[i], position: {left: left, right: right}}
            left = left + width
            right = 100 - width - left
            if (right === 0) break
        }
        setDatesDay(allDays)
        return setDates(subarray);
    }

    const getAllMonth = (firstDay, endDay, month, monthMap, prevYear, nextYear) => {
        const a = mydiff(firstDay, endDay, 'months')
        let monthArr = []
        let defaultMonth = month
        let limit = 11
        const count = Math.floor(a / limit)
        for (let j = 0; j <= count; j++) {
            if (j === count) limit = month
            if (j !== 0) defaultMonth = 0
            let name
            for (let i = defaultMonth; i <= limit; i++) {
                if (j === 0) name = monthMap[i].concat("'", prevYear.toString().slice(2))
                if (j !== 0 && j < count) name = monthMap[i]
                if (j === count) name = monthMap[i].concat("'", nextYear.toString().slice(2))
                let month = {id: i, name: name}
                monthArr = [...monthArr, month]
            }
        }
        let monthWidth = parseFloat((100 / monthArr.length).toFixed(4))

        let rightMonth = 100 - monthWidth
        let leftMoth = 0
        monthArr.map((item, i) => {
            monthArr[i] = {...monthArr[i], position: {left: leftMoth, right: rightMonth}}
            leftMoth = leftMoth + monthWidth
            rightMonth = 100 - monthWidth - leftMoth
        })
        setMonthMonth(monthArr)
    }

    const renderMonth = (periodView) => {
        if (periodView === 'week') {
            const getNameMonth = (item) => {
                const count = item.childs.length
                if (item.childs[0].nameMonth === item.childs[count - 1].nameMonth) {
                    return item.childs[0].nameMonth
                }
                const prevMonth = item.childs.filter(elem => elem.nameMonth === item.childs[0].nameMonth)
                const nextMonth = item.childs.filter(elem => elem.nameMonth === item.childs[count - 1].nameMonth)
                if (prevMonth.length > nextMonth.length) {
                    return item.childs[0].nameMonth
                }
                return item.childs[count - 1].nameMonth
            }

            const contentWeek = (
                <ContainerWeekStyled>
                    {dates.map(item => {
                        const month = getNameMonth(item)
                        let leftWeek = item.childs[0].position.left
                        let rightWeek = item.childs[item.childs.length - 1].position.right
                        return (
                            <BlockWeek left={leftWeek} right={rightWeek}>
                                <NameWeek>
                                    {month}
                                </NameWeek>
                                <BlockDateDay>
                                    {item.childs.map(elem => {
                                        const active = elem.dateString === moment(new Date()).format('DD.MM.YYYY')

                                        return (
                                            <OneDay color={elem.color}
                                                    active={active}
                                                    left={elem.position.left}
                                                    right={elem.position.right}>
                                                <OneDayDate>{elem.date}</OneDayDate>
                                                <div>{elem.dayString}</div>
                                            </OneDay>

                                        )
                                    })}
                                </BlockDateDay>
                            </BlockWeek>
                        )
                    })}
                </ContainerWeekStyled>
            )
            setContainerWeek(contentWeek)
        } else if (periodView === 'month') {
            const contentMonth = (
                <ContainerMonthStyled>
                    {monthMonth.map(item => {
                        return (
                            <BlockMonth left={item.position.left}
                                        right={item.position.right}>
                                <NameMonth>
                                    {item.name}
                                </NameMonth>
                            </BlockMonth>
                        )
                    })}
                </ContainerMonthStyled>
            )
            setContainerWeek(contentMonth)
        }
    }

    const getPeriods = (widthContainer, periodView) => {
        let content
        if (periodView === 'week') {
            content = (
                <ContainerPeriodsWeekStyled>
                    {dates.map((item, i) => {
                        let leftWeek = item.childs[0].position.left
                        let rightWeek = item.childs[item.childs.length - 1].position.right
                        return (
                            <Period left={leftWeek} right={rightWeek}/>
                        )
                    })}
                </ContainerPeriodsWeekStyled>
            )
        } else {
            content = (
                <ContainerPeriodsMonthStyled>
                    {monthMonth.map((item, i) => {
                        return (
                            <Period left={item.position.left}
                                    right={item.position.right}/>
                        )
                    })}
                </ContainerPeriodsMonthStyled>
            )
        }
        setContainerPeriods(content)
    }

    const getEpicTitle = () => {
        return epic.map(item => {
            const getTooltipText = (item) => {
                if (!item.start_date && !item.end_date) {
                    return <T.Text>Адрес свободен</T.Text>
                }
                return <T.Text>{item.start_date} - {item.end_date}</T.Text>
            }

            return (
                <T.Tooltip dir={'top'}>
                    <Target>
                        <Item>
                            <ItemText>
                                {item.title}
                            </ItemText>
                        </Item>
                    </Target>
                    {getTooltipText(item)}
                </T.Tooltip>
            )
        })
    }
    const getEpicDate = () => {
        return epic.map(item => {
            const startDay = datesDay.find(elem => {
                if (item.start_date === elem.dateString) {
                    return elem
                }
            })
            const endDay = datesDay.find(elem => {
                if (item.end_date === elem.dateString) {
                    return elem
                }
            })

            if (startDay && endDay) {
                return (
                    <AddressesItem>
                        <AddressesItemContainer left={startDay.position.left}
                                                end={true}
                                                start={true}
                                                right={endDay.position.right}/>
                    </AddressesItem>
                )
            } else if (!startDay && endDay) {
                return (
                    <AddressesItem>
                        <AddressesItemContainer left={0}
                                                end={true}
                                                start={false}
                                                right={endDay.position.right}/>
                    </AddressesItem>
                )
            } else if (startDay && !endDay) {
                return (
                    <AddressesItem>
                        <AddressesItemContainer left={startDay.position.left}
                                                end={false}
                                                start={true}
                                                right={0}/>
                    </AddressesItem>
                )
            }
            return (
                <AddressesItem>
                    <AddressesItemContainer opacity={'0'}/>
                </AddressesItem>
            )
        })
    }

    const getTodayPosition = (periodView, datesDay, widthContainer, monthMonth, month, monthMap) => {
        const today = new Date()
        const dateString = moment(today).format('DD.MM.YYYY')
        let b
        if (periodView === 'week') {
            const todayDays = datesDay.find(item => item.dateString === dateString)
            const a = todayDays && todayDays.position.left
            b = widthContainer / 100 * a + 15
        } else {
            const todayDays = monthMonth.find(item => item.name === monthMap[month])
            const a = todayDays && todayDays.position.left
            const day = dateString.slice(0, 2)
            let localYear = today.getFullYear(); // текущий год
            let localMonth = today.getMonth(); // тукещий месяц
            const countDays = getNumberOfDays(localYear, localMonth, 40)
            const positionDay = 100 / countDays * +day
            b = widthContainer / 100 * a + positionDay
        }
        setTodayPosition(b)
    }

    const getToday = (todayPosition) => {
        return (
            <Today left={todayPosition} ref={refToday}>
                <TodayItem/>
                <InfoToday>
                    Сегодня: {moment(new Date()).format('DD MMM YYYYг')}
                </InfoToday>
            </Today>
        )
    }

    const handleScrollAddress = (e) => {
        const a = e.target.scrollTop
        refTitleAddress.current.scrollTo(0, a)
    }


    return (
        <Container>
            <Container1>
                <ResizableBox width={300} height={'100%'} axis='x'
                              minConstraints={[300, 300]} maxConstraints={[900, 900]}>
                    <Offer ref={refOffer}>
                        <OfferHeader>
                            Услуга | Адрес
                        </OfferHeader>
                        <OfferBody ref={refTitleAddress}>
                            {getEpicTitle()}
                        </OfferBody>
                    </Offer>
                </ResizableBox>

                <BodyRows>
                    <BodyAddress>
                        <Address ref={refAddressList}>
                            <List widthContainer={widthContainer}>
                                <BodyHeaderContainer1 widthContainer={widthContainer}>
                                    {containerWeek}
                                </BodyHeaderContainer1>
                                <Periods widthContainer={widthContainer}>
                                    {containerPeriods}
                                </Periods>
                                <AddressesBlock onScroll={(e) => {
                                    handleScrollAddress(e)
                                }}>
                                    {getEpicDate()}
                                </AddressesBlock>
                                <TodayContainer>
                                    {todayPosition && getToday(todayPosition)}
                                </TodayContainer>
                            </List>
                        </Address>
                    </BodyAddress>
                </BodyRows>
            </Container1>
        </Container>
    )
}

export default compose(connect(null, {}))(ActiveAddress)
//
const BlockWeek = styled.div`
    display: flex;
    flex-direction: column;
    border-right:  1px solid #eeeff3;
    height: 50px;
    position: absolute;
    left: ${({left}) => left}%;
    right: ${({right}) => right}%;
`;
const ContainerWeekStyled = styled.div`
    
`;
const ContainerMonthStyled = styled.div`
    
`;
const ContainerPeriodsWeekStyled = styled.div`
    
`;
const ContainerPeriodsMonthStyled = styled.div`
    
`;
const BlockMonth = styled.div`
    display: flex;
    flex-direction: column;
    border-right:  1px solid #eeeff3;
    height: 50px;
    position: absolute;
    left: ${({left}) => left}%;
    right: ${({right}) => right}%;
`;
const BlockDateDay = styled.div`
    display: flex;
    height: 100%;
`;
const NameWeek = styled.div`
    text-align: center;
    font-size: 10px;
    font-weight: 600;
`;
const NameMonth = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    margin: 0 auto;
`;
const OneDayDate = styled.div`
    text-align: center;
`;
const OneDay = styled.div`
    text-align: center;
    font-size: 8px;
    font-weight: 600;
    width: 100%;
    color: ${({color, active}) => active ? 'rgb(38, 132, 255)' : color ? '#000' : '#737373'};
    &:not(:last-child){
      border-right: 1px solid #edeef3;
    }
`;

const Container = styled.div`
    position: relative;
    flex: 1 1 auto;
    height: 100%;
`;
const Container1 = styled.div`
    display: flex;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    
    .react-resizable {
      position: relative;
    }
    .react-resizable-handle {
      position: absolute;
      bottom: 0;
      right: 0;
      top: 0;
      min-width: 2px;
      max-width: 2px;
      width: 100%;
      height: 100%;
      cursor: col-resize;
      display: block;
      background-color: #c1c7d0;
      z-index: 9;
      transition: ease 0.4s;
    
    }
    .react-resizable-handle:hover {
      background-color: #1d3375;
      cursor: col-resize;
    }
    .react-resizable-handle:active {
      background-color: #1d3375;
      cursor: col-resize;
    }
`;
const BodyHeaderContainer1 = styled.div`
    display: flex;
    height: 50px;
    background-color: #f4f5f7;
    width: ${({widthContainer}) => widthContainer}px;
    .body-rows__table-appear {
      opacity: 0;
    }
    .body-rows__table-enter {
      opacity: 0;
    }
    .body-rows__table-enter-done{
       opacity: 1;
       transition: opacity ease 1s;
    }
`;
const Offer = styled.div`
    position: relative;
    height: 100%;
    background-color: #fff;
    overflow: hidden; 
    width: 100%;
    display: flex;
    flex-direction: column;   
`;

const OfferHeader = styled.div`
    height: 50px;
    background-color: rgb(244, 245, 247);
    display: flex;
    align-items: center;
    flex: 0 0 auto;
    padding: 10px;
`;
const OfferBody = styled.div`
    position: relative;
    flex-direction: column;
    display: flex;
    overflow: hidden;
    min-width: 300px;

::-webkit-scrollbar {
    width: 0;
}
/* Handle */
::-webkit-scrollbar-thumb {
  border-radius: 3px;
  background: transparent;
}

::-webkit-scrollbar-thumb:hover{
  background: transparent
}
`;

const Item = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    padding: 0 10px ;
    width: 100%;
    max-height: 40px;
    
`;
const ItemText = styled.div`
   overflow: hidden;
    width: 100%;
    white-space: nowrap;
    text-overflow: ellipsis;
`;

const BodyRows = styled.div`
    position: relative;
    height: 100%;
    width: 100%;
    min-height: 0;
    overflow: hidden;
    
    
`;
const List = styled.div`
    position: relative;
    height: 100%;
    background: #fff;
    width: ${({widthContainer}) => widthContainer}px;
`;
const Periods = styled.div`
    position: absolute;
    display: flex;
    height: 100%;
    top: 50px;
    z-index: 1;
    pointer-events: none;
    width: ${({widthContainer}) => widthContainer}px;
`;
const Period = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    box-sizing: border-box;
    flex: 0 0 auto;
    border-right: 1px solid #eeeff3;
    left: ${({left}) => left}%;
    right: ${({right}) => right}%
`;
const BodyAddress = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    overflow: hidden;
`;

const Address = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    overflow-x: auto;
    overflow-y: hidden;
    scroll-behavior: smooth
`;
const AddressesBlock = styled.div`
   height: 100%;
    overflow: scroll;
`;
const TodayContainer = styled.div`
   
`;
const InfoToday = styled.div`
    position: absolute;
    text-transform: capitalize;
    padding: 8px 20px;
    font-size: 10px;
    font-weight: 600;
    background-color: #2c2c2c;
    transition: ease 0.4s;
    opacity: 0;
    border-radius: 4px;
    color: #fff;
    top: 10px;
    left: 10px;
    z-index: 2;
    white-space: nowrap;
`;
const Today = styled.div`
    height: 100%;
    position: absolute;
    top: 50px;
    z-index: 2;
    left: ${({left}) => left}px;
`;

const TodayItem = styled.div`
   height: 100%;
   width: 2px;
   background-color: rgb(255, 153, 31);
    &:hover ~ ${InfoToday}{
      opacity: 1;
    }
   &::before {
    content: "";
    position: absolute;
    height: 0;
    width: 0;
    top: 0;
    left: -3px;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-top: 8px solid rgb(255, 153, 31);
   }

`;
const AddressesItem = styled.div`
    position: relative;
    height: 40px;
    border-bottom: 1px solid #ECEEF3;
    &:nth-child(even){
      background-color: rgb(244, 245, 247);
    }
    &:last-child{
        &::before{
            content: '';
            display: block;
            background-color: #c1c7d0;
            height: 2px;
            z-index: 1;
            width: 100%;
            position: absolute;
            top: 100%;
            left: 0;
         }
    }
`;
const AddressesItemContainer = styled.div`
    position: absolute;
    cursor: pointer;
    z-index: 1;
    height: 26px;
    background-color: ${({end, start}) => end && start ? '#0ABAB5' : start ? 'transparent' : '#c4c3d0'};
    border: 1px solid ${({start, end}) => start && !end ? '#0ABAB5' : 'transparent'};
    border-right: ${({end}) => !end ? 'transparent' : ''};
    border-radius: ${({start, end}) => !start ? '0 4px 4px 0' : !end ? '4px 0 0 4px' : '4px'} ;
    top: 50%;
    transform: translate(0, -50%);
    opacity: ${({opacity}) => opacity};
    left: ${({left}) => left}%;
    right: ${({right}) => right}%
`;



