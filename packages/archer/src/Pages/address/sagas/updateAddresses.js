
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {ARDM_PAGES_UPDATE_ADDRESSES_REQUEST, ardmPagesGetAddressesRequest} from "../actions";
import {put} from "redux-saga/effects";

function* prepareRequest ( payload) {
    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetAddressesRequest())
}

function* prepareFailure () {

}

export const updateAddresses = () => {
    return createRequestRest({
        url: '/autodoc/update_address',
        action: ARDM_PAGES_UPDATE_ADDRESSES_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}