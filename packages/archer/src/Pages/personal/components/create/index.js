import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import 'antd/dist/antd.css';
import {
    changePersonalData,
    changeSexType,
    updatePersonalData,
} from "../../actions";
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import locale from "antd/es/date-picker/locale/ru_RU";
import {DatePicker} from "antd";
import PassportCode from "../passportCodeDadata";
import NewModal from "FeatureARDM/modal/newmodal";
import ChoiseAddress from "PagesARDM/address";
import {setAddressForPersonal, setEditPersonalId} from "../../actions";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import {ardmPagesCreatePersonalRequest} from "../../actions";
import UpdateAddress from "Pages/address/components/update";
import {ArdmPagesPersonalGetPersonalReselect} from "Pages/personal/reselect";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import RequiredField from "ComponentsARJTTD/requiredField";
import {validationRus, validationRequired, validationInteger, validationEmail} from "UtilsARJTTD/validation";
import Promto from "ComponentsARJTTD/promto";
import moment from 'moment'

const CreatePersonal = (props) => {
    const {show, setShow, setShowCreate, address, ardmPagesCreatePersonalRequest} = props
    const [showAddressModal, setShowModalAddress] = useState(false)
    const [last_name, setlast_name] = useState('')
    const [last_nameIsValidation, setlast_nameIsValidation] = useState(true)
    const [first_name, setfirst_name] = useState('')
    const [first_nameIsValidation, setfirst_nameIsValidation] = useState(true)
    const [second_name, setsecond_name] = useState('')
    const [birth_place, setbirth_place] = useState('')
    const [birth_placeIsValidation, setbirth_placeIsValidation] = useState(true)
    const [birthdayMoment, setbirthdayMoment] = useState('')
    const [birthdayMomentIsValidation, setbirthdayMomentIsValidation] = useState(true)
    const [sex, setsex] = useState('Мужской')
    const [sexIsValidation, setsexIsValidation] = useState(true)
    const [inn, setinn] = useState('')
    const [innIsValidation, setinnIsValidation] = useState(false)
    const [email, setemail] = useState('')
    const [emailIsValidation, setemailIsValidation] = useState(true)
    const [phone_number, setphone_number] = useState('')
    const [phone_numberIsValidation, setphone_numberIsValidation] = useState(true)
    const [passport_s_number, setpassport_s_number] = useState('')
    const [passport_s_numberIsValidation, setpassport_s_numberIsValidation] = useState(false)
    const [passport_number, setpassport_number] = useState('')
    const [passport_numberIsValidation, setpassport_numberIsValidation] = useState(false)
    const [passport_given_dateMoment, setpassport_given_dateMoment] = useState('')
    const [passport_given_dateMomentIsValidation, setpassport_given_dateMomentIsValidation] = useState(true)
    const [passport_given_by_code, setpassport_given_by_code] = useState('')
    const [passport_given_by_codeIsTouch, setpassport_given_by_codeIsTouch] = useState(false)
    const [passport_given_by, setpassport_given_by] = useState('')
    const [passport_given_byIsTouch, setpassport_given_byIsTouch] = useState(false)
    const [addressId, setAddressId] = useState(null)
    const [addressString, setAddressString] = useState('')
    const [showUpdateAddress, setShowUpdateAddress] = useState('')
    const [comminValidation, setCommonValidation] = useState(false)

    useEffect(() => {
            if( !last_name ||
                !last_nameIsValidation ||
                !first_name ||
                !first_nameIsValidation ||
                !birth_place ||
                !birth_placeIsValidation ||
                !birthdayMoment ||
                !birthdayMomentIsValidation ||
                !sex ||
                !sexIsValidation ||
                !inn ||
                innIsValidation ||
                !email ||
                !emailIsValidation ||
                !phone_number ||
                !phone_numberIsValidation ||
                !passport_s_number ||
                passport_s_numberIsValidation ||
                !passport_number ||
                passport_numberIsValidation ||
                !passport_given_dateMoment ||
                !passport_given_dateMomentIsValidation ||
                !passport_given_by_code ||
                !passport_given_by ||
                !addressId){
                return setCommonValidation(false)
            }
        setCommonValidation(true)
    },[last_name,
        last_nameIsValidation,
        first_name,
        first_nameIsValidation,
        birth_place,
        birth_placeIsValidation,
        birthdayMoment,
        birthdayMomentIsValidation,
        sex,
        sexIsValidation,
        inn,
        innIsValidation,
        email,
        emailIsValidation,
        phone_number,
        phone_numberIsValidation,
        passport_s_number,
        passport_s_numberIsValidation,
        passport_number,
        passport_numberIsValidation,
        passport_given_dateMoment,
        passport_given_dateMomentIsValidation,
        passport_given_by_code,
        passport_given_by,
        addressId])

    useEffect(() => {
        getSearchAddress()
    }, [addressId])

    useEffect(() => {
        validationRequired(sex, setsexIsValidation)
    },[sex])

    // ищем адрес пользователя одной строкой
    const getSearchAddress = () => {
        address.map(item => {
            if (item.id === addressId) {
                const region_with_type = item.region && item.region.concat(' ', item.region_type_full)
                const city_with_type = item.city_type_full && item.city_type_full.concat(' ', item.city)
                const settlement_with_type = item.settlement_type_full && item.settlement_type_full.concat(' ', item.settlement)
                const street_with_type = item.street_type_full.concat(' ', item.street)
                const addressOneString = item.region === item.city
                    ? city_with_type.concat(' ',
                        settlement_with_type === null ? '' : settlement_with_type, ' ',
                        street_with_type === null ? '' : street_with_type, ' ',
                        item.house_type_full === null ? '' : item.house_type_full, ' ',
                        item.house === null ? '' : item.house, ' ',
                        item.block_type_full === null ? '' : item.block_type_full, ' ',
                        item.block === null ? '' : item.block, ' ',
                        item.flat_type_full === null ? '' : item.flat_type_full, ' ',
                        item.flat === null ? '' : item.flat)

                    : region_with_type.concat(' ',
                        city_with_type === null ? '' : city_with_type, ' ',
                        settlement_with_type === null ? '' : settlement_with_type, ' ',
                        street_with_type === null ? '' : street_with_type, ' ',
                        item.house_type_full === null ? '' : item.house_type_full, ' ',
                        item.house === null ? '' : item.house, ' ',
                        item.block_type_full === null ? '' : item.block_type_full, ' ',
                        item.block === null ? '' : item.block, ' ',
                        item.flat_type_full === null ? '' : item.flat_type_full, ' ',
                        item.flat === null ? '' : item.flat)
                return setAddressString(addressOneString)
            }
        })
    }

    const handleClickOpenAddress = () => {
        setShowModalAddress(true)
    }
    const handleChangeCheckbox = (alias, value) => {
        if (value) return setsex(alias)
        setsex('')

    }
    const handleChangeDate = (moment, date) => {
        setbirthdayMoment(moment)
    }
    const handleChangeDateGiven = (moment, date) => {
        setpassport_given_dateMoment(moment)
    }

    const handleClickSave = () => {
        const data = {
            last_name: last_name,
            first_name: first_name,
            second_name: second_name,
            birth_place: birth_place,
            birthday: moment(birthdayMoment).format('YYYY-MM-DD'),
            sex: sex,
            inn: inn,
            email: email,
            phone_number: phone_number,
            passport_s_number: passport_s_number,
            passport_number: passport_number,
            passport_given_date: moment(passport_given_dateMoment).format('YYYY-MM-DD'),
            passport_given_by_code: passport_given_by_code,
            passport_given_by: passport_given_by,
            addressId: addressId,
        }
        ardmPagesCreatePersonalRequest(data)
        setShowCreate(false)
    }
    const handleCloseModal = () => {
        setShowCreate(false)
    }

    // клик на адрес из списка
    const handleClickAddress = (id) => {
        setAddressId(id)
        setShowModalAddress(false)
    }

    const handleTouch = () => {
        setpassport_given_by_codeIsTouch(true)
    }
    return (
        <NewModal showModal={show} setShow={setShow}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Новый пользователь</ModalTitle>
                    <BlockIcon onClick={() => {handleCloseModal()}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <TitleGroup>Личные данные</TitleGroup>
                <Group>
                    <BlockFIO style={{width: '100%'}}>
                        <BlockFieldStyled>
                            <BlockField>
                                <TitleField>Фамилия</TitleField>
                                <RequiredField/>
                            </BlockField>
                            <BlockInput>
                                <Input value={last_name}
                                       placeholder={"Введите фамилию"}
                                       onBlur={()=>{validationRus(last_name, setlast_nameIsValidation)}}
                                       onChange={(e) => {setlast_name(e.target.value)}}/>
                            </BlockInput>
                            {!last_nameIsValidation && <Promto text={'Допустимы только русские символы'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <BlockField>
                                <TitleField>Имя</TitleField>
                                <RequiredField/>
                            </BlockField>
                            <BlockInput>
                                <Input value={first_name}
                                       placeholder={"Введите имя"}
                                       onBlur={(e)=>{validationRus(first_name,setfirst_nameIsValidation)}}
                                       onChange={(e) => {setfirst_name(e.target.value,)}}/>
                            </BlockInput>
                            {!first_nameIsValidation && <Promto text={'Допустимы только русские символы'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <BlockField>
                                <TitleField>Отчество</TitleField>
                            </BlockField>
                            <BlockInput>
                                <Input value={second_name}
                                       placeholder={"Введите отчество"}
                                       onChange={(e) => {
                                    setsecond_name(e.target.value)
                                }}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </BlockFIO>
                    <BlockInfo style={{width: '100%'}}>
                        <div>
                            <BlockFieldStyled>
                                <BlockField>
                                    <TitleField>Место Рождения</TitleField>
                                    <RequiredField/>
                                </BlockField>
                                <BlockInput>
                                    <Input value={birth_place}
                                           placeholder={"Введите место рождения"}
                                           onBlur={()=>{validationRequired(birth_place, setbirth_placeIsValidation)}}
                                           onChange={(e) => {setbirth_place(e.target.value)}}/>
                                </BlockInput>
                                {!birth_placeIsValidation && <Promto text={'Введите место рождения'}/>}
                            </BlockFieldStyled>
                            <BlockFieldStyled>
                                <BlockField>
                                    <TitleField>Дата рождения</TitleField>
                                    <RequiredField/>
                                </BlockField>
                                <DatePickerStyle
                                    locale={locale}
                                    value={birthdayMoment}
                                    onBlur={()=>{validationRequired(birthdayMoment, setbirthdayMomentIsValidation)}}
                                    placeholder={'Выберите дату'}
                                    onChange={handleChangeDate}
                                    style={{width: '100%'}}
                                    format="DD.MM.YYYY"/>
                                {!birthdayMomentIsValidation && <Promto text={'Введите дату рождения'}/>}
                            </BlockFieldStyled>
                        </div>
                        <BlockSexandINN>
                            <BlockSex style={{width: '100%'}}>
                                <BlockField>
                                    <TitleField>Пол</TitleField>
                                    <RequiredField/>
                                </BlockField>
                                <div style={{marginBottom: '5px'}}>
                                    <Checkbox checked={sex === 'Женский'} onChange={(e) => {
                                        handleChangeCheckbox('Женский', e.target.checked)
                                    }} rightText={'Женский'}/>
                                </div>
                                <div style={{marginBottom: '5px'}}>
                                    <Checkbox checked={sex === 'Мужской'} onChange={(e) => {
                                        handleChangeCheckbox('Мужской', e.target.checked)
                                    }} rightText={'Мужской'}/>
                                </div>
                                {!sexIsValidation && <Promto text={'Укажите пол'}/>}
                            </BlockSex>
                            <div style={{width: '100%'}}>
                                <BlockFieldStyled>
                                    <BlockField>
                                        <TitleField>ИНН</TitleField>
                                        <RequiredField/>
                                    </BlockField>
                                    <BlockInput>
                                        <Input value={inn}
                                               placeholder={"Введите ИНН"}
                                               onBlur={()=>{validationInteger(inn, setinnIsValidation, 12 , 12)}}
                                               onChange={(e) => {setinn(e.target.value)}}/>
                                    </BlockInput>
                                    {innIsValidation && <Promto text={innIsValidation}/>}
                                </BlockFieldStyled>
                            </div>
                        </BlockSexandINN>
                    </BlockInfo>
                </Group>
                <TitleGroup>Контакты</TitleGroup>
                <Group>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Email</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <BlockInput>
                            <Input value={email}
                                   placeholder={"Введите email"}
                                   onBlur={()=>{validationEmail(email, setemailIsValidation)}}
                                   onChange={(e) => {setemail(e.target.value)}}/>
                        </BlockInput>
                        {!emailIsValidation && <Promto text={'Введите корректный Email'}/>}
                    </BlockFieldStyled>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Телефон</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <BlockInput>
                            <Input value={phone_number}
                                   placeholder={"Введите телефон"}
                                   onChange={(e) => {
                                setphone_number(e.target.value)
                            }}/>
                        </BlockInput>
                    </BlockFieldStyled>
                </Group>
                <TitleGroup>Паспортные данные</TitleGroup>
                <Group>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Серия</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <BlockInput>
                            <Input value={passport_s_number}
                                   placeholder={"Введите серию паспорта"}
                                   onBlur={()=>{validationInteger(passport_s_number, setpassport_s_numberIsValidation, 4 , 4)}}
                                   onChange={(e) => {setpassport_s_number(e.target.value)}}/>
                        </BlockInput>
                        {passport_s_numberIsValidation && <Promto text={passport_s_numberIsValidation}/>}
                    </BlockFieldStyled>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Номер</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <BlockInput>
                            <Input value={passport_number}
                                   placeholder={"Введите номер паспорта"}
                                   onBlur={()=>{validationInteger(passport_number, setpassport_numberIsValidation, 6 , 6)}}
                                   onChange={(e) => {setpassport_number(e.target.value)}}/>
                        </BlockInput>
                        {passport_numberIsValidation && <Promto text={passport_numberIsValidation}/>}
                    </BlockFieldStyled>
                    <PassportGiveDate >
                        <BlockField>
                            <TitleField>Дата выдачи</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <DatePickerStyle
                            locale={locale}
                            value={passport_given_dateMoment}
                            placeholder={'Выберите дату'}
                            onBlur={()=>{validationRequired(passport_given_dateMoment, setpassport_given_dateMomentIsValidation)}}
                            onChange={handleChangeDateGiven}
                            style={{width: '100%'}}
                            format="DD.MM.YYYY"/>
                        {!passport_given_dateMomentIsValidation && <Promto text={'Введите дату выдачи паспорта'}/>}
                    </PassportGiveDate>
                </Group>
                <Grid>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Код подразделения</TitleField>
                            <RequiredField/></BlockField>
                        <BlockInput>
                            <PassportCode placeholder={"Введите код подразделения"}
                                          setpassport_given_by_code={setpassport_given_by_code}
                                          setpassport_given_by={setpassport_given_by}
                                          handleTouch={handleTouch}
                                          passport_given_by_code={passport_given_by_code}
                                          passport_given_by={passport_given_by}/>
                        </BlockInput>
                        {!passport_given_by_code && passport_given_by_codeIsTouch && <Promto text={'Введите код подразделения'}/>}
                    </BlockFieldStyled>
                    <BlockFieldStyled style={{width: '100%', padding: '0 20px'}}>
                        <BlockField>
                            <TitleField>Кем выдан</TitleField>
                            <RequiredField/>
                        </BlockField>
                        <BlockInput>
                            <Input value={passport_given_by}
                                   placeholder={"Введите отделение"}
                                   onFocus={()=>{setpassport_given_byIsTouch(true)}}
                                   onChange={(e) => {setpassport_given_by(e.target.value)}}/>
                        </BlockInput>
                        {!passport_given_by && passport_given_byIsTouch && <Promto text={'Введите отделение'}/>}
                    </BlockFieldStyled>
                </Grid>
                <TitleGroup>Адрес</TitleGroup>
                <BlockInfo style={{width: '100%'}}>
                    {addressString ? (
                        <Address>
                            <AddressText onClick={() => {setShowUpdateAddress(true)}}>
                                {addressString}
                            </AddressText>
                            <ButtonBlock>
                                <Button onClick={() => {handleClickOpenAddress()}}>
                                    Изменить
                                </Button>
                            </ButtonBlock>
                        </Address>
                    ) : (<ButtonBlock>
                        <WhiteButton onClick={() => {
                            handleClickOpenAddress()
                        }}>Выбрать</WhiteButton>
                    </ButtonBlock>)}
                </BlockInfo>
                <Footer>
                    <ButtonBlock>
                        <WhiteButton onClick={() => {handleClickSave()}} disabled={!comminValidation}>
                            Создать
                        </WhiteButton>
                    </ButtonBlock>
                </Footer>
            </ContainerModal>
            <ChoiseAddress
                onClickAddress={handleClickAddress}
                show={showAddressModal}
                setShow={setShowModalAddress}/>
            {showUpdateAddress && <UpdateAddress show={showUpdateAddress}
                                                 setShow={setShowUpdateAddress}
                                                 editAddressId={addressId}/>}
        </NewModal>
    )
}


const mapStateToProps = (state) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        address: getArdmPagesAddressReselect(state),
    }
}

export default connect(mapStateToProps, {
    changePersonalData, changeSexType, setAddressForPersonal,
    updatePersonalData, setEditPersonalId,


    ardmPagesCreatePersonalRequest,

})(CreatePersonal)

const ContainerModal = styled.div`
    padding: 20px;
    min-width: 800px;
    max-height: 800px;
    height: 100%;
    display: grid;
    overflow-y: auto ;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: 0 14px 34px rgba(0,0,0,0.4)
`;
const ModalHeader = styled.div`
    display: flex;
        min-height: 50px;
    justify-content: space-between;
`;
const BlockIcon = styled.div`
  cursor: pointer;
  width: 14px;
  height: 14px;
`;
const BlockInput = styled.div`
  height: 32px;
`;

const ModalTitle = styled.h4`
  
`;
const PassportGiveDate = styled.h4`
  position: relative;
    max-height: 48px;
    padding: 0 20px;
    width: 100%;
`;
const BlockField = styled.div`
  display: flex;
  margin-bottom: 5px;
`;

const AddPersonal = styled.button`
  background-color: #f3f7fb;
  border: 1px dotted #e0e4f1;
  border-radius: 4px;
  cursor:pointer;
  font-size: 16px;
  color: #888e9f;
  line-height: 1.1;
  text-align: center;
  padding: 0;
  margin: 0;
  width: 100%;
  transition: 0.3s background-color;
  
  &:not(:first-child){
    margin-top: 10px;
  }
  
  &:hover{
    background-color: #e9ecf5;
  }
`;

const TitleField = styled.div`
    line-height: 9px;
    font-size: 12px;
    font-weight: 500;
`
const BlockFieldStyled = styled.div`
    position: relative;
    width: 100%;
    margin-bottom: 30px;
    max-height: 48px;
    height: 100%;
`;
const BlockSexandINN = styled.div`
   display:flex;
   align-items: center;
`;
const BlockSex = styled.div`
 position: relative;
`;
const BlockFIO = styled.div`
  padding: 0 20px
`;

const BlockInfo = styled.div`
 padding: 0 20px
`;
const Group = styled.div`
  display: flex;
`;
const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin-top: 10px;
`;

const TitleGroup = styled.h6`
    padding: 0 20px;
    margin: 0;
`;
const Address = styled.div`
    justify-content: space-between;
    display: flex;
    align-items: center;
`;
const AddressText = styled.div`
  color: rgba(0,0,0,0.8);
  cursor: pointer;
  max-width: 500px;
  &:hover {
    color: #000;
  }
`;
const ButtonBlock = styled.div`
    width: 150px;
    height: 30px;
`;
const Footer = styled.div`
   display: flex; 
   justify-content: center;
`;

const DatePickerStyle = styled(DatePicker)`
  width: 100%;
  border-radius: 4px;
  padding: 0 20px!important;
  height: 32px;
  margin: 0;
  box-shadow: none!important;
  // background: ${({background}) => background || "#ffffff"};
  border: 1px solid #e0e4f1;
  font-family: ProximaNova-Regular, sans-serif;
  font-size: 100%;
  line-height: 1.15;
  color: #000;

  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 30px white inset;
  }

  ${props => (props.error ? "border: 1px solid #DE4D4D" : null)};

  &:hover {
    border-color: #788ece;
    color: #000;
  }
 
  &:focus {
    border: 1px solid #b8c0d9;
    color: #000000;
    //background: ${({backgroundOnFocus}) => backgroundOnFocus};
  }

  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
   color: #b8c0d9;
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color: #b8c0d9;
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color: #b8c0d9;
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color: #b8c0d9;
  }

  ${({styled}) => styled};
`;




