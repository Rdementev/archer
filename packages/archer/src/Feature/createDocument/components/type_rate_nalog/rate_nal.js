import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import {compose} from "redux";
import {connect} from "react-redux";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import RequiredField from "ComponentsARJTTD/requiredField";

const list = [
    {id:1, name: '6% от всех доходов'},
    {id:2, name: 'от 5% до 15% от разницы доходов и расходов'},
]

const TypeRateNalog = (props) => {
    const {typeRateNal, setTypeRateNal } = props
    const [value, setValue] = useState(typeRateNal)

    useEffect(()=> {
        if(typeRateNal){
            const active = list.find(item => item.name === typeRateNal)
            return setValue(active.name)
        }
        setValue('')
    },[typeRateNal])

    const handleClickItem = (item, multi , other) => {
        setTypeRateNal(item[0].name)
    }

    const getContent = (value) => {
        switch (value) {
            case '6% от всех доходов' : {
                return (
                    <BodyItem>
                        Налог будет рассчитываться как 6% от всех доходов вашего бизнеса, расходы при этом никак не влияют на сумму налога.
                    </BodyItem>
                )
            }
            case 'от 5% до 15% от разницы доходов и расходов' : {
                return (
                    <BodyItem>
                        Сначала из суммы доходов вычитаются расходы, и уже от полученной разницы вычисляется налог.
                        Ставка налогообложения обычно составляет 15%, при этом может изменяться в зависимости от субъекта РФ и выбранных видов деятельности (Статья 346.20 НК РФ).
                        Точное значение налоговой ставки можно узнать в вашей местной налоговой инспекции.
                    </BodyItem>
                )
            }
        }
    }

    return (
        <>
            <Header>
                <Title>Ставка налогообложения</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <StyledSelectSearch>
                    <SelectSearchContainer onClick={handleClickItem}
                                           value={value}
                                           displayValue={value}
                                           list={list}/>
                </StyledSelectSearch>
            </Offer>
            <Body>
                {getContent(typeRateNal)}
            </Body>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps,{
    }),
)(TypeRateNalog)
//


const Body = styled.div`
  padding: 10px;
  font-weight: 600;
`;
const BodyItem = styled.div`
    font-size: 12px;
    max-height: 75px;
    height: 100%;
    overflow: auto;
    &:not(:last-child){
      margin-bottom: 10px;
    }
`;
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 0 10px;
`;
const StyledSelectSearch = styled.div`
       max-width: 300px;
    width: 100%;
    height: 32px;
`;

