import {createSelector} from "reselect";

const GET_QUERY = 'GET_QUERY';
const GET_SUGGESTIONS = 'GET_SUGGESTIONS';
const TOGGLE_SHOW_SUGGESTIONS = 'TOGGLE_SHOW_SUGGESTIONS';
const TOGGLE_DISABLED_INPUT = 'TOGGLE_DISABLED_INPUT';
const TOGGLE_INPUT_FOCUSED = 'TOGGLE_INPUT_FOCUSED';
const SET_SUGGESTION_INDEX = 'SET_SUGGESTION_INDEX';

let initialState = {
    inputFocused: false,
    isValid: false,
    query: '',
    showSuggestions: true,
    suggestionIndex: 0,
    suggestions: [],
    count: 5,
    disabledInput: true,
};





const addressClient = (state = initialState, action) => {
    switch (action.type) {
        case GET_QUERY : {
            return {...state, query: action.query}
        }
        case GET_SUGGESTIONS : {
            return {...state, suggestions: action.suggestions}
        }
        case TOGGLE_SHOW_SUGGESTIONS : {
            return {...state, showSuggestions: action.showSuggestions}
        }
        case TOGGLE_DISABLED_INPUT : {
            return {...state, disabledInput: action.disabledInput,}
        }
        case TOGGLE_INPUT_FOCUSED : {
            return {...state, inputFocused: action.inputFocused}
        }
        case SET_SUGGESTION_INDEX : {
            return {...state, suggestionIndex: action.suggestionIndex}
        }
    }
    return state
};

export const getQuery = (query) => ({type: GET_QUERY, query});
export const getSuggestions = (suggestions) => ({type: GET_SUGGESTIONS, suggestions});
export const toggleShowSuggestions = (showSuggestions) => ({type: TOGGLE_SHOW_SUGGESTIONS, showSuggestions});
export const toggleInputFocused = (inputFocused) => ({type: TOGGLE_INPUT_FOCUSED, inputFocused});
export const setSuggestionIndex = (suggestionIndex) => ({type: SET_SUGGESTION_INDEX, suggestionIndex});

export default addressClient;

export const ardmPagesGetSuggestion = (state) => state.ARDM.addressClient.suggestions
export const ardmPagesGetSuggestionReselect = createSelector(ardmPagesGetSuggestion, (suggestions) => suggestions)