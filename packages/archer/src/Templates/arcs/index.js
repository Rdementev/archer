import {appARCSReducer} from 'RootReducerARCS'
import ARCS from "ExportARCS";
import {compose} from "redux";
import {withReducer} from "../../Core/HOC/withReducer";
import React, {useEffect} from "react";
import {withTheme} from "../../Core/HOC/withTheme";


const ArcsModule = (props) => {
    return <ARCS />
}

export default compose(
    withReducer({ARCS: appARCSReducer}),
    withTheme
)(ArcsModule)