import createRequestRest from "ApiARJTTD/createRequestRest";
import {put} from "redux-saga/effects";
import {isShowPreloader} from "ReducersARDM/requests";
import {push} from "connected-react-router";
import {ARDM_AUTH_GET_CONFIRM_EMAIL} from "../action";
import {ardmAuthSigninChange} from "../../signIn/action";

function* prepareRequest(payload) {
    yield put(isShowPreloader(true))
    return payload;

}

function* prepareSuccess(response, data) {
    yield put(isShowPreloader(false))
    if(response.status === 201){
        yield put(ardmAuthSigninChange('email', data.payload.email))
        yield put(push('/auth/signin'))
    }

}
function* prepareFailure(response, data) {
    yield put(isShowPreloader(false))
    if(response.status === 405 ){
        yield put(push('/auth/confirm_email'))
    }

}

export const getConfirmEmail = () => {
    return createRequestRest({
        url: "/heimdallr/confirm_email",
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        action: ARDM_AUTH_GET_CONFIRM_EMAIL,
        method: 'get'
    })
}



