import React, {Suspense, useEffect} from 'react'
import {connect, useDispatch} from "react-redux";
import styled, {ThemeProvider} from 'styled-components/macro'
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {compose} from "redux";
import {withTheme} from "Core/HOC/withTheme";
import PageDoc from "PagesARDM/pagedocs";
import RegistrationOOO from "FeatureARDM/createDocument/registrationOOO";
import RegistrationIP from "FeatureARDM/createDocument/registrationIP";
import {ardmPagesRequestDataPagedoc} from "PagesARDM/pagedocs/actions";
import Preloader from "ComponentsARJTTD/preloader";

const ARDM = (props) => {
    const {ardmPagesRequestDataPagedoc} = props

    useEffect(() => {
        ardmPagesRequestDataPagedoc()
    },[])

    return (
        <StyledARDM>
                <Switch>
                    <Route exact path={'/serves/document/reg-ooo/:id'} render={() => <RegistrationOOO {...props} title='Регистрация ООО'/> }/>
                    <Route exact path={'/serves/document/reg-ip/:id'} render={() => <RegistrationIP {...props} title='Регистрация ООО'/> }/>
                    <Route exact path={'/serves/document'} render={() => <PageDoc {...props}/>}/>
                    <Redirect to={'/serves/document'} />
                </Switch>
        </StyledARDM>
    )
}
const mapStateToProps = (state) => {
    return {

    }
}

export default compose(
    connect(mapStateToProps,{
        ardmPagesRequestDataPagedoc
    }),
    withTheme,
    withRouter,
)(ARDM)
//
const StyledARDM = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  //display: grid;
  //grid-template-rows: minmax(50px, 5%) minmax(50px, 5%) minmax(100px, 100%);
`;
