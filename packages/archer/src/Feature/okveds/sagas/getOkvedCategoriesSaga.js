import createRequestRest from "ApiARJTTD/createRequestRest";
import {GET_ARDM_FEATURE_OKVEDS_CATEGORIES} from "../actions";

function* prepareSuccess(response, payload) {

}

function* prepareFailure(response, payload) {
}


export const getOkvedCategoriesSaga = () => {
    return createRequestRest({
        url: '/autodoc/get_okvad_section',
        action: GET_ARDM_FEATURE_OKVEDS_CATEGORIES,
        prepareSuccess,
        prepareFailure,
    })
}
