







export const checkCommonRegistrationOOO = (
    type_proxy_id,
    type_reg,
    type_send,
    type_size_capital,
    type_create_date,
    type_response,
    type_company_name_rus,
    type_company_name_russian,
    type_size_capitalIsValidation,
    size_capitalIsValidation,
    type_company_name_rusIsValidation,
    type_company_name_russianIsValidation,
    ) => {
    if(type_size_capitalIsValidation) return false
    if(!size_capitalIsValidation) return false
    if(!type_company_name_rusIsValidation) return false
    if(!type_company_name_russianIsValidation) return false
    if(type_send === 'По доверености' || type_response === 'По доверености' && !type_proxy_id ) return false
    if(!type_company_name_rus|| !type_company_name_russian) return false
    if(!type_size_capital) return false
    if(!type_create_date) return false
    return true
}

export const checkFDARegistrationOOO = (type_reg, type_size_capital, type_send, type_proxy_id, type_peace_form, type_response,
                                           type_individuals, type_organizations, type_dir_personal_id, type_dir_position,
                                           type_dir_time, type_address_id, isEqually) => {
    if(type_reg === 'Общая регистарция ООО' && (type_individuals.length < 1 || type_organizations.length < 1)) return false
    if(type_send === 'По доверености' || type_response === 'По доверености' && !type_proxy_id ) return false
    if(!isEqually) return false
    if(!type_dir_position) return false
    if(!type_dir_time) return false
    if(!type_address_id) return false
    if(!type_dir_personal_id) return false
    return true
}

export const checkUNORegistrationOOO = (
    type_sys_nal,
    type_rate_nal,
    type_selected_okved,
    type_secretar,
    type_predsidatel,
    type_reg_organ,
    type_reg,
    type_individuals,
    type_organizations) => {

    if(type_reg === 'Упрощённая система налогообложения (УСН)' && (!type_rate_nal)) return false
    if((type_individuals && type_individuals.length > 0 && type_organizations && type_organizations.length > 0) && (!type_secretar || !type_predsidatel)) return false
    if(type_selected_okved && type_selected_okved.length < 1) return false
    if(type_selected_okved && !type_selected_okved.find(item => item.favorite)) return false
    if(!type_reg_organ) return false

    return true
}