export const CHANGE_ORGANIZATION_DATA = 'CHANGE_ORGANIZATION_DATA';
export const changeOrganizationData = (id, value, data) => ({type:CHANGE_ORGANIZATION_DATA, id, value, data});

export const CREATE_ORGANIZATION = 'CREATE_ORGANIZATION';
export const createOrganization = (newOrganization) => ({type:CREATE_ORGANIZATION, newOrganization});

export const CHANGE_EDIT_ORGANIZATION = 'CHANGE_EDIT_ORGANIZATION';
export const changeEditOrganization = (searchEditOrganization) => ({type:CHANGE_EDIT_ORGANIZATION, searchEditOrganization});

export const UPDATE_ORGANIZATION_DATA = 'UPDATE_ORGANIZATION_DATA';
export const updateOrganizationData = (id) =>  ({type:UPDATE_ORGANIZATION_DATA, id});

export const SET_EDIT_ORGANIZATION_ID = 'SET_EDIT_ORGANIZATION_ID';
export const setEditOrganizationId = (id,) => ({type:SET_EDIT_ORGANIZATION_ID, id});

export const DELETE_ORGANIZATION = 'DELETE_ORGANIZATION';
export const deleteOrganization = (id) => ({type:DELETE_ORGANIZATION, id});

export const SET_ADDRESS_FOR_ORGANIZATION = 'SET_ADDRESS_FOR_ORGANIZATION';
export const setAddressForOrganization = (addressId) => ({type:SET_ADDRESS_FOR_ORGANIZATION, addressId});

export const SET_PREDSTAVITEL_FOR_FIRM = 'SET_PREDSTAVITEL_FOR_FIRM';
export const setPredstavitelForFirm = (id) => ({type:SET_PREDSTAVITEL_FOR_FIRM, id})

// получение списка организация
export const ARDM_PAGES_GET_ORGANIZATION_REQUEST = 'ARDM_PAGES_GET_ORGANIZATION_REQUEST';
export const ardmPagesGetOrganizationRequest = () => ({type: ARDM_PAGES_GET_ORGANIZATION_REQUEST, });

export const ARDM_PAGES_GET_ORGANIZATION_SUCCESS = 'ARDM_PAGES_GET_ORGANIZATION_SUCCESS';
export const ardmPagesGetOrganizationSuccess = (payload) => ({type: ARDM_PAGES_GET_ORGANIZATION_SUCCESS, payload});

export const ARDM_PAGES_GET_ORGANIZATION_FAILURE = 'ARDM_PAGES_GET_ORGANIZATION_FAILURE';
export const ardmPagesGetOrganizationFailure = () => ({type: ARDM_PAGES_GET_ORGANIZATION_FAILURE});
// создание новой организации
export const ARDM_PAGES_CREATE_ORGANIZATION_REQUEST = 'ARDM_PAGES_CREATE_ORGANIZATION_REQUEST';
export const ardmPagesCreateOrganizationRequest = (payload) => ({type: ARDM_PAGES_CREATE_ORGANIZATION_REQUEST, payload});

export const ARDM_PAGES_CREATE_ORGANIZATION_SUCCESS = 'ARDM_PAGES_CREATE_ORGANIZATION_SUCCESS';
export const ardmPagesCreateOrganizationSuccess = (payload) => ({type: ARDM_PAGES_CREATE_ORGANIZATION_SUCCESS, payload});

export const ARDM_PAGES_CREATE_ORGANIZATION_FAILURE = 'ARDM_PAGES_CREATE_ORGANIZATION_FAILURE';
export const ardmPagesCreateOrganizationFailure = () => ({type: ARDM_PAGES_CREATE_ORGANIZATION_FAILURE});
// обновление организации
export const ARDM_PAGES_UPDATE_ORGANIZATION_REQUEST = 'ARDM_PAGES_UPDATE_ORGANIZATION_REQUEST';
export const ardmPagesUpdateOrganizationRequest = (payload) => ({type: ARDM_PAGES_UPDATE_ORGANIZATION_REQUEST, payload});

export const ARDM_PAGES_UPDATE_ORGANIZATION_SUCCESS = 'ARDM_PAGES_UPDATE_ORGANIZATION_SUCCESS';
export const ardmPagesUpdateOrganizationSuccess = (payload) => ({type: ARDM_PAGES_UPDATE_ORGANIZATION_SUCCESS, payload});

export const ARDM_PAGES_UPDATE_ORGANIZATION_FAILURE = 'ARDM_PAGES_UPDATE_ORGANIZATION_FAILURE';
export const ardmPagesUpdateOrganizationFailure = () => ({type: ARDM_PAGES_UPDATE_ORGANIZATION_FAILURE});





