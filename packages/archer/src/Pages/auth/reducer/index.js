import confirmEmailReducer from '../confirmEmail/reducer'
import signIn from '../signIn/reducer'
import signUpReducer from '../signUp/reducer'
import rememberReducer from '../resetPassword/reducer'
import {combineReducers} from "redux";

export const appAuthReducer = combineReducers({
    confirmEmail: confirmEmailReducer,
    signIn,
    signUp: signUpReducer,
    remember: rememberReducer
})



const createAuthReducer = (state = {}, action) => {
    switch (action.type) {
        case 'DELETE_AUTH_REDUCE' : {
             return {}
        }
    }
    return appAuthReducer( state, action)
}



export default createAuthReducer
