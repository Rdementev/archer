import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import ChoisePersonal from "PagesARDM/personal";
import {compose} from "redux";
import {connect} from "react-redux";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import UpdatePersonal from "Pages/personal/components/update";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import {ardmFeatureRegOOOSetProxy} from "FeatureARDM/createDocument/registrationOOO/actions";

const TypeProxy = (props) => {
    const {proxyId, personal, ardmFeatureRegOOOSetProxy, setProxyId} = props
    const [show, setShow] = useState(false)
    const [proxyObj, setProxyObj] = useState(false)
    const [name, setName] = useState(false)
    const [showUpdate, setShowUpdate] = useState(false)

    useEffect(()=>{
        const obj =  personal.find(item => item.id === proxyId)
        setProxyObj(obj)
    },[proxyId, personal])
    useEffect(()=>{
        if(!proxyId) return setName('')
    },[proxyId])

    useEffect(()=> {
        if(proxyObj){
            const ProxyObjName = proxyObj.last_name.concat(' ' , proxyObj.first_name, ' ', proxyObj.second_name )
            setName(ProxyObjName)
        }
    },[proxyObj])

    const handleClick = () => {
        setShowUpdate(true)
    }

    const handleClickPersonal = (id) => {
        setProxyId(id)
        setShow(false)
    }
    const handleClickClear = () => {
        ardmFeatureRegOOOSetProxy(null)
    }


    return (
        <>
            <Container>
                <Header>
                    <Title>Доверенность</Title>
                </Header>
                <Offer>
                    <StyledNameProxy >
                        <NameProxy onClick={()=>{handleClick()}}>
                            {name}
                        </NameProxy>
                        {name && <BlockClearButton onClick={() => {
                            handleClickClear()
                        }}>
                            <CircleCloseBtn/>
                        </BlockClearButton>}
                    </StyledNameProxy>
                </Offer>
                <Footer>
                    <StyledButton onClick={()=>{setShow(true)}}>
                        <Button>Выбрать</Button>
                    </StyledButton>
                </Footer>
            </Container>
            {show && <ChoisePersonal show={show} setShow={setShow} onClickPers={handleClickPersonal}/>}
            {showUpdate && <UpdatePersonal show={showUpdate}
                                           setShow={setShowUpdate}
                                           editPersonalId={proxyObj.id}/>}
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetProxy
    }),
)(TypeProxy)
//


const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`;
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
   
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
`;
const Offer = styled.div`
  padding: 0 10px;
  width: 100%;
  text-transform: uppercase;
`;
const StyledNameProxy = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: space-between;
`;
const StyledButton = styled.div`
  width: 150px;
`;
const NameProxy = styled.div`
`;
const BlockClearButton = styled.div`
  width: 14px;
  height: 14px;
`;

const Footer = styled.div`
  padding: 0 10px;
`;