import createRequestRest from "ApiARJTTD/createRequestRest";
import {
    ARDM_PAGES_GET_FORM_DATA_REQUEST,
    ardmPagesGetFormDataRegOOOSuccess,
    ardmPagesGetFormDataRegIPSuccess
} from "../actions";
import {put} from "redux-saga/effects";
import {push} from "connected-react-router";
import {addPreloader, deletePreloader} from "ReducerARJTTD/preloader";


function* prepareRequest(payload) {
        yield put(addPreloader())
    return payload
}
function* prepareSuccess(result, payload) {
    if(result.data.form_name === "Регистрация ООО"){
        Object.keys(result.data).map(item => {
            if(result.data[item] === null){
                switch (item) {
                    case 'type_selected_okved' : {
                        return  result.data[item] = []
                    }
                    case 'type_individuals' : {
                        return result.data[item] = []
                    }
                    case 'type_organizations' : {
                        return result.data[item] = []
                    }
                    case 'type_predsidatel' : {
                        return  result.data[item] = {}
                    }
                    case 'type_secretar' : {
                        return  result.data[item] = {}
                    }
                }
                return result.data[item] = ''
            }
            const elem = result.data[item]
            return elem
        })
        yield put(ardmPagesGetFormDataRegOOOSuccess(result.data))
    } else if(result.data.form_name === 'Регистрация ИП'){
        Object.keys(result.data).map(item => {
            if(result.data[item] === null){
                return result.data[item] = ''
            }
            const elem = result.data[item]
            return elem
        })
        yield put(ardmPagesGetFormDataRegIPSuccess(result.data))
    }
        yield put(deletePreloader())
}
function* prepareFailure(result, payload) {
        yield put(deletePreloader())
}

export const getFormDataSaga = () => {
    return createRequestRest({
        url: '/autodoc/get_form_data',
        action: ARDM_PAGES_GET_FORM_DATA_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}