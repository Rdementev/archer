import {createSelector} from "reselect";


const getOrganizationSelector = (state) => state.ARDM.organization.organization
export const getARDMOrganizationReselect = createSelector(getOrganizationSelector,(organization)=>organization)

const getOrganizationDefaultSelector = (state) => state.ARDM.organization.organizationDefault
export const getARDMOrganizationDefaultReselect = createSelector(getOrganizationDefaultSelector,(organizationDefault)=>organizationDefault)

const getEditOrganizationIdSelector = (state) => state.ARDM.organization.editOrganizationId
export const getARDMEditOrganizationIdReselect = createSelector(getEditOrganizationIdSelector,(editOrganizationId) => editOrganizationId)

