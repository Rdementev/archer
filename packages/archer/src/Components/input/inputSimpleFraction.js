import React, {useEffect, useState} from 'react'
import Input from "ComponentsARJTTD/input/inputComponent";
import styled from 'styled-components/macro'

const InputSimpleFraction = (props) => {
    const { onChange, id, value1, value2, alias1, alias2,group } = props
    const [valueValid1 , setValueValid1] = useState(null)
    const [color1 , setColor1] = useState()
    const [valueValid2 , setValueValid2] = useState(null)
    const [color2 , setColor2] = useState()

    useEffect(()=>{
        fas1()
    },[value1 ])
    useEffect(()=>{
        fas2()
    },[value2 ])

    const fas1 = () => {
        if(valueValid1 === null){
            return setColor1('#e0e4f1')
        }
        if(valueValid1){
            return setColor1('#068223')
        } setColor1('#950101')
    }
    const fas2 = () => {
        if(valueValid2 === null){
            return setColor2('#e0e4f1')
        }
        if(valueValid2){
            return setColor2('#068223')
        } setColor2('#950101')
    }

    const handleOnBlur1 = () => {
        if(valueValid1 === null){
            setColor1('#950101')
        }
        if(!value1){
            setColor1('#950101')
        }
    }
    const handleOnBlur2 = () => {
        if(valueValid2 === null){
            setColor2('#950101')
        }
        if(!value2){
            setColor2('#950101')
        }
    }
    const valueRex = /^[1-9]/;

    const handleChange1 = (e) => {
        const foo = e.target.value
        if (valueRex.test(e.target.value) || foo.length < 1){
            onChange(foo, id, alias1, group)
            return setValueValid1(true);
        }
    }
    const handleChange2 = (e) => {
        const foo = e.target.value
        if (valueRex.test(e.target.value) || foo.length < 1){
            onChange(foo, id, alias2, group)
            return setValueValid2(true);
        }
    }

    return (
        <Container>
            <Block >
                <BlockInput>
                    <Input style={{borderColor:`${color1}`, background:'#f5f8fa'}} value={value1} onBlur={()=>{handleOnBlur1()}} onChange={(e)=>{handleChange1(e)}}/>
                </BlockInput>
                <Slash>/</Slash>
            </Block>
            <BlockInput>
                <Input style={{borderColor:`${color2}`, background:'#f5f8fa'}} value={value2} onBlur={()=>{handleOnBlur2()}} onChange={(e)=>{handleChange2(e)}}/>
            </BlockInput>
        </Container>
    )
}

export default InputSimpleFraction

const Container = styled.div`
    display: flex;
    height: 25px;
`;
const Block = styled.div`
    display: flex;
`;
const BlockInput = styled.div`
  width: 60px ;
`;
const Slash = styled.div`
    font-size: 23px;
    margin: 0 5px;
    line-height: 22px;
`;