import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import {compose} from "redux";
import {withTheme} from "styled-components";
import 'antd/dist/antd.css';
import locale from "antd/es/date-picker/locale/ru_RU";
import {DatePicker} from "antd";
import Input from "ComponentsARJTTD/input/inputComponent";
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {getARDMOrganizationReselect,} from "PagesARDM/organization/reselect";
import NewModal from "FeatureARDM/modal/newmodal";
import ChoiseAddress from "PagesARDM/address";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import ChoisePersonal from "PagesARDM/personal";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import UpdatePersonal from "PagesARDM/personal/components/update";
import UpdateAddress from "PagesARDM/address/components/update";
import {ardmPagesCreateOrganizationRequest} from "../../actions";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import RequiredField from "ComponentsARJTTD/requiredField";
import Promto from "ComponentsARJTTD/promto";
import {validationInteger, validationRequired} from "UtilsARJTTD/validation";


const styleInput = {
    border:'none',
    height: '30px'
}
const CreateOrganization = (props) => {
    const {ardmPagesCreateOrganizationRequest, show, setShow, personal, theme, address} = props
    const [enterRus, setEnterRus] = useState(false)
    const [enterRussian, setEnterRussian] = useState(false)
    const [rus, setRus] = useState(false)
    const [russian, setRussian] = useState(false)
    const [showAddressModal, setShowModalAddress] = useState(false)
    const [showPersonalModal, setShowModalPersonal] = useState(false)
    const [showUpdatePersonal, setShowUpdatePersonal] = useState(false)
    const [showUpdateAddress, setShowUpdateAddress] = useState(false)

    const [citizenship, setcitizenship] = useState('')
    const [citizenshipIsTouch, setcitizenshipIsTouch] = useState(false)
    const [inn, setinn] = useState('')
    const [innIsValidation, setinnIsValidation] = useState(false)
    const [name, setname] = useState('')
    const [nameIsValidation, setnameIsValidation] = useState(true)
    const [name_full, setname_full] = useState('')
    const [name_fullIsValidation, setname_fullIsValidation] = useState(true)
    const [date_ogrn, setdate_ogrn] = useState('')
    const [date_ogrnIsValidation, setdate_ogrnIsValidation] = useState(true)
    const [ogrn, setogrn] = useState('')
    const [ogrnIsValidation, setogrnIsValidation] = useState(false)
    const [kpp, setkpp] = useState('')
    const [kppIsValidation, setkppIsValidation] = useState(false)
    const [polnomo, setpolnomo] = useState('')
    const [polnomoIsValidation, setpolnomoIsValidation] = useState(true)
    const [position, setposition] = useState('')
    const [positionIsValidation, setpositionIsValidation] = useState(true)
    const [predstavitelId, setprdstavitelId] = useState('')
    const [addressId, setAddressId] = useState('')
    const [addressOneString, setAddressOneString] = useState('')
    const [namePersonal, setNamePersonal] = useState('')

    const [commonValidation, setCommonValidation] = useState(false)

    useEffect(()=>{
        if(
                !citizenship ||
                !citizenshipIsTouch ||
                !inn ||
                innIsValidation ||
                !name ||
                !nameIsValidation ||
                !name_full ||
                !name_fullIsValidation ||
                !date_ogrn ||
                !date_ogrnIsValidation ||
                !ogrn ||
                ogrnIsValidation ||
                !kpp ||
                kppIsValidation ||
                !polnomo ||
                !polnomoIsValidation ||
                !position ||
                !positionIsValidation ||
                !predstavitelId ||
                !addressId
        ){
            return setCommonValidation(false)
        }
        setCommonValidation(true)
    },[
        citizenship,
        citizenshipIsTouch,
        inn,
        innIsValidation,
        name,
        nameIsValidation,
        name_full,
        name_fullIsValidation,
        date_ogrn,
        date_ogrnIsValidation,
        ogrn,
        ogrnIsValidation,
        kpp,
        kppIsValidation,
        polnomo,
        polnomoIsValidation,
        position,
        positionIsValidation,
        predstavitelId,
        addressId,
    ])

    useEffect(() => {
        getSearchAddress()
    }, [addressId, address])

    useEffect(() => {
        getNamePredctavitel(personal)
    }, [predstavitelId, personal])

    //ищем Имя представителя фирмы
    const getNamePredctavitel = () => {
        const pers = personal.find(item => item.id === predstavitelId)
        if(!pers) return
        const name = pers.last_name.concat(' ', pers.first_name, ' ', pers.second_name)
        setNamePersonal(name)
    }

    // клик на выбор представителя
    const handleClick = () => {
        setShowModalPersonal(true)
    }
    const onClickPers = (id) => {
        setprdstavitelId(id)
        setShowModalPersonal(false)
    }
    // клик на имя представителя
    const handleClickOnNamePersonal = () => {
        setShowUpdatePersonal(true)
    }
    // сетаем дату в стейт
    const handleChangeDate = (moment, date,) => {
        setdate_ogrn(moment)
    };
    // ищем адрес фирмы одной строкой
    const getSearchAddress = () => {
        address.map(item => {
            if(item.id === addressId){
                const region_with_type = item.region && item.region.concat(' ', item.region_type_full)
                const city_with_type = item.city_type_full && item.city_type_full.concat(' ', item.city)
                const settlement_with_type = item.settlement_type_full && item.settlement_type_full.concat(' ', item.settlement)
                const street_with_type = item.street_type_full.concat(' ', item.street)
                const addressOneString = item.region === item.city
                    ? city_with_type.concat(' ',
                        settlement_with_type === null ? '' : settlement_with_type, ' ',
                        street_with_type === null ? '' : street_with_type, ' ',
                        item.house_type_full === null ? '' : item.house_type_full, ' ',
                        item.house === null ? '' : item.house, ' ',
                        item.block_type_full === null ? '' : item.block_type_full, ' ',
                        item.block === null ? '' : item.block, ' ',
                        item.flat_type_full === null ? '' : item.flat_type_full, ' ',
                        item.flat === null ? '' : item.flat)

                    : region_with_type.concat(' ',
                        city_with_type === null ? '' : city_with_type, ' ',
                        settlement_with_type === null ? '' : settlement_with_type, ' ',
                        street_with_type === null ? '' : street_with_type, ' ',
                        item.house_type_full === null ? '' : item.house_type_full, ' ',
                        item.house === null ? '' : item.house, ' ',
                        item.block_type_full === null ? '' : item.block_type_full, ' ',
                        item.block === null ? '' : item.block, ' ',
                        item.flat_type_full === null ? '' : item.flat_type_full, ' ',
                        item.flat === null ? '' : item.flat)
                return setAddressOneString(addressOneString)
            }
        })
    }
    // открываем модальное окно адреса
    const handleClickOpenAddress = () => {
        setShowModalAddress(true)
    }
    // клик на адресс из списка
    const handleClickAddress = (id) => {
        setAddressId(id)
        setShowModalAddress(false)
    }
    // запрос по созданию на бэк
    const handleClickCreateOrganization = () => {
        const data = {
            citizenship: citizenship,
            inn: inn,
            name: name,
            name_full: name_full,
            date_ogrn: date_ogrn,
            ogrn: ogrn,
            kpp: kpp,
            polnomo: polnomo,
            position: position,
            predstavitel_id: predstavitelId,
            address_id: addressId,
        }
        ardmPagesCreateOrganizationRequest(data)
        setShow(false)
    }

    const handleBlurName = () => {
        const regRusName = /^[0-9аА-яЯёЁ\s]+$/
        setRus(!rus)
        if(regRusName.test(name)){
            setnameIsValidation(true)
        } else {
            setnameIsValidation(false)
        }

    }
    const handleBlurNameFull = () => {
        const regRusName = /^[0-9аА-яЯёЁ\s]+$/
        setRussian(!russian)
        if(regRusName.test(name_full)){
            setname_fullIsValidation(true)
        } else {
            setname_fullIsValidation(false)
        }
    }

    return (
        <NewModal showModal={show} setShow={setShow}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Создание организаций</ModalTitle>
                    <BlockIcon onClick={()=>{setShow(false)}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ModalBody>
                    <Block >
                        <BlockFieldStyled>
                            <Title>
                                <TitleField>Место происхождения</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={citizenship}
                                       onBlur={()=>{setcitizenshipIsTouch(true)}}
                                       placeholder={'Введите место происхождения'}
                                       onChange={(e) => {setcitizenship(e.target.value)}}/>
                            </BlockInput>
                            {!citizenship && citizenshipIsTouch && <Promto text={'Введите место происхождения организации'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <Title>
                                <TitleField>ИНН</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={inn}
                                       onBlur={()=>{validationInteger(inn, setinnIsValidation, 10 , 10)}}
                                       placeholder={'Введите ИНН'}
                                       onChange={(e) => {setinn(e.target.value)}}/>
                            </BlockInput>
                            {innIsValidation && <Promto text={innIsValidation}/>}
                        </BlockFieldStyled>
                    </Block>
                    <Group>
                        <BlockFieldStyled>
                            <Title>
                                <TitleField>Сокращённое наименование</TitleField>
                                <RequiredField/>
                            </Title>

                            <StyledInput onClick={()=>{setRus(!rus)}}
                                         enter={enterRus}
                                         theme={theme}
                                         rus={rus}
                                         onMouseLeave={()=>{setEnterRus(false)}}
                                         onMouseEnter={()=>{setEnterRus(true)}}>
                                <RightInput>OOO «</RightInput>
                                {rus ? <Input style={styleInput}
                                              placeholder={'Введите сокращенное наименование фирмы'}
                                              autoFocus={true}
                                              onBlur={()=>{handleBlurName()}}
                                              value={name}
                                              onChange={(e)=>{setname(e.target.value)}}/>
                                    : <InputText>{name}</InputText> }
                                <LeftInput>»</LeftInput>
                            </StyledInput>
                            {!nameIsValidation  && <Promto text={'Допустимы только русские символы и цифры'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <Title>
                                <TitleField>Полное наименование</TitleField>
                                <RequiredField/>
                            </Title>
                            <StyledInput onClick={()=>{setRussian(!russian)}}
                                         enter={enterRussian}
                                         theme={theme}
                                         rus={russian}
                                         onMouseLeave={()=>{setEnterRussian(false)}}
                                         onMouseEnter={()=>{setEnterRussian(true)}}>
                                <RightInput>OOO «</RightInput>
                                {russian ? <Input style={styleInput}
                                              autoFocus={true}
                                              placeholder={'Введите полное наименование фирмы'}
                                              onBlur={()=>{handleBlurNameFull()}}
                                              value={name_full}
                                              onChange={(e)=>{setname_full(e.target.value)}}/>
                                    : <InputText>{name_full}</InputText> }
                                <LeftInput>»</LeftInput>
                            </StyledInput>
                            {!name_fullIsValidation  && <Promto text={'Допустимы только русские символы и цифры'}/>}
                        </BlockFieldStyled>
                    </Group>
                    <Block >
                        <BlockFieldStyled >
                            <Title>
                                <TitleField>Дата присвоения ОГРН</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <DatePickerStyle
                                    locale={locale}
                                    placeholder={'Выберите дату присвоение ОГРН'}
                                    style={{width: '100%'}}
                                    value={date_ogrn}
                                    onBlur={()=>{validationRequired(date_ogrn, setdate_ogrnIsValidation)}}
                                    onChange={handleChangeDate}
                                    format="DD.MM.YYYY"/>
                            </BlockInput>
                            {!date_ogrnIsValidation && <Promto text={'Введите дату присвоение ОГРН'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled >
                            <Title>
                                <TitleField>ОГРН</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={ogrn}
                                       onBlur={()=>{validationInteger(ogrn, setogrnIsValidation, 13 , 13)}}
                                       placeholder={'Введите ОГРН'}
                                       onChange={(e) => {setogrn(e.target.value)}}/>
                            </BlockInput>
                            {ogrnIsValidation && <Promto text={ogrnIsValidation}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled >
                            <Title>
                                <TitleField>КПП</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={kpp}
                                       onBlur={()=>{validationInteger(kpp, setkppIsValidation, 9 , 9)}}
                                       placeholder={'Введите КПП'}
                                       onChange={(e) => {setkpp(e.target.value)}}/>
                            </BlockInput>
                            {kppIsValidation && <Promto text={kppIsValidation}/>}
                        </BlockFieldStyled>
                    </Block>
                    <Block >
                        <BlockFieldStyled>
                            <Title>
                                <TitleField>Основание полномочий</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={polnomo}
                                       onBlur={()=>{validationRequired(polnomo, setpolnomoIsValidation)}}
                                       placeholder={'Введите основание полномочий'}
                                       onChange={(e) => {setpolnomo(e.target.value)}}/>
                            </BlockInput>
                            {!polnomoIsValidation && <Promto text={'Введите основание полномочий'}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled >
                            <Title>
                                <TitleField>Наименование должности</TitleField>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={position}
                                       onBlur={()=>{validationRequired(position, setpositionIsValidation)}}
                                       placeholder={'Введите наименование должности'}
                                       onChange={(e) => {setposition(e.target.value)}}/>
                            </BlockInput>
                            {!positionIsValidation && <Promto text={'Введите наименование должности'}/>}
                        </BlockFieldStyled>
                    </Block>
                    <Predstavitel>
                        <Title>
                            <TitleField>Представитель</TitleField>
                            <RequiredField/>
                        </Title>
                        {namePersonal ? (
                            <PredstavitelBlock>
                                <PredstavitelText onClick={()=>{handleClickOnNamePersonal()}}>{namePersonal}</PredstavitelText>
                                <ButtonBlock>
                                    <Button onClick={()=>{handleClick()}}>Изменить</Button>
                                </ButtonBlock>
                            </PredstavitelBlock>
                        ) : (
                            <ButtonBlock>
                                <WhiteButton onClick={()=>{handleClick()}}>Выбрать</WhiteButton>
                            </ButtonBlock>
                        )}
                        {!namePersonal && <Promto text={'Выберите представителя фирмы'}/>}
                    </Predstavitel>
                    <AddressStyled>
                        <Title>
                            <TitleField>Адрес</TitleField>
                            <RequiredField/>
                        </Title>
                        <BlockInfo style={{width:'100%'}}>
                            {addressOneString ? (
                                <Address>
                                    <AddressText onClick={()=>{setShowUpdateAddress(true)}}>{addressOneString}</AddressText>
                                    <ButtonBlock>
                                        <Button onClick={()=>{handleClickOpenAddress()}}>Изменить</Button>
                                    </ButtonBlock>
                                </Address>
                            ) : (
                                <ButtonBlock>
                                    <WhiteButton onClick={()=>{handleClickOpenAddress()}}>Выбрать</WhiteButton>
                                </ButtonBlock>
                            )}
                        </BlockInfo>
                        {!addressOneString && <Promto text={'Выберите адрес фирмы'}/>}
                    </AddressStyled>
                    <Footer>
                        <ButtonBlock>
                            <WhiteButton onClick={() => {handleClickCreateOrganization()}} disabled={!commonValidation}>
                                Создать
                            </WhiteButton>
                        </ButtonBlock>
                    </Footer>
                </ModalBody>
            </ContainerModal>
            {showAddressModal && <ChoiseAddress onClickAddress={handleClickAddress} show={showAddressModal} setShow={setShowModalAddress}/>}
            {showPersonalModal && <ChoisePersonal onClickPers={onClickPers} show={showPersonalModal} setShow={setShowModalPersonal}/>}
            {showUpdatePersonal && <UpdatePersonal show={showUpdatePersonal}
                             setShow={setShowUpdatePersonal}
                             editPersonalId={predstavitelId}/>}
            {showUpdateAddress && <UpdateAddress show={showUpdateAddress}
                                          setShow={setShowUpdateAddress}
                                          editAddressId={addressId}/>}
        </NewModal>
    )
}



const mapStateToProps = (state) => {
    return {
        organization: getARDMOrganizationReselect(state),
        address: getArdmPagesAddressReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
}

export default compose(
    connect(mapStateToProps, {
        ardmPagesCreateOrganizationRequest
    }),
    withTheme
)(CreateOrganization)
//
const ContainerModal = styled.div`
    padding: 20px;
    max-height: 800px;
    height: 100%;
    min-width: 800px;
    overflow-y: auto ;
    display: grid;
    grid-template-rows: 50px minmax(400px, 100%);
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: 0 14px 34px #00000066;
`;
const BlockIcon = styled.div`
width: 14px;
height: 14px;
`;
const Predstavitel = styled.div`
    padding: 0 20px;
    position: relative;
    max-height: 48px;
`;


const AddressText = styled.div`
  color: rgba(0,0,0,0.8);
  cursor: pointer;
  max-width: 500px;
  &:hover {
    color: #000;
  }
`;
const ButtonBlock = styled.div`
    width: 150px;
    height: 30px;
    margin-top: auto;
`;
const ModalBody = styled.div`
    display: grid;
`;

const ModalHeader = styled.div`
   display: flex;
   justify-content: space-between;
`;
const ModalTitle = styled.h4`
  
`;
const TitleField = styled.div`
    font-size: 12px;
    font-weight: 500;
    line-height: 12px;
`;
const StyledInput = styled.div`
    cursor: pointer;
    height: 32px;
    margin: 0 10px 0 0;
    padding: 0 10px;
    border-radius: 4px;
    background: #fff;
    border: 1px solid #e5e5e5;
    border-color: ${({enter, rus, theme}) => enter || rus ? theme.semiHeavy : '#e5e5e5' };
    font-size: 100%;
    line-height: 1.15;
    display: flex;
    transition: ease 0.3s;
`;
const RightInput = styled.div`
    white-space: nowrap;
    display: flex;
    align-items: center;
`;
const InputText = styled.span`
    white-space: nowrap;
    display: flex;
    align-items: center;
`;
const LeftInput = styled.div`
    white-space: nowrap;
    display: flex;
    align-items: center;
`;
const BlockInput = styled.div`
    cursor: pointer;
    height: 32px;
`;
const BlockFieldStyled = styled.div`
   width: 100%;
   max-height: 48px;
   padding: 0 20px;
   position: relative;
`;
const Block = styled.div`
  display: flex;
`;
const Title = styled.div`
    display: flex;
    margin-bottom: 5px;
`;
const BlockInfo = styled.div`

`;
const Group = styled.div`
 margin-bottom: 20px;
  display: grid;
  grid-template-columns : 1fr 2fr;
`;
const AddressStyled = styled.div`
    padding: 0 20px;
    position: relative;
    max-height: 48px;
`;
const Address = styled.div`
    justify-content: space-between;
    display: flex;
    align-items: center;
`;
const Footer = styled.div`
    display: flex;
    justify-content: center;
`;
const PredstavitelBlock = styled(Address)`

`;
const PredstavitelText = styled.div`
    text-transform: uppercase;
    cursor: pointer;
    transition: ease 0.3s;
    max-width: 500px;
    &:hover {
      color: #000;
    }
`;

const DatePickerStyle = styled(DatePicker)`
  width: 100%;
  height: 100%;
  padding: 0 20px!important;
  margin: 0;
  box-shadow: none!important;
  background: ${({background}) => background || "#ffffff"};
  border: 1px solid #e0e4f1;
  font-family: ProximaNova-Regular, sans-serif;
  font-size: 100%;
  line-height: 1.15;
  color: #000;
  border-radius: 4px!important;

  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 30px white inset;
  }

  ${props => (props.error ? "border: 1px solid #DE4D4D" : null)};

  &:hover {
    border-color: #788ece;
    color: #000;
  }
 
  &:focus {
    border: 1px solid #b8c0d9;
    color: #000000;
    background: ${({backgroundOnFocus}) => backgroundOnFocus};
  }

  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: #000;
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color: #b8c0d9;
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color: #000;
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color: #000;
  }

  ${({styled}) => styled};
`;




