import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import styled from "styled-components/macro";
import Table from "@material-ui/core/Table";
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {getARDMOrganizationReselect} from "PagesARDM/organization/reselect";
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";


const headCells = [
    {id: 1, numeric: 'left', alias: 'name_full',  disablePadding: true, label: 'Наименование'},
    {id: 2, numeric: 'left', alias: 'dir',  disablePadding: false, label: 'Директор'},
    {id: 3, numeric: 'center', alias: 'ogrn',  disablePadding: false, label: 'ОГРН'},
    {id: 4, numeric: 'center', alias: 'inn',  disablePadding: false, label: 'ИНН'},
];


function RenderTableHeader(props) {
    const {} = props;
    return (
        <TableHeader >
            <TableCheck >
            </TableCheck>
            <TableHeaderItem>
                {props.headCells.map((headCell) => (
                    <Item key={headCell.id}
                          padding={headCell.disablePadding ? 'none' : 'default'}>
                        {headCell.label}
                    </Item>
                ))}
            </TableHeaderItem>
            <div style={{minWidth:'70px'}}>Действия</div>
        </TableHeader>
    );
}

const ListOrganization = (props) => {
    const {organization, onClickOrg, personal, setShowCreatrOrg, setShowUpdateOrg, setEditOrganizationId } = props
    const [searchInput, setSearchInput] = useState("");
    const [searchResults, setSearchResults] = useState(organization);

    useEffect(() => {
        const results = organization.filter(organization =>
            organization.name_full.toLowerCase().includes(searchInput.toLowerCase()) ||
            organization.name.toLowerCase().includes(searchInput.toLowerCase())
        );
        setSearchResults(results);
        getOrganization()
    }, [organization, searchInput])

    useEffect(() => {

    },[personal])

    const handleonClickorganizations = (id) => {
        onClickOrg(id)
    }


    //получаем организации
    const getOrganization = () => {
        return searchResults.map((item, i) => {
            const obj = personal.find(elem => elem.id === item.predstavitel_id)
            const name = obj &&  obj.last_name.concat(' ',  obj.first_name.split('')[0].toLowerCase(), '. ', obj.second_name.split('')[0].toLowerCase(), '.')
            if (item.id) {
                return (
                    <Row>
                        <TableCheck >
                            <Checkbox/>
                        </TableCheck>
                        <RowUser onClick={(e) => handleonClickorganizations( item.id )}
                                 tabIndex={-1}
                                 key={item.id}>
                            <TableHeaderItem>
                                <Item>{item.name_full}</Item>
                                {name ? <Item style={{textTransform: 'uppercase'}}>{name}</Item> : <Item/> }
                                <Item >{item.ogrn}</Item>
                                <Item >{item.inn}</Item>
                            </TableHeaderItem>
                        </RowUser>
                        <Item className='ml-auto d-flex justify-content-between' style={{minWidth:'70px', width:'70px', cursor:'pointer', zIndex: '100'}}>
                            <BlockIcon onClick={(e) => {editOrganization(item.id)}}>
                                <EditOutlinedIcon fontSize={'small'}/>
                            </BlockIcon>
                        </Item>
                    </Row>
                )
            }
        })
    }

    // функция по созданию новой организации
    const createNewOrganization = () => {
        setShowCreatrOrg(true)
    };
    // функция по изменению организации
    const editOrganization = (id) => {
        setEditOrganizationId(id)
        setShowUpdateOrg(true)
    };

    // поиск по фирмам
    const handleChange = e => {
        setSearchInput(e.target.value);
    };

    return (
        <>
            <BlockInput>
                <Input onChange={(e)=>{handleChange(e)}} placeholder='Поиск'/>
            </BlockInput>
            <RenderTableHeader rowCount={organization.length}
                               headCells={headCells}/>
            <TableContainer>
                <Table>
                    {getOrganization()}
                </Table>
            </TableContainer>
            <Footer>
                <BlockButtonCreate>
                    <WhiteButton onClick={() => {createNewOrganization()}}>
                        Создать
                    </WhiteButton>
                </BlockButtonCreate>
                <BlockButton>
                    <Button>Выбрать</Button>
                </BlockButton>
            </Footer>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        organization: getARDMOrganizationReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
};

export default connect(mapStateToProps, {

})(ListOrganization)

//



const BlockIcon = styled.div`
    margin-left: auto;
`;
const BlockButton = styled.div`
      width: 150px;
      height: 30px;
      display: flex;
      align-items: center;
`;
const BlockButtonCreate = styled(BlockButton)`
     margin-right: 15px;
`;
const Footer = styled.div`
      display: flex;
      justify-content: center;
      margin-top: 20px;
`;


const TableContainer = styled.div`
    overflow-y: auto;
    flex-grow: 1;
`;

const BlockInput = styled.div`
   margin-bottom: 10px;
   height: 32px;
`;

const TableHeader = styled.div`
   display: flex;
   background: #1f367d14;
   padding: 20px;
   height: 50px;
   align-items: center;
`;
const TableCheck = styled.div`
  max-width: 50px;
  width: 100%;
`;

const TableHeaderItem = styled.div`
   display: grid;
   grid-template-columns: repeat(4, 1fr);
   width: 100%;
`;

const Item = styled.div`
   width: 100%;
   display: flex;
    align-items: center;
`;
const RowUser = styled.div`
   width: 100%;
   display: flex;
   padding: 10px 0;
   cursor:pointer;
`;
const Row = styled.div`
    display: flex;
    padding: 0 20px;
    align-items: center;
    border: 1px solid #e0e4f1;
    border-top: none;
    height: 50px;
    &:hover{
        background: #e4e9f145;
        transition: ease 0.5s;
    }
`;

