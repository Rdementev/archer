import React, {useEffect} from 'react'
import {connect} from "react-redux";
import Input from "ComponentsARJTTD/input/inputComponent";
import {v4 as uuidv4} from "uuid";
import styled from "styled-components/macro";
import Table from "@material-ui/core/Table";
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {setAddressForPersonal} from "PagesARDM/personal/actions";
import {changeEditAddress, deleteAddress} from "../../actions";
import {setAddressForOrganization} from "PagesARDM/organization/actions";
import {WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {
    getArdmPagesAddressReselect,
} from "../../reducer/address";
import {ardmFeatureRegOOOSetAddressId} from "FeatureARDM/createDocument/registrationOOO/actions";


const headCells = [
    {id: 1, groupId: 1, numeric: 'left', alias: 'region_with_type', disablePadding: true, label: 'Регион'},
    {
        id: 3,
        groupId: 1,
        numeric: 'center',
        alias: 'settlement_with_type',
        disablePadding: false,
        label: 'Населенный пункт'
    },
    {id: 4, groupId: 1, numeric: 'center', alias: 'street_with_type', disablePadding: false, label: 'Улица'},
    {id: 5, groupId: 2, numeric: 'center', alias: 'house', disablePadding: false, label: 'Дом'},
    {id: 6, groupId: 2, numeric: 'center', alias: 'block', disablePadding: true, label: 'Корпус'},
    {id: 7, groupId: 2, numeric: 'center', alias: 'flat', disablePadding: false, label: 'Квартира'},

];

function RenderTableHeader(props) {
    const {} = props;

    const getGroup1 = () => {
        return props.headCells.map(headCell => {
            if (headCell.groupId === 1)
                return (
                    <Item key={headCell.id}
                          padding={headCell.disablePadding ? 'none' : 'default'}>
                        {headCell.label}
                    </Item>
                )
        })
    }
    const getGroup2 = () => {
        return props.headCells.map(headCell => {
            if (headCell.groupId === 2)
                return (
                    <Item key={headCell.id}
                          padding={headCell.disablePadding ? 'none' : 'default'}>
                        {headCell.label}
                    </Item>
                )
        })
    }

    return (
        <TableHeader>
            <TableCheck>
            </TableCheck>
            <HeaderRow>
                <TableHeaderItem>
                    <HeaderGroup1>
                        {getGroup1()}
                    </HeaderGroup1>
                    <HeaderGroup2>
                        {getGroup2()}
                    </HeaderGroup2>
                </TableHeaderItem>
                <div style={{minWidth: '70px'}}>Действия</div>
            </HeaderRow>
        </TableHeader>
    );
}

const ListAddress = (props) => {
    const {
        address,
        addressDefault,
        onClickAddress,
        setShowCreate,
        setShowUpdate,
        setEditAddressId,
    } = props
    const [searchAddressInput, setSearchAddressInput] = React.useState("");
    const [searchResultsAddress, setSearchResults] = React.useState(address);

    useEffect(() => {
        const results = address && address.length > 1 && address.filter(address => {
            const region_with_type = address.region_type_full.concat(' ', address.region)
            const city_with_type = address.city_type_full &&  address.city_type_full.concat(' ', address.city)
            const settlement_with_type = address.settlement_type_full &&  address.settlement_type_full.concat(' ', address.settlement)
            const street_with_type = address.street_type_full.concat(' ', address.street)
            return region_with_type.toLowerCase().includes(searchAddressInput.toLowerCase()) ||
                city_with_type && city_with_type.toLowerCase().includes(searchAddressInput.toLowerCase()) ||
                settlement_with_type && settlement_with_type.toLowerCase().includes(searchAddressInput.toLowerCase()) ||
                street_with_type.toLowerCase().includes(searchAddressInput.toLowerCase())
        });
        setSearchResults(results);
    }, [address, searchAddressInput, addressDefault])

    //получаем список адресов
    const getAddress = () => {
        if(searchResultsAddress && searchResultsAddress.length > 1){
            return searchResultsAddress.map((item, i) => {
                if (item.id) {
                    return (
                        <Row>
                            <TableCheck>
                                <Checkbox className='table__check table__check_item'
                                />
                            </TableCheck>
                            <RowUser onClick={() => {handleClickAddress(item.id)}}
                                     tabIndex={-1}
                                     key={item.id}
                            >
                                <User>
                                    <BodyGroup1>
                                        {item.region === item.city
                                            ? (
                                                <>
                                                    <Item/>
                                                    <ItemBlock>
                                                        {item.city && <Item>{item.city}</Item>}
                                                    </ItemBlock>
                                                </>
                                            )
                                            : (
                                                <>
                                                    <ItemBlock>
                                                        {item.region && <Item>{item.region}</Item>}
                                                    </ItemBlock>
                                                    {item.city_type_full && item.city
                                                        ? (
                                                            <ItemBlock>
                                                                {item.city && <Item>{item.city}</Item>}
                                                            </ItemBlock>
                                                        )
                                                        : (
                                                            <ItemBlock>
                                                                {item.settlement && <Item>{item.settlement}</Item>}
                                                            </ItemBlock>
                                                        )


                                                    }
                                                </>
                                            )
                                        }
                                        <ItemBlock>
                                            {item.street && <Item>{item.street}</Item>}
                                        </ItemBlock>
                                    </BodyGroup1>
                                    <BodyGroup2>
                                        {item.house ? <Item>{item.house}</Item> : <Item/>}
                                        {item.block ? <Item>{item.block}</Item> : <Item/>}
                                        {item.flat ? <Item>{item.flat}</Item> : <Item/>}
                                    </BodyGroup2>
                                </User>
                            </RowUser>
                            <Item className='ml-auto d-flex justify-content-between'
                                  style={{minWidth: '70px', width: '70px', cursor: 'pointer', zIndex: '100'}}>
                                <BlockIcon onClick={(e) => {handleEditAddress(item.id)}}>
                                    <EditOutlinedIcon fontSize={'small'}/>
                                </BlockIcon>
                            </Item>
                        </Row>
                    )
                }
            })
        }
    }

    // функция по созданию нового адреса
    const createNewAddress = () => {
        setShowCreate(true)
    };

    // поиск по адресам
    const handleChange = e => {
        setSearchAddressInput(e.target.value);
    };

    //клик на адрес в базе
    const handleClickAddress = (id) => {
        onClickAddress(id)
    }

    // открываем на редактирование
    const handleEditAddress = (id) => {
        setEditAddressId(id)
        setShowUpdate(true)
    }

    return (
        <>
            <BlockInput>
                <Input onChange={(e) => {handleChange(e)}} placeholder='Поиск'/>
            </BlockInput>
            <RenderTableHeader
                rowCount={address.length}
                headCells={headCells}
            />
            <TableContainer>
                <Table
                    aria-labelledby="tableTitle"
                    size={'medium'}
                    aria-label="enhanced table"
                >
                    {getAddress()}
                </Table>
            </TableContainer>
            <Footer>
                <BlockButton>
                    <WhiteButton onClick={() => {createNewAddress()}}>
                        Создать
                    </WhiteButton>
                </BlockButton>
            </Footer>

        </>
    )
}

const mapStateToProps = (state) => {
    return {
        address: getArdmPagesAddressReselect(state),
    }
};

export default connect(mapStateToProps, {
    setAddressForPersonal, deleteAddress, changeEditAddress,
    setAddressForOrganization, ardmFeatureRegOOOSetAddressId
})(ListAddress)

//
const BlockIcon = styled.div`
    margin-left: auto;
`;
const BodyGroup2 = styled.div`
     display: grid;
    grid-template-columns: repeat(3, 1fr);
   
`;
const BodyGroup1 = styled.div`
    display: grid;
   grid-template-columns: repeat(3, 1fr);
    
`;
const HeaderGroup1 = styled.div`
    display: grid;
   grid-template-columns: repeat(3, 1fr);
`;
const HeaderGroup2 = styled.div`
  
    display: grid;
    grid-template-columns: repeat(3, 1fr);
`;
const TableContainer = styled.div`   
    flex-grow: 1;
    flex-shrink: 1;
    overflow-y: auto;
`;

const BlockInput = styled.div`
   margin-bottom: 10px;
   height: 32px;
`;
const BlockButton = styled.div`
  width: 150px;
  height: 30px;
`;
const Footer = styled.div`
 display: flex;
 justify-content: center;
 margin-top: auto;
`;

const TableHeader = styled.div`
    display: flex;
    background: #1f367d14;
    padding: 20px;
    height: 50px;
    align-items: center;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
`;
const TableCheck = styled.div`
  max-width: 50px;
  width: 100%;
`;

const TableHeaderItem = styled.div`
   display: grid;
    grid-template-columns: 2fr 1fr;
    width: 100%;
`;

const Item = styled.div`
   width: 100%;
   
`;
const ItemBlock = styled.div`
  
`;
const RowUser = styled.div`
   width: 100%;
   display: flex;
   padding: 10px 0;
    cursor:pointer;
`;
const HeaderRow = styled.div`
   width: 100%;
   display: flex;
`;
const User = styled.div`
   width: 100%;
    display: grid;
    grid-template-columns: 2fr 1fr;
`;
const Row = styled.div`
    display: flex;
    padding: 0 20px;
    align-items: center;
    border: 1px solid #e0e4f1;
    border-top: none;
    height: 50px;
    &:last-child {
        border-bottom-right-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    &:hover{
        background: #e4e9f145;
        transition: ease 0.5s;
    }
`;

