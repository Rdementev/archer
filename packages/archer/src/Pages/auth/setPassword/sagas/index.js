import createRequestRest from "ApiARJTTD/createRequestRest";
import {put,race} from "redux-saga/effects";
import {isShowPreloader} from "ReducersARDM/requests";
import {push} from "connected-react-router";
import {ARDM_AUTH_SET_PASSWORD_CHANGE_PASSWORD} from "../action";

function* prepareRequest(payload) {
    yield put(isShowPreloader(true))
    return payload;

}

function* prepareSuccess(response, data) {
    yield put(push('/auth/signin'))
    yield put(isShowPreloader(false))


}
function* prepareFailure(response, data) {
    yield put(isShowPreloader(false))


}

export const setPassword = () => {
    return createRequestRest({
        url: "/heimdallr/set_password",
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        action: ARDM_AUTH_SET_PASSWORD_CHANGE_PASSWORD,
        method: 'post'
    })
}



