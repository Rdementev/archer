import React from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {Renta} from 'IconsARDM'
import {Switch} from "react-router-dom";




const ClientAddress = () => {
    return (
            <BlockOffer>
                <OfferHeader>
                    Клиент
                </OfferHeader>
                <OfferBody>

                </OfferBody>
                <OfferTable>

                </OfferTable>
            </BlockOffer>
    )
}


export default compose()(ClientAddress)
//
const BlockOffer = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  background-color: #fff;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`;

const OfferHeader = styled.div`
  background-color: red;
`;

const OfferBody = styled.div`
  
`;
const OfferTable = styled.div`
 
`;
const BlockMenuItem = styled.div`
 padding: 10px 5px;
 border-radius: 4px;
 transition: ease 0.4s;
 cursor: pointer;
 text-align: center;
 display: flex;
 flex-direction: column;
 &:hover {
  background-color: rgba(255,255,255,0.2);
 }
`;
const ItemIconBlock = styled.div`
    width: 40px;
    height: 40px;
    margin: 0 auto;
    & > svg {
      width: 100%;
      height: 100%;
      
    }
`;

const ItemNameBlock = styled.div`
 font-weight: 600;
 font-size: 10px;
 margin-top: 10px;  
 color: rgba(255,255,255,0.7);
 
`;