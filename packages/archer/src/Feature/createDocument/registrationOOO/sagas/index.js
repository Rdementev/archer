import {getRegOrgan} from "FeatureARDM/createDocument/components/type_reg_organ/sagas";
import {fork, all} from "redux-saga/effects";


export function* registrationOOOSaga() {
    yield all([
        fork(getRegOrgan),

    ])
}
