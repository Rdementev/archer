import {BreadCrumb} from "@rdementev/lib";
import React from "react";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import {withTheme} from "styled-components/macro";

const BreadcrumbContainer = (props) => {
    const {history} = props
    const styled = {
        container: {},
        item: {},
        seporator: {},
        lastItem:{},
    }
    const handleClickLink = (link) => {
        history.push(link)
    }

    return <BreadCrumb seporator={'/'} styled={styled} onClick={handleClickLink} {...props} />
}
export default compose(withRouter, withTheme)(BreadcrumbContainer)