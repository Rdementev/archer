import React, {useEffect, useRef, useState} from 'react'
import {compose} from "redux";
import styled from "styled-components/macro";
import TypeUstav from "FeatureARDM/createDocument/registrationOOO/components/ustav_nalog/ustav";
import TypeRegOrgan from "FeatureARDM/createDocument/components/type_reg_organ";
import TypeSysNalContainer from "FeatureARDM/createDocument/components/type_sys_nalog";
import TypeOkved from "FeatureARDM/createDocument/components/type_okved";
import TypeRateNalContainer from "FeatureARDM/createDocument/components/type_rate_nalog";
import {connect} from "react-redux";
import TypeSecretar from "./type_secretar";
import {
    getARDMFeatureRegistrationOOOTypeIndividualsReselect,
    getARDMFeatureRegistrationOOOTypeOrganizationsReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect,
    getARDMFeatureRegistrationOOOTypeSysNalReselect,
    getARDMFeatureRegistrationOOOTypePredsidatelReselect,
    getARDMFeatureRegistrationOOOTypeRateNalReselect,
    getARDMFeatureRegistrationOOOTypeSecretarReselect,
    getARDMFeatureRegistrationOOOTypeRegOrganReselect,
    getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect
} from "../../reducer";
import TypePredsidatel from "./type_predsidatel";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import {getARDMOrganizationReselect} from "PagesARDM/organization/reselect";
import {
    ardmFeatureRegOOOSetSysNalog,
    ardmFeatureRegOOOSetRateNalog,
    ardmFeatureRegOOOSetRegOrgan,
    ardmFeatureRegOOOSetSelectedOkveds, ardmFeatureRegOOOSetFavoriteOkved,
    ardmFeatureRegOOOChangeValidationUNO,
    ardmFeatureRegOOOChangeRegOrgan
} from "../../actions";
import {checkUNORegistrationOOO} from "FeatureARDM/createDocument/registrationOOO/utils";


const UstavNalog = (props) => {
    const {type_sys_nal,
        ardmFeatureRegOOOSetRateNalog,
        ardmFeatureRegOOOSetSysNalog,
        personal,
        ardmFeatureRegOOOChangeRegOrgan,
        organization,
        type_reg_organ,ardmFeatureRegOOOSetRegOrgan, ardmFeatureRegOOOSetSelectedOkveds, ardmFeatureRegOOOSetFavoriteOkved,
        type_selected_okved, type_reg, type_individuals, type_organizations , type_secretar, type_rate_nal, type_predsidatel} = props
    const [isUSN, setIsUSN] = useState(false)
    const [isShowSecretar, setIsShowSecretar] = useState(false)
    const [heightOkved, setHeightOkved] = useState(false)
    const [listIndividuals, setListIndividuals] = useState([])
    const [listOrganizations, setListOrganizations] = useState([])
    const [list, setList] = useState([])
    const typeOkvedRef = useRef(null)





    //преобразовываем массив type_individuals для selectsearch
    const getTransformIndividuals = (type_individuals) => {
        let newList = []
        let newItems = []
        type_individuals.map(item => {
            //получаем имя учредителя
            const pers = personal.find(per => per.id === item.clientId)
            if(!pers) return
            const name = pers.last_name.concat(' ' , pers.first_name, ' ', pers.second_name )
            const newObj = {...item, name: name, type: 'individuals'}
            newItems = [newObj, ...newItems]
            newList = [
                {
                    id:1,
                    type: 'group',
                    name: 'Физические лица',
                    items: newItems
                }]
        })
        setListIndividuals(newList)
    }
    //преобразовываем массив type_organizations для selectsearch
    const getTransformOrganizations = (type_organizations) => {
        let newList = []
        let newItems = []
        type_organizations.map(item => {
            //получаем организацию
            const org = organization.find(org => org.id === item.clientId)
            if(!org) return
            // получаем имя представителя
            const name = org.name
            const newObj = {...item, name: `ООО «${name}»`, type: 'organizations'}
            newItems = [newObj, ...newItems]
            newList = [
                {
                    id:1,
                    type: 'group',
                    name: 'Юридические лица',
                    items: newItems
                }]
        })
        setListOrganizations(newList)
    }
    //объеденяем массивы учредителей
    const getConcatFounders = (listIndividuals, listOrganizations) => {
        const newList = listIndividuals.concat(listOrganizations)
        setList(newList)
    }

    useEffect(()=> {
        getTransformIndividuals(type_individuals)
    },[type_individuals])
    useEffect(()=> {
        getTransformOrganizations(type_organizations)
    },[type_organizations])
    useEffect(()=> {
        getConcatFounders(listIndividuals, listOrganizations)
    },[listIndividuals, listOrganizations])
        //определяем показываем ли блок секретарь и председатель
    useEffect(() => {
        const count = type_individuals.length + type_organizations.length
        if(count > 1){
            setIsShowSecretar(true)
        }
    },[type_individuals, type_organizations])
        // определяем показываем ли блок ставка налогообложения
    useEffect(() => {
        if (type_sys_nal !== 'Упрощённая система налогообложения (УСН)') {
            ardmFeatureRegOOOSetRateNalog('')
            return setIsBlockRate(true)
        }
        setIsBlockRate(false)
    }, [type_sys_nal])
        // если упращенная регистарция то система налогообложения только усн
    useEffect(() => {
        if (type_reg === 'Упращенная регистрация ООО') {
            return setIsUSN(true)
        }
        setIsUSN(false)
    }, [type_reg])


    const [isBlockRate, setIsBlockRate] = useState(false)
    return (
        <Container>
            <Grid>
                <Block>
                    <SubBlock>
                        <SubGrid>
                            <Content>
                                <TypeUstav/>
                            </Content>
                        </SubGrid>
                    </SubBlock>
                    <SubBlock>
                        <SubGrid>
                            <Content>
                                <TypeSysNalContainer typeSysNal={type_sys_nal}
                                                     setTypeSysNal={ardmFeatureRegOOOSetSysNalog}
                                                     isUSN={isUSN}
                                                     setIsBlockRate={setIsBlockRate}/>
                            </Content>
                        </SubGrid>
                    </SubBlock>
                    <SubBlock>
                        <SubGrid>
                            {isBlockRate && <Overlay/>}
                            <Content>
                                <TypeRateNalContainer typeRateNal={type_rate_nal}
                                                      setTypeRateNal={ardmFeatureRegOOOSetRateNalog}
                                                      isUSN={isUSN}/>
                            </Content>
                        </SubGrid>
                    </SubBlock>
                </Block>
            </Grid>
            <Grid>
                <Block>
                    {isShowSecretar ? (
                        <>
                            <SubBlockIspl>
                                <SubGridSecretar>
                                    <Content>
                                        <TypeSecretar selected={type_secretar}
                                                      selectedPredstavitel={type_predsidatel}
                                                      list={list}/>
                                    </Content>
                                </SubGridSecretar>
                                <SubGridPredsidatel>
                                    <Content>
                                        <TypePredsidatel selected={type_predsidatel}
                                                         selectedSecretar={type_secretar}
                                                         list={list}/>
                                    </Content>
                                </SubGridPredsidatel>
                            </SubBlockIspl>
                            <SubBlock>
                                <SubGrid>
                                    <Content ref={typeOkvedRef}>
                                        <TypeOkved heightOkved={heightOkved}
                                                   type_selected_okved={type_selected_okved}
                                                   setFavorite={ardmFeatureRegOOOSetFavoriteOkved}
                                                   setSelected={ardmFeatureRegOOOSetSelectedOkveds}/>
                                    </Content>
                                </SubGrid>
                            </SubBlock>
                        </>
                    ) : (
                        <Content ref={typeOkvedRef}>
                            <TypeOkved heightOkved={heightOkved}
                                       type_selected_okved={type_selected_okved}
                                       setFavorite={ardmFeatureRegOOOSetFavoriteOkved}
                                       setSelected={ardmFeatureRegOOOSetSelectedOkveds}/>
                        </Content>
                    )}
                </Block>
            </Grid>
            <Grid>
                <Block>
                    <Content>
                        <TypeRegOrgan type_reg_organ={type_reg_organ}
                                      onChange={ardmFeatureRegOOOChangeRegOrgan}
                                      func={ardmFeatureRegOOOSetRegOrgan}/>
                    </Content>
                </Block>
            </Grid>
        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        type_sys_nal: getARDMFeatureRegistrationOOOTypeSysNalReselect(state),
        type_reg: getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_individuals: getARDMFeatureRegistrationOOOTypeIndividualsReselect(state),
        type_organizations: getARDMFeatureRegistrationOOOTypeOrganizationsReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        organization: getARDMOrganizationReselect(state),
        type_secretar: getARDMFeatureRegistrationOOOTypeSecretarReselect(state),
        type_predsidatel: getARDMFeatureRegistrationOOOTypePredsidatelReselect(state),
        type_rate_nal: getARDMFeatureRegistrationOOOTypeRateNalReselect(state),
        type_reg_organ: getARDMFeatureRegistrationOOOTypeRegOrganReselect(state),
        type_selected_okved: getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect(state),


    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetRateNalog, ardmFeatureRegOOOSetSysNalog, ardmFeatureRegOOOSetRegOrgan,
        ardmFeatureRegOOOSetSelectedOkveds,ardmFeatureRegOOOSetFavoriteOkved, ardmFeatureRegOOOChangeValidationUNO,
        ardmFeatureRegOOOChangeRegOrgan
    })
)(UstavNalog)
//
const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  height: 100%;
`;
const Block = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 14px;
  display: flex;
  flex-direction: column;
`;
const SubBlock = styled.div`
  width: 100%;
  height: 50%;
  border-radius: 14px;
  display: flex;
  flex-grow: 1;
  &:last-child{
  padding-top: 5px
  }
  &:first-child{
  padding-bottom: 5px
  }
`;
const Grid = styled.div`
  width: 100%;
  padding: 0 10px 0 0;
  overflow: auto;
   &:last-child{
      padding: 0;
  }
`;
const SubGrid = styled.div`
  position: relative;
  width: 100%;

`;
const SubBlockIspl = styled.div`
  display: flex;
  height: 30%;
`;
const SubGridSecretar = styled(SubGrid)`
  margin-right: 5px;
`;
const SubGridPredsidatel = styled(SubGrid)`
  margin-left: 5px;
`;
const Content = styled.div`
  width: 100%;
  padding: 10px 0;
  background: #fff;
  border-radius: 4px;
  height: 100%;
  display: flex;
  flex-direction: column;  
`;
const Overlay = styled.div`
    z-index: 1;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background: #f5f8fa;
    border-radius: 4px;
`;

