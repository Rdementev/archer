import React, {useEffect, useRef, useState} from 'react'
import {connect} from "react-redux";
import styled, {withTheme} from 'styled-components/macro'
import {compose} from "redux";
import {transparentize} from "polished";
import {withRouter} from "react-router-dom";
import {ActionButton} from "@rdementev/lib";
import ModalCreateDocument from "../modalCreateDocument";
import NewModal from "FeatureARDM/modal/newmodal";
import SearchBlockContainer from "ComponentsARJTTD/searchBlock/searchBlock";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFileInvoiceDollar, faStoreAltSlash, faTasks, faUser, faUsers} from "@fortawesome/free-solid-svg-icons";
import CSSTransition from "react-transition-group/CSSTransition";
import {setFiltersSubheaderARDMPagedoc} from "PagesARDM/pagedocs/reducer/pageDocList";
import {getARDMPagedocsSearchKeyReselect, changeARDMPagedocsSearchKey} from "PagesARDM/pagedocs/reducer/pageDocList";
import {ardmPagesGetFormsPagedocsRequest} from "PagesARDM/pagedocs/actions";

const templatesIconUrFace = {
    ooo : <FontAwesomeIcon icon={faUsers}/>,
    ip : <FontAwesomeIcon icon={faUser}/>,
}
const templatesIconTypeDoc = {
    reg : <FontAwesomeIcon icon={faFileInvoiceDollar}/>,
    change : <FontAwesomeIcon icon={faTasks}/>,
    liq : <FontAwesomeIcon icon={faStoreAltSlash}/>,

}

const HeaderClient = (props) => {
    const { search_key, changeARDMPagedocsSearchKey,theme ,setFiltersSubheaderARDMPagedoc, ardmPagesGetFormsPagedocsRequest} = props
    const [showModal, setShowModal] = useState(false)
    const [onFocus, setOnFocus] = useState(false)
    const SearchRef = useRef(null)

    useEffect(()=>{
        document.addEventListener('click', handleClickOutSideSearch, false)
        return function () {
            document.removeEventListener('click', handleClickOutSideSearch, false)
        }
    },[])

    const StyledActionButton = {
        button: {
            background: theme.semiHeavy,
            border: '1px solid transparent',
            '&:hover': {
                borderColor: theme.medium,
            },
        },
        blockIcon: {
            background: theme.medium
        }
    }
    const StyledSelectSearch = {
        list: {},
        suggestion: {
            top: '100%',
            borderRadius: '0',
            background: theme.medium,
            border: '1px solid',
            borderColor: theme.semiHeavy,
            padding: '5px',
            borderBottomLeftRadius: '4px',
            borderBottomRightRadius: '4px',
        },
        group: {},
        itemGroup: {},
        blockInput: {
        },
        input: {
            fontSize: '10px',
            borderRadius: '4px',
            padding: '10px 48px 10px 10px',
            borderBottomLeftRadius: '0',
            borderBottomRightRadius: '0',
            background: theme.medium,
            border: '1px solid',
            borderBottom: 'none',
            borderColor: theme.semiHeavy,
            color: theme.root,
            height: '32px',
            transition: 'ease 0.3s',
            '&:hover' :{
                borderColor: theme.semiHeavy,
                color: theme.root
            },
            '&:focus' :{
                borderColor: theme.semiHeavy,
                color: theme.root
            },
            '&::placeholder': {
                color: 'rgba(255,255,255,0.7)'
            },
        },
        buttonSelect: {
            padding: '10px 0 10px 10px',
            background: theme.semiHeavy,
            borderColor: theme.semiHeavy,
            transition: 'ease 0.3s',
            '&:hover' :{
                borderColor: theme.medium,
            },
        },
        styledContainer: { },
        blockIcon: {

            '& > svg ' : {
                width: '10px',
                height: '10px',
            },

        },
        blockIconInput: {
            border: 'none',
            '& > svg ' : {
                width: '10px',
                height: '10px',
            },
        },
        blockClear : {
            right: '20px',
            borderLeft: `1px solid ${theme.semiHeavy}`
        },
        blockIconClear: {
            background: theme.heavy,
            borderColor: theme.semiHeavy,
            width: '10px',
            height: '10px',
        },
        itemBlock: {
            fontSize: '10px',
            borderRadius: '4px',
            color: 'rgba(255,255,255,0.7)',
            '&:hover' : {
                background: 'rgba(255,255,255,0.1)',
                color: '#fff'
            },
            '& > div' : {
                width: '30px'
            },
            '& > div > svg > path ' : {
                fill: '#fff'
            },
        },
    }

    //юридическое лицо
    const [displayValueUrFace, setDisplayValueUrFace] = useState('Юридическое лицо')
    const [inputValueUrFace, setInputValueUrFace] = useState('Юридическое лицо')
    const handleClickItemUrFace = (selected, multi, other) => {
        const Icon = selected[0].db_type ? templatesIconUrFace[selected[0].db_type] : ''
        if (!multi) {
            setDisplayValueUrFace(
                <DisplayValue>
                    <BlockIcon>
                        {Icon}
                    </BlockIcon>
                    <DisplayValueText>{selected[0].name}</DisplayValueText>
                </DisplayValue>
            )
            setInputValueUrFace(selected[0].value)
            const select = selected[0].type
            const type = 'ur_face_ids'
            setFiltersSubheaderARDMPagedoc(type, select)

        }
    }
    const ListDataUrFace =  [
        {
            id: 1,
            name: 'Общество с ограниченной ответственностью',
            value: '',
            type: 'ООО',
            db_type: 'ooo',
            icon: <FontAwesomeIcon icon={faUsers}/>
        },
        {
            id: 2,
            name: 'Индивидуальный предприниматель',
            value: '',
            type: 'ИП',
            db_type: 'ip',
            icon: <FontAwesomeIcon icon={faUser}/>
        },
    ]
    const handleClickClearUrFace = () => {
        setDisplayValueUrFace('Юридическое лицо')
        setInputValueUrFace('Юридическое лицо')
        const type = 'ur_face_ids'
        setFiltersSubheaderARDMPagedoc(type, null)
    }

    //тип документа
    const [displayValueTypeDoc, setDisplayValueTypeDoc] = useState('Тип документа')
    const [inputValueTypeDoc, setInputValueTypeDoc] = useState('Тип документа')
    const handleClickItemTypeDoc = (selected, multi, other) => {
        const Icon = selected[0].db_type ? templatesIconTypeDoc[selected[0].db_type] : ''
        if (!multi) {
            setDisplayValueTypeDoc(
                <DisplayValue>
                    <BlockIcon>
                        {Icon}
                    </BlockIcon>
                    <DisplayValueText>{selected[0].name}</DisplayValueText>
                </DisplayValue>
            )
            setInputValueTypeDoc(selected[0].value)
            const select = selected[0].id
            const type = 'type_doc_ids'
            setFiltersSubheaderARDMPagedoc(type, select)
        }
    }
    const ListDataTypeDoc =  [
        {
            id: 1,
            name: 'Регистрация',
            value: '',
            type: 'reg',
            db_type: 'reg',
            icon: <FontAwesomeIcon icon={faFileInvoiceDollar}/>,
        },
        {
            id: 2,
            name: 'Внесение изменений',
            value: '',
            type: 'change',
            db_type: 'change',
            icon: <FontAwesomeIcon icon={faTasks}/>,
        },
        {
            id: 3,
            name: 'Ликвидация',
            value: '',
            type: 'liq',
            db_type: 'liq',
            icon: <FontAwesomeIcon icon={faStoreAltSlash}/>,
        },
    ]
    const handleClickClearTypeDoc = () => {
        setDisplayValueTypeDoc('Тип документа')
        setInputValueTypeDoc('Тип документа')
        const type = 'type_doc_ids'
        setFiltersSubheaderARDMPagedoc(type, null)
    }

    const handleClickCreate = (e) => {
        setShowModal(!showModal)
    }

    //searchBLock
    const handleClickOutSideSearch = (e) => {
        const item = SearchRef.current
        if(e.path){
            if(!e.path.includes(item)){
                setOnFocus(false)
            }
        }
    }

    useEffect(() => {
        document.addEventListener('keypress' , handleKeypress)
        return function () {
            document.removeEventListener('keypress' , handleKeypress)
        }
    },[search_key])

    const handleClickSearchBlock = (value) => {
        if(value === 'clear') {
            changeARDMPagedocsSearchKey('')
        }
         ardmPagesGetFormsPagedocsRequest()
    }
    const handleKeypress = (e) => {
        if(e.which === 13 && search_key){
            ardmPagesGetFormsPagedocsRequest()
        }
    }



    return (
        <StyledHeaderClient>
            <StyleActionButton>
                <ActionButton action={'create'}
                              title={'Создать документ'}
                              styled={StyledActionButton}
                              onClick={handleClickCreate}/>

            </StyleActionButton>
            <BlockSelectSearch>
            <SelectSearchContainer onClick={handleClickItemUrFace}
                                   displayValue={displayValueUrFace}
                                   placeholder={inputValueUrFace}
                                   onClickClear={handleClickClearUrFace}
                                   list={ListDataUrFace}
                                   styled={StyledSelectSearch}/>
            </BlockSelectSearch>
            <BlockSelectSearch>
                <SelectSearchContainer onClick={handleClickItemTypeDoc}
                                       onClickClear={handleClickClearTypeDoc}
                                       displayValue={displayValueTypeDoc}
                                       placeholder={inputValueTypeDoc}
                                       list={ListDataTypeDoc}
                                       styled={StyledSelectSearch}/>
            </BlockSelectSearch>
            <CSSContainer>
                <CSSTransition
                    timeout={{appear: 0, enter: 0, exit: 0}}
                    in={onFocus}
                    classNames="search-block">
                    <BlockSearchBlock onClick={() => {setOnFocus(true)}} ref={SearchRef}>
                        <SearchBlockContainer  onChange={changeARDMPagedocsSearchKey}
                                               onClick={handleClickSearchBlock}
                                               enter={(search_key && onFocus)}
                                               clear={(search_key && onFocus)}
                                               value={search_key}/>
                    </BlockSearchBlock>
                </CSSTransition>
            </CSSContainer>

            <NewModal showModal={showModal} >
                <ModalCreateDocument setShowModal={setShowModal}/>
            </NewModal>
        </StyledHeaderClient>
    )
}
const mapStateToProps = (state) => {
    return {
        search_key: getARDMPagedocsSearchKeyReselect(state)
    }
}

export default compose(
    connect(mapStateToProps,{
        changeARDMPagedocsSearchKey,setFiltersSubheaderARDMPagedoc, ardmPagesGetFormsPagedocsRequest
    }),
    withRouter,
    withTheme
)(HeaderClient)
//
const StyledHeaderClient = styled.div`
    height: 52px;
    background-color: ${props => transparentize(0, props.theme.heavy) };
    padding: 10px 20px;
    display: flex;
`;


const StyleActionButton = styled.div`
    cursor: pointer;
    color: ${props => props.theme.root};
    margin-right: auto;
`;

const DisplayValue = styled.div`
    display: flex;
    white-space: nowrap;
    overflow: hidden;
    align-items: center;
`;
const DisplayValueText = styled.div`
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 10px;
`;
const BlockSearchBlock = styled.div`
    width: 185px;
    height: 100%;
    max-height: 32px;
    color: ${props => props.theme.color}
`;
const BlockIcon = styled.div`
    height: 20px;
    margin-right: 10px;
    & > svg {
      width : 15px;
      height: 15px;
    }
`;
const BlockSelectSearch = styled.div`
    width: 220px;
    margin-right: 10px;
`;
const CSSContainer = styled.div`
    .search-block-appear{
        width: 185px;
    }
    .search-block-enter{
        width: 185px;
    }
    .search-block-enter-done{
        width: 370px;
        transition: 0.5s;
    }
    .search-block-exit{
        width: 370px;
    }
    .search-block-exit-done{
        width: 185px;
        transition: 0.5s;
    }
`;

