const { addBeforeLoader, loaderByName } = require("@craco/craco");
const path = require('path')
const fs = require('fs')
const cracoBabelLoader = require('craco-babel-loader')
const appDirectory = fs.realpathSync(process.cwd())
const resolvePackage = relativePath => path.resolve(appDirectory, relativePath)


module.exports = {
    webpack: {
        configure: webpackConfig => {
            const jsxLoader = {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }

            addBeforeLoader(webpackConfig, loaderByName("file-loader"), jsxLoader );

            const scopePluginIndex = webpackConfig.resolve.plugins.findIndex(
                ({ constructor }) => constructor && constructor.name === 'ModuleScopePlugin'
            );

            webpackConfig.resolve.plugins.splice(scopePluginIndex, 1);
            return webpackConfig;
        },
        alias: {
            // ====================================================== //
            ComponentsARDM: path.resolve(__dirname, "src/Components/"),
            FeatureARDM: path.resolve(__dirname, "src/Feature/"),
            PagesARDM: path.resolve(__dirname, "src/Pages/"),
            ReducersARDM: path.resolve(__dirname, "src/Reducers/"),
            RoutesARDM: path.resolve(__dirname, "src/Routes/"),
            SagasARDM: path.resolve(__dirname, "src/Sagas/"),
            CoreARDM: path.resolve(__dirname, "src/Core"),
            IconsARDM: path.resolve(__dirname, "src/Icons/"),
            TemplatesARDM: path.resolve(__dirname, "src/Templates/"),
            UtilsARDM: path.resolve(__dirname, "src/Utils/"),
            // ====================================================== //


            // ====================================================== //
            ActionsARCS: "arcs/src/Actions/",
            ComponentsARCS: "arcs/src/Components/",
            CoreARCS: "arcs/src/Core/",
            FeatureARCS: "arcs/src/Feature/",
            PagesARCS: "arcs/src/Pages/",
            IconsARCS: "arcs/src/Icons/",
            ReducersARCS: "arcs/src/Reducers/",
            SagasARCS: "arcs/src/Sagas/",
            UtilsARCS: "arcs/src/Utils/",
            ExportARCS: "arcs/src/App.js",
            PublicARCS: "arcs/public",
            RootReducerARCS: "arcs/src/Reducers/rootReducer.js",
            // ====================================================== //

            // ====================================================== //
            ComponentsARJTTD: "Space_jttd/src/Components/",
            ReducerARJTTD: "Space_jttd/src/Reducer/",
            ApiARJTTD: "Space_jttd/src/Api/",
            HOCARJTTD: "Space_jttd/src/HOC/",
            IconsARJTTD: "Space_jttd/src/Icons/",
            SagasARJTTD: "Space_jttd/src/Sagas/",
            UtilsARJTTD: "Space_jttd/src/Utils/",
            // ====================================================== //

            // ====================================================== //
            ActionsARLA: "ARLA/src/Actions/",
            ComponentsARLA: "ARLA/src/Components/",
            CoreARLA: "ARLA/src/Core/",
            FeatureARLA: "ARLA/src/Feature/",
            PagesARLA: "ARLA/src/Pages/",
            ReducersARLA: "ARLA/src/Reducers/",
            SagasARLA: "ARLA/src/Sagas/",
            UtilsARLA: "ARLA/src/Utils/",
            ExportARLA: "ARLA/src/App.js",
            IconsARLA: "ARLA/src/Icons/",
            RootReducerARLA: "ARLA/src/Reducers/rootReducer.js",
            // ====================================================== //

        },
        resolve: { extensions: [".jsx", ".js", ".json"] },
    },
    babel: {
        presets: [
            '@babel/preset-env',
            '@babel/react',
        ],
        plugins: [
            ['@babel/plugin-proposal-class-properties', {"loose": true}],
        ]
    },
    plugins: [
        {
            plugin: cracoBabelLoader,
            options: {
                includes: [
                    resolvePackage('../arcs'),
                    resolvePackage('../arjttd'),
                    resolvePackage('../ARLA'),
                ],
            },
        },
    ],

};

