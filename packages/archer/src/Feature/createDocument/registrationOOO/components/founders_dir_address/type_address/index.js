import React, {useEffect, useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import {
    getARDMFeatureRegistrationOOOTypeAddressIdReselect,
} from "../../../reducer";
import styled from "styled-components/macro";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import ChoiseAddress from "PagesARDM/address";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {getARDMFeatureRegistrationOOOTypeIndividualsReselect} from "FeatureARDM/createDocument/registrationOOO/reducer";
import {ardmFeatureRegOOOSetAddressId} from "FeatureARDM/createDocument/registrationOOO/actions";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import RequiredField from "ComponentsARJTTD/requiredField";
import UpdateAddress from "Pages/address/components/update";



const TypeAddress = (props) => {
    const {address, type_address_id, type_dir_is_dir_address, type_individuals, ardmFeatureRegOOOSetAddressId, personal} = props
    const [show, setShow] = useState(false)
    const [addressObj, setAddressObj] = useState(false)
    const [name, setName] = useState(false)
    const [list, setList] = useState([])
    const [value, setValue] = useState('')
    const [isFounderAddress, setIsFounderAddress] = useState(false)
    const [showUpdate, setShowUpdate] = useState(false)
    const [editAddressId, setEditAddressId] = useState(false)

    useEffect(()=> {
        const obj =  address.find(item => item.id === type_address_id)
        if(obj) return setAddressObj(obj)
        setAddressObj(false)
    },[type_address_id, address, type_dir_is_dir_address])

    useEffect(()=> {
        setValue('')
    },[isFounderAddress])

    useEffect(()=> {
        if(show === true){
            ardmFeatureRegOOOSetAddressId(null)
            setIsFounderAddress(false)
        }
    },[show])


    useEffect(()=> {
        if(type_individuals && type_individuals.length < 1 && isFounderAddress){
            ardmFeatureRegOOOSetAddressId(null)
            setIsFounderAddress(false)
            setList([])
            setValue('')
        }
    },[type_individuals, isFounderAddress])



    useEffect(()=> {
        if(addressObj){
            const region_with_type = addressObj.region && addressObj.region.concat(' ', addressObj.region_type_full)
            const city_with_type = addressObj.city_type_full && addressObj.city_type_full.concat(' ', addressObj.city)
            const settlement_with_type = addressObj.settlement_type_full && addressObj.settlement_type_full.concat(' ', addressObj.settlement)
            const street_with_type = addressObj.street_type_full.concat(' ', addressObj.street)
            const addressOneString = addressObj.region === addressObj.city
                ? city_with_type.concat(' ',
                    settlement_with_type === null ? '' : settlement_with_type, ' ',
                    street_with_type === null ? '' : street_with_type, ' ',
                    addressObj.house_type_full === null ? '' : addressObj.house_type_full, ' ',
                    addressObj.house === null ? '' : addressObj.house, ' ',
                    addressObj.block_type_full === null ? '' : addressObj.block_type_full, ' ',
                    addressObj.block === null ? '' : addressObj.block, ' ',
                    addressObj.flat_type_full === null ? '' : addressObj.flat_type_full, ' ',
                    addressObj.flat === null ? '' : addressObj.flat)

                : region_with_type.concat(' ',
                    city_with_type === null ? '' : city_with_type, ' ',
                    settlement_with_type === null ? '' : settlement_with_type, ' ',
                    street_with_type === null ? '' : street_with_type, ' ',
                    addressObj.house_type_full === null ? '' : addressObj.house_type_full, ' ',
                    addressObj.house === null ? '' : addressObj.house, ' ',
                    addressObj.block_type_full === null ? '' : addressObj.block_type_full, ' ',
                    addressObj.block === null ? '' : addressObj.block, ' ',
                    addressObj.flat_type_full === null ? '' : addressObj.flat_type_full, ' ',
                    addressObj.flat === null ? '' : addressObj.flat)
            return setName(addressOneString)
        }
        setName('')
    },[addressObj])

    useEffect(()=>{
        getTransformIndividuals(type_individuals)
    },[type_individuals])



    const handleClickAddress = () => {
        setEditAddressId(addressObj.id)
        setShowUpdate(true)

    }

    const handleChangeFirmAddress = (e) => {
        const value = e.target.checked
        setIsFounderAddress(value)
        ardmFeatureRegOOOSetAddressId(null)
    }

    //преобразовываем массив type_individuals для selectsearch
    const getTransformIndividuals = (type_individuals) => {
        let newList = []
        type_individuals.map(item => {
            //получаем имя учредителя
            const pers = personal.find(per => per.id === item.clientId)
            if(!pers) return
            const name = pers.last_name.concat(' ' , pers.first_name, ' ', pers.second_name )
            const newObj = {...item, name: name}
            newList = [newObj, ...newList]
        })
        setList(newList)
    }

    const handleClickItemSelect = (item, multi, other) => {
        setValue(item[0].name)
        const pers = personal.find(pers => pers.id === item[0].clientId)
        const addresId = pers.address_id
        ardmFeatureRegOOOSetAddressId(addresId)
    }

    // клик на адресс из списка
    const handleClickAddressOnList = (id) => {
        ardmFeatureRegOOOSetAddressId(id)
        setShow(false)
    }

    return (
        <>
            <Container>
                <Header>
                    <Title>Сведения об адресе</Title>
                    <RequiredField/>
                </Header>
                <Offer>
                    {isFounderAddress && type_individuals && type_individuals.length > 0 &&
                        <BlockSelectSearch>


                    <SelectSearchContainer list={list}
                                           displayValue={value}
                                           value={value}
                                           onClick={handleClickItemSelect}/>
                        </BlockSelectSearch>
                    }
                    <StyledAddress onClick={()=>{handleClickAddress()}}>
                        {name}
                    </StyledAddress>
                </Offer>
                <Footer>
                    <StyledButton onClick={()=>{setShow(true)}}>
                        <Button>Выбрать</Button>
                    </StyledButton>
                    {type_individuals && type_individuals.length > 0 && <StyledCheckbox>
                        <Checkbox onChange={(e) => {handleChangeFirmAddress(e)}}
                                  checked={isFounderAddress}
                                  rightText={'Зарегистрировать фирму на домашний адрес учредителя'}/>
                    </StyledCheckbox>}
                </Footer>
            </Container>
            <ChoiseAddress show={show} setShow={setShow} onClickAddress={handleClickAddressOnList} />
            {showUpdate && <UpdateAddress show={showUpdate}
                                          setShow={setShowUpdate}
                                          editAddressId={editAddressId}/>}
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        type_address_id: getARDMFeatureRegistrationOOOTypeAddressIdReselect(state),
        address: getArdmPagesAddressReselect(state),
        type_individuals: getARDMFeatureRegistrationOOOTypeIndividualsReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state,)
    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetAddressId
    }),
)(TypeAddress)
//
const StyledCheckbox = styled.div`
   & > label > p {
    max-width: 250px;
   }
`;
const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    position: relative;
`;
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
    padding: 5px 10px;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;
const BlockSelectSearch = styled.div`
    height: 32px;
`;
const StyledAddress = styled.div`
    cursor: pointer;
    margin-top: auto;
`;
const StyledButton = styled.div`
    width: 150px;
    margin-right: 20px;
`;
const Footer = styled.div`
    padding: 10px;
    margin-top: auto;
    display: flex;
`;
