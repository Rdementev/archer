import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import styled from "styled-components/macro";
import { ardmAuthGetLogin} from "./action";
import Input from "../components/input";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import {NavLink} from "react-router-dom";
import {getIsMistakeSigninReselect} from "./reducer";
import {ardmAuthSigninChange} from "PagesARDM/auth/signIn/action";
import {getEmailSigninReselect, getPasswordSigninReselect} from "PagesARDM/auth/signIn/reducer";




const SignIn = (props) => {
    const { password, email, ardmAuthGetLogin, isMistakeLogin, ardmAuthSigninChange } = props

    useEffect(()=>{
        document.addEventListener('keypress', handleKeypress, false)
        return  function () {
            document.removeEventListener('keypress', handleKeypress, false)
        }
    },[password, email])
    const handleClick = () => {
        ardmAuthGetLogin(email, password)
    }
    const handleKeypress = (e) => {
        if(e.which === 13){
            ardmAuthGetLogin(email, password)
        }
    }

    return (
        <Container>
            <Row>
                <Block>
                    <Header>
                        <Title>AR Document manager</Title>
                    </Header>
                    <Offer>
                        <Items>
                            <OfferText>
                                <OfferSpan>
                                    Email
                                </OfferSpan>
                                <OfferSpanDec>
                                    *
                                </OfferSpanDec>
                            </OfferText>
                            <BlockInput>
                                <Input value={email} onChange={(e)=>{ardmAuthSigninChange('email', e.target.value)}}/>
                            </BlockInput>
                        </Items>
                        <Items>
                            <OfferText>
                                <OfferSpan>
                                    Password
                                </OfferSpan>
                                <OfferSpanDec>
                                    *
                                </OfferSpanDec>
                            </OfferText>
                            <BlockInput>
                                <Input type={'password'} value={password} onChange={(e)=>{ardmAuthSigninChange('password', e.target.value)}}/>
                            </BlockInput>
                        </Items>
                        {isMistakeLogin && <Error>
                            Не удается войти в систему. Пожалуйста, убедитесь, что ваш адрес электронной почты и пароль верны.
                        </Error>}
                        <BlockButton onClick={()=>{handleClick()}}>
                            <Button >Войти в систему</Button>
                        </BlockButton>
                    </Offer>
                    <OfferFooter>
                        <FooterBlock>
                            <FooterItem to={'/auth/reset_password'}>
                                Забыли пароль ?
                            </FooterItem>
                            <FooterItem to={'/auth/signup'}>
                                Написать в поддержку
                            </FooterItem>
                            <OfferSpan>
                                Еще нет аккаунта ?
                            </OfferSpan>
                            <FooterItem style={{display:'inline-block', margin: '0 0 0 10px'}} to={'/auth/signup'}>
                                Бесплтно получить аккаунт
                            </FooterItem>
                        </FooterBlock>
                    </OfferFooter>
                </Block>
                <BlockInfo>

                </BlockInfo>
            </Row>

            <Footer>

            </Footer>
        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        isMistakeLogin: getIsMistakeSigninReselect(state),
        email: getEmailSigninReselect(state),
        password: getPasswordSigninReselect(state),

    }
}

export default connect(mapStateToProps, {
    ardmAuthGetLogin, ardmAuthSigninChange
})(SignIn)
//

const Container = styled.div`
    width: 100vw;
    display: flex;
    flex-direction: column;
    min-height: 100vh;
`;
const Row = styled.div`
    display: flex;  
    height: 100%;
    flex-grow: 1;
`;
const Block = styled.div`
    width: 65%;
    display: flex;
    flex-direction: column;
`;
const BlockInfo = styled.div`
    width: 35%;
    display: flex;
    background: radial-gradient(circle, rgba(81,109,190,1) 0%, rgba(41,68,147,1) 76%);
`;
const Header = styled.div`
    padding: 120px 0 0 0;
`;
const OfferFooter = styled.div`
    margin: 30px auto 30px;
    width: 400px;
`;
const Title = styled.h3`
  color: #000;
  text-align: center;
    white-space: nowrap;
    letter-spacing: 9px;
    font-weight: 400;
    font-size: 20px;
    text-transform: uppercase;
    margin-bottom: 15px;
`;

const Offer = styled.div`
    width: 400px;
    margin: 50px auto 0 auto;
    display: flex;
    flex-direction: column;
   
`;
const OfferText = styled.div`
  display: flex;
  margin: 0 0 7px 0;
`;
const OfferSpan = styled.span`

`;
const OfferSpanDec = styled.span`
  margin-left: 5px;
  color: red;
`;
const BlockInput = styled.div`
    height: 33px;
    margin-bottom: 5px;
    width: 250px;
`;
const BlockButton = styled.div`
    width: 150px;
    height: 30px;
`;
const Items = styled.div`
    &:not(:last-child){
        margin-bottom: 30px;
    }
`;
const Error = styled.div`
    color: red;
    min-height: 18px;
    margin-bottom: 30px;
    font-size: 12px;
`;
const Footer = styled.div`
    display: flex;
    padding: 20px 0;
   
`;
const FooterBlock = styled.div`
    
`;
const FooterItem = styled(NavLink)`
    
    text-transform: none;
    text-decoration: none;
    font-size: 12px;
    font-weight: 600;
    margin-bottom: 10px;
    display: block;
    color: rgba(40,68,147,0.7);
    &:hover{
      color: #284493;
    }
`;


