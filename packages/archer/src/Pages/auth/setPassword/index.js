import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import styled from "styled-components/macro";
import {ardmAuthGetLogin, ardmAuthSetPasswordChangePassword} from "./action";
import Input from "../components/input";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import {NavLink} from "react-router-dom";
import {compose} from "redux";
import {withRouter} from "react-router";




const SetPassword = (props) => {
    const {location, ardmAuthSetPasswordChangePassword} = props
    const [password, setPassword] = useState('')
    const [passwordToo, setPasswordToo] = useState('')
    const [isMistakeSetPassword, setIsMistakeSetPassword] = useState(false)

    const data = location.search.replace('?email=', '' ).replace('auth_key=', '').split('&')

    useEffect(()=>{
        document.addEventListener('keypress', handleKeypress, false)
        return  function () {
            document.removeEventListener('keypress', handleKeypress, false)
        }
    },[])


    const handleClick = () => {
       if(password !== passwordToo || !password){
           setIsMistakeSetPassword(true)
       }else {
           setIsMistakeSetPassword(false)
           ardmAuthSetPasswordChangePassword(data[0], data[1], password)
       }
    }
    const handleKeypress = (e) => {
        if(e.which === 13 && password !== passwordToo || !password){
            setIsMistakeSetPassword(true)
        }else {
            setIsMistakeSetPassword(false)
            ardmAuthSetPasswordChangePassword(data[0], data[1], password)
        }

    }

    return (
        <Container>
            <Row>
                <Block>
                    <Header>
                        <Title>AR Document manager</Title>
                    </Header>
                    <Offer>
                        <Items>
                            <OfferText>
                                <OfferSpan>
                                    Password
                                </OfferSpan>
                                <OfferSpanDec>
                                    *
                                </OfferSpanDec>
                            </OfferText>
                            <BlockInput>
                                <Input type={'password'} value={password} onChange={(e)=>{setPassword(e.target.value)}}/>
                            </BlockInput>
                        </Items>
                        <Items>
                            <OfferText>
                                <OfferSpan>
                                    Confirm password
                                </OfferSpan>
                                <OfferSpanDec>
                                    *
                                </OfferSpanDec>
                            </OfferText>
                            <BlockInput>
                                <Input type={'password'} value={passwordToo} onChange={(e)=>{setPasswordToo(e.target.value)}}/>
                            </BlockInput>
                        </Items>
                        {isMistakeSetPassword && <Error>
                            Не удается изменить пароль. Пожалуйста, убедитесь, что введенные пароли совпадают.
                        </Error>}
                        <BlockButton onClick={()=>{handleClick()}}>
                            <Button >Изменить пароль</Button>
                        </BlockButton>
                    </Offer>
                </Block>
                <BlockInfo>

                </BlockInfo>
            </Row>

            <Footer>

            </Footer>
        </Container>
    )
}
const mapStateToProps = (state) => {
    return {

    }
}

export default compose(
    connect(mapStateToProps, {
        ardmAuthSetPasswordChangePassword
    }),
    withRouter,
)(SetPassword)
//

const Container = styled.div`
    width: 100vw;
    display: flex;
    flex-direction: column;
    min-height: 100vh;
`;
const Row = styled.div`
    display: flex;  
    height: 100%;
    flex-grow: 1;
`;
const Block = styled.div`
    width: 65%;
    display: flex;
    flex-direction: column;
`;
const BlockInfo = styled.div`
    width: 35%;
    display: flex;
    background: radial-gradient(circle, rgba(41,68,147,1) 0%, rgba(81,109,190,1) 76%);
`;
const Header = styled.div`
    padding: 120px 0 0 0;
`;
const OfferFooter = styled.div`
    margin: 30px auto 30px;
    width: 400px;
`;
const Title = styled.h3`
  color: #000;
  text-align: center;
    white-space: nowrap;
    letter-spacing: 9px;
    font-weight: 400;
    font-size: 20px;
    text-transform: uppercase;
    margin-bottom: 15px;
`;

const Offer = styled.div`
    width: 400px;
    margin: 50px auto 0 auto;
    display: flex;
    flex-direction: column;
   
`;
const OfferText = styled.div`
  display: flex;
  margin: 0 0 7px 0;
`;
const OfferSpan = styled.span`

`;
const OfferSpanDec = styled.span`
  margin-left: 5px;
  color: red;
`;
const BlockInput = styled.div`
    height: 33px;
    margin-bottom: 5px;
    width: 250px;
`;
const BlockButton = styled.div`
    width: 150px;
    height: 30px;
`;
const Items = styled.div`
    &:not(:last-child){
        margin-bottom: 30px;
    }
`;
const Error = styled.div`
    color: red;
    min-height: 18px;
    margin-bottom: 30px;
    font-size: 12px;
`;
const Footer = styled.div`
    display: flex;
    padding: 20px 0;
   
`;
const FooterBlock = styled.div`
    
`;
const FooterItem = styled(NavLink)`
    
    text-transform: none;
    text-decoration: none;
    font-size: 12px;
    font-weight: 600;
    margin-bottom: 10px;
    display: block;
    color: rgba(40,68,147,0.7);
    &:hover{
      color: #284493;
    }
`;


