import {store} from "Core/store";


export const clearReducer = (keys) => {
    const initialState = store.getState()
    if(keys === 'all'){
        const reducers = Object.keys(initialState).filter(item => item !== 'router' && item !== '_persist')
        reducers.map(item => {
            const action = 'RESET_'.concat(item.toUpperCase(), '_REDUCER')
            store.dispatch({type: action})
        })
        store.asyncReducers = {}
    } else {
        keys.map(item => {
            const action = 'RESET_'.concat(item.toUpperCase(), '_REDUCER')
            store.dispatch({type: action})
            store.asyncReducers[item] = null
        })
    }
    store.reInitialReducer()
}
// очистить localStorage
export const clearLocalStorage = () => {
    store.persistor.purge().then(r => r)
}
