import React, {PureComponent} from "react";
import {connect} from "react-redux";
import {NavLink, withRouter} from "react-router-dom";
import styled from "styled-components/macro";
import ClientMenu from "FeatureARDM/header/ClienMenu"
import ClientProfile from "FeatureARDM/header/ClientProfile";
import {transparentize} from "polished";
import ClientSupport from "FeatureARDM/header/clientSupport";
import {Pace} from "Icons/header";



const HeaderClient = (props) =>  {
    const { part } = props
        return (
            <Header>
                <ClientMenu/>
                <BlockLogo>
                    <BlockS to={'/serves'}>
                        <BlockSS>
                            <Pace/>
                        </BlockSS>
                    </BlockS>
                    <BlockSubLogo>
                        {part}
                    </BlockSubLogo>
                </BlockLogo>
                <HeaderBlock>
                    <ClientSupport/>
                    {/*<ClientNotification/>*/}
                    {/*<ClientChat/>*/}
                    <ClientProfile/>
                </HeaderBlock>
            </Header>
        );
}

const mapStateToProps = (state) => {
    return {
    }
}

export default withRouter(
    connect(mapStateToProps)(HeaderClient),
);

//
const Header = styled.header`
  height: 40px;
  width: 100%;
  padding: 0 20px 0 0;
  position: relative;
  display: flex;
  background-color: ${props => transparentize(0, props.theme.heavy) };
  color: #ffffff;
`;
const BlockSS = styled.div`
    display: flex;
    border-bottom: 1px solid transparent;
    transition: ease 0.3s;
`;
const BlockLogo = styled.div`
    display: flex;
    align-items: center;
    
`;
const BlockS = styled(NavLink)`
    height: 100%;
    padding: 12px 0 10px 0;
    display: flex;
    cursor: pointer;
    align-items: center;
    &:hover ${BlockSS} {
      border-color: #fff;
    }
`;

const BlockSubLogo = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    padding-bottom: 2px;
    margin-left: 3px;
    text-transform: none;
    line-height: 13px;
    max-height: 10px;
    font-size: 11px;
    color: rgba(255,255,255,0.8);  
`;

const HeaderBlock = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    width: 160px;
    justify-content: flex-end;
    margin-left: auto;
`;
