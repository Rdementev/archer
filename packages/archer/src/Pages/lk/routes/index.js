import React from "react";
import {Redirect, Route} from "react-router-dom";
import AdminLk from "../admin";
import ClientLk from "../client";


const getComponents = payload => {
    const {type = 'admin', props,  isAuth, isAdmin} = payload;


    const getComponent = (type, props) => {
        switch (type) {
            case 'user': {
                return <ClientLk {...props}/>
            }
            case 'admin': {
                return <AdminLk {...props}/>
            }
            // case 'agent': {
            //     return <AgentLk {...props}/>
            // }
        }
    }
    return getComponent(type, props)
};


const AccountRoute = (props) => {
    const {user,  ...rest} = props;
    const {permission:type,} = user
    return (
        <Route
            {...rest}
            render={props =>
                getComponents({
                    type,
                    props,
                })
            }
        />
    );
}

export default AccountRoute;
