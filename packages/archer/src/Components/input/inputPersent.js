import React, {useEffect, useState} from 'react'
import Input from "ComponentsARJTTD/input/inputComponent";
import styled from "styled-components/macro";

const InputPersent = (props) => {
    const { group, value, id, onChange} = props
    const [valueValid , setValueValid] = useState(null)
    const [locValue , setLocValue] = useState(value)
    const [color , setColor] = useState()

    useEffect(()=>{
        fas()
    },[value])


    const handleChange = (e) => {
        const foo = e.target.value
        const valueRex = /^\d+$/;
        if (valueRex.test(e.target.value) || foo.length < 1){
            if(+e.target.value < 101 && e.target.value !== "0" ){
                onChange(foo, id, 'peace_value')
                setLocValue(foo)
                return setValueValid(true);
            }
        }
        setValueValid(false);
    }
    const handleOnBlur = () => {
        if(valueValid === null){
            setColor('#950101')
        }
        if(!value){
            setColor('#950101')
        }
    }

    const fas = () => {
        if(valueValid === null){
            return setColor('#e0e4f1')
        }
        if(valueValid){
            return setColor('#068223')
        } setColor('#950101')
    }

    return (
            <Block >
                <BlockInput>
                    <Input style={{borderColor:`${color}`, borderRadius:'0', background:'#f5f8fa'}}
                           value={locValue}
                           onBlur={()=>{handleOnBlur()}}
                           onChange={(e)=>{handleChange(e)}}/>
                </BlockInput>

                <Persent>%</Persent>
            </Block>
    )
}

export default InputPersent

//

const Persent = styled.div`
    height: 100%;
    font-size: 13px;
    border: 1px solid #e0e4f1;
    padding: 2px;
    background-color: #eeeff4;
    border-left: none;
`;
const Block = styled.div`
   display: flex;
   height: 25px;

`;
const BlockInput = styled.div`
    width: 100px;
    height: 100%;
`;