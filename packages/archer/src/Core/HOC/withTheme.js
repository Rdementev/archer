import React from 'react'
import * as Redux from 'react-redux'
import {defaultTheme} from "../style/theme";


export const withTheme = (Component) => {
    return (props) => {
        let idTheme = Redux.useSelector(state => state.user.theme_id);
        if(!idTheme){
            idTheme = 5
        }
        return <Component {...props} theme={defaultTheme[idTheme]} />
    }
}
