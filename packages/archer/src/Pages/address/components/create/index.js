import React, {useEffect, useState} from "react";
import {compose} from "redux";
import styled from "styled-components/macro";
import AddressDadata from "PagesARDM/address/components/addressDaData";
import NewModal from "FeatureARDM/modal/newmodal";
import Input from "ComponentsARJTTD/input/inputComponent";
import {connect} from "react-redux";
import {ardmPagesGetSuggestionReselect} from "../../reducer/addressClient";
import {getAddressNewAddressReselect} from "../../reducer/address";
import {ardmPagesCreateAddressesRequest} from "../../actions";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";
import RequiredField from "ComponentsARJTTD/requiredField";
import {WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {validationInteger} from "UtilsARJTTD/validation";
import Promto from "ComponentsARJTTD/promto";


const CreateAdress = (props) => {
    const { newAddress, show, setShowCreate, ardmPagesCreateAddressesRequest} = props

    const [ postal_code, setpostal_code] = useState('')
    const [ postal_codeIsValidation, setpostal_codeIsValidation] = useState('')
    const [ region_kladr_id, setregion_kladr_id] = useState('')
    const [ region_kladr_idIsValidation, setregion_kladr_idIsValidation] = useState('')
    const [ tax_office_legal, settax_office_legal] = useState('')
    const [ region_type_full, setregion_type_full] = useState('')
    const [ region, setregion] = useState('')
    const [ area_type_full, setarea_type_full] = useState('')
    const [ area, setarea] = useState('')
    const [ city_type_full, setcity_type_full] = useState('')
    const [ city, setcity] = useState('')
    const [ settlement_type_full, setsettlement_type_full] = useState('')
    const [ settlement, setsettlement] = useState('')
    const [ street_type_full, setstreet_type_full] = useState('')
    const [ street, setstreet] = useState('')
    const [ house_type_full, sethouse_type_full] = useState('')
    const [ house, sethouse] = useState('')
    const [ block_type_full, setblock_type_full] = useState('')
    const [ block, setblock] = useState('')
    const [ flat_type_full, setflat_type_full] = useState('')
    const [ flat, setflat] = useState('')

    const handleClickCreateAddress = () => {
        const regionKl = region_kladr_id.slice(0,2)
        const doublertf = region_type_full === city_type_full ? '' : region_type_full
        const doubler = region === city ? '' : region
        const data = {
            postal_code: postal_code,
            region_kladr_id: regionKl,
            tax_office_legal: tax_office_legal,
            region_type_full: doublertf,
            region: doubler,
            area_type_full: area_type_full,
            area: area,
            city_type_full: city_type_full,
            city: city,
            settlement_type_full: settlement_type_full,
            settlement: settlement,
            street_type_full: street_type_full,
            street: street,
            house_type_full: house_type_full,
            house: house,
            block_type_full: block_type_full,
            block: block,
            flat_type_full: flat_type_full,
            flat: flat,
        }
        ardmPagesCreateAddressesRequest(data)
        setShowCreate(false)
    }

    useEffect(() => {
        if(newAddress){
            const regionKl = newAddress.region_kladr_id && newAddress.region_kladr_id.slice(0,2)
            setpostal_code(newAddress.postal_code ? newAddress.postal_code : '')
            setregion_kladr_id(regionKl)
            settax_office_legal(newAddress.tax_office_legal ? newAddress.tax_office_legal : '')
            setregion_type_full(newAddress.region_type_full && newAddress.region_type_full !== newAddress.city_type_full ? newAddress.region_type_full : '')
            setarea_type_full(newAddress.area_type_full ? newAddress.area_type_full : '')
            setarea(newAddress.area ? newAddress.area : '')
            setregion(newAddress.region && newAddress.region !==  newAddress.city ? newAddress.region : '')
            setcity_type_full(newAddress.city_type_full ? newAddress.city_type_full : '')
            setcity(newAddress.city ? newAddress.city : '')
            setsettlement_type_full(newAddress.settlement_type_full ? newAddress.settlement_type_full : '')
            setsettlement(newAddress.settlement ? newAddress.settlement : '')
            setstreet_type_full(newAddress.street_type_full ? newAddress.street_type_full : '')
            setstreet(newAddress.street ? newAddress.street : '')
            sethouse_type_full(newAddress.house_type_full ? newAddress.house_type_full : '')
            sethouse(newAddress.house ? newAddress.house : '')
            setblock_type_full(newAddress.block_type_full ? newAddress.block_type_full : '')
            setblock(newAddress.block ? newAddress.block : '')
            setflat_type_full(newAddress.flat_type_full ? newAddress.flat_type_full : '')
            setflat(newAddress.flat ? newAddress.flat : '')
        }
    },[newAddress])

    return (
        <NewModal showModal={show}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Новый адрес</ModalTitle>
                    <BlockIcon onClick={()=>{setShowCreate(false)}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ModalOffer>
                    <AddressDadata placeholder={'Начните вводить'} />
                </ModalOffer>
                <ModalBody>
                    <Grid1>
                        <BlockFieldStyled>
                            <Title>
                                <TitleF>Индекс</TitleF>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={postal_code}
                                       onBlur={()=>{validationInteger(postal_code, setpostal_codeIsValidation, 6 , 6)}}
                                       placeholder={'Введите индекс'}
                                       onChange={(e) => {setpostal_code(e.target.value)}}/>
                            </BlockInput>
                            {postal_codeIsValidation && <Promto text={postal_codeIsValidation}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <Title>
                                <TitleF>Код регион</TitleF>
                                <RequiredField/>
                            </Title>
                            <BlockInput>
                                <Input value={region_kladr_id}
                                       onBlur={()=>{validationInteger(region_kladr_id, setregion_kladr_idIsValidation, 2 , 2)}}
                                       placeholder={'Введите код региона'}
                                       onChange={(e) => {setregion_kladr_id(e.target.value)}}/>
                            </BlockInput>
                            {region_kladr_idIsValidation && <Promto text={region_kladr_idIsValidation}/>}
                        </BlockFieldStyled>
                        <BlockFieldStyledIFNS>
                            <TitleField>Ифнс</TitleField>
                            <BlockInput>
                                <Input value={tax_office_legal} onChange={(e) => {settax_office_legal(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyledIFNS>
                    </Grid1>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Район</TitleField>
                            <BlockInput>
                                <Input value={area_type_full}
                                       placeholder={'Введите район'}
                                       onChange={(e) => {setarea_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование района</TitleField>
                            <BlockInput>
                                <Input value={area}
                                       placeholder={'Введите наименование района'}
                                       onChange={(e) => {setarea(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Город</TitleField>
                            <BlockInput>
                                <Input value={city_type_full}
                                       placeholder={'Введите город'}
                                       onChange={(e) => {setcity_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование города</TitleField>
                            <BlockInput>
                                <Input value={city}
                                       placeholder={'Введите наименование города'}
                                       onChange={(e) => {setcity(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Населенный пункт</TitleField>
                            <BlockInput>
                                <Input value={settlement_type_full}
                                       placeholder={'Введите населенный пункт'}
                                       onChange={(e) => {setsettlement_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование населенного пункта</TitleField>
                            <BlockInput>
                                <Input value={settlement}
                                       placeholder={'Введите наименование населенного пункта'}
                                       onChange={(e) => {setsettlement(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>

                    <Grid2>
                        <BlockFieldStyled>
                            <TitleField>Улица</TitleField>
                            <BlockInput>
                                <Input value={street_type_full}
                                       placeholder={'Введите улицу'}
                                       onChange={(e) => {setstreet_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                        <BlockFieldStyled>
                            <TitleField>Наименование улицы</TitleField>
                            <BlockInput>
                                <Input value={street}
                                       placeholder={'Введите наименование улицы'}
                                       onChange={(e) => {setstreet(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid2>
                    <Grid3>
                        <BlockFieldStyled>
                            <TitleField>Дом</TitleField>
                            <BlockInput>
                                <Input value={house_type_full}
                                       placeholder={'Введите дом'}
                                       onChange={(e) => {sethouse_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Номер дома</TitleField>
                            <BlockInput>
                                <Input value={house}
                                       placeholder={'Введите номер дома'}
                                       onChange={(e) => {sethouse(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Корпус</TitleField>
                            <BlockInput>
                                <Input value={block_type_full}
                                       placeholder={'Введите корпус'}
                                       onChange={(e) => {setblock_type_full(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>

                        <BlockFieldStyled>
                            <TitleField>Номер корпуса</TitleField>
                            <BlockInput>
                                <Input value={block}
                                       placeholder={'Введите номер корпуса'}
                                       onChange={(e) => {setblock(e.target.value)}}/>
                            </BlockInput>
                        </BlockFieldStyled>
                    </Grid3>

                    <Grid4>
                        <OfficeBlock>
                            <BlockFieldStyled>
                                <TitleField>Офис</TitleField>
                                <BlockInput>
                                    <Input value={flat_type_full}
                                           placeholder={'Введите офис'}
                                           onChange={(e) => {setflat_type_full(e.target.value)}}/>
                                </BlockInput>
                            </BlockFieldStyled>
                            <BlockFieldStyled>
                                <TitleField>Номер офиса</TitleField>
                                <BlockInput>
                                    <Input value={flat}
                                           placeholder={'Введите номер офиса'}
                                           onChange={(e) => {setflat(e.target.value)}}/>
                                </BlockInput>
                            </BlockFieldStyled>
                        </OfficeBlock>
                    </Grid4>
                </ModalBody>
                <Footer>
                    <ButtonBlock>
                        <WhiteButton onClick={() => {
                            handleClickCreateAddress()
                        }}>Создать</WhiteButton>
                    </ButtonBlock>
                </Footer>
            </ContainerModal>
        </NewModal>
    )
}



const mapStateToProps = (state) => {
    return {
        suggestions: ardmPagesGetSuggestionReselect(state),
        newAddress: getAddressNewAddressReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
        ardmPagesCreateAddressesRequest
    })
)(CreateAdress)
//
const ButtonBlock = styled.div`
    width: 150px;
    height: 30px;
    margin-top: auto;
`;
const Footer = styled.div`
   display: flex; 
   justify-content: center;
`;
const ContainerModal = styled.div`
    padding: 20px;
    grid-template-rows: 50px 50px minmax(300px, 100%) 50px;
    display: grid;
    min-width: 800px;
    max-height: 800px;
    height: 100%;
    overflow: hidden;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
`;

const ModalBody = styled.div`
    flex-grow: 1;
    flex-shrink: 0;
    display: flex;
    flex-direction: column;
`;
const Grid1 = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 2fr;
    margin-bottom: 30px;
`;
const Grid2 = styled.div`
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin-bottom: 30px;
`;
const Grid3 = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    margin-bottom: 30px;
`;
const Grid4 = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
`;
const ModalOffer = styled.div`
    
`;

const OfficeBlock = styled.div`
   display: flex;
`;

const ModalHeader = styled.div`
   display: flex;
   height: 50px;
   justify-content: space-between;
`;
const ModalTitle = styled.h4`
  
`;

const TitleField = styled.div`
    font-size: 12px;
    font-weight: 500;
    line-height: 10px;
    margin-bottom: 5px;
`;
const TitleF = styled(TitleField)`
    font-size: 12px;
    font-weight: 500;
    line-height: 10px;
    margin: 0;
`;
const Title = styled.div`
    display: flex;
    margin-bottom: 5px;
`;
const BlockInput = styled.div`
    height: 32px;
    margin: 0 0 3px 0;
`;
const BlockFieldStyled = styled.div`
    position: relative;
    padding: 0 5px;
    width: 100%;
    max-height: 48px;
`;
const BlockFieldStyledIFNS = styled(BlockFieldStyled)`
    padding: 10px 5px;
    max-width: 100px;
    margin-left: auto;
`;
const BlockIcon = styled.div`
    cursor: pointer;
    width: 14px;
    height: 14px;
`;

