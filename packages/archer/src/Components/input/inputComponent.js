import styled from "styled-components";
import {transparentize} from "polished";

const Input = styled.input`
  width: 100%;
  height: 100%;
  padding: ${({ padding }) => padding ? padding : "9px 10px"};
  margin: 0;
  border-radius: 4px;
  background: ${props => props.background ?  transparentize( 1, props.theme.light) : '#fff'};
  border: 1px solid ${props => transparentize( 0 , props.theme.lightMedium)};
  font-family: ProximaNova-Regular, sans-serif;
  font-size: 100%;
  line-height: 1.15;
  color: ${props => props.theme.color};
  
  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 30px white inset;
  }

  ${props => (props.error ? "border: 1px solid #DE4D4D" : null)};

  &:hover {
    border-color: ${props => transparentize( 0.3, props.theme.heavy)};
    color: ${props => props.theme.color};
  }

  &:focus {
    border-color: ${props => transparentize( 0.3, props.theme.heavy)};
    color: ${props => props.theme.color};
    background: ${({ backgroundOnFocus }) => backgroundOnFocus};
  }

  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color:   ${props => transparentize(0.3, props.theme.color)};
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color:   ${props => transparentize(0.3, props.theme.color)};
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color:   ${props => transparentize(0.3, props.theme.color)};
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color:  ${props => transparentize(0.3, props.theme.color)};
  }

  ${({ styled }) => styled};
`;

export const BoldInput = styled(Input)`
  background: ${props => transparentize( 0, props.theme.semiHeavy)};
  color: ${props => transparentize(0, props.theme.root)};
  border: 1px solid ${props => transparentize( 1 , props.theme.semiHeavy)};  
  height: ${props => props.height ? props.height : '38px'} ;
  
  &:hover {
    border-color: ${props => transparentize( 0, props.theme.medium)};
    color: ${props => props.theme.root};
  }

  &:focus {
    border-color: ${props => transparentize( 0, props.theme.medium)};
    color: ${props => props.theme.root};
  }
  
  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color:   ${props => transparentize(0.3, props.theme.root)};
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color:   ${props => transparentize(0.3, props.theme.root)};
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color:   ${props => transparentize(0.3, props.theme.root)};
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color:  ${props => transparentize(0.3, props.theme.root)};
  }

`;

export default Input;
