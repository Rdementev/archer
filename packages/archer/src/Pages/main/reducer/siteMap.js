import { createSelector } from 'reselect'
const TOGGLE_IS_DRAWER_HOME_PAGE_SERVES = 'TOGGLE_IS_DRAWER_HOME_PAGE_SERVES'
const TOGGLE_IS_SHOW_OVERLAY = 'TOGGLE_IS_SHOW_OVERLAY'


let initialState = {
    isDrawerHomePageServes: false,
    isShowOverlay: false,

    site_map: [
        {
            id: 9,
            name: 'reports',
            title: 'Регистрационные документы',
            page_group_id: 1,
            page_subgroup_id: null,
            order: 1,
            location: '/serves/document',
            icon: '',
            func: null,
            is_freeze: false
        },
        {
            id: 9,
            name: 'reports',
            title: 'Юридические адреса',
            page_group_id: 1,
            page_subgroup_id: null,
            order: 1,
            location: '/serves/sales-address',
            icon: '',
            func: null,
            is_freeze: true
        },
        {
            id: 9,
            name: 'reports',
            title: 'Дашбоард',
            page_group_id: 1,
            page_subgroup_id: null,
            order: 1,
            location: '/serves',
            icon: '',
            func: null,
            is_freeze: true
        },
    ],
};




const siteMap = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_IS_DRAWER_HOME_PAGE_SERVES : {
            return {
                ...state,
                isDrawerHomePageServes: action.value,
                isShowOverlay: action.value
            }
        }
        case TOGGLE_IS_SHOW_OVERLAY : {
            return {
                ...state,
                isShowOverlay: action.value
            }
        }

        case 'LOG_OUT_USER' : {
            return state = initialState
        }
    }
    return state
};

export const toggleIsDrawerHomePage = (value) => ({type: TOGGLE_IS_DRAWER_HOME_PAGE_SERVES, value})
export const toggleIsShowOverlay = (value) => ({type: TOGGLE_IS_SHOW_OVERLAY, value})

//
const getSiteMap = (state) => state.ARDM.siteMap.site_map
export const getSiteMapSelector = createSelector(getSiteMap,(site_map) => site_map)
//
const getIsShowOverlay = (state) => state.ARDM.siteMap.isShowOverlay
export const getIsShowOverlaySelector = createSelector(getIsShowOverlay,(isShowOverlay) => isShowOverlay)
//
const getIsDrawerHomePageServes = (state) => state.ARDM.siteMap.isDrawerHomePageServes
export const getIsDrawerHomePageServesSelector = createSelector(getIsDrawerHomePageServes,(isDrawerHomePageServes) => isDrawerHomePageServes)

export default siteMap;
