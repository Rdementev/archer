import React from "react";
import styled from "styled-components";
import {ReactComponent as Check} from "Icons/check.svg";

const Checkbox = (props) => {
    const {className, checked, onChange, rightText, onClick, onDoubleClick, disabled = false} = props;

    return (
        <CheckboxLabel>
            <CheckBoxInput
                onChange={onChange}
                checked={checked}
                type="checkbox"
            />
            <CheckboxSpan disabled={disabled}>
                <Check/>
            </CheckboxSpan>
            {rightText && <CheckboxRightText>{rightText}</CheckboxRightText>}
        </CheckboxLabel>
    );
}

export default Checkbox;

//

const disabledLabelStyle = `
  & > input:checked ~ span {
    background-color: #ccd1e0;
  }

  & > input:checked ~ span svg {
    display: block;
  }
`;

const enabledLabelStyle = `
  & > input:checked ~ span {
    background-color: #1f367d;
  }

  & > input:checked ~ span svg {
    display: block;
  }
`;

const CheckboxLabel = styled.label`
  display: flex;
  align-items: center;

  position: relative;

  color: #888e9f;
  font-size: 22px;

  cursor: pointer;
  user-select: none;
  margin: 0;


  ${props => (props.disabled ? disabledLabelStyle : enabledLabelStyle)}
`;

const disabledSpanStyle = `
  background-color: transparent;
  cursor: auto;
`;

const enabledSpanStyle = `
  &:hover {
    border: 1px solid #788ece;
  }

  &:focus {
    border: 1px solid #788ece;
  }
`;

const CheckboxSpan = styled.span`
  all: initial;

  height: ${props => (props.height ? props.height : "16px")};
  width: ${props => (props.width ? props.width : "16px")};
  display: grid;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;

  background-color: #ffffff;
  border: 1px solid #e0e4f1;

  cursor: pointer;

  svg {
    display: none;
  }

  ${({disabled}) => (disabled ? disabledSpanStyle : enabledSpanStyle)};
`;

const CheckBoxInput = styled.input`
    position: absolute;
    width: 100%;
    height: 100%;
    cursor: pointer;
    opacity: 0;
`;

const CheckboxRightText = styled.p`
    margin-left: 10px;
    margin-bottom: 0;
    color: #888e9f;
    font-size: 14px;
    line-height: 12px;
`;
