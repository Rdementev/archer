import {createSelector} from "reselect";
import {ARDM_AUTH_SIGNUP_CHANGE_DOUBLE_SEND_EMAIL, ARDM_AUTH_SIGNUP_SHOW_CHECK_EMAIL} from "../action";
import {ARDM_AUTH_SIGNUP_CHANGE} from "PagesARDM/auth/signUp/action";


let initialState = {
    email: '',
    password: '',
    login: '',
    agreeing: '',

    doubleSend: false,
}

const signUp = (state = initialState, action ) => {
    switch (action.type) {
        case ARDM_AUTH_SIGNUP_SHOW_CHECK_EMAIL : {
            return {...state, showCheckEmail: action.payload.value}
        }
        case ARDM_AUTH_SIGNUP_CHANGE : {
            return {...state, [action.alias]: action.value}
        }
        case ARDM_AUTH_SIGNUP_CHANGE_DOUBLE_SEND_EMAIL : {
            return {...state, doubleSend: action.value}
        }


        case 'LOG_OUT_USER' : {
            return initialState
        }
        default:
            return state;
    }
};

export default signUp

// reselect
const getShowCheckEmail = (state) => state.ARDM.auth.signUp.showCheckEmail
export const getShowCheckEmailReselect = createSelector(getShowCheckEmail, (showCheckEmail)=> showCheckEmail )

const getEmailSignup = (state) => state.ARDM.auth.signUp.email
export const getEmailSignupReselect = createSelector(getEmailSignup, (email)=> email )

const getPasswordSignup = (state) => state.ARDM.auth.signUp.password
export const getPasswordSignupReselect = createSelector(getPasswordSignup, (password)=> password )

const getLoginSignup = (state) => state.ARDM.auth.signUp.login
export const getLoginSignupReselect = createSelector(getLoginSignup, (login)=> login )

const getAgreeingSignup = (state) => state.ARDM.auth.signUp.agreeing
export const getAgreeingSignupReselect = createSelector(getAgreeingSignup, (agreeing)=> agreeing )

const getDoubleSendSignup = (state) => state.ARDM.auth.signUp.doubleSend
export const getDoubleSendSignupReselect = createSelector(getDoubleSendSignup, (doubleSend)=> doubleSend )
