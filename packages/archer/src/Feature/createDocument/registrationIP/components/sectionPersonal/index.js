import React, {useEffect, useState} from "react";
import styled from "styled-components/macro";
import Input from "ComponentsARJTTD/input/inputComponent";
import {compose} from "redux";
import {connect} from "react-redux";
import {ardmFeatureRegIPSetTypePersonalId, ardmFeatureRegIPSetTypeSnils} from "../../actions";
import ChoisePersonal from "PagesARDM/personal";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import {setEditPersonalId} from "PagesARDM/personal/actions";
import RequiredField from "ComponentsARJTTD/requiredField";

const SectionPersonal = (props) => {
    const {type_snils, type_send, type_personalId, personal,setEditPersonalId,
        ardmFeatureRegIPSetTypeSnils, ardmFeatureRegIPSetTypePersonalId} = props
    const [showSNILS, setShowSNILS] = useState(true)
    const [show, setShow] = useState(false)
    const [personalObj, setPersonalObj] = useState(false)
    const [name, setName] = useState('')

    useEffect(() => {
        if (type_send && type_send === 'Электронно со своей КЭП' || type_send === 'Электронно с помощью КЭП нотариуса') {
            return setShowSNILS(true)
        }
        setShowSNILS(false)
    }, [type_send])

    useEffect(()=>{
        const obj =  personal.find(item => item.id === type_personalId)
        setPersonalObj(obj)
    },[type_personalId, personal])

    useEffect(()=> {
        if(personalObj){
            const dirObjName = personalObj.last_name.concat(' ' , personalObj.first_name, ' ', personalObj.second_name )
            setName(dirObjName)
        }
    },[personalObj, personal])

    const handleChange = (value) => {
        ardmFeatureRegIPSetTypeSnils(value)
    }
    const handleClickPersonal = (id) => {
        ardmFeatureRegIPSetTypePersonalId(id)
        setShow(false)
    }
    const handleClickFounder = (id) => {
        setShow(true)
        setEditPersonalId(personalObj.id)
    }

    return (
        <>
            <Header>
                <Title>Заявитель</Title>
                <RequiredField/>
            </Header>
            <Body>
                <StyledButton onClick={()=>{setShow(true)}}>
                    <Button>Выбрать</Button>
                </StyledButton>
                <StyledName>
                    <NameStyle onClick={()=>{handleClickFounder()}}>
                        {name}
                    </NameStyle>
                </StyledName>
            </Body>
            <Offer>
                {showSNILS && <OfferBlock>
                    <Title>Снилс</Title>
                    <BlockInput>
                        <Input value={type_snils} onChange={(e) => {handleChange(e.target.value)}}/>
                    </BlockInput>
                </OfferBlock>}
            </Offer>
            <ChoisePersonal show={show} setShow={setShow} onClickPers={handleClickPersonal}/>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),

    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegIPSetTypeSnils, ardmFeatureRegIPSetTypePersonalId, setEditPersonalId
    })
)(SectionPersonal)
//
const OfferBlock = styled.div`
    &:not(:last-child){
      margin-bottom: 10px;
    }
`;
const BlockInput = styled.div`
   height: 32px;
`;
const StyledButton = styled.div`
    width: 150px;
     margin-bottom: 20px;
`;
const Offer = styled.div`
  padding: 0 10px;
  height: 100%;
`;
const Body = styled.div`
  padding: 0 10px;
`;
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const StyledName = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    flex-grow: 1;
    font-size: 16px;
    margin-bottom: 20px;
    text-transform: uppercase;
`;
const NameStyle = styled.div`
    transition: ease 0.3s;
    &:hover {
      color: #000
    }
`;