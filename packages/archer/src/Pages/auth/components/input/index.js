import styled from "styled-components/macro";

const Input = styled.input`
  width: 100%;
  height: 100% ;
  padding: 3.5px 0.5em;
  border-radius: 4px;
  margin: 0;
  background: #fff;
  border: 1px solid #000;
  font-size: 100%;
  line-height: 1.15;
  color: #000;
  
  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 30px white inset;
  }

  ${props => (props.error ? "border: 1px solid #DE4D4D" : null)};

  &:hover {
    border-color: #000;
    color: #000;
  }

  &:focus {
    border-color: #000;
    color: #000;
    background: #fff;
  }

  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color:   rgba(0,0,0,0.6);
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color:   rgba(0,0,0,0.6);
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color:   rgba(0,0,0,0.6);
  }
  :-moz-placeholder {
    /* Firefox 18- */
    color:  rgba(0,0,0,0.6);
  }

  ${({styled}) => styled};
`;

export default Input;
