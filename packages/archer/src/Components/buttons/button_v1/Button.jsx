import styled from "styled-components";

const Button = styled.button`
  min-width: 100px;
  padding: 10px 15px;

  display: flex;
  justify-content: center;
  align-items: center;

  text-align: center;
  font-family: "ProximaNova-Regular", sans-serif;
  cursor: pointer;
  transition: 0.3s all;

  background-color: #73061acc;
  color: #ffffff;
  border-color: 1px solid #000;
  &:hover {
    background-color: #73061A;
    color: #fff;
  }

  &:active {
    background-color: #73061A;
    color: #ffffff;
    transform: scale(0.95);
  }

  &:disabled {
    color: white;
    background-color: #e0e4f1 !important;
    cursor: unset;
  }
`;

const WhiteButton = styled(Button)`
  background-color: #ffffff;
  border: 1px solid #e0e4f1;
  color: #000000;
`;

export default Button;

export { WhiteButton };
