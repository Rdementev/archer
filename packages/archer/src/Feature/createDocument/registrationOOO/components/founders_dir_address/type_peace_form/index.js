import React, {useEffect, useState} from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {getARDMFeatureRegistrationOOOTypePeaceFormIdReselect} from "../../../reducer";
import {ardmFeatureRegOOOClearPeaceValue, ardmFeatureRegOOOSetPeaceFormId} from "../../../actions";
import styled from "styled-components/macro";
import RequiredField from "ComponentsARJTTD/requiredField";
import Promto from "ComponentsARJTTD/promto";

const list = [
    {id:1, name: "в виде простой дроби",},
    {id:2, name: "в виде десятичной дроби",},
    {id:3, name: "в виде процентов",}]

const PromtoStyled = {
    position: 'absolute',
    right: '20px',
    top: '50%',
    transform: 'translate(0, -50%)'
}

const PeaceForm = (props) => {
    const {type_peace_form, type_reg, isEqually,
        ardmFeatureRegOOOSetPeaceFormId, ardmFeatureRegOOOClearPeaceValue} = props
    const [value, setValue] = useState('')
    const [firstRender, setFirstRender] = useState(true)
    useEffect(()=> {
        if(type_peace_form){
            const active = list.find(item => item.name === type_peace_form)
            setValue(active.name)
        }
        if(firstRender) return setFirstRender(false)
        ardmFeatureRegOOOClearPeaceValue()
    },[type_peace_form])

    useEffect(()=>{
        if(type_reg === 'Упращенная регистрация ООО'){
            ardmFeatureRegOOOSetPeaceFormId('')
        }
    },[type_reg])

    const handleClickItem = (item, multi , other) => {
        ardmFeatureRegOOOSetPeaceFormId(item[0].name)
    }

    return (
        <>
            <Header>
                <Title>Формат указания долей</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <StyledSelectSearch>
                    <SelectSearchContainer onClick={handleClickItem}
                                           value={value}
                                           displayValue={value}
                                           list={list}/>
                </StyledSelectSearch>
                {!isEqually && <Promto text={'Номинальная сумма долей не равна уставному капиталу'} styled={PromtoStyled}/>}
            </Offer>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        type_peace_form: getARDMFeatureRegistrationOOOTypePeaceFormIdReselect(state),
    }
}

export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetPeaceFormId, ardmFeatureRegOOOClearPeaceValue
    }),
)(PeaceForm)
//
const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
   
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  position: relative;
`;
const StyledSelectSearch = styled.div`
    width: 300px;
    height: 28px;
    padding: 0 0 0 10px;
`;

