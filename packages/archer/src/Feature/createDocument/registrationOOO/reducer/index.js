import {createSelector} from 'reselect'
import {
    ARDM_FEATURE_REGOOO_CHANGE_ACTIVE_STEP_ID,
    ARDM_FEATURE_REGOOO_CHANGE_TYPE_SIZE_CAPITAL,
    ARDM_FEATURE_REGOOO_CHANGE_TYPE_RESPONSE,
    ARDM_FEATURE_REGOOO_SET_PROXY,
    ARDM_FEATURE_REGOOO_CREATE_FOUNDER_INDIVIDUALS,
    ARDM_FEATURE_REGOOO_CREATE_FOUNDER_ORGANIZATIONS,
    ARDM_FEATURE_REGOOO_DELETE_FOUNDER_INDIVIDUALS,
    ARDM_FEATURE_REGOOO_DELETE_FOUNDER_ORGANIZATIONS,
    ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_INDIVIDUALS,
    ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_ORGANIZATIONS,
    ARDM_FEATURE_REGOOO_CLEAR_PEACE_VALUE,
    ARDM_FEATURE_REGOOO_SET_DIR_PERSONAL_ID,
    ARDM_FEATURE_REGOOO_SET_ADDRESS_ID,
    ARDM_FEATURE_REGOOO_SET_DIR_TIME,
    ARDM_FEATURE_REGOOO_SET_DIR_POSITION,
    ARDM_FEATURE_REGOOO_CHANGE_COMPLETED,
    ARDM_FEATURE_REGOOO_CLEAR_FOUNDERS,
    ARDM_FEATURE_REGOOO_SET_PEACE_FORM,
    ARDM_FEATURE_REGOOO_SET_SECRETAR,
    ARDM_FEATURE_REGOOO_SET_PREDSIDATEL,
    ARDM_FEATURE_REGOOO_CHANGE_TYPE_CREATE_DATE,
    ARDM_FEATURE_REGOOO_SET_RATE_NALOG, ARDM_FEATURE_REGOOO_SET_REG_ORGAN,
    ARDM_FEATURE_REGOOO_SET_SYS_NALOG,
    ARDM_FEATURE_REGOOO_SET_TYPE_REG,
    ARDM_FEATURE_REGOOO_SET_TYPE_SEND,
    ARDM_FEATURE_REGOOO_SET_USTAV_CERTIFY_SOLUTIONS,
    ARDM_FEATURE_REGOOO_SET_USTAV_MIN_ADDRESS, ARDM_FEATURE_REGOOO_SET_USTAV_OKVED_IN_USTAV,
    ARDM_FEATURE_REGOOO_SET_SELECTED_OKVEDS, ARDM_FEATURE_REGOOO_SET_FAVORITE_OKVED,
    ARDM_FEATURE_REGOOO_SET_DIR_IS_DIR_ADDRESS,
    ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENG, ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENGLISH,
    ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUS,
    ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUSSIAN,
    ARDM_FEATURE_REGOOO_SET_PEACE_SUM,
    ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_COMMON,
    ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_FDA,
    ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_UNO,
} from "../actions";
import {ARDM_PAGES_GET_FORM_DATA_REG_OOO_SUCCESS} from "PagesARDM/pagedocs/actions";
import {ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN} from "Feature/createDocument/registrationIP/actions";
import {ARDM_FEATURE_REGOOO_CHANGE_REG_ORGAN} from "Feature/createDocument/registrationOOO/actions";

let initialState = {
    activeStepId: 1,
    form_id: null,
    id: null,
    validation:{
        common: false,
        fda: false,
        uno: false
    },

//============= common ================//
//     form_id: '',
//     group_form_id: '',
//     type_reg: '',
//     type_send: '',
//     type_company_name_rus: '',
//     type_company_name_russian: '',
//     type_company_name_eng: '',
//     type_company_name_english: '',
//     type_size_capital: '',
//     type_create_date: '',
//     type_response: '',
//============= fda ================//
//     type_proxy_id: null,
//     type_peace_form: '',
//     type_individuals: [],
//     type_organizations: [],
//     type_dir_personal_id: '',
//     type_dir_position: 'Генеральный директор',
//     type_dir_time: 'Бессрочно',
//     type_dir_is_dir_address: false,
//     type_address_id: '',
//     is_founder_address: false,
//============= uno ================//
//     type_ustav_min_address: false,
//     type_ustav_certify_solution: false,
//     type_ustav_okved_in_ustav: false,
//     type_sys_nal: '',
//     type_rate_nal: '',
//     type_selected_okved: [],
//     type_secretar: {},
//     type_predsidatel: {},
//     type_reg_organ_is_load: false,
//     type_reg_organ: {
//
//     }
}
const registrationOOO = (state = initialState, action) => {
    switch (action.type) {
        case ARDM_PAGES_GET_FORM_DATA_REG_OOO_SUCCESS: {
            return {...state, ...action.payload }
        }
        case ARDM_FEATURE_REGOOO_CHANGE_ACTIVE_STEP_ID: {
            return {...state, activeStepId: action.id }
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_REG: {
            return {...state, type_reg: action.value }
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_SEND: {
            return {...state, type_send: action.value }
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUS: {
            return {...state, type_company_name_rus: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_NAME_RUSSIAN: {
            return {...state, type_company_name_russian: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENG: {
            return {...state, type_company_name_eng: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_TYPE_NAME_ENGLISH: {
            return {...state, type_company_name_english: action.value}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_TYPE_SIZE_CAPITAL: {
            return {...state, type_size_capital: action.value}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_TYPE_CREATE_DATE: {
            return {...state, type_create_date: action.value}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_TYPE_RESPONSE: {
            return {...state, type_response: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_PROXY: {
            return {...state, type_proxy_id: action.id}
        }
        case ARDM_FEATURE_REGOOO_SET_PEACE_FORM: {
            return {...state, type_peace_form: action.value}
        }
        case ARDM_FEATURE_REGOOO_CREATE_FOUNDER_INDIVIDUALS: {
            return {...state, type_individuals: [action.founder, ...state.type_individuals]}
        }
        case ARDM_FEATURE_REGOOO_DELETE_FOUNDER_INDIVIDUALS: {
            return {...state, type_individuals: state.type_individuals.filter(item => !action.id.includes(item.id))}
        }
        case ARDM_FEATURE_REGOOO_CLEAR_FOUNDERS: {
            return {...state, type_individuals: [], type_organizations: []}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_INDIVIDUALS: {
            return {...state, type_individuals: state.type_individuals.map(item => item.id === action.id ? {...item, [action.alias]: action.value} : {...item})}
        }
        case ARDM_FEATURE_REGOOO_CLEAR_PEACE_VALUE: {
            return {...state,
                type_individuals: state.type_individuals.map(item => item
                    ? {...item, peace_value:"", peace_value1:"", peace_value2:"",} : {...item}),
                type_organizations: state.type_organizations.map(item => item
                    ? {...item, peace_value:"", peace_value1:"", peace_value2:"",} : {...item})}
        }
        case ARDM_FEATURE_REGOOO_CREATE_FOUNDER_ORGANIZATIONS: {
            return {...state, type_organizations: [action.founder, ...state.type_organizations]}
        }
        case ARDM_FEATURE_REGOOO_DELETE_FOUNDER_ORGANIZATIONS: {
            return {...state, type_organizations: state.type_organizations.filter(item => !action.id.includes(item.id))}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_PEACE_VALUE_ORGANIZATIONS: {
            return {...state, type_organizations: state.type_organizations.map(item => item.id === action.id ? {...item, [action.alias]: action.value} : {...item})}
        }
        case ARDM_FEATURE_REGOOO_SET_PEACE_SUM: {
            return {...state, [action.alias]: state[action.alias].map(item => item.id === action.id ? {...item, peace_sum: action.value} : {...item})}
        }
        case ARDM_FEATURE_REGOOO_SET_DIR_PERSONAL_ID: {
            return {...state, type_dir_personal_id: action.id}
        }
        case ARDM_FEATURE_REGOOO_SET_DIR_POSITION: {
            return {...state, type_dir_position: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_DIR_TIME: {
            return {...state, type_dir_time: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_DIR_IS_DIR_ADDRESS: {
            return {...state, type_dir_is_dir_address: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_ADDRESS_ID: {
            return {...state, type_address_id: action.id}
        }
        case ARDM_FEATURE_REGOOO_SET_USTAV_MIN_ADDRESS: {
            return {...state, type_ustav_min_address: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_USTAV_CERTIFY_SOLUTIONS: {
            return {...state, type_ustav_certify_solution:  action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_USTAV_OKVED_IN_USTAV: {
            return {...state, type_ustav_okved_in_ustav: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_SYS_NALOG: {
            return {...state, type_sys_nal: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_RATE_NALOG: {
            return {...state, type_rate_nal: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_SECRETAR: {
            return {...state, type_secretar: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_PREDSIDATEL: {
            return {...state, type_predsidatel: action.value}
        }
        case ARDM_FEATURE_REGOOO_SET_SELECTED_OKVEDS: {
            return {...state, type_selected_okved: state.type_selected_okved.some(item => item.code === action.value.code)
                    ? state.type_selected_okved.filter(item => item.code !== action.value.code)
                    : [...state.type_selected_okved, action.value]}
        }
        case ARDM_FEATURE_REGOOO_SET_FAVORITE_OKVED: {
            return {...state,
                type_selected_okved: state.type_selected_okved.map(item =>
                    item.code === action.value.code
                        ? item.favorite ? {...item, favorite: false} : {...item, favorite: true}
                        : {...item, favorite: false})
                }
        }
        case ARDM_FEATURE_REGOOO_SET_REG_ORGAN: {
            return {...state, type_reg_organ: action.value}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_REG_ORGAN: {
            return {...state, type_reg_organ: {...state.type_reg_organ, [action.alias]:action.value}}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_COMPLETED: {
            return {...state, completed: action.value}
        }



        // validation
        case ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_COMMON: {
            return {...state, validation: {...state.validation, common: action.value}}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_FDA: {
            return {...state, validation: {...state.validation, fda: action.value}}
        }
        case ARDM_FEATURE_REGOOO_CHANGE_VALIDATION_UNO: {
            return {...state, validation: {...state.validation, uno: action.value}}
        }
    }
    return state
}
export default registrationOOO

// validation
const getARDMFeatureRegOOOValidationCommon = (state) => state.ARDM.registrationOOO.validation.common
export const getARDMFeatureRegOOOValidationCommonReselect = createSelector(getARDMFeatureRegOOOValidationCommon, (common) => common)

const getARDMFeatureRegOOOValidationFDA = (state) => state.ARDM.registrationOOO.validation.fda
export const getARDMFeatureRegOOOValidationFDAReselect = createSelector(getARDMFeatureRegOOOValidationFDA, (fda) => fda)

const getARDMFeatureRegOOOValidationUNO = (state) => state.ARDM.registrationOOO.validation.uno
export const getARDMFeatureRegOOOValidationUNOReselect = createSelector(getARDMFeatureRegOOOValidationUNO, (uno) => uno)


//
const getARDMFeatureRegOOOFormId = (state) => state.ARDM.registrationOOO.form_id
export const getARDMFeatureRegOOOFormIdReselect = createSelector(getARDMFeatureRegOOOFormId, (form_id) => form_id)

const getARDMFeatureRegOOOId = (state) => state.ARDM.registrationOOO.id
export const getARDMFeatureRegOOOIdReselect = createSelector(getARDMFeatureRegOOOId, (id) => id)
//
const getARDMFeatureRegOOOActiveStepId = (state) => state.ARDM.registrationOOO.activeStepId
export const getARDMFeatureRegOOOActiveStepIdReselect = createSelector(getARDMFeatureRegOOOActiveStepId, (activeStepId) => activeStepId)

const getARDMFeatureRegistrationOOOTypeReg = state => state.ARDM.registrationOOO.type_reg
export const getARDMFeatureRegistrationOOOTypeRegReselect = createSelector(getARDMFeatureRegistrationOOOTypeReg, (type_reg) => type_reg)

const getARDMFeatureRegistrationOOOTypeSend = state => state.ARDM.registrationOOO.type_send
export const getARDMFeatureRegistrationOOOTypeSendReselect = createSelector(getARDMFeatureRegistrationOOOTypeSend, (type_send) => type_send)

const getARDMFeatureRegistrationOOOTypeNameRus = state => state.ARDM.registrationOOO.type_company_name_rus
export const getARDMFeatureRegistrationOOOTypeNameRusReselect = createSelector(getARDMFeatureRegistrationOOOTypeNameRus, (type_company_name_rus) => type_company_name_rus)

const getARDMFeatureRegistrationOOOTypeNameRussian = state => state.ARDM.registrationOOO.type_company_name_russian
export const getARDMFeatureRegistrationOOOTypeNameRussianReselect = createSelector(getARDMFeatureRegistrationOOOTypeNameRussian, (type_company_name_russian) => type_company_name_russian)

const getARDMFeatureRegistrationOOOTypeNameEng = state => state.ARDM.registrationOOO.type_company_name_eng
export const getARDMFeatureRegistrationOOOTypeNameEngReselect = createSelector(getARDMFeatureRegistrationOOOTypeNameEng, (type_company_name_eng) => type_company_name_eng)

const getARDMFeatureRegistrationOOOTypeNameEnglish = state => state.ARDM.registrationOOO.type_company_name_english
export const getARDMFeatureRegistrationOOOTypeNameEnglishReselect = createSelector(getARDMFeatureRegistrationOOOTypeNameEnglish, (type_company_name_english) => type_company_name_english)

const getARDMFeatureRegistrationOOOTypeSizecapital = state => state.ARDM.registrationOOO.type_size_capital
export const getARDMFeatureRegistrationOOOTypeSizecapitalReselect = createSelector(getARDMFeatureRegistrationOOOTypeSizecapital, (type_size_capital) => type_size_capital)

const getARDMFeatureRegistrationOOOTypeCreateDate = state => state.ARDM.registrationOOO.type_create_date
export const getARDMFeatureRegistrationOOOTypeCreateDateReselect = createSelector(getARDMFeatureRegistrationOOOTypeCreateDate, (type_create_date) => type_create_date)

const getARDMFeatureRegistrationOOOTypeResponse = state => state.ARDM.registrationOOO.type_response
export const getARDMFeatureRegistrationOOOTypeResponseReselect = createSelector(getARDMFeatureRegistrationOOOTypeResponse, (type_response) => type_response)

const getARDMFeatureRegistrationOOOTypeProxy = state => state.ARDM.registrationOOO.type_proxy_id
export const getARDMFeatureRegistrationOOOTypeProxyReselect = createSelector(getARDMFeatureRegistrationOOOTypeProxy, (type_proxy_id) => type_proxy_id)

const getARDMFeatureRegistrationOOOTypePeaceFormId = state => state.ARDM.registrationOOO.type_peace_form
export const getARDMFeatureRegistrationOOOTypePeaceFormIdReselect = createSelector(getARDMFeatureRegistrationOOOTypePeaceFormId, (type_peace_form) => type_peace_form)

const getARDMFeatureRegistrationOOOTypeIndividuals = state => state.ARDM.registrationOOO.type_individuals
export const getARDMFeatureRegistrationOOOTypeIndividualsReselect = createSelector(getARDMFeatureRegistrationOOOTypeIndividuals, (type_individuals) => type_individuals)

const getARDMFeatureRegistrationOOOTypeOrganizations = state => state.ARDM.registrationOOO.type_organizations
export const getARDMFeatureRegistrationOOOTypeOrganizationsReselect = createSelector(getARDMFeatureRegistrationOOOTypeOrganizations, (type_organizations) => type_organizations)

const getARDMFeatureRegistrationOOOTypeDirPersonalId = state => state.ARDM.registrationOOO.type_dir_personal_id
export const getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect = createSelector(getARDMFeatureRegistrationOOOTypeDirPersonalId, (type_dir_personal_id) => type_dir_personal_id)

const getARDMFeatureRegistrationOOOTypeDirPosition = state => state.ARDM.registrationOOO.type_dir_position
export const getARDMFeatureRegistrationOOOTypeDirPositionReselect = createSelector(getARDMFeatureRegistrationOOOTypeDirPosition, (type_dir_position) => type_dir_position)

const getARDMFeatureRegistrationOOOTypeDirTime = state => state.ARDM.registrationOOO.type_dir_time
export const getARDMFeatureRegistrationOOOTypeDirTimeReselect = createSelector(getARDMFeatureRegistrationOOOTypeDirTime, (type_dir_time) => type_dir_time)

const getARDMFeatureRegistrationOOOTypeDirIsDirAddress = state => state.ARDM.registrationOOO.type_dir_is_dir_address
export const getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect = createSelector(getARDMFeatureRegistrationOOOTypeDirIsDirAddress, (type_dir_is_dir_address) => type_dir_is_dir_address)

const getARDMFeatureRegistrationOOOTypeDirAddressId = state => state.ARDM.registrationOOO.type_address_id
export const getARDMFeatureRegistrationOOOTypeAddressIdReselect = createSelector(getARDMFeatureRegistrationOOOTypeDirAddressId, (type_address_id) => type_address_id)

const getARDMFeatureRegistrationOOOTypeUstavMinAddress = state => state.ARDM.registrationOOO.type_ustav_min_address
export const getARDMFeatureRegistrationOOOTypeUstavMinAddressReselect = createSelector(getARDMFeatureRegistrationOOOTypeUstavMinAddress, (type_ustav_min_address) => type_ustav_min_address)

const getARDMFeatureRegistrationOOOTypeUstavCertifySolutions = state => state.ARDM.registrationOOO.type_ustav_certify_solution
export const getARDMFeatureRegistrationOOOTypeUstavCertifySolutionsReselect = createSelector(getARDMFeatureRegistrationOOOTypeUstavCertifySolutions, (type_ustav_certify_solution) => type_ustav_certify_solution)

const getARDMFeatureRegistrationOOOTypeUstavOkvedInUstav = state => state.ARDM.registrationOOO.type_ustav_okved_in_ustav
export const getARDMFeatureRegistrationOOOTypeUstavOkvedInUstavReselect = createSelector(getARDMFeatureRegistrationOOOTypeUstavOkvedInUstav, (type_ustav_okved_in_ustav) => type_ustav_okved_in_ustav)

const getARDMFeatureRegistrationOOOTypeSysNal = state => state.ARDM.registrationOOO.type_sys_nal
export const getARDMFeatureRegistrationOOOTypeSysNalReselect = createSelector(getARDMFeatureRegistrationOOOTypeSysNal, (type_sys_nal) => type_sys_nal)

const getARDMFeatureRegistrationOOOTypeRateNal = state => state.ARDM.registrationOOO.type_rate_nal
export const getARDMFeatureRegistrationOOOTypeRateNalReselect = createSelector(getARDMFeatureRegistrationOOOTypeRateNal, (type_rate_nal) => type_rate_nal)

const getARDMFeatureRegistrationOOOTypeSecretar = state => state.ARDM.registrationOOO.type_secretar
export const getARDMFeatureRegistrationOOOTypeSecretarReselect = createSelector(getARDMFeatureRegistrationOOOTypeSecretar, (type_secretar) => type_secretar)

const getARDMFeatureRegistrationOOOTypePredsidatel = state => state.ARDM.registrationOOO.type_predsidatel
export const getARDMFeatureRegistrationOOOTypePredsidatelReselect = createSelector(getARDMFeatureRegistrationOOOTypePredsidatel, (type_predsidatel) => type_predsidatel)

const getARDMFeatureRegistrationOOOTypeSelectedOkved = state => state.ARDM.registrationOOO.type_selected_okved
export const getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect = createSelector(getARDMFeatureRegistrationOOOTypeSelectedOkved, (type_selected_okved) => type_selected_okved)

const getARDMFeatureRegistrationOOOTypeRegOrgan = state => state.ARDM.registrationOOO.type_reg_organ
export const getARDMFeatureRegistrationOOOTypeRegOrganReselect = createSelector(getARDMFeatureRegistrationOOOTypeRegOrgan, (type_reg_organ) => type_reg_organ)

const getARDMFeatureRegistrationOOOCompleted = state => state.ARDM.registrationOOO.completed
export const getARDMFeatureRegistrationOOOCompletedReselect = createSelector(getARDMFeatureRegistrationOOOCompleted, (completed) => completed)

