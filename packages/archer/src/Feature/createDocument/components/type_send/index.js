import React from "react";
import TypeSend from "./type_send";
import {compose} from "redux";

const TypeSendContainer = (props) => {
    const {value, setValue, list} = props
    return <TypeSend value={value} setValue={setValue} list={list}/>
}

export default compose()(TypeSendContainer)
