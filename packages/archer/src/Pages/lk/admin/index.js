import React from "react";
import {compose} from "redux";
import styled from 'styled-components/macro'
import {Renta} from 'IconsARDM'
import {Route, Switch, withRouter} from "react-router-dom";
import AddminAddress from "./adminAddress";
import Account from "../account";

const AdminLk = (props) => {
    const { history } = props

    return (
        <Container>
            <BlockMenu>
                <BlockMenuItem onClick={()=>history.push('/serves/account/addresses')}>
                    <ItemIconBlock>
                        <Renta/>
                    </ItemIconBlock>
                    <ItemNameBlock>
                        Аренда адресов
                    </ItemNameBlock>
                </BlockMenuItem>
            </BlockMenu>
            <Switch>
                <Route exact path={'/serves/account'}  render={() => <Account/>}/>
                <Route exact path={'/serves/account/addresses'}  render={() => <AddminAddress/>}/>
            </Switch>
        </Container>
    )
}


export default compose(
    withRouter
)(AdminLk)

//
const Container = styled.div`
    height: 100%;
    padding: 10px 20px;
    display: grid;
    grid-template-columns: minmax(120px, 120px) minmax(200px, 100%);
    grid-gap: 10px;
`;
const BlockMenu = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  background-color: #1C3375;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`;

const BlockOffer = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px;
  background-color: #fff;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`;

const OfferHeader = styled.div`
  background-color: red;
`;

const OfferBody = styled.div`
  
`;
const OfferTable = styled.div`
 
`;
const BlockMenuItem = styled.div`
    max-height: 75px;
 padding: 10px 5px;
 border-radius: 4px;
 transition: ease 0.4s;
 cursor: pointer;
 text-align: center;
 display: flex;
 flex-direction: column;
 &:hover {
  background-color: rgba(255,255,255,0.2);
 }
`;
const ItemIconBlock = styled.div`
    width: 20px;
    height: 20px;
    margin: 0 auto;
    & > svg {
      width: 100%;
      height: 100%;
      
    }
`;

const ItemNameBlock = styled.div`
 font-weight: 600;
 font-size: 10px;
 margin-top: 10px;  
 color: rgba(255,255,255,0.7);
 
`;




