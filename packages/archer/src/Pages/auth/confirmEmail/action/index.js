export const ARDM_AUTH_GET_CONFIRM_EMAIL = 'ARDM_AUTH_GET_CONFIRM_EMAIL'
export const ardmAuthGetConfirmEmail = (email, auth_key) => ({type: ARDM_AUTH_GET_CONFIRM_EMAIL, payload: {email, auth_key}})
