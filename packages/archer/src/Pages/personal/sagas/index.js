import {all, fork} from "redux-saga/effects";
import {createPersonal} from "./createPersonal";
import {getPersonal} from "./getPersonal";
import {updatePersonal} from "./updatePersonal";


export function* personalSaga() {
    yield all([
        fork(createPersonal),
        fork(getPersonal),
        fork(updatePersonal),
    ])
}