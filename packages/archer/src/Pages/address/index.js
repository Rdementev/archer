import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import {changeAddressData, createAddress, updateAddressData, clearAddressDefaultValue, setEditAddressId} from "./actions";
import {setAddressForPersonal} from "PagesARDM/personal/actions";
import ListAddress from "./components/list";
import NewModal from "FeatureARDM/modal/newmodal";
import {
    getArdmPagesAddressReselect,
} from "./reducer/address";
import CreateAdress from "./components/create";
import UpdateAddress from "./components/update";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";


const ChoiseAddress = (props) => {
    const {show, setShow, onClickAddress} = props
    const [showCreate, setShowCreate] = useState(false)
    const [showUpdate, setShowUpdate] = useState(false)
    const [editAddressId, setEditAddressId] = useState(null)


    const handleCloseModalAddress = () => {
        setEditAddressId(null)
        setShow(false)
    }

    return (
            <NewModal showModal={show} setShow={setShow}>
                <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Список адресов</ModalTitle>
                    <BlockIcon onClick={()=>{handleCloseModalAddress()}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ModalBody>
                    <ListAddress onClickAddress={onClickAddress}
                                 setShow={setShow}
                                 setShowUpdate={setShowUpdate}
                                 setEditAddressId={setEditAddressId}
                                 setShowCreate={setShowCreate}/>
                </ModalBody>
                    {(showCreate  || showUpdate) &&  <ModalFooter>
                        <CreateAdress show={showCreate} setShowCreate={setShowCreate}/>
                        <UpdateAddress show={showUpdate}
                                                      setShow={setShowUpdate}
                                                      editAddressId={editAddressId}/>
                    </ModalFooter>}
                </ContainerModal>
            </NewModal>
    )
}

const mapStateToProps = (state) => {
    return {
        address: getArdmPagesAddressReselect(state),
    }
}

export default connect(mapStateToProps, {
    changeAddressData, updateAddressData, createAddress, setAddressForPersonal, setEditAddressId,
    clearAddressDefaultValue,
})(ChoiseAddress)

const ContainerModal = styled.div`
    padding: 20px;
    min-width: 800px;
    display: grid;
    grid-template-rows: 50px minmax(400px, 100%);
    overflow-y: auto;
    max-height: 800px;
    height: 100%;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
`;
const BlockIcon = styled.div`
    cursor: pointer;
    width: 14px;
    height: 14px;
`;


const ModalBody = styled.div`
    display: grid;
    grid-template-rows: 40px 50px minmax(300px, 100%) 50px;
`;
const ModalFooter = styled.div`
   
`;

const ModalHeader = styled.div`
   display: flex;
   justify-content: space-between;
`;
const ModalTitle = styled.h4`
  
`;

