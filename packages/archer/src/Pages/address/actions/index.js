export const CHANGE_ADDRESS_DATA = 'CHANGE_ADDRESS_DATA'
export const changeAddressData = (alias, value) => ({type:CHANGE_ADDRESS_DATA, alias, value})

export const CREATE_ADDRESS = 'CREATE_ADDRESS'
export const createAddress = (newAddress) => ({type:CREATE_ADDRESS, newAddress})

export const UPDATE_ADDRESS_DATA = 'UPDATE_ADDRESS_DATA'
export const updateAddressData = (id, updateAddress) => ({type:UPDATE_ADDRESS_DATA, id, updateAddress})

export const CHANGE_EDIT_ADDRESS = 'SET_EDIT_ADDRESS'
export const changeEditAddress = (searchEditAddress) => ({type:CHANGE_EDIT_ADDRESS, searchEditAddress})

export const SET_EDIT_ADDRESS_ID = 'SET_EDIT_ADDRESS_ID'
export const setEditAddressId = (id) => ({type:SET_EDIT_ADDRESS_ID, id})

export const DELETE_ADDRESS = 'DELETE_ADDRESS'
export const deleteAddress = (id) => ({type:DELETE_ADDRESS, id})

export const SET_FIELDS_ADDRESS = 'SET_FIELDS_ADDRESS';
export const setFieldsAddress = (value) => ({type: SET_FIELDS_ADDRESS, value});

export const CLEAR_ADDRESS_DEFAULT_VALUE = 'CLEAR_ADDRESS_DEFAULT_VALUE';
export const clearAddressDefaultValue = () => ({type: CLEAR_ADDRESS_DEFAULT_VALUE});
// получение списка адресов
export const ARDM_PAGES_GET_ADDRESSES_REQUEST = 'ARDM_PAGES_GET_ADDRESSES_REQUEST';
export const ardmPagesGetAddressesRequest = (payload) => ({type: ARDM_PAGES_GET_ADDRESSES_REQUEST, payload});

export const ARDM_PAGES_GET_ADDRESSES_SUCCESS = 'ARDM_PAGES_GET_ADDRESSES_SUCCESS';
export const ardmPagesGetAddressesSuccess = (payload) => ({type: ARDM_PAGES_GET_ADDRESSES_SUCCESS, payload});

export const ARDM_PAGES_GET_ADDRESSES_FAILURE = 'ARDM_PAGES_GET_ADDRESSES_FAILURE';
export const ardmPagesGetAddressesFailure = () => ({type: ARDM_PAGES_GET_ADDRESSES_FAILURE});
// создание нового адреса
export const ARDM_PAGES_CREATE_ADDRESSES_REQUEST = 'ARDM_PAGES_CREATE_ADDRESSES_REQUEST';
export const ardmPagesCreateAddressesRequest = (payload) => ({type: ARDM_PAGES_CREATE_ADDRESSES_REQUEST, payload});

export const ARDM_PAGES_CREATE_ADDRESSES_SUCCESS = 'ARDM_PAGES_CREATE_ADDRESSES_SUCCESS';
export const ardmPagesCreateAddressesSuccess = (payload) => ({type: ARDM_PAGES_CREATE_ADDRESSES_SUCCESS, payload});

export const ARDM_PAGES_CREATE_ADDRESSES_FAILURE = 'ARDM_PAGES_CREATE_ADDRESSES_FAILURE';
export const ardmPagesCreateAddressesFailure = () => ({type: ARDM_PAGES_CREATE_ADDRESSES_FAILURE});
// обновление адреса
export const ARDM_PAGES_UPDATE_ADDRESSES_REQUEST = 'ARDM_PAGES_UPDATE_ADDRESSES_REQUEST';
export const ardmPagesUpdateAddressesRequest = (payload) => ({type: ARDM_PAGES_UPDATE_ADDRESSES_REQUEST, payload});

export const ARDM_PAGES_UPDATE_ADDRESSES_SUCCESS = 'ARDM_PAGES_UPDATE_ADDRESSES_SUCCESS';
export const ardmPagesUpdateAddressesSuccess = (payload) => ({type: ARDM_PAGES_UPDATE_ADDRESSES_SUCCESS, payload});

export const ARDM_PAGES_UPDATE_ADDRESSES_FAILURE = 'ARDM_PAGES_UPDATE_ADDRESSES_FAILURE';
export const ardmPagesUpdateAddressesFailure = () => ({type: ARDM_PAGES_UPDATE_ADDRESSES_FAILURE});


//удаление адреса

