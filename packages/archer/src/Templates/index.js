import React, {Suspense, useEffect, useState, lazy} from 'react'
import {connect, useDispatch} from "react-redux";
import styled, {ThemeProvider} from 'styled-components/macro'
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {compose} from "redux";
import {withTheme} from "HOCARJTTD/withTheme";
import HeaderClient from "FeatureARDM/header/Header";
import Main from "PagesARDM/main";
import Preloader from "ComponentsARJTTD/preloader";
import ARDM from "TemplatesARDM/ardm";
import {Document, BusinessAddress} from "Icons/header";


const Lk = lazy(() => import ('PagesARDM/lk'))
const ArlaModule = lazy(()=> import ('TemplatesARDM/arla'))


const Serves = (props) => {
    const { user, theme, history, location } = props
    const [partLogo, setPartLogo] = useState(false)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch({type: 'ARDM_AUTH_IS_LOGGED_IN'})
    },[])

    const getLogoPart = (pathname) => {
       if(pathname.includes('/serves/document')){
           setPartLogo('Auto Document')
       } else if (pathname.includes('/serves/sales-address')){
           setPartLogo('Business Address')
       } else if (pathname === '/serves'){
           setPartLogo('')
       } else if (pathname === '/serves/account'){
           setPartLogo('Account')
       }
    }
    useEffect(()=>{
        getLogoPart(location.pathname)
    },[location])

    return (
        <ThemeProvider theme={theme}>
            <StyledARDM className={'Serves'}>
                <HeaderClient part={partLogo}/>
                <Suspense fallback={<Preloader/>}>
                    <Switch>
                        <Route exact path={'/serves'} render={() => <Main {...props}/>}/>
                        <Route path={'/serves/account'} render={() => <Lk {...props}/>}/>
                        <Route path={'/serves/sales-address'} render={() => <ArlaModule {...props}/>}/>
                        <Route path={'/serves/document'} render={() => <ARDM {...props} setPartLogo={setPartLogo}/>}/>
                        <Redirect to="/serves" />
                    </Switch>
                </Suspense>
            </StyledARDM>
        </ThemeProvider>
    )
}
const mapStateToProps = (state) => {
    return {
        user : state.user
    }
}

export default compose(
    connect(mapStateToProps,{}),
    withTheme,
    withRouter,
)(Serves)
//
const StyledARDM = styled.div`
    height: 100%;
    display: grid;
    grid-template-rows: 40px minmax(100px,100%);

`;
