import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import 'antd/dist/antd.css';
import * as moment from "moment";
import ListOrganization from "./components/list";
import {
    getARDMEditOrganizationIdReselect,
    getARDMOrganizationDefaultReselect,
    getARDMOrganizationReselect,
} from "PagesARDM/organization/reselect";
import NewModal from "FeatureARDM/modal/newmodal";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import {compose} from "redux";
import {withTheme} from "styled-components";
import CreateOrganization from "./components/create";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import UpdateOrganization from "./components/update";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";

const ChoiseOrganization = (props) => {
    const {show, setShow, onClickOrg, setAddressForOrganization,} = props
    const [showCreateOrg, setShowCreatrOrg] = useState(false)
    const [showUpdateOrg, setShowUpdateOrg] = useState(false)
    const [editOrganizationId, setEditOrganizationId] = useState(null)
    // клик на имя представителя
    const handleClick = (id) => {

    }


    //
    const handleCloseModal = () => {
        setShow(false)
    }

    return (
        <NewModal showModal={show} setShow={setShow}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Список организаций</ModalTitle>
                    <BlockIcon onClick={()=>{handleCloseModal()}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ModalBody>
                    <ListOrganization onClickOrg={onClickOrg}
                                      setShow={setShow}
                                      setEditOrganizationId={setEditOrganizationId}
                                      setShowUpdateOrg={setShowUpdateOrg}
                                      setShowCreatrOrg={setShowCreatrOrg}/>
                    {showCreateOrg && <CreateOrganization show={showCreateOrg} setShow={setShowCreatrOrg}/>}
                    {showUpdateOrg && <UpdateOrganization show={showUpdateOrg}
                                                          editOrganizationId={editOrganizationId}
                                                          setShow={setShowUpdateOrg}/>}
                </ModalBody>
            </ContainerModal>
        </NewModal>
    )
}



const mapStateToProps = (state) => {
    return {
        organization: getARDMOrganizationReselect(state),
        address: getArdmPagesAddressReselect(state),
        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
}

export default compose(
    connect(mapStateToProps, {

    }),
    withTheme
)(ChoiseOrganization)

const ContainerModal = styled.div`
    padding: 20px;
    min-width: 800px;
    display: grid;
    grid-template-rows: 50px minmax(400px, 100%);
    overflow-y: auto;
    max-height: 800px;
    height: 100%;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: 0 14px 34px rgba(0,0,0,0.4)
`;
const BlockIcon = styled.div`
    width: 14px;
    height: 14px;
`;

const ModalBody = styled.div`
    display: grid;
    grid-template-rows: 40px 50px minmax(400px, 100%) 50px;
`;


const ModalHeader = styled.div`
   display: flex;
   justify-content: space-between;
   
`;
const ModalTitle = styled.h4`
  
`;





