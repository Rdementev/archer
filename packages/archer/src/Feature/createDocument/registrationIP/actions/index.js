// ARDM_FEATURE_REGIP_
// ardmFeatureRegIp

export const ARDM_FEATURE_REGIP_SET_TYPE_SEND = 'ARDM_FEATURE_REGIP_SET_TYPE_SEND'
export const ardmFeatureRegIpSetTypeSend = (value) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_SEND , value})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_RESPONSE = 'ARDM_FEATURE_REGIP_SET_TYPE_RESPONSE'
export const ardmFeatureRegIpSetTypeResponse = (value) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_RESPONSE , value})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_PROXY_ID = 'ARDM_FEATURE_REGIP_SET_TYPE_PROXY_ID'
export const ardmFeatureRegIpSetTypeProxy = (id) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_PROXY_ID , id})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_SYS_NAL = 'ARDM_FEATURE_REGIP_SET_TYPE_SYS_NAL'
export const ardmFeatureRegIpSetTypeSysNal = (value) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_SYS_NAL , value})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_RATE_NAL = 'ARDM_FEATURE_REGIP_SET_TYPE_RATE_NAL'
export const ardmFeatureRegIpSetTypeRateNal = (value) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_RATE_NAL , value})
//
export const ARDM_FEATURE_REGIP_SET_REG_ORGAN = 'ARDM_FEATURE_REGIP_SET_REG_ORGAN'
export const ardmFeatureRegIPSetRegOrgan = (value) => ({type: ARDM_FEATURE_REGIP_SET_REG_ORGAN, value})
//
export const ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN = 'ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN'
export const ardmFeatureRegIPChangeRegOrgan = (alias,value) => ({type: ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN, alias, value})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_SNILS = 'ARDM_FEATURE_REGIP_SET_TYPE_SNILS'
export const ardmFeatureRegIPSetTypeSnils = (value) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_SNILS, value})
//
export const ARDM_FEATURE_REGIP_SET_TYPE_PERSONAL_ID = 'ARDM_FEATURE_REGIP_SET_TYPE_PERSONAL_ID'
export const ardmFeatureRegIPSetTypePersonalId = (id) => ({type: ARDM_FEATURE_REGIP_SET_TYPE_PERSONAL_ID, id})
//
export const ARDM_FEATURE_REGIP_SET_SELECTED_OKVEDS = 'ARDM_FEATURE_REGIP_SET_SELECTED_OKVEDS'
export const ardmFeatureRegIPSetSelectedOkveds = (value) => ({type: ARDM_FEATURE_REGIP_SET_SELECTED_OKVEDS, value})
//
export const ARDM_FEATURE_REGIP_SET_FAVORITE_OKVED = 'ARDM_FEATURE_REGIP_SET_FAVORITE_OKVED'
export const ardmFeatureRegIPSetFavoriteOkved = (value) => ({type: ARDM_FEATURE_REGIP_SET_FAVORITE_OKVED, value})
//
export const ARDM_FEATURE_REGIP_CLEAR_FORM = 'ARDM_FEATURE_REGIP_CLEAR_FORM'
export const ardmFeatureRegIPClearForm = () => ({type: ARDM_FEATURE_REGIP_CLEAR_FORM, })
//