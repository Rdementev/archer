import createRequestRest from "ApiARJTTD/createRequestRest";
import {
    ARDM_PAGES_DOWNLOAD_FORM_REQUEST,
    ardmPagesDownloadFormSuccess,
    ardmPagesDownloadFormFailure,
} from "../actions";
import {all, put, call, select, take, takeEvery} from "redux-saga/effects";
import FileSaver from "file-saver";
import makeRequest from "ApiARJTTD";
import moment from 'moment'
import {addPreloader, deletePreloader} from "ReducerARJTTD/preloader";



function* download(payload) {
    yield put(addPreloader())

        const result = yield call(
            makeRequest,
            "post",
            '/autodoc/download_form_file',
            payload.payload,
            {Accept: "application/octet-stream"},
            false,
            true,
            {responseType: "arraybuffer"},
        );
        if(result.status !== 200){
            yield put(ardmPagesDownloadFormFailure())
        } else {
            const date = new Date()
            const dates = moment(date).format('DD.MM.YYYY')
            let fileName = payload.payload.fileName.concat(` от ${dates}.zip`)

            const blob = new Blob([result.data], {
                type: result.headers['content-type'] || 'application/octet-stream',
            });

            FileSaver.saveAs(blob, fileName);
            yield put(ardmPagesDownloadFormSuccess())
        }

        yield put(deletePreloader())

}

export function* downloadFormSaga() {

    yield takeEvery(ARDM_PAGES_DOWNLOAD_FORM_REQUEST, download, )
}


