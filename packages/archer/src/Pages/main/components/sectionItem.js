import React, {useEffect, useState, useRef} from 'react'
import {connect } from 'react-redux'
import styled from "styled-components";
import {NavLink} from "react-router-dom";

const data = [
    {
        id: 1,
        name: 'первый элемент',
    },
    {
        id: 2,
        name: 'второй элемент',
    },
    {
        id: 3,
        name: 'третий элемент',
    },
]


const SectionItem = (props) => {
    const {  } = props

    const getDataBody = (data) => {
        return data.map(item => {
            return (
                <BodyItem>
                    {item.name}
                </BodyItem>
            )
        })
    }

    return (
        <>
            {getDataBody(data)}
        </>
    )

}

const mapStateToProps = (state) => {
    return {

    }
}
export default connect(mapStateToProps,{

})(SectionItem)

const BodyItem = styled.div`
    margin-bottom: 4px;
    text-transform: none;
    font-size: 14px;
    padding: 5px;
    background: rgba(32, 54, 125, 0.1);
    width: 100%;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    
`;
