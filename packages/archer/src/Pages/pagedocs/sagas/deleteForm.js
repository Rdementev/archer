import createRequestRest from "ApiARJTTD/createRequestRest";

import {put} from "redux-saga/effects";
import {ARDM_PAGES_DELETE_PAGEDOCS_REQUEST, ardmPagesDeleteFormsPagedocsSuccess} from "../actions";
import {generateTempoMessage} from "ReducerARJTTD/flashMessages";


function* prepareRequest(payload) {
    return payload
}
function* prepareSuccess(result, payload) {
    yield put(ardmPagesDeleteFormsPagedocsSuccess(payload.id))
}

function* prepareFailure(result, payload) {

}

export const deleteFormSaga = () => {
    return createRequestRest({
        url: '/autodoc/delete_form',
        action: ARDM_PAGES_DELETE_PAGEDOCS_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        failure: generateTempoMessage,
        failureText:'',
        successText:'',
    })
}