import React from "react";
import TypeResponse from "./type_response";
import {compose} from "redux";

const TypeResponseContainer = (props) => {
    const {value, setValue, list} = props
    return <TypeResponse value={value} setValue={setValue} list={list}/>
}

export default compose()(TypeResponseContainer)
