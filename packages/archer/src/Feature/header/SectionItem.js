import React, { PureComponent } from "react";
import {NavLink, withRouter} from "react-router-dom";
import styled from "styled-components";
import {connect} from "react-redux";
import {toggleIsDrawerHomePage} from "PagesARDM/main/reducer/siteMap";


const section = `
  padding: 10px 10px;
  display: flex;
  width: 100%;
  align-items: center;
  transition: all 0.3s;
  border-radius: 4px;
`;

const SectionDiv = styled.div`
  ${section};
  &:hover {
    box-shadow: inset 0px 0px 20px 0px #9195974d;
    
  }
  
`;

const SectionLink = styled(NavLink)`
  ${section} &:hover {
    background-color: #ffffff26;
    color: #fff;
    box-shadow: inset 0px 0px 25px #1e3375;
  }
`;

const IconBlock = styled.div`
      min-width: 20px;
    margin-right: 15px;
    text-align: center;
    min-height: 20px;
    background: #000;
    max-width: 20px;

  & svg {
    max-width: 20px;
    max-height: 20px;
    fill: #2c4695;
    stroke: none;
  }
`;


const SectionItem = (props) => {

        const { link, name, history, toggleIsDrawerHomePage } = props;
        let Tag = SectionDiv;
        let prop = {};

        const handleClick = (e) => {
            history.push(link)
            toggleIsDrawerHomePage(false)
            e.stopPropagation()
        }

        return (
            <Li >
                <Line></Line>
                <Tag {...prop} onClick={(e)=>{handleClick(e)}}>
                    <IconBlock>
                    </IconBlock>
                    <SubTitleItems>{name}</SubTitleItems>
                </Tag>

            </Li>
        );

}
const RouterSectionItem  = withRouter(SectionItem)

export default connect(null,{
    toggleIsDrawerHomePage
})(RouterSectionItem);

//
const SubTitleItems = styled.div`
    text-transform:none;
`;
const Line = styled.div`
    margin: 0 13px 0 7px;
    width: 3px;
    height: 20px;
    background: #73061a;
    border-radius: 10px;
    opacity: 0;
`;
const Li = styled.div`
    display: flex;
    align-items: center;
    &:hover ${Line}{
        opacity: 1
    }
`;
