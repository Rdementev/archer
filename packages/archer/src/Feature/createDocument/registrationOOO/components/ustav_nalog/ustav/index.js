import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'

import {compose} from "redux";
import {connect} from "react-redux";
import Checkbox from "ComponentsARJTTD/checkBox/Checkbox";
import {
    getARDMFeatureRegistrationOOOTypeUstavCertifySolutionsReselect,
    getARDMFeatureRegistrationOOOTypeUstavMinAddressReselect, getARDMFeatureRegistrationOOOTypeUstavOkvedInUstavReselect
} from "../../../reducer";
import {
    ardmFeatureRegOOOSetUstavCertifySolutions,
    ardmFeatureRegOOOSetUstavMinAddress, ardmFeatureRegOOOSetUstavOkvedInUstav
} from "../../../actions";



const TypeUstav = (props) => {
    const {min_address, certify_solution, okved_in_ustav, ardmFeatureRegOOOSetUstavOkvedInUstav,
        ardmFeatureRegOOOSetUstavMinAddress, ardmFeatureRegOOOSetUstavCertifySolutions} = props

    const handleChangeMinAddress = (e) => {
        ardmFeatureRegOOOSetUstavMinAddress(e.target.checked)
    }
    const handleChangeCertifySolutions = (e) => {
        ardmFeatureRegOOOSetUstavCertifySolutions(e.target.checked)
    }
    const handleChangeOkvedInUstav = (e) => {
        ardmFeatureRegOOOSetUstavOkvedInUstav(e.target.checked)
    }

    return (
        <>
            <Header>
                <Title>Возможности устава</Title>
            </Header>
            <Offer>
                <StyledCheckbox>
                    <Checkbox onChange={(e) => {handleChangeMinAddress(e)}}
                              checked={min_address}
                              rightText={'Указать в уставе неполный адрес организации'}/>
                </StyledCheckbox>
                <StyledCheckbox>
                    <Checkbox onChange={(e) => {handleChangeCertifySolutions(e)}}
                              checked={certify_solution}
                              rightText={'Требовать заверять решения участников общества у нотариуса'}/>
                </StyledCheckbox>
                <StyledCheckbox>
                    <Checkbox onChange={(e) => {handleChangeOkvedInUstav(e)}}
                              checked={okved_in_ustav}
                              rightText={'Указать виды деятельности в уставе'}/>
                </StyledCheckbox>
            </Offer>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        min_address: getARDMFeatureRegistrationOOOTypeUstavMinAddressReselect(state),
        certify_solution: getARDMFeatureRegistrationOOOTypeUstavCertifySolutionsReselect(state),
        okved_in_ustav: getARDMFeatureRegistrationOOOTypeUstavOkvedInUstavReselect(state),
    }
}
export default compose(
    connect(mapStateToProps,{
        ardmFeatureRegOOOSetUstavMinAddress, ardmFeatureRegOOOSetUstavCertifySolutions, ardmFeatureRegOOOSetUstavOkvedInUstav
    }),
)(TypeUstav)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 100%;
  padding: 5px 10px;
`;
const StyledCheckbox = styled.div`
   & > label > p {
   font-size: 12px;
    
   }
`;
const Body = styled.div`
  
`;
const Footer = styled.div`
  
`;
