import React, {useEffect, useRef, useState} from 'react'
import styled from 'styled-components/macro'
import {compose} from "redux";
import {connect} from "react-redux";
import ContainerInputRus from "./rus";
import ContainerInputRussian from "./russian";
import ContainerInputEng from "./eng";
import ContainerInputEnglish from "./english";
import RequiredField from "ComponentsARJTTD/requiredField";



const TypeName = (props) => {
    const {
        type_company_name_rus,
        onChangeRus,
        type_company_name_rusIsValidation,
        type_company_name_rusIsTouch,
        setType_company_name_rusIsTouch,

        type_company_name_russian,
        type_company_name_russianIsValidation,
        type_company_name_russianIsTouch,
        setType_company_name_russianIsTouch,
        onChangeRussian,

        type_company_name_eng,
        type_company_name_english} = props

    return (
    <>
        <Header>
            <Title>Сокращенное наименование</Title>
            <Title>Полное наименование</Title>
        </Header>
        <Offer>
            <OfferBlock>
                <OfferText>
                    <OfferSpan>На русском</OfferSpan>
                    <RequiredField/>
                </OfferText>
                <Items>
                    <ContainerInputRus value={type_company_name_rus}
                                       valueIsValidation={type_company_name_rusIsValidation}
                                       valueIsTouch={type_company_name_rusIsTouch}
                                       setValueIsTouch={setType_company_name_rusIsTouch}
                                       onChange={onChangeRus}/>

                    <ContainerInputRussian value={type_company_name_russian}
                                           valueIsValidation={type_company_name_russianIsValidation}
                                           valueIsTouch={type_company_name_russianIsTouch}
                                           setValueIsTouch={setType_company_name_russianIsTouch}
                                           onChange={onChangeRussian}/>
                </Items>
            </OfferBlock>
            <OfferBlock>
                <OfferText>
                    <OfferSpan>На английском</OfferSpan>
                </OfferText>
                <Items>
                    <ContainerInputEng value={type_company_name_eng}/>
                    <ContainerInputEnglish value={type_company_name_english}/>
                </Items>
            </OfferBlock>
        </Offer>
    </>
    )
}
const mapStateToProps = (state) => {
    return {

    }
}

export default compose(
    connect(mapStateToProps, {

    }),
)(TypeName)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: grid;
    grid-template-columns: 2fr 3fr;
`;
const Title = styled.h3`
    margin: 0;
    letter-spacing: 0.04em;
    font-size: 9px;
    text-transform: uppercase;
    padding: 0 10px;
`;
const Offer = styled.div`
  padding: 0 10px;

`;
const OfferBlock = styled.div`
  &:not(:last-child){
    margin-bottom: 30px;
  }
`;
const Items = styled.div`
  display: grid;
  grid-template-columns: minmax(150px,30%) minmax(300px, 70%);
`;
const OfferText = styled.div`
    display: flex;
`;
const OfferSpan = styled.span`
    text-transform: uppercase;
    font-size: 10px;
    margin: 0 0 8px 0;
`;
