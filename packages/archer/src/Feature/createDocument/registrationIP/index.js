import React, {useEffect, useRef, useState} from 'react'
import {connect} from "react-redux";
import {compose} from "redux";
import BreadcrumbContainer from "Feature/breadcrumb";
import styled from "styled-components/macro";
import TypeSendContainer from "FeatureARDM/createDocument/components/type_send";
import TypeSysNalContainer from "FeatureARDM/createDocument/components/type_sys_nalog";
import TypeRateNalContainer from "FeatureARDM/createDocument/components/type_rate_nalog";
import TypeRegOrgan from "FeatureARDM/createDocument/components/type_reg_organ";
import TypeOkved from "FeatureARDM/createDocument/components/type_okved";
import TypeResponseContainer from "FeatureARDM/createDocument/components/type_response";

import {
    getARDMFeatureRegIPTypePersonalIdReselect,
    getARDMFeatureRegIPTypeProxyReselect,
    getARDMFeatureRegIPTypeRateNalReselect,
    getARDMFeatureRegIPTypeRegOrganReselect,
    getARDMFeatureRegIPTypeResponseReselect, getARDMFeatureRegIPTypeSelectedOkvedReselect,
    getARDMFeatureRegIPTypeSendReselect, getARDMFeatureRegIPTypeSnilsReselect,
    getARDMFeatureRegIPTypeSysNalReselect
} from "./reselect";
import {
    ardmFeatureRegIpSetTypeProxy,
    ardmFeatureRegIpSetTypeRateNal,
    ardmFeatureRegIpSetTypeResponse,
    ardmFeatureRegIpSetTypeSysNal,
    ardmFeatureRegIpSetTypeSend,
    ardmFeatureRegIPSetRegOrgan,
    ardmFeatureRegIPSetSelectedOkveds,
    ardmFeatureRegIPSetFavoriteOkved,
    ardmFeatureRegIPChangeRegOrgan
} from "./actions";
import SectionPersonal from "./components/sectionPersonal";
import Preloader from "ComponentsARJTTD/preloader";
import {ardmPagesGetFormDataRequest, ardmPagesUpdateFormsPagedocsRequest} from "PagesARDM/pagedocs/actions";
import {withRouter} from "react-router-dom";
import {Button, WhiteButton} from "ComponentsARJTTD/buttons/button_v2";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";


const listSend = [
    {id:1, name: 'Лично',text: 'В этом случае вам нужно будет лично явиться в инспекцию и подать весь комплект документов.'},
    {id:2, name: 'По доверености', text: 'Вам и вашему доверенному лицу не обойтись без посещения нотариуса. Нужно будет подготовить несколько дополнительных документов. Копию паспорта и доверенность сделает сам нотариус.'},
    {id:3, name: 'Электронно со своей КЭП', text: 'Не требуется оплата госпошлины с 1 января 2019 года.Без посещения нотариуса и налоговой инспекции.Требуется электронно-цифровая подпись.'},
    {id:4, name: 'Электронно с помощью КЭП нотариуса',},
]
const listResponse = [
    {id:1, name: 'Лично',},
    {id:2, name: 'По доверености',},
    {id:3, name: 'Направить на почту',},
]
const whiteButtonStyled =  {
    width: '150px',
}

const buttonStyledSave =  {
    width: '200px',
    marginLeft: '10px',
    fontSize: '12px',
    whiteSpace: 'no wrap',
}

const RegistrationIP = (props) => {

    const {
        ardmFeatureRegIpSetTypeSend, ardmFeatureRegIpSetTypeResponse, isShowPreloaderForm,
        ardmFeatureRegIpSetTypeRateNal,ardmFeatureRegIpSetTypeSysNal, ardmFeatureRegIPSetRegOrgan,
        ardmFeatureRegIPSetSelectedOkveds, ardmFeatureRegIPSetFavoriteOkved,ardmPagesGetFormDataRequest,
        location,personal,ardmFeatureRegIPChangeRegOrgan,
        ardmPagesUpdateFormsPagedocsRequest,
        type_proxy_id,
        type_send, type_response,
        type_sys_nal, type_rate_nal, type_reg_organ = {},
        type_personal_id, type_snils, type_selected_okved,} = props

    const [isUSN, setIsUSN] = useState(false)
    const [namePers, setNamePers] = useState('')

    useEffect(() => {
        const result = personal.find(item => item.id === type_personal_id)

        if(result) {
            const name = result.last_name.concat(' ', result.first_name.slice(0,1), '.', result.second_name.slice(0,1), '.' )
            return setNamePers(name)
        }
        setNamePers('')
    },[type_personal_id, personal])


    useEffect(()=>{
        if(type_sys_nal && type_sys_nal === 'Упрощённая система налогообложения (УСН)'){
            return setIsUSN(true)
        }
        setIsUSN(false)
    },[type_sys_nal])

    const url = location.pathname.split('/')
    const count = url.length
    const id = url[count - 1]

    useEffect(()=>{
        ardmPagesGetFormDataRequest({id:id})
    },[id])



    const handleClickSave = () => {
        const data = {
            id:id,
            type_send: type_send,
            type_response: type_response,
            type_proxy_id: type_proxy_id,
            type_sys_nal: type_sys_nal,
            type_rate_nal: type_rate_nal,
            type_reg_organ: type_reg_organ,
            type_personal_id: type_personal_id,
            type_snils: type_snils,
            type_selected_okved: type_selected_okved,
        }
        Object.keys(data).map(key => {
            if(!data[key]){
                if(key === 'type_proxy_id') return data[key] = null
                delete data[key]
            }
        })
        ardmPagesUpdateFormsPagedocsRequest(data)
    }

    const breadcrumb = [
        {
            title: 'сервис',
            link: '/serves',
        },
        {
            title: 'регистрационные документы',
            link: '/serves/document',
        },
        {
            title: `Регистрация ИП ${namePers}`,
            link: '',
        },
    ]

    return (
        <ContentWrapper>
            <BreadcrumbContainer list={breadcrumb}/>
            <Container>

                {isShowPreloaderForm ? <Preloader/> :
                (
                    <>
                    <Grid>
                    <Block>
                        <SubBlock>
                            <SubGrid>
                                <Content>
                                    <TypeSendContainer value={type_send} setValue={ardmFeatureRegIpSetTypeSend} list={listSend}/>
                                </Content>
                            </SubGrid>
                        </SubBlock>
                        <SubBlock>
                            <SubGrid>
                                <Content>
                                    <TypeResponseContainer list={listResponse} value={type_response} setValue={ardmFeatureRegIpSetTypeResponse}/>
                                </Content>
                            </SubGrid>
                        </SubBlock>
                        <SubBlock>
                            <SubGrid>
                                <Content>
                                    <TypeSysNalContainer typeSysNal={type_sys_nal}
                                                         setTypeSysNal={ardmFeatureRegIpSetTypeSysNal}/>
                                </Content>
                            </SubGrid>
                        </SubBlock>
                        <SubBlock>
                            <SubGrid>
                                {!isUSN && < Overlay/>}
                                <Content>
                                    {isUSN && <TypeRateNalContainer typeRateNal={type_rate_nal}
                                                                    setTypeRateNal={ardmFeatureRegIpSetTypeRateNal}/>}
                                </Content>
                            </SubGrid>
                        </SubBlock>
                    </Block>
                </Grid>
                <Grid>
                    <Block>
                        <SubBlock height={'35%'}>
                            <SubGrid>
                                <Content>
                                    <SectionPersonal type_snils={type_snils}
                                                     type_personalId={type_personal_id}
                                                     type_send={type_send}/>
                                </Content>
                            </SubGrid>
                        </SubBlock>
                        <SubBlock height={'65%'}>
                            <SubGrid>
                                <Content>
                                    <TypeRegOrgan func={ardmFeatureRegIPSetRegOrgan}
                                                  onChange={ardmFeatureRegIPChangeRegOrgan}
                                                  type_reg_organ={type_reg_organ}/>
                                </Content>
                            </SubGrid>
                        </SubBlock>
                    </Block>
                </Grid>
                <Grid>
                    <Block padding={'5px 0'}>
                        <Content>
                            <TypeOkved type_selected_okved={type_selected_okved}
                                       setFavorite={ardmFeatureRegIPSetFavoriteOkved}
                                       setSelected={ardmFeatureRegIPSetSelectedOkveds}/>
                        </Content>
                    </Block>
                </Grid>

                    </>)}
            </Container>

            <Footer>
                <BlockButton>
                    <WhiteButton styled={whiteButtonStyled} onClick={()=>{handleClickSave()}}>Сохранить</WhiteButton>
                    <Button styled={buttonStyledSave}  onClick={() => {alert()}}>Подготовить документы</Button>
                </BlockButton>
            </Footer>
        </ContentWrapper>
    )
}
const mapStateToProps = (state) => {
    return {
        type_send: getARDMFeatureRegIPTypeSendReselect(state),
        type_response: getARDMFeatureRegIPTypeResponseReselect(state),
        type_proxy_id: getARDMFeatureRegIPTypeProxyReselect(state),

        type_sys_nal: getARDMFeatureRegIPTypeSysNalReselect(state),
        type_rate_nal: getARDMFeatureRegIPTypeRateNalReselect(state),
        type_reg_organ: getARDMFeatureRegIPTypeRegOrganReselect(state),

        type_personal_id: getARDMFeatureRegIPTypePersonalIdReselect(state),
        type_snils: getARDMFeatureRegIPTypeSnilsReselect(state),
        type_selected_okved: getARDMFeatureRegIPTypeSelectedOkvedReselect(state),

        personal: ArdmPagesPersonalGetPersonalReselect(state),
    }
}
export default compose(
    connect(mapStateToProps,{
        ardmFeatureRegIpSetTypeSend, ardmFeatureRegIpSetTypeResponse, ardmFeatureRegIpSetTypeProxy,
        ardmFeatureRegIpSetTypeRateNal, ardmFeatureRegIpSetTypeSysNal, ardmFeatureRegIPSetRegOrgan,
        ardmFeatureRegIPSetSelectedOkveds, ardmFeatureRegIPSetFavoriteOkved, ardmPagesGetFormDataRequest,
        ardmPagesUpdateFormsPagedocsRequest, ardmFeatureRegIPChangeRegOrgan
    }),
    withRouter
)(RegistrationIP)
//
const Container = styled.div`
    position: relative;
    display: grid;
    grid-template-columns: repeat(3, minmax(300px, 100%));
    overflow: auto;
    height: 100%;
    padding: 5px 20px;
`;
const ContentWrapper = styled.div`
    padding-bottom: 10px;
    background-color: ${({transparent}) =>
    transparent ? transparent : "#f5f8fa"};
    grid-template-rows: 50px minmax(100px, 100%) 50px;
    display: grid;
    flex-direction: column;
    width: 100%;
    height: 100%;
`;
const Overlay = styled.div`
    z-index: 1;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background: #f5f8fa;
    border-radius: 4px;
`;
const Block = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 14px;
  display: flex;
  flex-direction: column;
  padding: ${({padding}) => padding ? padding : '0'}
`;
const SubBlock = styled.div`
  width: 100%;
  padding: 5px 0;
  height: ${({height}) => height ? height : '100%'};
  border-radius: 14px;
  display: flex;
  
`;

const Grid = styled.div`
  width: 100%;
  padding: 0 10px 0 0;
   &:last-child{
      padding: 0;
  }
`;

const SubGrid = styled.div`
  position: relative;
  width: 100%;
`;

const Content = styled.div`
  width: 100%;
  padding: 10px 0;
  background: #fff;
  border-radius: 4px;
  height: 100%;
  display: flex;
  flex-direction: column;  
`;

const BlockButton = styled.div`
    margin-top: 10px;
    display: flex;
`;
const Footer = styled.div`
    padding: 10px 20px;
`;

