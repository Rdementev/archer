import {all, fork} from "redux-saga/effects"
import loginSaga from 'PagesARDM/auth/sagas'
import {registrationOOOSaga} from "FeatureARDM/createDocument/registrationOOO/sagas";
import {AddressSaga} from "PagesARDM/address/sagas";
import {personalSaga} from "PagesARDM/personal/sagas";
import {organizationSaga} from "PagesARDM/organization/sagas";
import {pageDocsSaga} from "PagesARDM/pagedocs/sagas";
import {getOkvedsSaga} from "FeatureARDM/okveds/sagas";
import {rootSagaARJTTD} from "SagasARJTTD";


export default function* rootSaga() {
    yield all([
        fork(loginSaga),
        fork(registrationOOOSaga),
        fork(AddressSaga),
        fork(personalSaga),
        fork(pageDocsSaga),
        fork(organizationSaga),
        fork(rootSagaARJTTD),
        fork(getOkvedsSaga),
    ])
}
