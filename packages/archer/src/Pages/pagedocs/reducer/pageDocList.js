import {createSelector} from "reselect";
import {ARDM_PAGES_GET_PAGEDOCS_SUCCESS,
    ARDM_PAGES_DELETE_PAGEDOCS_SUCCESS,
    ARDM_PAGES_UPDATE_PAGEDOCS_SUCCESS,
    ARDM_PAGES_UPDATE_PAGEDOCS_FAILURE,
    ARDM_PAGES_IS_SHOW_PRELOADER} from "../actions";
export const SET_FILTERS_SUBHEADER_ARDM_PAGEDOC = 'SET_FILTERS_SUBHEADER_ARDM_PAGEDOC';
export const CHANGE_ARDM_PAGEDOCS_SEARCH_KEY = 'CHANGE_ARDM_PAGEDOCS_SEARCH_KEY';
export const CHANGE_ARDM_PAGEDOCS_OFF_SET = 'CHANGE_ARDM_PAGEDOCS_OFF_SET';
export const SET_ARDM_PAGEDOCS_TOTAL_COUNT = 'SET_ARDM_PAGEDOCS_TOTAL_COUNT';
export const TOGGLE_ARDM_PAGEDOCS_IS_LOAD = 'TOGGLE_ARDM_PAGEDOCS_IS_LOAD';


let initialState = {
    list: [],
    ur_face_ids: [],
    type_doc_ids: [],
    search_key: '',
    totalCount: null,
    isLoadList: false,
    isLoad: false,
    offset: 0,
    limit: 30,
    isShowPreloader: false,
};


const pageDocList = (state = initialState, action) => {
    switch (action.type) {
        case ARDM_PAGES_GET_PAGEDOCS_SUCCESS : {
            return {...state, list: action.payload}
        }
        case ARDM_PAGES_DELETE_PAGEDOCS_SUCCESS : {
            return {...state, list: state.list.filter(item => item.id !== action.payload)}
        }
        case ARDM_PAGES_IS_SHOW_PRELOADER : {
            return {...state, isShowPreloader: action.value}
        }
        case SET_FILTERS_SUBHEADER_ARDM_PAGEDOC : {
            return {...state, [action.types]: action.filters}
        }
        case CHANGE_ARDM_PAGEDOCS_SEARCH_KEY : {
            return {...state, search_key: action.value}
        }
        case CHANGE_ARDM_PAGEDOCS_OFF_SET : {
            return {...state, offset: state.offset + 1}
        }
        case SET_ARDM_PAGEDOCS_TOTAL_COUNT : {
            return {...state, totalCount: action.value}
        }
        case TOGGLE_ARDM_PAGEDOCS_IS_LOAD : {
            return {...state, isLoad: action.value}
        }
        case ARDM_PAGES_UPDATE_PAGEDOCS_FAILURE :
        case ARDM_PAGES_UPDATE_PAGEDOCS_SUCCESS : {
            return {...state, list: state.list.map(item => item.id === action.payload.id
                    ? {...item, completed: action.payload.completed}
                    : {...item}
                )}
        }


        case 'LOG_OUT_USER' : {
            return state = initialState
        }
    }
    return state
};

export const setFiltersSubheaderARDMPagedoc = (types, filters) => ({type: SET_FILTERS_SUBHEADER_ARDM_PAGEDOC, types, filters})
export const changeARDMPagedocsSearchKey = (value) => ({type: CHANGE_ARDM_PAGEDOCS_SEARCH_KEY, value})
export const changeARDMPagedocsOffset = () => ({type: CHANGE_ARDM_PAGEDOCS_OFF_SET})
export const setARDMPagedocsTotalCount = (value) => ({type: SET_ARDM_PAGEDOCS_TOTAL_COUNT, value})
export const toggleARDMPagedocsIsLoad = (value) => ({type: TOGGLE_ARDM_PAGEDOCS_IS_LOAD, value})

export default pageDocList;
// reselect
const getListDoc = (state) => state.ARDM.pageDocList.list
export const getListDocSelector = createSelector(getListDoc,(list) => list)
//
const getIsLoadList = (state) => state.ARDM.pageDocList.isLoadList
export const getIsLoadListSelector = createSelector(getIsLoadList,(isLoadList) => isLoadList)
//
const getIsShowPreloader = (state) => state.ARDM.pageDocList.isShowPreloader
export const getIsShowPreloaderReselect = createSelector(getIsShowPreloader,(isShowPreloader) => isShowPreloader)
//
const getARDMPagedocsUrFaceIds = (state) => state.ARDM.pageDocList.ur_face_ids
export const getARDMPagedocsUrFaceIdsReselect = createSelector(getARDMPagedocsUrFaceIds,(ur_face_ids) => ur_face_ids)
//
const getARDMPagedocsTypedocsId = (state) => state.ARDM.pageDocList.type_doc_ids
export const getARDMPagedocsTypedocsIdReselect = createSelector(getARDMPagedocsTypedocsId,(type_doc_ids) => type_doc_ids)
//
const getARDMPagedocsSearchKey = (state) => state.ARDM.pageDocList.search_key
export const getARDMPagedocsSearchKeyReselect = createSelector(getARDMPagedocsSearchKey,(search_key) => search_key)
//
const getARDMPagedocsLimit = (state) => state.ARDM.pageDocList.limit
export const getARDMPagedocsLimitReselect = createSelector(getARDMPagedocsLimit,(limit) => limit)
//
const getARDMPagedocsOffset = (state) => state.ARDM.pageDocList.offset
export const getARDMPagedocsOffsetReselect = createSelector(getARDMPagedocsOffset,(offset) => offset)
//
const getARDMPagedocsTotalCount = (state) => state.ARDM.pageDocList.totalCount
export const getARDMPagedocsTotalCountReselect = createSelector(getARDMPagedocsTotalCount,(totalCount) => totalCount)
//
const getARDMPagedocsIsLoad = (state) => state.ARDM.pageDocList.isLoad
export const getARDMPagedocsIsLoadReselect = createSelector(getARDMPagedocsIsLoad,(isLoad) => isLoad)
//
export const getARDMPagedocsIsLoadMore = state =>  {
    const {totalCount, offset, limit} = state.ARDM.pageDocList;
    return offset !== 0 ? offset * limit < totalCount : limit < totalCount;
};