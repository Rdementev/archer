import React, {Component, useEffect, useState} from "react";
import {connect} from "react-redux";
import styled, {keyframes} from "styled-components/macro";

import {toggleIsDrawerHomePage} from "PagesARDM/main/reducer/siteMap";
import {LosoS, Pace} from "Icons/header";
import {getIsDrawerHomePageServesSelector, getSiteMapSelector} from "PagesARDM/main/reducer/siteMap";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import {CSSTransition} from "react-transition-group";


const ClientSection = (props) => {
    const {siteMap, toggleIsDrawerHomePage, history} = props;
    const [changeSearch, setChangeSearch] = useState('')
    const [entered, setEntered] = useState(false)
    useEffect(()=>{
        setEntered(true)
    },[])

    const createGroupItems = (siteMap) => {
        return siteMap.map(item => {
            return (
                <CSSTransition
                    key={item.id}
                    unmountOnExit
                    in={entered}
                    timeout={{ appear:0, enter: 0, exit: 300 }}
                    classNames='roll'
                    appear
                >
                    <BlockGroup onClick={(e)=>{handleClickItem(e, item.location)}}>
                        {item.title && <TitleGroup>{item.title}</TitleGroup>}
                    </BlockGroup>
                </CSSTransition>
            )
        })
    };

    const handleChangeSearch = (value) => {
        setChangeSearch(value)
    }

    const handleClickItem = (e, link) => {
        setEntered(false)
        setTimeout(()=>{
            history.push(link)
            toggleIsDrawerHomePage(false)
        }, 300)
        e.stopPropagation()
    }
    const handleClick = (e) => {
        toggleIsDrawerHomePage(false)
        e.stopPropagation()
    }



    return (
        <>
            <ListHeader>
                <Item>
                    <NewIconStyledBlock onClick={(e) => {handleClick(e)}}>
                        <Icon/>
                    </NewIconStyledBlock>
                </Item>
                <BlockLogo>
                    <Pace/>
                </BlockLogo>
                {/*<Search>*/}
                {/*    <SearchBlockContainer value={changeSearch}*/}
                {/*                 fill={'rgba(255,255,255,0.7)'}*/}
                {/*                 placeholder={'Поиск...'}*/}
                {/*                 onChange={handleChangeSearch}/>*/}
                {/*</Search>*/}
            </ListHeader>
            <StyledList>
                {siteMap && createGroupItems(siteMap)}
            </StyledList>

        </>
    )
}

const mapStateToProps = state => {
    return {
        siteMap: getSiteMapSelector(state),
        isDrawerHomePageServes: getIsDrawerHomePageServesSelector(state),
    };
};



export default compose(
    connect(
        mapStateToProps, {
            toggleIsDrawerHomePage
        }
        ),
    withRouter
)(ClientSection);



const BlockGroup = styled.div`
    height: 220px;
    border-radius: 10px;
    background: #1d3375;
    color: #fff;
    display: flex;
    align-items: center;
    padding: 10px;
    &:not(:last-child){
        margin-bottom:20px;
    }
    &:hover {
        transform: scale(1.03);
        box-shadow: 0 10px 10px rgba(0, 0, 0, 0.3);
    }
    
    transition: ease 0.4s;
`;


const StyledList = styled.ul`
    padding: 10px;
    flex-grow: 1;
    overflow: scroll;
    color: #000;
    margin: 0 2px 0 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background: #fff;
  &::-webkit-scrollbar { 
    width:2px;
   }
  &::-webkit-scrollbar-thumb {
    margin-right: 2px;
    background-color: #A4ADC8;
    max-height: 56px!important;
    border-radius:2px;
  }
`;

const ListHeader = styled.div`
     display: flex;
    max-height: 40px;
    cursor: default;
    height: 100%;
    background: ${props => props.theme.heavy};
    align-items: center;
`;
const TitleGroup = styled.h2`
    text-transform: none;
    color: #fff;
    font-size: 15px;
    padding: 0;
    letter-spacing: 0.35em;
    font-weight: bold;
    text-align: center;
    margin: auto;
`;

const Item = styled.div`
  max-width: 200px;
  height: 100%;
  padding: 0 5px 0 20px;

  position: relative;

  display: flex;
  align-items: center;

  font-size: 16px;
  font-family: ProximaNova-Semibold, sans-serif;
  color: #ffffff;
  line-height: 1;
  cursor: pointer;

  transition: 0.3s all;
`;

const Icon = styled(LosoS)`
    border-radius: 50%;
    width: 100%;
    height: 100%;
    & > rect {
      transition: ease 0.4s;
    }
`;
const NewIconStyledBlock = styled.div`
  width: 24px;
  height: 24px;
  cursor: pointer;
  &:hover > ${Icon} > rect {
    fill-opacity: 1;
  }
`;
const BlockLogo = styled.div`
    display: flex;
    align-items: center;
`;