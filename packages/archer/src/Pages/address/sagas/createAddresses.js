
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {
    ARDM_PAGES_CREATE_ADDRESSES_REQUEST,
    ardmPagesCreateAddressesSuccess,
    ardmPagesGetAddressesRequest
} from "../actions";
import {put} from "redux-saga/effects";

function* prepareRequest ( payload) {

    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesCreateAddressesSuccess())
    yield put(ardmPagesGetAddressesRequest())
}

function* prepareFailure () {

}

export const createAddresses = () => {
    return createRequestRest({
        url: '/autodoc/create_address',
        action: ARDM_PAGES_CREATE_ADDRESSES_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}