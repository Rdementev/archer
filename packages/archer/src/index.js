import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {store, history, persistor } from './Core/store'
import {Provider} from "react-redux";
import App from "./App";
import { PersistGate } from 'redux-persist/integration/react'


ReactDOM.render(
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App history={history}/>
            </PersistGate>
        </Provider>
        , document.getElementById('root'));

serviceWorker.unregister();
