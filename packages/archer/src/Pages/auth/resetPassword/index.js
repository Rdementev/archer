import React, {useState} from 'react'
import {connect} from "react-redux";
import {getIsAuthSelector, getIsCheckEmailSelector, getIsCheckPhoneSelector} from "Core/selector/selector";
import Input from "PagesARDM/auth/components/input";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import styled from "styled-components/macro";
import {NavLink} from "react-router-dom";
import {ardmAuthResetPasswordGetPassword} from "./action";

const ResetPassword = (props) => {
    const { ardmAuthResetPasswordGetPassword} = props
    const [email, setEmail] = useState('')
    const handleClick = () => {
        ardmAuthResetPasswordGetPassword(email)
    }
    return (
        <Container>
            <BlockInfo/>
            <Block>
                <Header>
                    <Title>AR Document manager</Title>
                    <SubTitle>Введите Email указанный при регистрации <br/> и мы отправим ссылку для изменения пароля</SubTitle>
                </Header>
                <Offer>
                    <Items>
                        <OfferText>
                            <OfferSpan>
                                Email
                            </OfferSpan>
                            <OfferSpanDec>
                                *
                            </OfferSpanDec>
                        </OfferText>
                        <BlockInput>
                            <Input value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
                        </BlockInput>
                    </Items>


                </Offer>
                <Footer>
                    <BlockButton onClick={()=>{handleClick()}}>
                        <Button>Отправить письмо</Button>
                    </BlockButton>
                    <FooterText>
                        или
                    </FooterText>
                    <FooterItem to={'/auth/signin'}>
                        Войти в систему
                    </FooterItem>
                </Footer>
            </Block>

        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        isAuth: getIsAuthSelector(state),
        isCheckPhone: getIsCheckPhoneSelector(state),
        isCheckEmail: getIsCheckEmailSelector(state)
    }
}

export default connect(mapStateToProps, {
    ardmAuthResetPasswordGetPassword,
})(ResetPassword)
//
const BlockInfo = styled.div`
    width: 65%;
    min-height: 100vh;
    display: flex;
    background: radial-gradient(circle, rgba(81,109,190,1) 0%, rgba(41,68,147,1) 76%);
`;

const Container = styled.div`
    position: relative;
    min-width: 100vw;
    min-height: 100vh;
    display: flex;
`;
const Block = styled.div`
    margin: auto;
    padding: 20px 40px;
    display: flex;
    flex-direction: column;
   
`;
const Header = styled.div`
`;
const Title = styled.h3`
  color: #000;
    white-space: nowrap;
    letter-spacing: 9px;
    font-weight: 400;
    font-size: 20px;
    text-transform: uppercase;
    margin-bottom: 20px;
    
`;
const SubTitle = styled.h3`
    color: #000;
    letter-spacing: 0;
    white-space: nowrap;
    line-height: 25px;
    font-weight: 400;
`;
const Offer = styled.div`
    margin: 30px 0;
`;
const OfferText = styled.div`
  display: flex;
  margin: 0 0 7px 0;
`;
const OfferSpan = styled.span`

`;
const OfferSpanDec = styled.span`
  margin-left: 5px;
  color: red;
`;
const BlockInput = styled.div`
    height: 33px;
    margin-bottom: 5px;
    width: 408px;

`;
const BlockButton = styled.div`
    height: 30px;
    width: 150px;
`;
const Items = styled.div`
    &:not(:last-child){
        margin-bottom: 30px;
    }
`;
const Footer = styled.div`
    display: flex;
`;
const FooterText = styled.span`
   margin: 0 10px;
   display: flex;
   align-items: center;
`;
const FooterItem = styled(NavLink)`
    text-transform: none;
    text-decoration: none;
    font-size: 12px;
    font-weight: 600;
    display: flex;
    align-items: center;
    color: rgba(40,68,147,0.7);
    &:hover{
      color: #284493;
    }
`;

