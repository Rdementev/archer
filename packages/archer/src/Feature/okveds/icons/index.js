import React from "react";


import {ReactComponent as CloseHierarchei} from "./closeHierarchie.svg";
import {ReactComponent as OpenHierarchie} from './OpenHierarchie.svg'
import {ReactComponent as SimpleHierarchie} from './simpleHerarchie.svg'
import {ReactComponent as OpenGroup} from './openGroup.svg'
import {ReactComponent as Ring} from './ring.svg'


export {
    CloseHierarchei,
    OpenHierarchie,
    SimpleHierarchie,
    OpenGroup,
    Ring,
}