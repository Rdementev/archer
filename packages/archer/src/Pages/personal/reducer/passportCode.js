const GET_QUERY_PASSPORT = 'GET_QUERY_PASSPORT';
const GET_SUGGESTIONS_PASSPORT = 'GET_SUGGESTIONS_PASSPORT';
const TOGGLE_SHOW_SUGGESTIONS_PASSPORT = 'TOGGLE_SHOW_SUGGESTIONS_PASSPORT';
const TOGGLE_DISABLED_INPUT_PASSPORT = 'TOGGLE_DISABLED_INPUT_PASSPORT';
const TOGGLE_INPUT_FOCUSED_PASSPORT = 'TOGGLE_INPUT_FOCUSED_PASSPORT';
const SET_SUGGESTION_INDEX_PASSPORT = 'SET_SUGGESTION_INDEX_PASSPORT';

let initialState = {
    inputFocused: false,
    isValid: false,
    query: '',
    showSuggestions: true,
    suggestionIndex: 0,
    suggestions: [],
    count: 5,
    disabledInput: true,
};


const passportCode = (state = initialState, action) => {
    switch (action.type) {
        case GET_QUERY_PASSPORT : {
            return {...state, query: action.query}
        }
        case GET_SUGGESTIONS_PASSPORT : {
            return {...state, suggestions: action.suggestions}
        }
        case TOGGLE_SHOW_SUGGESTIONS_PASSPORT : {
            return {...state, showSuggestions: action.showSuggestions}
        }
        // case TOGGLE_DISABLED_INPUT_PASSPORT : {
        //     return {...state, disabledInput: action.disabledInput,}
        // }
        case TOGGLE_INPUT_FOCUSED_PASSPORT : {
            return {...state, inputFocused: action.inputFocused}
        }
        case SET_SUGGESTION_INDEX_PASSPORT : {
            return {...state, suggestionIndex: action.suggestionIndex}
        }
    }
    return state
};

export const getQuery = (query) => ({type: GET_QUERY_PASSPORT, query});
export const getSuggestions = (suggestions) => ({type: GET_SUGGESTIONS_PASSPORT, suggestions});
export const toggleShowSuggestions = (showSuggestions) => ({type: TOGGLE_SHOW_SUGGESTIONS_PASSPORT, showSuggestions});
export const toggleInputFocused = (inputFocused) => ({type: TOGGLE_INPUT_FOCUSED_PASSPORT, inputFocused});
export const setSuggestionIndex = (suggestionIndex) => ({type: SET_SUGGESTION_INDEX_PASSPORT, suggestionIndex});

export default passportCode;