// ARDM_AUTH_SIGNUP
// ardmAuthSignUp

export const ARDM_AUTH_SIGNUP_GET_SIGN_UP = 'ARDM_AUTH_SIGNUP_GET_SIGN_UP'
export const ardmAuthSignUpGetSignUp = (login, email, password) => ({type: ARDM_AUTH_SIGNUP_GET_SIGN_UP, payload: {login, email, password}})
//
export const ARDM_AUTH_SIGNUP_CHANGE = 'ARDM_AUTH_SIGNUP_CHANGE'
export const ardmAuthSignupChange = (alias, value) => ({type: ARDM_AUTH_SIGNUP_CHANGE, alias, value})

export const ARDM_AUTH_SIGNUP_SHOW_CHECK_EMAIL = 'ARDM_AUTH_SIGNUP_SHOW_CHECK_EMAIL'
export const ardmAuthSignUpShowCheckEmail = (value) => ({type:ARDM_AUTH_SIGNUP_SHOW_CHECK_EMAIL, payload: {value} })


export const ARDM_AUTH_SIGNUP_CHANGE_DOUBLE_SEND_EMAIL = 'ARDM_AUTH_SIGNUP_CHANGE_DOUBLE_SEND_EMAIL'
export const ardmAuthSignUpChangeDoubleSendEmail = (value) => ({type:ARDM_AUTH_SIGNUP_CHANGE_DOUBLE_SEND_EMAIL, payload: {value} })
