import React, {useEffect, useState} from 'react'
import {compose} from "redux";
import styled from 'styled-components/macro'
import PeaceForm from "./type_peace_form";
import {connect} from "react-redux";
import {
    getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect,
    getARDMFeatureRegistrationOOOTypeIndividualsReselect,
    getARDMFeatureRegistrationOOOTypeOrganizationsReselect, getARDMFeatureRegistrationOOOTypeSizecapitalReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect,
    getARDMFeatureRegistrationOOOTypeAddressIdReselect,
    getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect,
    getARDMFeatureRegistrationOOOTypeDirPositionReselect,
    getARDMFeatureRegistrationOOOTypeDirTimeReselect,
    getARDMFeatureRegistrationOOOTypePeaceFormIdReselect,
    getARDMFeatureRegistrationOOOTypeProxyReselect,
    getARDMFeatureRegistrationOOOTypeResponseReselect,
    getARDMFeatureRegistrationOOOTypeSendReselect,
} from "../../reducer";
import TableFounders from "./tableFounders";
import TypeDir from "./type_dir";
import TypeAddress from "./type_address";
import {checkFDARegistrationOOO} from "../../utils";
import {ardmFeatureRegOOOChangeValidationFDA} from "../../actions";


const Founders = (props) => {
    const {type_individuals, type_organizations, type_reg,
        isEqually,

        type_dir_is_dir_address,} = props
    const [isBlockFounders, setIsBlockFounders] = useState(false)


    useEffect(()=>{
        if(type_reg === 'Упращенная регистрация ООО'){
            return setIsBlockFounders(true)
        }
        setIsBlockFounders(false)
    },[type_reg])


    return (
        <Container>
            <Grid>
                {isBlockFounders && <Overlay/>}
                <Block>
                    <Content>
                        <PeaceForm type_reg={type_reg} isEqually={isEqually}/>

                        <TableContainer>
                            <StyledTable>
                                <TableFounders title={'individuals'}  founders={type_individuals}/>
                            </StyledTable>
                            <StyledTable>
                                <TableFounders title={'organizations'}  founders={type_organizations}/>
                            </StyledTable>
                        </TableContainer>
                    </Content>
                </Block>
            </Grid>
            <Grid>
                <Block>
                    <SubBlock>
                        <SubGrid>
                            <Content>
                                <TypeDir />
                            </Content>
                        </SubGrid>
                    </SubBlock>
                    <SubBlock>
                        <SubGrid>
                            <Content>
                                {type_dir_is_dir_address && <Overlay/>}
                                <TypeAddress type_dir_is_dir_address={type_dir_is_dir_address}/>
                            </Content>
                        </SubGrid>
                    </SubBlock>
                </Block>
            </Grid>
        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        type_individuals: getARDMFeatureRegistrationOOOTypeIndividualsReselect(state),
        type_organizations: getARDMFeatureRegistrationOOOTypeOrganizationsReselect(state),
        type_dir_is_dir_address: getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect(state),
        type_reg: getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_size_capital: getARDMFeatureRegistrationOOOTypeSizecapitalReselect(state),


        type_send:  getARDMFeatureRegistrationOOOTypeSendReselect(state),
        type_response:  getARDMFeatureRegistrationOOOTypeResponseReselect(state),
        type_proxy_id:  getARDMFeatureRegistrationOOOTypeProxyReselect(state),
        type_peace_form:  getARDMFeatureRegistrationOOOTypePeaceFormIdReselect(state),
        type_dir_personal_id:  getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect(state),
        type_dir_position:  getARDMFeatureRegistrationOOOTypeDirPositionReselect(state),
        type_dir_time:  getARDMFeatureRegistrationOOOTypeDirTimeReselect(state),
        type_address_id:  getARDMFeatureRegistrationOOOTypeAddressIdReselect(state),

    }
}
export default compose(
    connect(mapStateToProps,{
        ardmFeatureRegOOOChangeValidationFDA
    }),
)(Founders)
//
const Container = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  height: 100%;
`;
const Overlay = styled.div`
    z-index: 1;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background: #f5f8fa;
    border-radius: 4px;
`;
const Grid = styled.div`
  position: relative;
  width: 100%;
  padding: 0 10px 0 0;
  &:last-child{
      padding: 0;
  }
`;
const Block = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 14px;
`;
const Content = styled.div`
  width: 100%;
  padding: 10px 0;
  background: #fff;
  border-radius: 4px;
  height: 100%;
  display: flex;
  flex-direction: column;  
  position: relative; 
`;
const StyledTable = styled.div`
    height: 100%;
    &:last-child{
      margin: 20px 0 0 0;
      border-top: 1px solid #e8eaf3;
    }
`;
const TableContainer = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;
`;
const SubBlock = styled.div`
  width: 100%;
  height: 50%;
  border-radius: 14px;
  display: flex;
  &:last-child{
  padding-top: 5px
  }
  &:first-child{
  padding-bottom: 5px
  }
`;
const SubGrid = styled.div`
  width: 100%;
  &:last-child{
     margin-left: 5px;
  }
  &:first-child{
   
    margin-right: 5px;
  }
`;
