import { createSelector } from 'reselect'



export const getPersonalSelector = (state) => state.ARDM.personal.personal
export const getPersonalDefaultSelector = (state) => state.ARDM.personal.personalDefault
export const getEditPersonalIdSelector = (state) => state.ARDM.personal.editPersonalId
export const getIsAuthSelector = (state) => state.user.is_auth
export const getIsAdminSelector = (state) => state.user.isAdmin
export const getIsCheckPhoneSelector = (state) => state.user.isCheckPhone
export const getIsCheckEmailSelector = (state) => state.user.isCheckEmail
