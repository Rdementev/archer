import createRequestRest from "ApiARJTTD/createRequestRest";
import {ARDM_PAGES_GET_PAGEDOCS_REQUEST, ardmPagesGetFormsPagedocsSuccess, ardmPagesIsShowPreloaderPagedocs} from "../actions";
import {put, all, select} from "redux-saga/effects";
import {SET_FILTERS_SUBHEADER_ARDM_PAGEDOC,
    changeARDMPagedocsOffset,
    getARDMPagedocsOffsetReselect,
    getARDMPagedocsLimitReselect,
    getARDMPagedocsSearchKeyReselect,
    setARDMPagedocsTotalCount,
    toggleARDMPagedocsIsLoad,
    getARDMPagedocsTypedocsIdReselect,
    getARDMPagedocsUrFaceIdsReselect} from "PagesARDM/pagedocs/reducer/pageDocList";
import {addPreloader, deletePreloader} from "ReducerARJTTD/preloader";


function* prepareRequest(payload) {
    yield put(addPreloader())
    // yield put(toggleARDMPagedocsIsLoad(true))
    const [ur_face_ids, type_doc_ids, search_key, limit, offset] = yield all([
        select(getARDMPagedocsUrFaceIdsReselect),
        select(getARDMPagedocsTypedocsIdReselect),
        select(getARDMPagedocsSearchKeyReselect),
        select(getARDMPagedocsLimitReselect),
        select(getARDMPagedocsOffsetReselect)
    ])

    const data = {group_form_id:type_doc_ids, org_type: ur_face_ids, search_key:search_key, lim:limit, off:offset}
    return data
}
function* prepareSuccess(result, payload) {
    yield put(ardmPagesGetFormsPagedocsSuccess(result.data.forms))
    yield put(setARDMPagedocsTotalCount(result.data.total_count))
    yield put(deletePreloader())
}
function* prepareFailure(result, payload) {
    yield put(deletePreloader())}

export const getFormsSaga = () => {
    return createRequestRest({
        url: '/autodoc/get_ready_forms',
        action: [ARDM_PAGES_GET_PAGEDOCS_REQUEST, SET_FILTERS_SUBHEADER_ARDM_PAGEDOC],
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        successText: 'Данные успешно загружены',
        mathod:'get',
    })
}