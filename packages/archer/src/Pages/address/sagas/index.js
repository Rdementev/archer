import {all, fork} from "redux-saga/effects";
import {getAddresses} from "./getAddresses";
import {createAddresses} from "./createAddresses";
import {updateAddresses} from "./updateAddresses";


export function* AddressSaga() {
    yield all([
        fork(getAddresses),
        fork(updateAddresses),
        fork(createAddresses),
    ])
}