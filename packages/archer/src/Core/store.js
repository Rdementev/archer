import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import ReduxThunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import {persistReducer, persistStore} from 'redux-persist'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import storage from 'redux-persist/es/storage'

import CreateReducers from "ReducersARDM/rootReducer";
import rootSaga from 'SagasARDM'
const history = createBrowserHistory()

const persistConfig = {
    key: 'root',
    storage: storage,
    whitelist: ['user', 'router'],
};



const initializeStore = () => {

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const sagaMiddleware = createSagaMiddleware()

    const store = createStore(
        CreateReducers(history, persistConfig, null),
        composeEnhancers(
            applyMiddleware(
                routerMiddleware(history),
                sagaMiddleware,
                ReduxThunk,
            ),
    ))

    sagaMiddleware.run(rootSaga)

    store.asyncReducers = {};


    store.injectReducer = (reducers ) => {
        Object.keys(reducers).map(item =>  store.asyncReducers[item] = reducers[item])
        store.replaceReducer(CreateReducers(history, persistConfig, store.asyncReducers));
        return store;
    };

    store.reInitialReducer = () => {
        store.replaceReducer(CreateReducers(history, persistConfig, store.asyncReducers));
        return store
    }
    return store;
};


let store = initializeStore()
const persistor = persistStore(store);
store.persistor = persistor


export { store, history , persistor, persistConfig }
