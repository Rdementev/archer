import {
    ARDM_FEATURE_REGIP_SET_FAVORITE_OKVED,
    ARDM_FEATURE_REGIP_SET_REG_ORGAN, ARDM_FEATURE_REGIP_SET_SELECTED_OKVEDS, ARDM_FEATURE_REGIP_SET_TYPE_PERSONAL_ID,
    ARDM_FEATURE_REGIP_SET_TYPE_RATE_NAL,
    ARDM_FEATURE_REGIP_SET_TYPE_RESPONSE,
    ARDM_FEATURE_REGIP_SET_TYPE_SEND, ARDM_FEATURE_REGIP_SET_TYPE_SNILS,
    ARDM_FEATURE_REGIP_SET_TYPE_SYS_NAL
} from "../actions";
import {ARDM_PAGES_GET_FORM_DATA_REG_IP_SUCCESS} from "PagesARDM/pagedocs/actions";
import {ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN} from "Feature/createDocument/registrationIP/actions";


let initialState = {
    // type_send: '',
    // type_response: '',
    // type_proxy_id: '',
    // type_sys_nal: '',
    // type_rate_nal: '',
    // type_reg_organ: {},
    // type_personal_id: '',
    // type_snils: '',
    // type_selected_okved: []
}
const registrationIP = (state  = initialState, action) => {
    switch (action.type) {
        case ARDM_PAGES_GET_FORM_DATA_REG_IP_SUCCESS : {
            return {...state, ...action.payload }
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_SEND : {
            return {...state, type_send: action.value}
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_RESPONSE : {
            return {...state, type_response: action.value}
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_SYS_NAL : {
            return {...state, type_sys_nal: action.value}
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_RATE_NAL : {
            return {...state, type_rate_nal: action.value}
        }
        case ARDM_FEATURE_REGIP_SET_REG_ORGAN : {
            return {...state, type_reg_organ: action.value}
        }
        case ARDM_FEATURE_REGIP_CHANGE_REG_ORGAN : {
            return {...state, type_reg_organ: {...state.type_reg_organ, [action.alias]:action.value}}
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_SNILS : {
            return {...state, type_snils: action.value}
        }
        case ARDM_FEATURE_REGIP_SET_TYPE_PERSONAL_ID : {
            return {...state, type_personal_id: action.id}
        }
        case ARDM_FEATURE_REGIP_SET_SELECTED_OKVEDS : {
            return {...state, type_selected_okved: state.type_selected_okved.includes(action.value)
                    ? state.type_selected_okved.filter(item => item.code !== action.value.code)
                    : [...state.type_selected_okved, action.value]}
        }
        case ARDM_FEATURE_REGIP_SET_FAVORITE_OKVED : {
            return {...state,
                type_selected_okved: state.type_selected_okved.map(item =>
                    item.code === action.value.code
                        ? item.favorite ? {...item, favorite: false} : {...item, favorite: true}
                        : {...item, favorite: false})}
        }
    }
    return state
}

export default registrationIP
//
