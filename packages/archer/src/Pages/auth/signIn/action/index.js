// for sagas
export const ARDM_AUTH_GET_LOGIN = 'ARDM_AUTH_GET_LOGIN'
export const ardmAuthGetLogin = (email, password) => ({type: ARDM_AUTH_GET_LOGIN, payload: {email, password}})

export const ARDM_AUTH_GET_LOGOUT = 'ARDM_AUTH_GET_LOGOUT'
export const ardmAuthGetLogout = () => ({type: ARDM_AUTH_GET_LOGOUT, })

// for reduce
export const ARDM_AUTH_SIGNIN_CHANGE = 'ARDM_AUTH_SIGNIN_CHANGE'
export const ardmAuthSigninChange = (alias, value) => ({type:ARDM_AUTH_SIGNIN_CHANGE, alias, value })

export const ARDM_AUTH_SIGNIN_IS_MISTAKE = 'ARDM_AUTH_SIGNIN_IS_MISTAKE'
export const ardmAuthSigninIsMistake = (value) => ({type:ARDM_AUTH_SIGNIN_IS_MISTAKE, value })
//common
