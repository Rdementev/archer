import {DELETE_ADDRESS} from "PagesARDM/address/actions";
import {DELETE_PERSONAL, SET_ADDRESS_FOR_PERSONAL,} from "../actions";
import {ARDM_PAGES_GET_PERSONAL_SUCCESS} from "../actions";



let initialState = {
    personal: [],
};


const personal = (state = initialState, action) => {
    switch (action.type) {
        case DELETE_PERSONAL : {
            return {...state, personal: state.personal.filter(item => item.id !== action.id)}
        }
        case ARDM_PAGES_GET_PERSONAL_SUCCESS : {
            return {...state, personal: action.payload}
        }
        case SET_ADDRESS_FOR_PERSONAL : {
            return {
                ...state,
                personal: state.personal.map(item => item.id === state.editPersonalId
                    ? {...item, addressId: action.addressId} : {...item})
            }
        }
        case DELETE_ADDRESS : {
            return {
                ...state,
                personal: state.personal.map(item => item.addressId === action.id ? {...item, addressId:''} : {...item})
            }
        }



        case 'LOG_OUT_USER' : {
            return state = initialState
        }
    }
    return state
};

export default personal;
//

