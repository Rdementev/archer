import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import Input from "PagesARDM/auth/components/input";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import styled from "styled-components/macro";
import {NavLink} from "react-router-dom";
import {ardmAuthSignUpGetSignUp} from "./action";
import {ardmAuthSignupChange} from "PagesARDM/auth/signUp/action";
import {
    getAgreeingSignupReselect,
    getEmailSignupReselect,
    getLoginSignupReselect,
    getPasswordSignupReselect
} from "PagesARDM/auth/signUp/reducer";

const SignUp = (props) => {
    const {ardmAuthSignUpGetSignUp, ardmAuthSignupChange, login, email, password, agreeing} = props

    useEffect(()=>{
        document.addEventListener('keypress', handleKeypress, false)
        return  function () {
            document.removeEventListener('keypress', handleKeypress, false)
        }
    },[login, password, email, agreeing])

    const handleClick = () => {
        ardmAuthSignUpGetSignUp(login, email, password)
    }

    const handleKeypress = (e) => {
        if(e.which === 13){
            ardmAuthSignUpGetSignUp(login, email, password)
        }
    }

    return (
        <Container>
            <BlockInfo/>
            <Block>
                <Header>
                    <Title>AR Document manager</Title>
                    <SubTitle>Начните 30 - дневную бесплатную <br/> пробную версию</SubTitle>
                </Header>
                <Offer>
                    <Items>
                        <OfferText>
                            <OfferSpan>
                                Login
                            </OfferSpan>
                            <OfferSpanDec>
                                *
                            </OfferSpanDec>
                        </OfferText>
                        <BlockInput>
                            <Input value={login} onChange={(e)=>{ardmAuthSignupChange('login', e.target.value)}}/>
                        </BlockInput>
                    </Items>
                    <Items>
                        <OfferText>
                            <OfferSpan>
                                Email
                            </OfferSpan>
                            <OfferSpanDec>
                                *
                            </OfferSpanDec>
                        </OfferText>
                        <BlockInput>
                            <Input value={email} onChange={(e)=>{ardmAuthSignupChange('email', e.target.value)}}/>
                        </BlockInput>
                    </Items>
                    <Items>
                        <OfferText>
                            <OfferSpan>
                                Password
                            </OfferSpan>
                            <OfferSpanDec>
                                *
                            </OfferSpanDec>
                        </OfferText>
                        <BlockInput>
                            <Input type={'password'} value={password} onChange={(e)=>{ ardmAuthSignupChange('password', e.target.value)}}/>
                        </BlockInput>
                    </Items>
                    <BlockButton >
                        <Button onClick={()=>{handleClick()}}>Начать работу</Button>
                    </BlockButton>
                </Offer>
                <Footer>
                    <FooterBlock>
                        <FooterText>
                             Уже есть аккаунт ?
                        </FooterText>
                        <FooterItem to={'/auth/signin'}>
                            Войти в систему
                        </FooterItem>
                    </FooterBlock>
                </Footer>
            </Block>

        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        login: getLoginSignupReselect(state),
        email: getEmailSignupReselect(state),
        password: getPasswordSignupReselect(state),
        agreeing: getAgreeingSignupReselect(state),
    }
}

export default connect(mapStateToProps, {
    ardmAuthSignUpGetSignUp,ardmAuthSignupChange
})(SignUp)
//
const BlockInfo = styled.div`
    min-height: 100vh;
    display: flex;
    background: radial-gradient(circle, rgba(81,109,190,1) 0%, rgba(41,68,147,1) 76%);
`;

const Container = styled.div`
    position: relative;
    display: grid;
    grid-template-columns: minmax(500px, 100%)minmax(200px ,50%);
`;
const Block = styled.div`
    padding: 20px 40px;
    justify-content: center;
    display: flex;
    flex-direction: column;
   
`;
const Header = styled.div`
`;
const Title = styled.h3`
    color: #000;
    letter-spacing: 9px;
    font-weight: 400;
    font-size: 20px;
    text-transform: uppercase;
    margin-bottom: 20px;
    
`;
const SubTitle = styled.h3`
    color: #000;
    letter-spacing: 0;
    white-space: nowrap;
    line-height: 25px;
    font-weight: 400;
`;
const Offer = styled.div`
    margin: 30px 0;
`;
const OfferText = styled.div`
  display: flex;
  margin: 0 0 7px 0;
`;
const OfferSpan = styled.span`

`;
const OfferSpanDec = styled.span`
  margin-left: 5px;
  color: red;
`;
const BlockInput = styled.div`
    height: 32px;
    max-width: 300px;
    margin-bottom: 5px;
`;
const BlockButton = styled.div`
    height: 30px;
    width: 150px;
`;
const Items = styled.div`
    &:not(:last-child){
        margin-bottom: 30px;
    }
`;
const Footer = styled.div`
    display: flex;
`;
const FooterBlock = styled.div`
    display: flex;
`;
const FooterText = styled.span`
    
`;
const FooterItem = styled(NavLink)`
    text-transform: none;
    text-decoration: none;
    font-size: 12px;
    font-weight: 600;
    display: flex;
    align-items: center;
    margin-left: 10px;
    color: rgba(40,68,147,0.7);
    &:hover{
      color: #284493;
    }
`;

