import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import {compose} from "redux";
import {connect} from "react-redux";
import {getTypeRegOrganIfnsData} from "./actions";
import Input from "ComponentsARJTTD/input/inputComponent";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import RequiredField from "ComponentsARJTTD/requiredField";
import {getARDMFeatureRegistrationOOOTypeRegOrganIsLoadReselect} from "./reducer";


const TypeRegOrgan = (props) => {
    const {getTypeRegOrganIfnsData, type_reg_organ, type_reg_organ_is_load, func, onChange} = props

    const [value, setValue] = useState('')
    const handleClickItem = (func) => {
        if(!value) return
        let payload = {query: value, func:func};
        getTypeRegOrganIfnsData(payload)
    }

    return (
        <>
            <Header>
                <Title >Регистрирующая налоговая</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <BlockSend>
                    <OfferBlock>
                        <BodyText>Код</BodyText>
                        <BlockInput>
                            <Input value={value} onChange={(e)=>{setValue(e.target.value)}}/>
                        </BlockInput>
                    </OfferBlock>
                    <BlockButton>
                        <Button disabled={type_reg_organ_is_load} onClick={()=>{handleClickItem(func)}}>Загрузить</Button>
                    </BlockButton>
                </BlockSend>

            </Offer>
            <Body>
                <BodyBlock>
                    <BodyText>Наименование</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.name} onChange={(e)=>{onChange('name', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Инн</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.inn} onChange={(e)=>{onChange('inn', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Кпп</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.kpp} onChange={(e)=>{onChange('kpp', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Получатель платежа</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.payment_name} onChange={(e)=>{onChange('payment_name', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Октмо</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.oktmo} onChange={(e)=>{onChange('oktmo', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Наименование банка</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.bank_name} onChange={(e)=>{onChange('bank_name', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Счет</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.bank_account} onChange={(e)=>{onChange('bank_account', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
                <BodyBlock>
                    <BodyText>Бик</BodyText>
                    <BlockInputLong>
                        <Input value={type_reg_organ.bank_bic} onChange={(e)=>{onChange('bank_bic', e.target.value)}}/>
                    </BlockInputLong>
                </BodyBlock>
            </Body>
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        type_reg_organ_is_load: getARDMFeatureRegistrationOOOTypeRegOrganIsLoadReselect(state),

    }
}
export default compose(
    connect(mapStateToProps,{
        getTypeRegOrganIfnsData
    }),
)(TypeRegOrgan)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  padding: 5px 0;
`;
const BlockSend = styled.div`
  display: flex;
  padding:  0 20px;
  max-width: 450px;
  width: 100%;
  justify-content: space-between;
`;
const BlockInput = styled.div`
    width: 150px;
    height: 30px;
`;
const BlockInputLong = styled(BlockInput)`
  width: 100%;
`;

const BlockButton = styled.div`
    width: 150px;
    height: 30px;
    margin: auto 0 0 20px;  
`;
const Body = styled.div`
   padding: 5px 20px;
   max-width: 450px;
   overflow: auto;
`;
const BodyBlock = styled.div`
  margin-bottom: 5px;
`;
const OfferBlock = styled.div`

`;
const BodyText = styled.div`
  margin-bottom: 3px;
  font-size: 10px;
`;
const Footer = styled.div`
  
`;
