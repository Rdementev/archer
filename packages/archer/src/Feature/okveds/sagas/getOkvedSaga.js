import createRequestRest from "ApiARJTTD/createRequestRest";
import {GET_ARDM_FEATURE_OKVEDS, setARDMFeatureOkveds} from "../actions";
import {} from "Feature/okveds/actions";
import {put, all, select} from "redux-saga/effects";

function* prepareSuccess(response, payload) {

    const [search_key] = yield all([
        select()
    ])


    yield put(setARDMFeatureOkveds(response.data))
}

function* prepareFailure(response, payload) {
}

export const getOkvedSaga = () => {
    return createRequestRest({
        url: '/autodoc/get_okvad',
        action: GET_ARDM_FEATURE_OKVEDS,
        prepareSuccess,
        prepareFailure,
    })
}
