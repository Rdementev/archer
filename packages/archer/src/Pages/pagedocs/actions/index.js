// прелоадер
export const ARDM_PAGES_IS_SHOW_PRELOADER = 'ARDM_PAGES_IS_SHOW_PRELOADER';
export const ardmPagesIsShowPreloaderPagedocs = (value) => ({type: ARDM_PAGES_IS_SHOW_PRELOADER, value});

// на получение всех необходимых данных с бэка для форм
export const ARDM_PAGES_REQUEST_PAGEDOC_DATA = 'ARDM_PAGES_REQUEST_PAGEDOC_DATA';
export const ardmPagesRequestDataPagedoc = () => ({type: ARDM_PAGES_REQUEST_PAGEDOC_DATA});


// получение списка форм
export const ARDM_PAGES_GET_PAGEDOCS_REQUEST = 'ARDM_PAGES_GET_PAGEDOCS_REQUEST';
export const ardmPagesGetFormsPagedocsRequest = (payload) => ({type: ARDM_PAGES_GET_PAGEDOCS_REQUEST, payload});

export const ARDM_PAGES_GET_PAGEDOCS_SUCCESS = 'ARDM_PAGES_GET_PAGEDOCS_SUCCESS';
export const ardmPagesGetFormsPagedocsSuccess = (payload) => ({type: ARDM_PAGES_GET_PAGEDOCS_SUCCESS, payload});

export const ARDM_PAGES_GET_PAGEDOCS_FAILURE = 'ARDM_PAGES_GET_PAGEDOCS_FAILURE';
export const ardmPagesGetFormsPagedocsFailure = () => ({type: ARDM_PAGES_GET_PAGEDOCS_FAILURE});

// создание новой формы
export const ARDM_PAGES_CREATE_PAGEDOCS_REQUEST = 'ARDM_PAGES_CREATE_PAGEDOCS_REQUEST';
export const ardmPagesCreateFormsPagedocsRequest = (payload) => ({type: ARDM_PAGES_CREATE_PAGEDOCS_REQUEST, payload});

export const ARDM_PAGES_CREATE_PAGEDOCS_SUCCESS = 'ARDM_PAGES_CREATE_PAGEDOCS_SUCCESS';
export const ardmPagesCreateFormsPagedocsSuccess = (payload) => ({type: ARDM_PAGES_CREATE_PAGEDOCS_SUCCESS, payload});

export const ARDM_PAGES_CREATE_PAGEDOCS_FAILURE = 'ARDM_PAGES_CREATE_PAGEDOCS_FAILURE';
export const ardmPagesCreateFormsPagedocsFailure = () => ({type: ARDM_PAGES_CREATE_PAGEDOCS_FAILURE});

// получение  формы
export const ARDM_PAGES_GET_FORM_DATA_REQUEST = 'ARDM_PAGES_GET_FORM_DATA_REQUEST';
export const ardmPagesGetFormDataRequest = (payload) => ({type: ARDM_PAGES_GET_FORM_DATA_REQUEST, payload});

export const ARDM_PAGES_GET_FORM_DATA_REG_OOO_SUCCESS = 'ARDM_PAGES_GET_FORM_DATA_REG_OOO_SUCCESS';
export const ardmPagesGetFormDataRegOOOSuccess = (payload) => ({type: ARDM_PAGES_GET_FORM_DATA_REG_OOO_SUCCESS, payload});

export const ARDM_PAGES_GET_FORM_DATA_REG_OOO_FAILURE = 'ARDM_PAGES_GET_FORM_DATA_REG_OOO_FAILURE';
export const ardmPagesGetFormDataRegOOOFailure = () => ({type: ARDM_PAGES_GET_FORM_DATA_REG_OOO_FAILURE});

export const ARDM_PAGES_GET_FORM_DATA_REG_IP_SUCCESS = 'ARDM_PAGES_GET_FORM_DATA_REG_IP_SUCCESS';
export const ardmPagesGetFormDataRegIPSuccess = (payload) => ({type: ARDM_PAGES_GET_FORM_DATA_REG_IP_SUCCESS, payload});

export const ARDM_PAGES_GET_FORM_DATA_REG_IP_FAILURE = 'ARDM_PAGES_GET_FORM_DATA_REG_IP_FAILURE';
export const ardmPagesGetFormDataRegIPFailure = () => ({type: ARDM_PAGES_GET_FORM_DATA_REG_IP_FAILURE});

// обновление формы
export const ARDM_PAGES_UPDATE_PAGEDOCS_REQUEST = 'ARDM_PAGES_UPDATE_PAGEDOCS_REQUEST';
export const ardmPagesUpdateFormsPagedocsRequest = (payload) => ({type: ARDM_PAGES_UPDATE_PAGEDOCS_REQUEST, payload});

export const ARDM_PAGES_UPDATE_PAGEDOCS_SUCCESS = 'ARDM_PAGES_UPDATE_PAGEDOCS_SUCCESS';
export const ardmPagesUpdateFormsPagedocsSuccess = (payload) => ({type: ARDM_PAGES_UPDATE_PAGEDOCS_SUCCESS, payload});

export const ARDM_PAGES_UPDATE_PAGEDOCS_FAILURE = 'ARDM_PAGES_UPDATE_PAGEDOCS_FAILURE';
export const ardmPagesUpdateFormsPagedocsFailure = () => ({type: ARDM_PAGES_UPDATE_PAGEDOCS_FAILURE});

//удаление формы
export const ARDM_PAGES_DELETE_PAGEDOCS_REQUEST = 'ARDM_PAGES_DELETE_PAGEDOCS_REQUEST';
export const ardmPagesDeleteFormsPagedocsRequest = (payload) => ({type: ARDM_PAGES_DELETE_PAGEDOCS_REQUEST, payload});

export const ARDM_PAGES_DELETE_PAGEDOCS_SUCCESS = 'ARDM_PAGES_DELETE_PAGEDOCS_SUCCESS';
export const ardmPagesDeleteFormsPagedocsSuccess = (payload) => ({type: ARDM_PAGES_DELETE_PAGEDOCS_SUCCESS, payload});

export const ARDM_PAGES_DELETE_PAGEDOCS_FAILURE = 'ARDM_PAGES_DELETE_PAGEDOCS_FAILURE';
export const ardmPagesDeleteFormsPagedocsFailure = () => ({type: ARDM_PAGES_DELETE_PAGEDOCS_FAILURE});

//сборка формы
export const ARDM_PAGES_BUILD_FORM_REQUEST = 'ARDM_PAGES_BUILD_FORM_REQUEST';
export const ardmPagesBuildFormRequest = (payload) => ({type: ARDM_PAGES_BUILD_FORM_REQUEST, payload});

export const ARDM_PAGES_BUILD_FORM_SUCCESS = 'ARDM_PAGES_BUILD_FORM_SUCCESS';
export const ardmPagesBuildFormSuccess = (payload) => ({type: ARDM_PAGES_BUILD_FORM_SUCCESS, payload});

export const ARDM_PAGES_BUILD_FORM_FAILURE = 'ARDM_PAGES_BUILD_FORM_FAILURE';
export const ardmPagesBuildFormFailure = () => ({type: ARDM_PAGES_BUILD_FORM_FAILURE});

//скачивание формы
export const ARDM_PAGES_DOWNLOAD_FORM_REQUEST = 'ARDM_PAGES_DOWNLOAD_FORM_REQUEST';
export const ardmPagesDownloadFormRequest = (payload) => ({type: ARDM_PAGES_DOWNLOAD_FORM_REQUEST, payload});

export const ARDM_PAGES_DOWNLOAD_FORM_SUCCESS = 'ARDM_PAGES_DOWNLOAD_FORM_SUCCESS';
export const ardmPagesDownloadFormSuccess = (payload) => ({type: ARDM_PAGES_DOWNLOAD_FORM_SUCCESS, payload});

export const ARDM_PAGES_DOWNLOAD_FORM_FAILURE = 'ARDM_PAGES_DOWNLOAD_FORM_FAILURE';
export const ardmPagesDownloadFormFailure = () => ({type: ARDM_PAGES_DOWNLOAD_FORM_FAILURE});

