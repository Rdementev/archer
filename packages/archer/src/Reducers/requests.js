import {createSelector} from "reselect";

const IS_SHOW_PRELOADER = 'IS_SHOW_PRELOADER';

let initialState = {
    isShowPreloader: false
};


const requests = (state = initialState, action) => {
    switch (action.type) {

        case IS_SHOW_PRELOADER : {
            return {...state, isShowPreloader: action.value}
        }

        case 'LOG_OUT_USER' : {
            return state = initialState
        }
    }
    return state
};

//ac
export const isShowPreloader = (value) => ({type: IS_SHOW_PRELOADER, value});
//reselect

const getIsShowPreloader = (state) => state.ARDM.requests.isShowPreloader
export const getIsShowPreloaderSelector = createSelector(getIsShowPreloader,(isShowPreloader) => isShowPreloader)

export default requests;
