import createRequestRest from 'ApiARJTTD/createRequestRest'
import {put} from "redux-saga/effects";
import {ARDM_PAGES_GET_PERSONAL_REQUEST, ardmPagesGetPersonalSuccess} from "../actions";


function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetPersonalSuccess(response.data))

}

function* prepareFailure () {

}

export const getPersonal = () => {
    return createRequestRest({
        url: '/autodoc/get_individuals',
        action: ARDM_PAGES_GET_PERSONAL_REQUEST,
        prepareSuccess,
        prepareFailure,
        mathod:'get'
    })
}