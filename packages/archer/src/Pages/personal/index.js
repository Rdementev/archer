import React, {useEffect, useState} from 'react'
import styled from "styled-components/macro";
import {connect} from "react-redux";
import 'antd/dist/antd.css';
import {
    changePersonalData,
    changeSexType,
    updatePersonalData,
} from "PagesARDM/personal/actions";
import {DatePicker} from "antd";
import ListPersonal from "./components/list";
import NewModal from "FeatureARDM/modal/newmodal";
import ChoiseAddress from "PagesARDM/address";
import {setAddressForPersonal, setEditPersonalId} from "./actions";
import {getArdmPagesAddressReselect} from "PagesARDM/address/reducer/address";
import CreatePersonal from "./components/create";
import UpdatePersonal from "./components/update";
import {ArdmPagesPersonalGetPersonalReselect} from "PagesARDM/personal/reselect";
import CircleCloseBtn from "ComponentsARJTTD/buttons/circle_close_btn";


const ChoisePersonal = (props) => {
    const {
        onClickPers,
        show,
        setShow,
        setAddressForPersonal} = props

    const [showAddressModal, setShowModalAddress] = useState(false)
    const [showCreate, setShowCreate] = useState(false)
    const [showUpdate, setShowUpdate] = useState(false)
    const [editPersonalId, setEditPersonalId] = useState(null)

    const handleCloseModal = () => {
        setEditPersonalId(null)
        setShow(false)
    }

    // клик на адресс из списка
    const handleClickAddress = (id) => {
        setAddressForPersonal(id)
        setShowModalAddress(false)
    }

    return (
            <NewModal showModal={show} setShow={setShow}>
            <ContainerModal>
                <ModalHeader>
                    <ModalTitle>Список пользователей</ModalTitle>
                    <BlockIcon onClick={()=>{handleCloseModal()}}>
                        <CircleCloseBtn/>
                    </BlockIcon>
                </ModalHeader>
                <ListPersonal onClickPers={onClickPers}
                              setShowCreate={setShowCreate}
                              setShowUpdate={setShowUpdate}
                              setEditPersonalId={setEditPersonalId}
                              setShow={setShow} />
                <CreatePersonal show={showCreate}
                                setShowCreate={setShowCreate}/>
                {showUpdate && <UpdatePersonal show={showUpdate}
                                setShow={setShowUpdate}
                                editPersonalId={editPersonalId}/>}
            </ContainerModal>
                <ChoiseAddress onClickAddress={handleClickAddress} show={showAddressModal} setShow={setShowModalAddress}/>
            </NewModal>
    )
}



const mapStateToProps = (state) => {
    return {
        personal: ArdmPagesPersonalGetPersonalReselect(state),
        address: getArdmPagesAddressReselect(state),
    }
}

export default connect(mapStateToProps, {
     changePersonalData, changeSexType,setAddressForPersonal,
     updatePersonalData, setEditPersonalId,

})(ChoisePersonal)

const ContainerModal = styled.div`
    padding: 20px;
    min-width: 800px;
    display: grid;
    grid-template-rows: 50px 40px 50px minmax(400px, 100%) 50px;
    overflow-y: auto;
    max-height: 800px;
    height: 100%;
    background: #fff;
    border-radius: 8px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    box-shadow: 0 14px 34px rgba(0,0,0,0.4)
`;
const ModalHeader = styled.div`
    display: flex;
    justify-content: space-between;
`;
const BlockIcon = styled.div`
  cursor: pointer;
  width: 14px;
    height: 14px;
`;

const ModalTitle = styled.h4`
  
`;




