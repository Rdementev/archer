import React, {useState} from 'react'
import styled from 'styled-components/macro'
import {SearchBlock} from '@rdementev/lib'
import {compose} from "redux";
import {withTheme} from "styled-components";

/**
 *
 * value - string
 * onChange - func
 * bold - boolean (для определния стиля)
 * **/



const SearchBlockContainer = (props) => {
    const {theme , style, onFocus = null} = props
    const [value, setValue] = useState('')
    const searchBlock = {
        container: {},
        blockIconSearch : {},
        input: {
            height: '100%',
            padding: '10px 10px 10px 32px',
            borderRadius: '4px',
            backgroundColor: theme.semiHeavy,
            borderColor: theme.semiHeavy,
            color: theme.root,
            width: '100%',
            transition: 'ease 0.3s',
            '::placeholder': {
                color: 'rgba(255,255,255,0.7)'
            },

            '&:hover': {
                borderColor: theme.medium,
                color: '#fff',
            },
            '&:focus': {
                borderColor: theme.medium,
                color: '#fff'
            }

        },
        blockClear : {},
        blockIconClear : {},
        blockEnter : {},
        blockEnterText : {},
        blockEnterIcon : {},
    }
    const handleChange = (value) => {
        setValue(value)
    }
    const handleClick = () => {

    }

    const getStyle = () => {
        if(style) {
            Object.keys(searchBlock).map(item => {
                Object.keys(style).map(elem => {
                    if(item === elem){
                        searchBlock[item] = {...searchBlock[item], ...style[elem] }
                    }
                })
            })
        }
        return searchBlock
    }

    return <SearchBlock value={value}
                        styled={getStyle()}
                        onChange={handleChange}
                        placeholder={"Поиск"}
                        onFocus={onFocus}
                        clear={false}
                        enter={false}
                        {...props}/>

}
export default compose(withTheme)(SearchBlockContainer)
//
