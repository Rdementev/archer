import {createSelector} from "reselect";
import {ARDM_CHANGE_REG_ORGAN_IS_LOAD} from "../actions";


const regOrgan = (state = {type_reg_organ_is_load: false}, action) => {
    switch (action.type) {
        case ARDM_CHANGE_REG_ORGAN_IS_LOAD: {
            return {...state, type_reg_organ_is_load: action.value}
        }
    } return state
}
export default regOrgan
//
const getARDMFeatureRegistrationOOOTypeRegOrganIsLoad = state => state.ARDM.regOrgan.type_reg_organ_is_load
export const getARDMFeatureRegistrationOOOTypeRegOrganIsLoadReselect = createSelector(getARDMFeatureRegistrationOOOTypeRegOrganIsLoad, (type_reg_organ_is_load) => type_reg_organ_is_load)
