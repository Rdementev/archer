import {
    ARDM_PAGES_GET_ADDRESSES_SUCCESS,
    CHANGE_ADDRESS_DATA,
    CHANGE_EDIT_ADDRESS, CLEAR_ADDRESS_DEFAULT_VALUE,
    CREATE_ADDRESS,
    DELETE_ADDRESS,
    SET_EDIT_ADDRESS_ID, SET_FIELDS_ADDRESS, UPDATE_ADDRESS_DATA
} from "../actions";
import {createSelector} from "reselect";


let initialState = {
    address :[

    ],
    newAddress: {},
};


const getUpdateAddressDataAfterEdit = (personal, personalDefault, action) => {
    return personal.map(item => {
        if(item.id === action.id){
            let updateAddress = {id: action.id,}
            Object.keys(item).map(key => {
                personalDefault.map(elem => {
                    if(key === elem.alias){
                        if(elem.sexType){
                            if(elem.value === true){
                                updateAddress = {...updateAddress, [key]: elem.sexType}
                            }return
                        } updateAddress = {...updateAddress, [key]: elem.value}
                    }
                })
            })
            return updateAddress
        }
        return {...item}
    })
}
const setNewValueDataAddress = (addressDefault,addressPattern, action) => {
    let serchEditAddress = []
    addressDefault.map(item => {
        Object.keys(addressPattern).map(key => {
            if (item.alias === key) {
                serchEditAddress = [...serchEditAddress, {...item, value: action.value[key] === null ? '' : action.value[key] }]
            }
        })
    })
    return serchEditAddress
}

const address = (state = initialState, action) => {
   switch (action.type) {
       case CREATE_ADDRESS :{
           return {...state, newAddress: action.newAddress,}
       }
       case ARDM_PAGES_GET_ADDRESSES_SUCCESS : {
           return {...state, address: action.payload}
       }

       case 'LOG_OUT_USER' : {
           return state = initialState
       }
   }
    return state
};

export default address;

const getEditAddressIdSelector = (state) => state.ARDM.address.editAddresslId
export const getArdmPagesEditAddressIdReselect = createSelector(getEditAddressIdSelector,(editAddresslId)=> editAddresslId)

const getAddressSelector = (state) => state.ARDM.address.address
export const getArdmPagesAddressReselect = createSelector(getAddressSelector,(address) => address)

const getAddressNewAddress = (state) => state.ARDM.address.newAddress
export const getAddressNewAddressReselect = createSelector(getAddressNewAddress,(newAddress) => newAddress)
