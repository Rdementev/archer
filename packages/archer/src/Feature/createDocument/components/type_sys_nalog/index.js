import React from "react";
import TypeSysNal from "./sys_nal";
import {compose} from "redux";


const list = [
    {id:1, name: 'Упрощённая система налогообложения (УСН)'},
    {id:2, name: 'Единый сельскохозяйственный налог (ЕСХН)'},
    {id:3, name: 'Общая система налогообложения (ОСНО)'},
]
const TypeSysNalContainer = (props) => {
    const {typeSysNal, setTypeSysNal, isUSN = false, setIsBlockRate} = props
    return <TypeSysNal typeSysNal={typeSysNal}
                       isUSN={isUSN}
                       setIsBlockRate={setIsBlockRate}
                       setTypeSysNal={setTypeSysNal}
                       list={list}/>
}

export default compose()(TypeSysNalContainer)
