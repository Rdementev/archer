import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import {compose} from "redux";
import {connect} from "react-redux";
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import RequiredField from "ComponentsARJTTD/requiredField";



const TypeSysNalog = (props) => {
    const {typeSysNal, setTypeSysNal, isUSN, setIsBlockRate, list} = props
    const [value, setValue] = useState(typeSysNal)

    useEffect(()=> {
        if(typeSysNal){
            const active = list.find(item => item.name === typeSysNal)
            setValue(active.name)
        }
    },[typeSysNal])

    useEffect(()=> {
        if(isUSN){
            setTypeSysNal('Упрощённая система налогообложения (УСН)')
            setIsBlockRate(false)
        }
    },[isUSN])


    const handleClickItem = (item, multi , other) => {
        if(isUSN){
            return setTypeSysNal('Упрощённая система налогообложения (УСН)')
        }
        setTypeSysNal(item[0].name)
    }
    const getContent = (value) => {
        switch (value) {
            case 'Упрощённая система налогообложения (УСН)' : {
                return (
                    <BodyItem>
                        Добровольный, специальный налоговый режим для малого бизнеса, позволяющий существенно уменьшить количество уплачиваемых налогов и упростить документирование деятельности (Глава 26.2 НК РФ).
                        Часто используется совместно с ЕНВД (единый налог на вмененный доход) — обязательным режимом налогообложения для организаций, осуществляющих определенные виды деятельности.
                        Налоговые ставки: 6% (на доходы) и 5-15%% (доходы за вычетом расходов).
                        Для перехода на УСН нужно подать заявление в налоговый орган по месту нахождения.
                    </BodyItem>
                )
            }
            case 'Общая система налогообложения (ОСНО)' : {
                return (
                    <BodyItem>
                        Базовая система налогообложения, применяемая «по умолчанию».
                        Предусматривает уплату всех определенных действующим законодательством налогов и строгое документирование деятельности и отчетности.
                        В то же время, несмотря на большие чем при УСН и ЕСХН налоговое бремя и объем документооборота, предоставляет несоразмерно большие возможности для бизнеса.
                    </BodyItem>
                )
            }
            case 'Единый сельскохозяйственный налог (ЕСХН)' : {
                return (
                    <BodyItem>
                        Добровольный, специальный налоговый режим для малых сельскохозяйственных товаропроизводителей (Глава 26.1 НК РФ).
                        Так же как УСН существенно уменьшает количество уплачиваемых налогов и упрощает документирование деятельности.
                        Налоговая ставка: 6%.
                        Для перехода на ЕСХН нужно подать заявление в налоговый орган по месту нахождения.
                    </BodyItem>
                )
            }
        }
    }

    return (
        <>
            <Header>
                <Title>Систиме налогообложения</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <StyledSelectSearch>
                    <SelectSearchContainer onClick={handleClickItem}
                                           value={value}
                                           displayValue={value}
                                           list={list}/>
                </StyledSelectSearch>
            </Offer>
            <Body>
                {getContent(typeSysNal)}
            </Body>
        </>
    )
}
const mapStateToProps = (state) => {
    return {

    }
}
export default compose(
    connect(mapStateToProps,{
    }),
)(TypeSysNalog)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 0 10px;
`;
const StyledSelectSearch = styled.div`
        max-width: 300px;
    width: 100%;
    height: 32px;
`;
const Body = styled.div`
  padding: 10px;
  font-weight: 600;
`;
const BodyItem = styled.div`
    font-size: 12px;
    max-height: 75px;
    height: 100%;
    overflow: auto;
    &:not(:last-child){
      margin-bottom: 10px;
    }
`;

