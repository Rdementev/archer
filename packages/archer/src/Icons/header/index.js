
import { ReactComponent as MultiMedia } from "./multimedia.svg";
import { ReactComponent as HeaderInfo } from "./HeaderInfo.svg";
import { ReactComponent as HeaderProfile } from "./HeaderProfile.svg";
import { ReactComponent as HeaderExit } from "./HeaderExit.svg";
import { ReactComponent as LosoS } from "./LogoS.svg";
import { ReactComponent as Pace } from "./pace.svg";
import { ReactComponent as Document } from "./Document.svg";
import { ReactComponent as BusinessAddress } from "./Business_address.svg";

export {  MultiMedia, HeaderInfo, HeaderProfile, HeaderExit, LosoS, Pace, Document, BusinessAddress};
