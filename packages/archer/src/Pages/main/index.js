import React, {Fragment, useEffect} from "react";
import styled from "styled-components/macro";
import Items from "./components/items";
import {connect} from "react-redux";
import {getSiteMapSelector} from "./reducer/siteMap";


const ServesMain = (props) => {
    const {siteMap} = props;
    const createGroupItems = (siteMap) => {
        return siteMap.map(item => {
            return (
                <>
                    <Items item={item} color={item.color} id={item.id}/>
                </>
            )
        })
    };

    return (
        <StyledDataManagment>
            {siteMap && createGroupItems(siteMap)}
        </StyledDataManagment>
    )


};


const mapStateToProps = (state) => {
    return {
        siteMap: getSiteMapSelector(state)
    }
}
export default connect(mapStateToProps, {})(ServesMain);
//
const StyledDataManagment = styled.div`
    background-color :#f5f8fa;
    overflow: scroll;
    width: 100%;
    height: 100%;
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
`;



