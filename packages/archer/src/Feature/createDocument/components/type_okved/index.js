import React, {useEffect, useRef, useState} from 'react'
import styled from 'styled-components/macro'

import {compose} from "redux";
import {connect} from "react-redux";
import RequiredField from "ComponentsARJTTD/requiredField";
import Okveds from "FeatureARDM/okveds";
import {Button} from "ComponentsARJTTD/buttons/button_v2";
import {CSSTransition} from "react-transition-group";
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import CloseIcon from '@material-ui/icons/Close';
import {getARDMFeatureRegOOOActiveStepIdReselect} from "../../registrationOOO/reducer";



const TypeOkved = (props) => {
    const {type_selected_okved, heightOkved, activeStepId, setFavorite, setSelected} = props
    const [show, setShow] = useState(false)
    const [heightHeader, setHeightHeader] = useState(false)
    const [heightOffer, setHeightOffer] = useState(false)
    const [heightBody, setHeightBody] = useState(false)
    const headerRef = useRef(null)
    const offerRef = useRef(null)

    useEffect(()=>{
        const cH = heightOkved - (heightHeader + heightOffer + 40)
        setHeightBody(cH)
    },[heightOkved])

    useEffect(()=> {
        const cHH = headerRef.current.clientHeight
        const cHO = offerRef.current.clientHeight
        setHeightHeader(cHH)
        setHeightOffer(cHO)
    },[activeStepId])

    // удаления окведов из блока selected
    const handleClickDeleteOkveds = (e,elem) => {
        e.stopPropagation()
        setSelected(elem )
    }
    const handleClickSetFavoriteOkved = (e, elem) => {
        e.stopPropagation()
        setFavorite(elem)
    }


    // поулчаем все окведы
    const getSelectedOkveds = (type_selected_okved) => {
        type_selected_okved.sort((a, b) => a.code > b.code ? 1 : -1)
        return type_selected_okved.map(elem => {
            const entered = elem ? true : false
            if(!elem.favorite){
                return (
                    <CSSContainer>
                        <CSSTransition
                            unmountOnExit
                            in={entered}
                            timeout={{ appear:0, enter: 0, exit: 400 }}
                            classNames='selected__okved'
                            appear
                        >
                            <BlockOkvedsElem onClick={(e)=>{handleClickSetFavoriteOkved(e,elem)}}>
                                <BlockIconTitle >
                                    <TitleOkveds style={{lineHeight: '28px'}}>{elem.code}</TitleOkveds>
                                    <BlockIcon >
                                        <StarBorderIcon style={{color:'#fecd00'}} />
                                    </BlockIcon>
                                    <BlockIconDeleted  onClick={(e)=>{handleClickDeleteOkveds(e, elem)}} >
                                        <CloseIcon />
                                    </BlockIconDeleted>
                                </BlockIconTitle>
                                <OkvedTitle>{elem.title}</OkvedTitle>
                            </BlockOkvedsElem>
                        </CSSTransition>
                    </CSSContainer>
                )
            }
        })

    };
    // получаем главный оквед
    const getSelectedOkvedsFavor = (type_selected_okved) => {
        return type_selected_okved.map(elem => {
            if (elem.favorite) {
                return (
                    <CSSContainer>
                        <CSSTransition
                            unmountOnExit
                            in={elem}
                            timeout={{ appear:0, enter: 0, exit: 400 }}
                            classNames='selected__okved'
                            appear
                        >
                            <BlockOkvedsElem onClick={(e)=>{handleClickSetFavoriteOkved(e,elem)}}>
                                <BlockIconTitle className='d-flex '>
                                    <TitleOkveds style={{lineHeight: '28px'}}>{elem.code}</TitleOkveds>
                                    <BlockIcon>
                                        <StarIcon style={{color: '#fecd00'}}/>
                                    </BlockIcon>
                                    <BlockIconDeleted  onClick={(e)=>{handleClickDeleteOkveds(e,elem)}} >
                                        <CloseIcon/>
                                    </BlockIconDeleted>
                                </BlockIconTitle>
                                <OkvedTitle>{elem.title}</OkvedTitle>
                            </BlockOkvedsElem>
                        </CSSTransition>
                    </CSSContainer>
                )
            }
        })
    };


    return (
        <>
            <Header ref={headerRef}>
                <Title>Виды деятельности</Title>
                <RequiredField/>
            </Header>
            <Offer ref={offerRef}>
                <BlockButton>
                    <Button onClick={()=>{setShow(true)}}>Выбрать ОКВЭД</Button>
                </BlockButton>
                <Okveds show={show} setShow={setShow} setSelected={setSelected} type_selected_okved={type_selected_okved} />
            </Offer>
            {type_selected_okved && <Body height={heightBody}>
                {getSelectedOkvedsFavor(type_selected_okved)}
                {getSelectedOkveds(type_selected_okved)}
            </Body>}
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        activeStepId: getARDMFeatureRegOOOActiveStepIdReselect(state),
    }
}
export default compose(
    connect(mapStateToProps,{

    }),
)(TypeOkved)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  
`;
const BlockButton = styled.div`
    width: 150px;
    padding: 10px;
`;
const Body = styled.div`
  flex-grow: 1;
  height: ${({height}) => height ? `${height}px` : 'calc(100vh - 500px)'};
  overflow: auto;
::-webkit-scrollbar {
width: 4px;
}

/* Track */
::-webkit-scrollbar-track {
  border-radius: 3px;
}

/* Handle */
::-webkit-scrollbar-thumb {
  border-radius: 3px;
  background: rgba(31, 54, 125, 0.16);
}

::-webkit-scrollbar-thumb:hover{
  background: rgba(31, 54, 125, 0.16);
}
`;

const BlockIcon = styled.div`
    
`;

const BlockIconDeleted = styled.div`
    color: #e0e4f1;
    cursor: pointer;
    position:absolute;
    top: 0;
    right: 0;
    z-index: 1;
    &:hover {
      color:#950001
    }
    
`;
const BlockIconTitle = styled.div`
    position:relative;
    display: flex;
`;
const OkvedTitle = styled.div`
    
`;

const TitleOkveds = styled.div`
  min-width: 65px;
`;

const CSSContainer = styled.div`
  min-width: 65px;
`;


const BlockOkvedsElem = styled.div`
    padding: 10px;
    cursor: pointer;
    border-bottom: 1px dotted #e0e4f1;
    &:hover {
      border-bottom: 1px dotted #fecd00;
    }
`;



