import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {compose} from "redux";
import {connect} from "react-redux";
import RequiredField from "ComponentsARJTTD/requiredField";


const TypeReg = (props) => {
    const {value, setValue, list} = props

    useEffect(() => {
        if (value) {
            const active = list.find(item => item.name === value)
            setValue(active.name)
        }
    }, [])

    const handleClickItem = (item, multi, other) => {
        if (!multi) {
            setValue(item[0].name)
        }
    }
    const getContent = (value) => {
        switch (value) {
            case "Общая регистрация ООО" : {
                return (
                    <>
                        <BodyItem>
                            Несколько учредителей, любой руководитель
                        </BodyItem>
                        <BodyItem>
                            ОСНО, ЕСХН, УСН на выбор
                        </BodyItem>
                    </>
                )
            }
            case "Упращенная регистрация ООО" : {
                return (
                    <>
                        <BodyItem>
                            1 учредитель, он же руководитель
                        </BodyItem>
                        <BodyItem>
                            Упращенная система налогообложения
                        </BodyItem>

                    </>
                )
            }
        }
    }

    return (
        <>
            <Header>
                <Title>Тип Регистрации</Title>
                <RequiredField/>
            </Header>
            <Offer>
                <StyledSelectSearch>
                    <SelectSearchContainer onClick={handleClickItem}
                                           value={value}
                                           displayValue={value}
                                           list={list}/>
                </StyledSelectSearch>
            </Offer>
            <Body>
                {getContent(value)}
            </Body>
        </>
    )
}
const mapStateToProps = (state) => {
    return {}
}
export default compose(
    connect(mapStateToProps, {}),
)(TypeReg)
//


const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
    letter-spacing: 0.04em;
`;
const Offer = styled.div`
  padding: 0 10px;
`;
const StyledSelectSearch = styled.div`
    max-width: 300px;
    width: 100%;
    height: 32px;
`;
const Body = styled.div`
  padding: 10px;
  font-weight: 600;
  max-height: 75px;
    height: 100%;
`;
const BodyItem = styled.div`
  font-size: 12px;
 &:not(:last-child){
  margin-bottom: 10px;
 }
`;
