import React, {useEffect, useState} from 'react'
import {compose} from "redux";
import styled from 'styled-components/macro'
import TypeName from "./type_name";
import TypeSizeCapital from "../../../components/type_sizeCapital";
import TypeCreateDate from "./type_create_date";
import TypeProxy from "../../../components/type_proxy";
import TypeRegContainer from "FeatureARDM/createDocument/components/type_reg";
import {connect} from "react-redux";
import {
    ardmFeatureRegOOOChangeTypeResponse,
    ardmFeatureRegOOOSetProxy,
    ardmFeatureRegOOOSetTypeRegId,
    ardmFeatureRegOOOSetTypeSendId,
    ardmFeatureRegOOOChangeValidationCommon,
    ardmFeatureRegOOOChangeTypeSizecapital,
    ardmFeatureRegOOOSetTypeNameRus,
    ardmFeatureRegOOOSetTypeNameRussian
} from "../../actions";
import {
    getARDMFeatureRegistrationOOOTypeProxyReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect, getARDMFeatureRegistrationOOOTypeResponseReselect,
    getARDMFeatureRegistrationOOOTypeSendReselect,
    getARDMFeatureRegistrationOOOTypeCreateDateReselect,
    getARDMFeatureRegistrationOOOTypeNameEnglishReselect,
    getARDMFeatureRegistrationOOOTypeNameEngReselect,
    getARDMFeatureRegistrationOOOTypeNameRusReselect,
    getARDMFeatureRegistrationOOOTypeNameRussianReselect,
    getARDMFeatureRegistrationOOOTypeSizecapitalReselect
} from "../../reducer";
import TypeSendContainer from "FeatureARDM/createDocument/components/type_send";
import TypeResponseContainer from "FeatureARDM/createDocument/components/type_response";
import TypeProxyContainer from "FeatureARDM/createDocument/components/type_proxy";
import {checkCommonRegistrationOOO} from "FeatureARDM/createDocument/registrationOOO/utils";
import {validationInteger} from "UtilsARJTTD/validation";
import {validationRus} from "UtilsARJTTD/validation";

const listReg = [
    {id: 1, name: 'Общая регистрация ООО'},
    {id: 2, name: 'Упращенная регистрация ООО'}]

const listSend = [
    {
        id: 1,
        name: 'Лично',
        text: 'Все учредители вместе подают документы в налоговую. В этом случае не понадобится удостоверять документы у нотариуса. Либо один из учредителей подает документы в налоговую лично с предоставлением одновременно формы заявления Р11001 от каждого из неприсутствующих учредителей. В этом случае нужно нотариально удостоверить подписи неприсутствующих учредителей на Р11001. При этом, также нужна доверенность (доверенности), которая должна быть оформлена от имени каждого неприсутствующего учредителя.'
    },
    {
        id: 2,
        name: 'По доверености',
        text: 'Подать документы вместо учредителя может доверенное лицо. Доверенность нужно будет заверить у нотариуса. При подаче документов через представителя, необходимо предварительно также заверить у нотариуса подписи каждого из учредителей на форме заявления Р11001.'
    },
    {
        id: 3,
        name: 'Электронно со своей КЭП',
        text: 'У вас должна быть в наличии квалифицированная электронная подпись (КЭП).Не требуется оплата госпошлины. Без посещения нотариуса и налоговой инспекции.'
    },
    {id: 4, name: 'Электронно с помощью КЭП нотариуса', text: '1'},
]
const listResponse = [
    {id: 1, name: 'Лично',},
    {id: 2, name: 'По доверености',},
    {id: 3, name: 'Направить на почту',},
]
const CommonInfoRegOOO = (props) => {
    const {
        type_size_capitalIsValidation,
        size_capitalIsValidation,
        size_capitalIsTouch,
        setSize_capitalIsTouch,
        type_company_name_rusIsValidation,
        type_company_name_rusIsTouch,
        setType_company_name_rusIsTouch,
        type_company_name_russianIsValidation,
        type_company_name_russianIsTouch,
        setType_company_name_russianIsTouch,
        type_proxy_id,
        type_reg,
        type_send,
        type_size_capital,
        type_response,
        type_company_name_rus,
        type_company_name_russian,
        type_company_name_eng,
        type_company_name_english,
        ardmFeatureRegOOOSetTypeNameRussian,
        ardmFeatureRegOOOSetTypeNameRus,
        ardmFeatureRegOOOChangeTypeSizecapital,
        ardmFeatureRegOOOSetProxy,
        ardmFeatureRegOOOSetTypeRegId,
        ardmFeatureRegOOOSetTypeSendId,
        ardmFeatureRegOOOChangeTypeResponse,

    } = props



    return (
        <Container>
            <Grid>
                <Block>
                    <SubBlock>
                        <SubGrid>
                            <Content>
                                <TypeRegContainer value={type_reg} setValue={ardmFeatureRegOOOSetTypeRegId}
                                                  list={listReg}/>
                            </Content>
                        </SubGrid>
                        <SubGrid>
                            <Content>
                                <TypeSendContainer value={type_send} setValue={ardmFeatureRegOOOSetTypeSendId}
                                                   list={listSend}/>
                            </Content>
                        </SubGrid>
                    </SubBlock>
                    <SubBlock>
                        <Content>
                            <TypeName type_company_name_rus={type_company_name_rus}
                                      onChangeRus={ardmFeatureRegOOOSetTypeNameRus}
                                      type_company_name_rusIsValidation={type_company_name_rusIsValidation}
                                      type_company_name_rusIsTouch={type_company_name_rusIsTouch}
                                      setType_company_name_rusIsTouch={setType_company_name_rusIsTouch}

                                      type_company_name_russian={type_company_name_russian}
                                      type_company_name_russianIsValidation={type_company_name_russianIsValidation}
                                      type_company_name_russianIsTouch={type_company_name_russianIsTouch}
                                      setType_company_name_russianIsTouch={setType_company_name_russianIsTouch}
                                      onChangeRussian={ardmFeatureRegOOOSetTypeNameRussian}

                                      type_company_name_eng={type_company_name_eng}
                                      type_company_name_english={type_company_name_english}/>
                        </Content>
                    </SubBlock>
                </Block>
            </Grid>
            <Grid>
                <Block>
                    <Content>
                        <Section>
                            <TypeSizeCapital value={type_size_capital}
                                             valueIsValidation={type_size_capitalIsValidation}
                                             sizeValueIsValidation={size_capitalIsValidation}
                                             setValueIsTouch={setSize_capitalIsTouch}
                                             valueIsTouch={size_capitalIsTouch}
                                             onChange={ardmFeatureRegOOOChangeTypeSizecapital}/>
                        </Section>
                        <Section>
                            <TypeCreateDate/>
                        </Section>
                        <Section>
                            <TypeResponseContainer list={listResponse} value={type_response}
                                                   setValue={ardmFeatureRegOOOChangeTypeResponse}/>
                        </Section>
                        <Section>
                            <TypeProxyContainer proxyId={type_proxy_id} setProxyId={ardmFeatureRegOOOSetProxy}/>
                        </Section>
                    </Content>
                </Block>
            </Grid>
        </Container>
    )
}

const mapStateToProps = (state) => {
    return {
        type_proxy_id: getARDMFeatureRegistrationOOOTypeProxyReselect(state),
        type_reg: getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_send: getARDMFeatureRegistrationOOOTypeSendReselect(state),
        type_size_capital: getARDMFeatureRegistrationOOOTypeSizecapitalReselect(state),
        type_create_date: getARDMFeatureRegistrationOOOTypeCreateDateReselect(state),
        type_response: getARDMFeatureRegistrationOOOTypeResponseReselect(state),
        type_company_name_rus: getARDMFeatureRegistrationOOOTypeNameRusReselect(state),
        type_company_name_russian: getARDMFeatureRegistrationOOOTypeNameRussianReselect(state),
        type_company_name_eng: getARDMFeatureRegistrationOOOTypeNameEngReselect(state),
        type_company_name_english: getARDMFeatureRegistrationOOOTypeNameEnglishReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
        ardmFeatureRegOOOSetTypeRegId, ardmFeatureRegOOOSetTypeSendId, ardmFeatureRegOOOChangeTypeResponse,
        ardmFeatureRegOOOSetProxy, ardmFeatureRegOOOChangeValidationCommon, ardmFeatureRegOOOChangeTypeSizecapital,
        ardmFeatureRegOOOSetTypeNameRus, ardmFeatureRegOOOSetTypeNameRussian
    }),
)(CommonInfoRegOOO)
//
const Container = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  height: 100%;
`;
const Block = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 14px;
`;
const SubBlock = styled.div`
  width: 100%;
  height: 50%;
  border-radius: 14px;
  display: flex;
  &:last-child{
  padding-top: 5px
  }
  &:first-child{
  padding-bottom: 5px
  }
`;
const Grid = styled.div`
  width: 100%;
  padding: 0 10px 0 0;
   &:last-child{
      padding: 0;
  }
`;
const SubGrid = styled.div`
  width: 100%;
  &:last-child{
     margin-left: 5px;
  }
  &:first-child{
   
    margin-right: 5px;
  }
`;

const Content = styled.div`
  width: 100%;
  padding: 10px 0;
  background: #fff;
  border-radius: 4px;
  height: 100%;
  display: flex;
  flex-direction: column;  
`;

const Section = styled.div`
    height: 100%;
    &:not(:first-child){
      padding: 10px 0;
    }    
`;

