
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {ARDM_PAGES_CREATE_PERSONAL_REQUEST} from "../actions";
import {ardmPagesGetPersonalRequest} from "../actions";
import {put} from "redux-saga/effects";

function* prepareRequest ( payload) {

    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetPersonalRequest())
}

function* prepareFailure () {

}

export const createPersonal = () => {
    return createRequestRest({
        url: '/autodoc/create_individual',
        action: ARDM_PAGES_CREATE_PERSONAL_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}