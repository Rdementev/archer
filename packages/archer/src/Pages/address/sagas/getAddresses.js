import createRequestRest from 'ApiARJTTD/createRequestRest'
import {ARDM_PAGES_GET_ADDRESSES_REQUEST, ardmPagesGetAddressesSuccess} from "../actions";
import {put} from "redux-saga/effects";


function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetAddressesSuccess(response.data))
}

function* prepareFailure () {

}

export const getAddresses = () => {
    return createRequestRest({
        url: '/autodoc/get_addresses',
        action: ARDM_PAGES_GET_ADDRESSES_REQUEST,
        prepareSuccess,
        prepareFailure,
        mathod:'get'
    })
}