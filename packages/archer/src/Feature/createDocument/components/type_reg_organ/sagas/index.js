import createRequestRest from "ApiARJTTD/createRequestRest";
import {GET_TYPE_REG_ORGAN_IFNS_DATA} from "../actions";
import {put} from "redux-saga/effects";
import {ardmChangeRegOrganIsLoad} from "../actions";


function* prepareRequest(payload) {
        yield put(ardmChangeRegOrganIsLoad(true))
        return payload
}

function* prepareSuccess(response, payload) {
        const result = response.suggestions[0].data
        const oktmos = result.oktmo.split(',')
        const data = {
                code: result.code,
                name: result.name_short,
                inn: result.inn,
                kpp: result.kpp,
                payment_name: result.payment_name,
                oktmo: oktmos[0],
                bank_name: result.bank_name,
                bank_account: result.bank_account,
                bank_bic: result.bank_bic,
        }
        yield put(payload.func(data))
        yield put(ardmChangeRegOrganIsLoad(false))
}

function* prepareFailure(response, payload) {
        yield put(ardmChangeRegOrganIsLoad(false))
}

export const getRegOrgan = () =>
    createRequestRest({
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        url:'findById/fns_unit',
        action: GET_TYPE_REG_ORGAN_IFNS_DATA,
        daData: true,

})
