
export const CREATE_PERSONAL = 'CREATE_PERSONAL'
export const createPersonal = (newPersonal) => ({type:CREATE_PERSONAL, newPersonal})

export const UPDATE_PERSONAL_DATA = 'UPDATE_PERSONAL_DATA'
export const updatePersonalData = (id) => ({type:UPDATE_PERSONAL_DATA, id})

export const CHANGE_PERSONAL_DATA = 'CHANGE_PERSONAL_DATA'
export const changePersonalData = (id, value, date) => ({type:CHANGE_PERSONAL_DATA, id, value, date})

export const CHANGE_EDIT_PERSONAL = 'SET_EDIT_PERSONAL'
export const changeEditPersonal = (searchEditPersonal) => ({type:CHANGE_EDIT_PERSONAL, searchEditPersonal})

export const SET_EDIT_PERSONAL_ID = 'SET_EDIT_PERSONAL_ID'
export const setEditPersonalId = (id) => ({type:SET_EDIT_PERSONAL_ID, id})

export const CHANGE_SEX_TYPE = 'CHANGE_SEX_TYPE'
export const changeSexType = (id, groupId) => ({type:CHANGE_SEX_TYPE, id, groupId})

export const DELETE_PERSONAL = 'DELETE_PERSONAL'
export const deletePersonal = (id) => ({type:DELETE_PERSONAL, id})

export const SET_ADDRESS_FOR_PERSONAL = 'SET_ADDRESS_FOR_PERSONAL'
export const setAddressForPersonal = (addressId) => ({type:SET_ADDRESS_FOR_PERSONAL, addressId})

export const SET_PASSPORT_GIVEN_BY_CODE = 'SET_PASSPORT_GIVEN_BY_CODE'
export const setPassportGivenByCode = (value) => ({type:SET_PASSPORT_GIVEN_BY_CODE, value})




/////////////////////

// получение списка пользователей
export const ARDM_PAGES_GET_PERSONAL_REQUEST = 'ARDM_PAGES_GET_PERSONAL_REQUEST';
export const ardmPagesGetPersonalRequest = () => ({type: ARDM_PAGES_GET_PERSONAL_REQUEST, });

export const ARDM_PAGES_GET_PERSONAL_SUCCESS = 'ARDM_PAGES_GET_PERSONAL_SUCCESS';
export const ardmPagesGetPersonalSuccess = (payload) => ({type: ARDM_PAGES_GET_PERSONAL_SUCCESS, payload});

export const ARDM_PAGES_GET_PERSONAL_FAILURE = 'ARDM_PAGES_GET_PERSONAL_FAILURE';
export const ardmPagesGetPersonalFailure = () => ({type: ARDM_PAGES_GET_PERSONAL_FAILURE});
// создание нового пользователя
export const ARDM_PAGES_CREATE_PERSONAL_REQUEST = 'ARDM_PAGES_CREATE_PERSONAL_REQUEST';
export const ardmPagesCreatePersonalRequest = (payload) => ({type: ARDM_PAGES_CREATE_PERSONAL_REQUEST, payload});

export const ARDM_PAGES_CREATE_PERSONAL_SUCCESS = 'ARDM_PAGES_CREATE_PERSONAL_SUCCESS';
export const ardmPagesCreatePersonalSuccess = (payload) => ({type: ARDM_PAGES_CREATE_PERSONAL_SUCCESS, payload});

export const ARDM_PAGES_CREATE_PERSONAL_FAILURE = 'ARDM_PAGES_CREATE_PERSONAL_FAILURE';
export const ardmPagesCreatePersonalFailure = () => ({type: ARDM_PAGES_CREATE_PERSONAL_FAILURE});
// обновление пользователя
export const ARDM_PAGES_UPDATE_PERSONAL_REQUEST = 'ARDM_PAGES_UPDATE_PERSONAL_REQUEST';
export const ardmPagesUpdatePersonalRequest = (payload) => ({type: ARDM_PAGES_UPDATE_PERSONAL_REQUEST, payload});

export const ARDM_PAGES_UPDATE_PERSONAL_SUCCESS = 'ARDM_PAGES_UPDATE_PERSONAL_SUCCESS';
export const ardmPagesUpdatePersonalSuccess = (payload) => ({type: ARDM_PAGES_UPDATE_PERSONAL_SUCCESS, payload});

export const ARDM_PAGES_UPDATE_PERSONAL_FAILURE = 'ARDM_PAGES_UPDATE_PERSONAL_FAILURE';
export const ardmPagesUpdatePersonalFailure = () => ({type: ARDM_PAGES_UPDATE_PERSONAL_FAILURE});


