const {createProxyMiddleware} = require("http-proxy-middleware");

module.exports = function(app) {
    const web01 = "http://25.22.214.159:1245";
    app.use("/api", createProxyMiddleware({
        target: web01,
        changeOrigin: true
    }))
};
