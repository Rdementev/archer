import React from "react";
import {Route, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {getIsAdminSelector, getIsAuthSelector} from "Core/selector/selector";

const getComponents = payload => {
    const {type, props, Component, isAuth, isAdmin } = payload;
    const {location} = props;
    const component = <Component {...props} />;
    const redirect = pathname => <Redirect to={{pathname, state: {from: location}}}/>;
    switch (type) {
        case "client": {
            if (isAuth) return component;
            return redirect("/auth/signin");
        }
        case "login": {
            if (isAuth) return redirect("/serves");
            return component;
        }
        case "admin": {
            if (isAdmin && isAuth) return component;
            return redirect("/serves");
        }
        default:
            break;
    }
};

const PrivateRoute = (props) => {
    const {component: Component, isLoggedIn, isAuth, isAdmin, type = "client", ...rest} = props;

    return (
        <Route
            {...rest}
            render={props =>
                getComponents({
                    type,
                    props,
                    Component,
                    isAuth,
                    isAdmin
                })
            }
        />
    );
}

const mapStateToProps = state => {
    return {
        isAuth: getIsAuthSelector(state),
        isAdmin: getIsAdminSelector(state)
    };
};

export default connect(mapStateToProps)(PrivateRoute);
