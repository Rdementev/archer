import React, {useEffect, useState} from 'react'
import styled from 'styled-components/macro'
import SelectSearchContainer from "ComponentsARJTTD/selectSearch";
import {compose} from "redux";
import {connect} from "react-redux";
import RequiredField from "ComponentsARJTTD/requiredField";

const list = [
    {id:1, name: 'Лично',},
    {id:2, name: 'По доверености',},
    {id:3, name: 'Направить на почту',},
]

const TypeResponse = (props) => {
    const {value, setValue} = props

    const handleClickItem = (item, multi , other) => {
        setValue(item[0].name)
    }
    useEffect(()=> {
        if(value){
            const active = list.find(item => item.name === value)
            setValue(active.name)
        }
    },[value])

    return (
    <>
        <Header>
            <Title>Способ получения</Title>
            <RequiredField/>
        </Header>
        <Offer>
            <StyledSelectSearch>
                <SelectSearchContainer onClick={handleClickItem}
                                       value={value}
                                       displayValue={value}
                                       list={list}/>
            </StyledSelectSearch>
        </Offer>
        <Body></Body>
        <Footer></Footer>
    </>
    )
}
const mapStateToProps = (state) => {
    return {

    }
}
export default compose(
    connect(mapStateToProps, {}),
)(TypeResponse)
//



const Header = styled.div`
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #e8eaf3;
    margin-bottom: 10px;
    display: flex;
`;
const Title = styled.h3`
    margin: 0;
    font-size: 9px;
    text-transform: uppercase;
`;
const Offer = styled.div`
  padding: 0 10px;
  display: flex;
  justify-content: space-around;
  flex-direction: column;
`;
const StyledSelectSearch = styled.div`
      max-width: 300px;
    width: 100%;
    height: 32px; 
`;

const Body = styled.div`
  max-height: 75px;
    height: 100%;
`;
const Footer = styled.div`
  
`;
