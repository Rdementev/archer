import createRequestRest from "ApiARJTTD/createRequestRest";

import {put} from "redux-saga/effects";
import {ARDM_PAGES_CREATE_PAGEDOCS_REQUEST, ardmPagesCreateFormsPagedocsSuccess, ardmPagesGetFormsPagedocsRequest} from "../actions";
import {generateTempoMessage} from "ReducerARJTTD/flashMessages";


function* prepareRequest(payload) {
    return payload
}
function* prepareSuccess(result, payload) {
    yield put(ardmPagesCreateFormsPagedocsSuccess(result.data))
    yield put(ardmPagesGetFormsPagedocsRequest())
}
function* prepareFailure(result, payload) {

}

export const createFormSaga = () => {
    return createRequestRest({
        url: '/autodoc/create_new_form',
        action: ARDM_PAGES_CREATE_PAGEDOCS_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
        failure: generateTempoMessage,
        failureText:'',
        successText:'',
    })
}