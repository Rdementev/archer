import React, {PureComponent, useEffect, useRef, useState} from "react";
import {connect} from "react-redux";
import styled, {keyframes} from "styled-components/macro";
import {Check, CloseIcon, StatusDone} from 'Icons'
import {HeaderInfo} from 'Icons/header'
import {NavLink} from "react-router-dom";

const ClientSupport = () => {
    const [showToolTip, setShowToolTip] = useState(false)
    const [isVisibleList, setIsVisibleList] = useState(false)
    const [isWork, setIsWork] = useState(true)
    const SupportRef = useRef()

    useEffect(() => {
        document.addEventListener("click", handleClickOutSide, false)
        return function () {
            document.removeEventListener("click", handleClickOutSide, false);
        }
    })

    const handleClickOutSide = (e) => {
        const item = SupportRef.current
        if (!e.path.includes(item)) {
            setIsVisibleList(false)
        }
    };


    const handleClick = (e) => {
        setIsVisibleList(!isVisibleList)
    };


    return (
        <>
            <BlockSupport onClick={() => {handleClick()}} ref={SupportRef}>
                <HeaderBlockIcon onMouseEnter={() => {setShowToolTip(true)}}
                                 onBlur={() => {setShowToolTip(false)}}
                                 isVisibleList={isVisibleList}
                                 onMouseLeave={() => {setShowToolTip(false)}}>
                    <Icon isVisibleList={isVisibleList}/>
                    {isVisibleList && <AvatarInfoStatus>
                        <IconUtil/>
                    </AvatarInfoStatus>}
                </HeaderBlockIcon>
                {isVisibleList && (
                    <>
                        <SupportList>
                            <SupportTitle>
                                Поддержка
                            </SupportTitle>
                            <div style={{padding: '7px 10px 0 10px'}}>
                                <SupportListItem to={'/help/3'}>
                                    <SupportText>Документация</SupportText>
                                </SupportListItem>

                            </div>
                            <SupportTitle>
                                Статус платформы
                            </SupportTitle>
                            <SupportStatusBlock onClick={()=>{setIsWork(!isWork)}}>
                                {isWork ? (
                                        <>
                                            <StatusBlock>
                                                <StatusIconDone/>
                                            </StatusBlock>
                                            <SupportText>Активна</SupportText>
                                        </>
                                    )
                                    : (
                                        <>
                                            <StatusBlock style={{ background: '#F2764C', boxShadow: '0px 4px 4px rgba(242, 118, 76, 0.3)'}}>
                                                <StatusIconError/>
                                            </StatusBlock>
                                            <SupportText>Недоступна</SupportText>
                                        </>
                                    )
                                }
                            </SupportStatusBlock>
                        </SupportList>

                    </>
                )}

            </BlockSupport>

        </>
    )

}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ClientSupport);

//
const SupportListItem = styled(NavLink)`
  padding: 9px 7px;
  border-radius: 2px;
  display: flex;
  align-items: center;
  font-size: 13px;
  white-space: nowrap;
  cursor: pointer;
  &:hover {
    background: #f2f2f2;
  }
`;
const SupportStatusBlock = styled.div`
    cursor: default;
    display: flex;
    padding: 10px 17px;
`;
const StatusIconDone = styled(Check)`
    margin: auto;
`;
const StatusIconError = styled(CloseIcon)`
    margin: auto;
`;

const SupportTitle = styled.div`
    padding: 8px 17px 5px 17px;
    border-bottom: 1px solid #ECEEF3;
    font-size: 12px;
    line-height: 16px;
    color: #4D4D4D;
    opacity: 0.9;
`;
const animation = keyframes`
  0%{
   
    opacity: 0;
  }
  100%{
   
    opacity: 1;
  }
`;
const SupportList = styled.ul`
    margin: 0;
    width: 156px;
    position: absolute;
    top: 48px;
    transform: translate(-100%, 10px);
    z-index: 1000;
    background: #fff;
    border: 1px solid #f2f2f2;
    color: #000;
   
   
    border-radius: 4px;
   
    box-shadow: 0 6px 24px rgba(0, 0, 0, 0.06);

  animation: ${animation} 0.3s 0s;

`;
const StatusBlock = styled.div`
    background: #46B04A;
    width: 14px;
    height: 14px;
    display: flex;
    border-radius: 50%;
    box-shadow: 0 4px 4px #46af494d;
    margin-right: 10px;
`;


const Icon = styled(HeaderInfo)`
 margin: auto;
  height: 12px;
    width: 12px;
    & > * {
    fill: ${({isVisibleList}) => isVisibleList ? '#fff' : '#2C2C55'};
   }
`;
const SupportText = styled.div`




font-size: 14px;
line-height: 16px;


display: flex;
align-items: center;

color: #1A1A1A;

opacity: 0.9;
`;


const BlockSupport = styled.div`
 width: 22px;
 height: 22px;
 cursor: pointer;
 margin: 0 10px;
`;

export const IconUtil = styled(StatusDone)`
  margin: auto;
  & > g > * {
     fill: #4F69B2;
  }
`;

export const AvatarInfoStatus = styled.div`
    display:flex;
    width: 12px;
    height: 12px;
    border-radius: 2px;
    position: absolute;
    top: -4px;
    right: -4px;
    background: #fff;
    box-shadow: 0 4px 14px #2844934d;
`;


const HeaderBlockIcon = styled.div`
    position: relative;
    width: 22px;
    height: 22px;
    display: flex;
    align-items: center;
    transition: all 0.3s;
    background-color: rgba(255, 255, 255, 0.25);
    border-radius: 50%;
  &:hover {
     background-color: ${props => props.theme.medium};
  }
  & ${Icon} { 
     & > * {
      fill: #fff;
     }
  }
`;

