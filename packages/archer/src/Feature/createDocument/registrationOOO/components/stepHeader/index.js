import React, {useEffect, useState} from 'react'
import {compose} from "redux";
import styled from 'styled-components/macro'
import {BallIcon} from "Icons";
import {connect} from "react-redux";
import {
    getARDMFeatureRegistrationOOOTypeCreateDateReselect,
    getARDMFeatureRegistrationOOOTypeRegReselect, getARDMFeatureRegistrationOOOTypeResponseReselect,
    getARDMFeatureRegistrationOOOTypeSendReselect,
    getARDMFeatureRegistrationOOOTypeSizecapitalReselect,
    getARDMFeatureRegOOOActiveStepIdReselect,
    getARDMFeatureRegistrationOOOTypeNameEnglishReselect,
    getARDMFeatureRegistrationOOOTypeNameEngReselect,
    getARDMFeatureRegistrationOOOTypeNameRusReselect,
    getARDMFeatureRegistrationOOOTypeNameRussianReselect,
    getARDMFeatureRegOOOValidationCommonReselect,
    getARDMFeatureRegistrationOOOCompletedReselect,
    getARDMFeatureRegistrationOOOTypeAddressIdReselect,
    getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect,
    getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect,
    getARDMFeatureRegistrationOOOTypeDirPositionReselect,
    getARDMFeatureRegistrationOOOTypeDirTimeReselect,
    getARDMFeatureRegistrationOOOTypeIndividualsReselect,
    getARDMFeatureRegistrationOOOTypeOrganizationsReselect,
    getARDMFeatureRegistrationOOOTypePeaceFormIdReselect,
    getARDMFeatureRegistrationOOOTypePredsidatelReselect,
    getARDMFeatureRegistrationOOOTypeProxyReselect,
    getARDMFeatureRegistrationOOOTypeRateNalReselect, getARDMFeatureRegistrationOOOTypeRegOrganReselect,
    getARDMFeatureRegistrationOOOTypeSecretarReselect,
    getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect,
    getARDMFeatureRegistrationOOOTypeSysNalReselect,
    getARDMFeatureRegistrationOOOTypeUstavCertifySolutionsReselect,
    getARDMFeatureRegistrationOOOTypeUstavMinAddressReselect,
    getARDMFeatureRegistrationOOOTypeUstavOkvedInUstavReselect,
    getARDMFeatureRegOOOIdReselect,
    getARDMFeatureRegOOOValidationFDAReselect, getARDMFeatureRegOOOValidationUNOReselect
} from "FeatureARDM/createDocument/registrationOOO/reducer";
import {ardmFeatureRegOOOChangeActiveStepId} from "FeatureARDM/createDocument/registrationOOO/actions";
import {WhiteButton_v3} from "ComponentsARJTTD/buttons/button_v3";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faUsers} from "@fortawesome/free-solid-svg-icons";
import {ardmPagesUpdateFormsPagedocsRequest} from "PagesARDM/pagedocs/actions";

const steps = [
    {id:1, balls:[{id:1},{id:2},{id:3},{id:4},{id:5},{id:6},{id:7},{id:8}]},
    {id:2, balls:[{},{},{},{},{},{},{},{}]},
    {id:3},
]



const StepHeader = (props) => {
    const {
        fda,
        common,
        uno,
        activeStepId,
        ardmFeatureRegOOOChangeActiveStepId,
        ardmPagesUpdateFormsPagedocsRequest,
        id,
        completed,
        type_reg,
        type_send,
        type_company_name_rus,
        type_company_name_russian,
        type_company_name_eng,
        type_company_name_english,
        type_size_capital,
        type_create_date,
        type_response,
        type_proxy_id,
        type_peace_form,
        type_individuals,
        type_organizations,
        type_dir_personal_id,
        type_dir_position,
        type_dir_time,
        type_dir_is_dir_address,
        type_address_id,
        type_ustav_min_address,
        type_ustav_certify_solution,
        type_ustav_okved_in_ustav,
        type_sys_nal,
        type_rate_nal,
        type_secretar,
        type_predsidatel,
        type_selected_okved,
        type_reg_organ} = props


    const getBall = (balls, background, valid) => {
      return balls.map(item => {
          return (
              <BlockIconBall background={background} valid={valid}>
                  <BallIcon />
              </BlockIconBall>
          )
      })
    }

    const getStep = (steps) => {
        return steps.map(item => {
            const background = '#2e828b'
            const active = activeStepId === item.id
            const valid = item.id === 1 ? common : item.id === 2 ? fda : uno
            return (
                <>
                <Step onClick={()=>{handleClickStep(item.id)}}
                      active={active}
                      valid={valid}>
                    {item.id}
                </Step>
                    {item.balls && <Balls>
                        {getBall(item.balls, background, valid)}
                    </Balls>}
                </>
            )
        })
    }

    const handleClickStep = (id) => {
        ardmFeatureRegOOOChangeActiveStepId(id)
    }

    const handleClick = (value) => {
        if(activeStepId === 1 && value === 'prev') return
        if(activeStepId === 3 && value === 'next') return
        const id = value === 'next' ? activeStepId + 1 : activeStepId - 1
        ardmFeatureRegOOOChangeActiveStepId(id)
    }
    const handleClickSave = () => {
        const data = {
            completed:completed,
            id:id,
            type_reg: type_reg,
            type_send: type_send,
            type_company_name_rus: type_company_name_rus,
            type_company_name_russian: type_company_name_russian,
            type_company_name_eng: type_company_name_eng,
            type_company_name_english: type_company_name_english,
            type_size_capital: type_size_capital,
            type_create_date: type_create_date,
            type_response: type_response,
            type_proxy_id: type_proxy_id,
            type_peace_form: type_peace_form,
            type_individuals: type_individuals,
            type_organizations: type_organizations,
            type_dir_personal_id: type_dir_personal_id,
            type_dir_position: type_dir_position,
            type_dir_time: type_dir_time,
            type_dir_is_dir_address: type_dir_is_dir_address,
            type_address_id: type_address_id,
            type_ustav_min_address: type_ustav_min_address,
            type_ustav_certify_solution: type_ustav_certify_solution,
            type_ustav_okved_in_ustav: type_ustav_okved_in_ustav,
            type_sys_nal: type_sys_nal,
            type_rate_nal: type_rate_nal,
            type_secretar: type_secretar,
            type_predsidatel: type_predsidatel,
            type_selected_okved: type_selected_okved,
            type_reg_organ: type_reg_organ,
        }
        Object.keys(data).map(key => {
            if(!data[key]){
                if(key === 'type_proxy_id') return data[key] = null
                delete data[key]
            }
        })
        ardmPagesUpdateFormsPagedocsRequest(data)
    }

    return (
        <Container>
            <BlockButton>
                <WhiteButton_v3 onClick={()=>{handleClick('prev')}}>
                    <BlockIcon>
                        <FontAwesomeIcon icon={faArrowLeft}/>
                    </BlockIcon>
                    Назад
                </WhiteButton_v3>
            </BlockButton>
            <Steps>
                {getStep(steps)}
            </Steps>
            <BlockButton>
                <WhiteButton_v3 styled={{ color: '#5F5F5F', borderColor: '#5F5F5F'}} onClick={()=>{handleClickSave()}}>
                    Сохранить
                </WhiteButton_v3>
            </BlockButton>
        </Container>
    )
}

const mapStateToProps = (state) => {
    return {
        activeStepId:getARDMFeatureRegOOOActiveStepIdReselect(state),
        common: getARDMFeatureRegOOOValidationCommonReselect(state),
        fda: getARDMFeatureRegOOOValidationFDAReselect(state),
        uno: getARDMFeatureRegOOOValidationUNOReselect(state),

        completed: getARDMFeatureRegistrationOOOCompletedReselect(state),
        id : getARDMFeatureRegOOOIdReselect(state),
        type_reg : getARDMFeatureRegistrationOOOTypeRegReselect(state),
        type_send : getARDMFeatureRegistrationOOOTypeSendReselect(state),
        type_company_name_rus : getARDMFeatureRegistrationOOOTypeNameRusReselect(state),
        type_company_name_russian : getARDMFeatureRegistrationOOOTypeNameRussianReselect(state),
        type_company_name_eng : getARDMFeatureRegistrationOOOTypeNameEngReselect(state),
        type_company_name_english : getARDMFeatureRegistrationOOOTypeNameEnglishReselect(state),
        type_size_capital : getARDMFeatureRegistrationOOOTypeSizecapitalReselect(state),
        type_create_date : getARDMFeatureRegistrationOOOTypeCreateDateReselect(state),
        type_response : getARDMFeatureRegistrationOOOTypeResponseReselect(state),
        type_proxy_id : getARDMFeatureRegistrationOOOTypeProxyReselect(state),
        type_peace_form : getARDMFeatureRegistrationOOOTypePeaceFormIdReselect(state),
        type_individuals : getARDMFeatureRegistrationOOOTypeIndividualsReselect(state),
        type_organizations : getARDMFeatureRegistrationOOOTypeOrganizationsReselect(state),
        type_dir_personal_id : getARDMFeatureRegistrationOOOTypeDirPersonalIdReselect(state),
        type_dir_position : getARDMFeatureRegistrationOOOTypeDirPositionReselect(state),
        type_dir_time : getARDMFeatureRegistrationOOOTypeDirTimeReselect(state),
        type_dir_is_dir_address : getARDMFeatureRegistrationOOOTypeDirIsDirAddressReselect(state),
        type_address_id : getARDMFeatureRegistrationOOOTypeAddressIdReselect(state),
        type_ustav_min_address : getARDMFeatureRegistrationOOOTypeUstavMinAddressReselect(state),
        type_ustav_certify_solution : getARDMFeatureRegistrationOOOTypeUstavCertifySolutionsReselect(state),
        type_ustav_okved_in_ustav : getARDMFeatureRegistrationOOOTypeUstavOkvedInUstavReselect(state),
        type_sys_nal : getARDMFeatureRegistrationOOOTypeSysNalReselect(state),
        type_rate_nal : getARDMFeatureRegistrationOOOTypeRateNalReselect(state),
        type_secretar : getARDMFeatureRegistrationOOOTypeSecretarReselect(state),
        type_predsidatel : getARDMFeatureRegistrationOOOTypePredsidatelReselect(state),
        type_selected_okved : getARDMFeatureRegistrationOOOTypeSelectedOkvedReselect(state),
        type_reg_organ : getARDMFeatureRegistrationOOOTypeRegOrganReselect(state),
    }
}

export default compose(
    connect(mapStateToProps,{ardmFeatureRegOOOChangeActiveStepId, ardmPagesUpdateFormsPagedocsRequest}),
)(StepHeader)
//
const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 20px;
  max-height: 50px;
`;
const Steps = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-around;
    padding: 0 100px;
`;

const Step = styled.div`
    font-size: 15px;
    text-align: center;
    line-height: 30px;
    color: ${({valid})=> valid ? '#fff' : 'rgba(0,0,0,0.5)'};
    border-radius: 50%;
    border: 1px solid transparent;
    border-color: ${({active}) => active ? '#2e828b' : 'transparent'};
    background: ${({valid})=>  valid ?  '#2e828b' : '#cccccc33'};
    min-width: 30px;
    min-height: 30px;
    cursor: pointer;
`;

const BlockIconBall = styled.div`
  display: flex;
  width: 10px;
  & > svg  {
      margin: auto;
      width: 10px;
    height: 10px;
  }
  & > svg > rect {
      min-width: 6px;
    min-height: 6px;
   
    width: 3px;
    height: 3px;
      fill: ${({valid, background}) => valid && valid !== null ? background : '#ccc'}
  }
`;
const Balls = styled.div`
  display: flex;
  width: 100%;
  padding: 0 10px;
  justify-content: space-around;
`;
const BlockIcon = styled.div`
    display: flex;
    height: 100%;
    margin-right: 10px;
    & > svg {
        margin: auto;
        fill: #ccc;
    }
    & > svg > path{
        transition: 0.3s all;
        fill: #D7D9DE;
    }
`;
const BlockButton = styled.div`
    width: 150px;
    
    position: relative;
    color: #D7D9DE;
    & > button {
    display: flex;
    align-items: center;
    justify-content: center;
    }
  &:hover ${BlockIcon} {
    & > svg > path {
      fill: #5F5F5F;
    }
  }
`;

