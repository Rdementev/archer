//
import {createSelector} from "reselect";

const getARDMFeatureRegIPTypeSend = (state) => state.ARDM.registrationIP.type_send
export const getARDMFeatureRegIPTypeSendReselect = createSelector(getARDMFeatureRegIPTypeSend, (type_send) => type_send)

const getARDMFeatureRegIPTypeResponse = (state) => state.ARDM.registrationIP.type_response
export const getARDMFeatureRegIPTypeResponseReselect = createSelector(getARDMFeatureRegIPTypeResponse, (type_response) => type_response)

const getARDMFeatureRegIPTypeProxy = (state) => state.ARDM.registrationIP.type_proxy_id
export const getARDMFeatureRegIPTypeProxyReselect = createSelector(getARDMFeatureRegIPTypeProxy, (type_proxy_id) => type_proxy_id)

const getARDMFeatureRegIPTypeSysNal = (state) => state.ARDM.registrationIP.type_sys_nal
export const getARDMFeatureRegIPTypeSysNalReselect = createSelector(getARDMFeatureRegIPTypeSysNal, (type_sys_nal) => type_sys_nal)

const getARDMFeatureRegIPTypeRateNal = (state) => state.ARDM.registrationIP.type_rate_nal
export const getARDMFeatureRegIPTypeRateNalReselect = createSelector(getARDMFeatureRegIPTypeRateNal, (type_rate_nal) => type_rate_nal)

const getARDMFeatureRegIPTypeRegOrgan = (state) => state.ARDM.registrationIP.type_reg_organ
export const getARDMFeatureRegIPTypeRegOrganReselect = createSelector(getARDMFeatureRegIPTypeRegOrgan, (type_reg_organ) => type_reg_organ)




const getARDMFeatureRegIPTypePersonalId = (state) => state.ARDM.registrationIP.type_personal_id
export const getARDMFeatureRegIPTypePersonalIdReselect = createSelector(getARDMFeatureRegIPTypePersonalId, (type_personal_id) => type_personal_id)

const getARDMFeatureRegIPTypeSnils = (state) => state.ARDM.registrationIP.type_snils
export const getARDMFeatureRegIPTypeSnilsReselect = createSelector(getARDMFeatureRegIPTypeSnils, (type_snils) => type_snils)

const getARDMFeatureRegIPTypeSelectedOkved = (state) => state.ARDM.registrationIP.type_selected_okved
export const getARDMFeatureRegIPTypeSelectedOkvedReselect = createSelector(getARDMFeatureRegIPTypeSelectedOkved, (type_selected_okved) => type_selected_okved)


