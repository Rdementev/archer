import styled from "styled-components/macro";
import {compose} from "redux";
import React, {useEffect, useState} from "react";
import {getARDMCategoriesOkvedsReselect, getARDMOkvedsReselect} from "../../reducer";
import {connect} from "react-redux";
import {CloseHierarchei, OpenHierarchie, Ring,} from '../../icons/index'
import Sections from "../sections";

const OkvedsTable = (props) => {
    const {okveds , okvedsCategories,type_selected_okved, setSelected } = props
    const [showCat, setShowCat] = useState([])
    const [group, setGroup] = useState([])
    const [subGroup, setSubGroup] = useState([])
    const [elem, setElem] = useState([])
    const [subElem, setSubElem] = useState([])




    const getGroup = () => {
        let newGroup = []
        let newSubGroup = []
        let newElem = []
        let newSubElem = []
        okveds && okveds.map(group => {
            const { childerns, ...other} = group
            newGroup = [...newGroup, other]
            childerns.map(subGroup => {
                const { childerns, ...other} = subGroup
                newSubGroup = [...newSubGroup, other]
                childerns.map(elem => {
                    const { childerns, ...other} = elem
                    if(childerns.length < 1) {
                        other.type = 'elem'
                        newElem = [...newElem, other]
                    } else {
                        newElem = [...newElem, other]
                        childerns.map(subElem => {
                            newSubElem = [...newSubElem, subElem]
                        })
                    }
                })
            })
        })
        setGroup(newGroup)
        setSubGroup(newSubGroup)
        setElem(newElem)
        setSubElem(newSubElem)
    }

    useEffect(()=>{
        getGroup(okveds)
    },[okveds])

    const handleOpenCategory = (id) => {
        if(!showCat.includes(id)){
            setShowCat([...showCat, id])
        } else {
            const newShowCat = showCat.filter(item => item !== id)
            setShowCat(newShowCat)
        }
    }

    const getActualGroup = (id) => {
        let actualGroup =  group.filter(item =>  item && item.code.substr(0,2) === id)
        let actualSubGroup = subGroup.filter(item => item && item.code.substr(0,2) === id)
        let actualElem = elem.filter(item => item && item.code.substr(0,2) === id)
        let actualSubElem = subElem.filter(item => item && item.code.substr(0,2) === id)
        const actual = {
            actualGroup:actualGroup,
            actualSubGroup:actualSubGroup,
            actualElem:actualElem,
            actualSubElem:actualSubElem
        }
        return actual
    }

    const getOkvedsCategories = () => {
        return okvedsCategories && okvedsCategories.map(item => {
            const actual = getActualGroup(item.code)
            const active = showCat.includes(item.code)
            return (
                <Container active={active}>
                    <BlockCategory active={active} onClick={()=>{handleOpenCategory(item.code)}}>
                        <BlockCategoryBlockIcon>
                            {active ? <CloseHierarchei/> :  <OpenHierarchie/>  }
                        </BlockCategoryBlockIcon>
                        <ItemCategoryCode>
                            {item.code}
                        </ItemCategoryCode>
                        <ItemCategoryTitle>
                            {item.title}
                        </ItemCategoryTitle>
                    </BlockCategory>
                    {active && <Sections  group={actual.actualGroup}
                                          setSelected={setSelected}
                                          type_selected_okved={type_selected_okved}
                                          subGroup={actual.actualSubGroup}
                                          elem={actual.actualElem}
                                          subElem={actual.actualSubElem}/>}
                </Container>
            )
        })
    }

    return (
        <Table>
            {getOkvedsCategories()}
        </Table>
    )
}

const mapStateToProps = (state) => {
    return {
        okveds: getARDMOkvedsReselect(state),
        okvedsCategories: getARDMCategoriesOkvedsReselect(state),
    }
}
export default compose(
    connect(mapStateToProps, {
    })
)(OkvedsTable)
//
const Table = styled.div`
    width: 100%;
    overflow: scroll;
`;
const Container = styled.div`
    padding: 8px 10px;
    border-radius: 4px;
    margin: 10px 10px 10px 0;
    background: #fff;
    
    transition: ease 0.3s;
    
    border: ${({active}) => active ? '1px solid transparent' :  '1px solid #E1E1F1'};
    transform: ${({active}) => active ? 'translate(0px, -3px)' : ''} ;
    
    
    &:hover {
      box-shadow: 0 4px 6px rgba(0,0,0,0.05);
      transform: translate(0px, -3px);
    }
`;
const BlockCategory = styled.div`
    cursor: pointer;
    display: flex;
    color: ${({active}) => active ? '#284493' : ''} ;
    margin-bottom: ${({active}) => active ? '10px' : '0'} ;
    &:hover {
      color: #284493;
    }
`;
const ItemCategoryCode = styled.div`
    margin-right: 5px;
`;
const BlockCategoryBlockIcon = styled.div`
    display: flex;
    margin-right: 10px;
    z-index: 1;
    & > svg {
      width: 21px;
      height: 21px;
    }
`;
const ItemCategoryTitle = styled.div`
   
`;

