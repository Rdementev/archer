
import createRequestRest from 'ApiARJTTD/createRequestRest'
import {put} from "redux-saga/effects";
import {ARDM_PAGES_UPDATE_ORGANIZATION_REQUEST, ardmPagesGetOrganizationRequest} from "../actions";

function* prepareRequest ( payload) {
    return payload
}

function* prepareSuccess (response, payload) {
    yield put(ardmPagesGetOrganizationRequest())
}

function* prepareFailure () {

}

export const updateOrganization = () => {
    return createRequestRest({
        url: '/autodoc/update_organization',
        action: ARDM_PAGES_UPDATE_ORGANIZATION_REQUEST,
        prepareRequest,
        prepareSuccess,
        prepareFailure,
    })
}